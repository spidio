<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the AuthPrivilege class.  It uses the code-generated
	 * AuthPrivilegeMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a AuthPrivilege columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both auth_privilege_edit.php AND
	 * auth_privilege_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class AuthPrivilegeEditPanel extends QPanel {
		// Local instance of the AuthPrivilegeMetaControl
		protected $mctAuthPrivilege;

		// Controls for AuthPrivilege's Data Fields
		public $txtId;
		public $lstAuthRoleEnum;
		public $txtWho;
		public $lstAuthAction;
		public $lstAuthPrivilegeTypeEnum;
		public $lstAuthClassEnum;
		public $txtRelatedId;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'AuthPrivilegeEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the AuthPrivilegeMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctAuthPrivilege = AuthPrivilegeMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on AuthPrivilege's data fields
			$this->txtId = $this->mctAuthPrivilege->txtId_Create();
			$this->lstAuthRoleEnum = $this->mctAuthPrivilege->lstAuthRoleEnum_Create();
			$this->txtWho = $this->mctAuthPrivilege->txtWho_Create();
			$this->lstAuthAction = $this->mctAuthPrivilege->lstAuthAction_Create();
			$this->lstAuthPrivilegeTypeEnum = $this->mctAuthPrivilege->lstAuthPrivilegeTypeEnum_Create();
			$this->lstAuthClassEnum = $this->mctAuthPrivilege->lstAuthClassEnum_Create();
			$this->txtRelatedId = $this->mctAuthPrivilege->txtRelatedId_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('AuthPrivilege') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctAuthPrivilege->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the AuthPrivilegeMetaControl
			$this->mctAuthPrivilege->SaveAuthPrivilege();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the AuthPrivilegeMetaControl
			$this->mctAuthPrivilege->DeleteAuthPrivilege();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
<?php

if (!defined('__TEMPLATES__')) {
	trigger_error('Constant __TEMPLATES__ is not defined, please define it in configuration.inc.php.', E_USER_ERROR);
}

require sprintf('%s/%s/template_logic.php', __TEMPLATES__, QApplication::GetConfig('site_template'));

?>
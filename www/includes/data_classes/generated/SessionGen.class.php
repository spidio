<?php
	/**
	 * The abstract SessionGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Session subclass which
	 * extends this SessionGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Session class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $UserId the value for strUserId 
	 * @property string $IpAddress the value for strIpAddress (Not Null)
	 * @property boolean $AutoLogin the value for blnAutoLogin (Not Null)
	 * @property QDateTime $Added the value for dttAdded (Not Null)
	 * @property QDateTime $Changed the value for dttChanged (Not Null)
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property User $User the value for the User object referenced by strUserId 
	 * @property SessionKey $SessionKeyAsSession the value for the SessionKey object that uniquely references this Session
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class SessionGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_session.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_session.user_id
		 * @var string strUserId
		 */
		protected $strUserId;
		const UserIdMaxLength = 36;
		const UserIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_session.ip_address
		 * @var string strIpAddress
		 */
		protected $strIpAddress;
		const IpAddressMaxLength = 49;
		const IpAddressDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_session.auto_login
		 * @var boolean blnAutoLogin
		 */
		protected $blnAutoLogin;
		const AutoLoginDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_session.added
		 * @var QDateTime dttAdded
		 */
		protected $dttAdded;
		const AddedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_session.changed
		 * @var QDateTime dttChanged
		 */
		protected $dttChanged;
		const ChangedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_session.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_session.user_id.
		 *
		 * NOTE: Always use the User property getter to correctly retrieve this User object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var User objUser
		 */
		protected $objUser;

		/**
		 * Protected member variable that contains the object which points to
		 * this object by the reference in the unique database column spidio_session_key.session_id.
		 *
		 * NOTE: Always use the SessionKeyAsSession property getter to correctly retrieve this SessionKey object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var SessionKey objSessionKeyAsSession
		 */
		protected $objSessionKeyAsSession;
		
		/**
		 * Used internally to manage whether the adjoined SessionKeyAsSession object
		 * needs to be updated on save.
		 * 
		 * NOTE: Do not manually update this value 
		 */
		protected $blnDirtySessionKeyAsSession;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Session from PK Info
		 * @param string $strId
		 * @return Session
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Session::QuerySingle(
				QQ::Equal(QQN::Session()->Id, $strId)
			);
		}

		/**
		 * Load all Sessions
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Session[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Session::QueryArray to perform the LoadAll query
			try {
				return Session::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Sessions
		 * @return int
		 */
		public static function CountAll() {
			// Call Session::QueryCount to perform the CountAll query
			return Session::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Session::GetDatabase();

			// Create/Build out the QueryBuilder object with Session-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'session');
			Session::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'session');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Session object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Session the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Session::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Session object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Session::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Session objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Session[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Session::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Session::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Session objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Session::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Session::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'session_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Session-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Session::GetSelectFields($objQueryBuilder);
				Session::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Session::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Session
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'session';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'user_id', $strAliasPrefix . 'user_id');
			$objBuilder->AddSelectItem($strTableName, 'ip_address', $strAliasPrefix . 'ip_address');
			$objBuilder->AddSelectItem($strTableName, 'auto_login', $strAliasPrefix . 'auto_login');
			$objBuilder->AddSelectItem($strTableName, 'added', $strAliasPrefix . 'added');
			$objBuilder->AddSelectItem($strTableName, 'changed', $strAliasPrefix . 'changed');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Session from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Session::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Session
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;


			// Create a new instance of the Session object
			$objToReturn = new Session();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'user_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'user_id'] : $strAliasPrefix . 'user_id';
			$objToReturn->strUserId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'ip_address', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'ip_address'] : $strAliasPrefix . 'ip_address';
			$objToReturn->strIpAddress = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'auto_login', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auto_login'] : $strAliasPrefix . 'auto_login';
			$objToReturn->blnAutoLogin = $objDbRow->GetColumn($strAliasName, 'Bit');
			$strAliasName = array_key_exists($strAliasPrefix . 'added', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'added'] : $strAliasPrefix . 'added';
			$objToReturn->dttAdded = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'changed', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'changed'] : $strAliasPrefix . 'changed';
			$objToReturn->dttChanged = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'session__';

			// Check for User Early Binding
			$strAlias = $strAliasPrefix . 'user_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objUser = User::InstantiateDbRow($objDbRow, $strAliasPrefix . 'user_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);


			// Check for SessionKeyAsSession Unique ReverseReference Binding
			$strAlias = $strAliasPrefix . 'sessionkeyassession__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if ($objDbRow->ColumnExists($strAliasName)) {
				if (!is_null($objDbRow->GetColumn($strAliasName)))
					$objToReturn->objSessionKeyAsSession = SessionKey::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionkeyassession__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					// We ATTEMPTED to do an Early Bind but the Object Doesn't Exist
					// Let's set to FALSE so that the object knows not to try and re-query again
					$objToReturn->objSessionKeyAsSession = false;
			}



			return $objToReturn;
		}

		/**
		 * Instantiate an array of Sessions from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Session[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Session::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Session::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Session object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Session
		*/
		public static function LoadById($strId) {
			return Session::QuerySingle(
				QQ::Equal(QQN::Session()->Id, $strId)
			);
		}
			
		/**
		 * Load an array of Session objects,
		 * by UserId Index(es)
		 * @param string $strUserId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Session[]
		*/
		public static function LoadArrayByUserId($strUserId, $objOptionalClauses = null) {
			// Call Session::QueryArray to perform the LoadArrayByUserId query
			try {
				return Session::QueryArray(
					QQ::Equal(QQN::Session()->UserId, $strUserId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Sessions
		 * by UserId Index(es)
		 * @param string $strUserId
		 * @return int
		*/
		public static function CountByUserId($strUserId) {
			// Call Session::QueryCount to perform the CountByUserId query
			return Session::QueryCount(
				QQ::Equal(QQN::Session()->UserId, $strUserId)
			);
		}
			
		/**
		 * Load an array of Session objects,
		 * by IpAddress Index(es)
		 * @param string $strIpAddress
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Session[]
		*/
		public static function LoadArrayByIpAddress($strIpAddress, $objOptionalClauses = null) {
			// Call Session::QueryArray to perform the LoadArrayByIpAddress query
			try {
				return Session::QueryArray(
					QQ::Equal(QQN::Session()->IpAddress, $strIpAddress),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Sessions
		 * by IpAddress Index(es)
		 * @param string $strIpAddress
		 * @return int
		*/
		public static function CountByIpAddress($strIpAddress) {
			// Call Session::QueryCount to perform the CountByIpAddress query
			return Session::QueryCount(
				QQ::Equal(QQN::Session()->IpAddress, $strIpAddress)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Session
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Session::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'session` (
							`id`,
							`user_id`,
							`ip_address`,
							`auto_login`,
							`added`,
							`changed`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strUserId) . ',
							' . $objDatabase->SqlVariable($this->strIpAddress) . ',
							' . $objDatabase->SqlVariable($this->blnAutoLogin) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'session`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Session');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'session`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`user_id` = ' . $objDatabase->SqlVariable($this->strUserId) . ',
							`ip_address` = ' . $objDatabase->SqlVariable($this->strIpAddress) . ',
							`auto_login` = ' . $objDatabase->SqlVariable($this->blnAutoLogin) . ',
							`added` = ' . $objDatabase->SqlVariable($this->dttAdded) . ',
							`changed` = ' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

		
		
				// Update the adjoined SessionKeyAsSession object (if applicable)
				// TODO: Make this into hard-coded SQL queries
				if ($this->blnDirtySessionKeyAsSession) {
					// Unassociate the old one (if applicable)
					if ($objAssociated = SessionKey::LoadBySessionId($this->strId)) {
						$objAssociated->SessionId = null;
						$objAssociated->Save();
					}

					// Associate the new one (if applicable)
					if ($this->objSessionKeyAsSession) {
						$this->objSessionKeyAsSession->SessionId = $this->strId;
						$this->objSessionKeyAsSession->Save();
					}

					// Reset the "Dirty" flag
					$this->blnDirtySessionKeyAsSession = false;
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'session`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Session
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Session with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Session::GetDatabase();

			
			
			// Update the adjoined SessionKeyAsSession object (if applicable) and perform a delete

			// Optional -- if you **KNOW** that you do not want to EVER run any level of business logic on the disassocation,
			// you *could* override Delete() so that this step can be a single hard coded query to optimize performance.
			if ($objAssociated = SessionKey::LoadBySessionId($this->strId)) {
				$objAssociated->Delete();
			}

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'session`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Sessions
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Session::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'session`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'session table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Session::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'session`');
		}

		/**
		 * Reload this Session from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Session object.');

			// Reload the Object
			$objReloaded = Session::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->UserId = $objReloaded->UserId;
			$this->strIpAddress = $objReloaded->strIpAddress;
			$this->blnAutoLogin = $objReloaded->blnAutoLogin;
			$this->dttAdded = $objReloaded->dttAdded;
			$this->dttChanged = $objReloaded->dttChanged;
			$this->strOptlock = $objReloaded->strOptlock;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'UserId':
					/**
					 * Gets the value for strUserId 
					 * @return string
					 */
					return $this->strUserId;

				case 'IpAddress':
					/**
					 * Gets the value for strIpAddress (Not Null)
					 * @return string
					 */
					return $this->strIpAddress;

				case 'AutoLogin':
					/**
					 * Gets the value for blnAutoLogin (Not Null)
					 * @return boolean
					 */
					return $this->blnAutoLogin;

				case 'Added':
					/**
					 * Gets the value for dttAdded (Not Null)
					 * @return QDateTime
					 */
					return $this->dttAdded;

				case 'Changed':
					/**
					 * Gets the value for dttChanged (Not Null)
					 * @return QDateTime
					 */
					return $this->dttChanged;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;


				///////////////////
				// Member Objects
				///////////////////
				case 'User':
					/**
					 * Gets the value for the User object referenced by strUserId 
					 * @return User
					 */
					try {
						if ((!$this->objUser) && (!is_null($this->strUserId)))
							$this->objUser = User::Load($this->strUserId);
						return $this->objUser;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

		
		
				case 'SessionKeyAsSession':
					/**
					 * Gets the value for the SessionKey object that uniquely references this Session
					 * by objSessionKeyAsSession (Unique)
					 * @return SessionKey
					 */
					try {
						if ($this->objSessionKeyAsSession === false)
							// We've attempted early binding -- and the reverse reference object does not exist
							return null;
						if (!$this->objSessionKeyAsSession)
							$this->objSessionKeyAsSession = SessionKey::LoadBySessionId($this->strId);
						return $this->objSessionKeyAsSession;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'UserId':
					/**
					 * Sets the value for strUserId 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objUser = null;
						return ($this->strUserId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'IpAddress':
					/**
					 * Sets the value for strIpAddress (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strIpAddress = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AutoLogin':
					/**
					 * Sets the value for blnAutoLogin (Not Null)
					 * @param boolean $mixValue
					 * @return boolean
					 */
					try {
						return ($this->blnAutoLogin = QType::Cast($mixValue, QType::Boolean));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Added':
					/**
					 * Sets the value for dttAdded (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttAdded = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Changed':
					/**
					 * Sets the value for dttChanged (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttChanged = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'User':
					/**
					 * Sets the value for the User object referenced by strUserId 
					 * @param User $mixValue
					 * @return User
					 */
					if (is_null($mixValue)) {
						$this->strUserId = null;
						$this->objUser = null;
						return null;
					} else {
						// Make sure $mixValue actually is a User object
						try {
							$mixValue = QType::Cast($mixValue, 'User');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED User object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved User for this Session');

						// Update Local Member Variables
						$this->objUser = $mixValue;
						$this->strUserId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'SessionKeyAsSession':
					/**
					 * Sets the value for the SessionKey object referenced by objSessionKeyAsSession (Unique)
					 * @param SessionKey $mixValue
					 * @return SessionKey
					 */
					if (is_null($mixValue)) {
						$this->objSessionKeyAsSession = null;

						// Make sure we update the adjoined SessionKey object the next time we call Save()
						$this->blnDirtySessionKeyAsSession = true;

						return null;
					} else {
						// Make sure $mixValue actually is a SessionKey object
						try {
							$mixValue = QType::Cast($mixValue, 'SessionKey');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						}

						// Are we setting objSessionKeyAsSession to a DIFFERENT $mixValue?
						if ((!$this->SessionKeyAsSession) || ($this->SessionKeyAsSession->Id != $mixValue->Id)) {
							// Yes -- therefore, set the "Dirty" flag to true
							// to make sure we update the adjoined SessionKey object the next time we call Save()
							$this->blnDirtySessionKeyAsSession = true;

							// Update Local Member Variable
							$this->objSessionKeyAsSession = $mixValue;
						} else {
							// Nope -- therefore, make no changes
						}

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Session"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="User" type="xsd1:User"/>';
			$strToReturn .= '<element name="IpAddress" type="xsd:string"/>';
			$strToReturn .= '<element name="AutoLogin" type="xsd:boolean"/>';
			$strToReturn .= '<element name="Added" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Changed" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Session', $strComplexTypeArray)) {
				$strComplexTypeArray['Session'] = Session::GetSoapComplexTypeXml();
				User::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Session::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Session();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'User')) &&
				($objSoapObject->User))
				$objToReturn->User = User::GetObjectFromSoapObject($objSoapObject->User);
			if (property_exists($objSoapObject, 'IpAddress'))
				$objToReturn->strIpAddress = $objSoapObject->IpAddress;
			if (property_exists($objSoapObject, 'AutoLogin'))
				$objToReturn->blnAutoLogin = $objSoapObject->AutoLogin;
			if (property_exists($objSoapObject, 'Added'))
				$objToReturn->dttAdded = new QDateTime($objSoapObject->Added);
			if (property_exists($objSoapObject, 'Changed'))
				$objToReturn->dttChanged = new QDateTime($objSoapObject->Changed);
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Session::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objUser)
				$objObject->objUser = User::GetSoapObjectFromObject($objObject->objUser, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strUserId = null;
			if ($objObject->dttAdded)
				$objObject->dttAdded = $objObject->dttAdded->__toString(QDateTime::FormatSoap);
			if ($objObject->dttChanged)
				$objObject->dttChanged = $objObject->dttChanged->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeSession extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Session';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'session';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'UserId':
					return new QQNode('user_id', 'UserId', 'string', $this);
				case 'User':
					return new QQNodeUser('user_id', 'User', 'string', $this);
				case 'IpAddress':
					return new QQNode('ip_address', 'IpAddress', 'string', $this);
				case 'AutoLogin':
					return new QQNode('auto_login', 'AutoLogin', 'boolean', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'SessionKeyAsSession':
					return new QQReverseReferenceNodeSessionKey($this, 'sessionkeyassession', 'reverse_reference', 'session_id', 'SessionKeyAsSession');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeSession extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Session';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'session';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'UserId':
					return new QQNode('user_id', 'UserId', 'string', $this);
				case 'User':
					return new QQNodeUser('user_id', 'User', 'string', $this);
				case 'IpAddress':
					return new QQNode('ip_address', 'IpAddress', 'string', $this);
				case 'AutoLogin':
					return new QQNode('auto_login', 'AutoLogin', 'boolean', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'SessionKeyAsSession':
					return new QQReverseReferenceNodeSessionKey($this, 'sessionkeyassession', 'reverse_reference', 'session_id', 'SessionKeyAsSession');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
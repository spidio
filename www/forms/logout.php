<?php

class LogoutForm extends SpidioTemplateForm {
	protected function Form_Create() {
		parent::Form_Create();
		$this->PageTitle = QApplication::Translate('Logout');
		
		$this->Auth->Logout();
	}
	
	protected function Form_Exit() {
		QApplication::RedirectToReturnPage();
	}
}

LogoutForm::Run('LogoutForm', QApplication::GetTemplatePath(__FILE__));

?>
#!/bin/sh
# Copyright (c) 2008 Spidio Project
# Licensed under original BSD License.

if [ -f "svn-path" ]
then
	sh svn-commit.sh
	sh svn-gc.sh
fi

echo --------------------------
echo Running git-push upstream
echo WC: $PWD
echo --------------------------
git-push upstream
echo

sh git-gc.sh
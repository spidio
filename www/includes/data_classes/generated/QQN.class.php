<?php
	class QQN {
		/**
		 * @return QQNodeAddress
		 */
		static public function Address() {
			return new QQNodeAddress('' . DB_TABLE_PREFIX_1 . 'address', null, null);
		}
		/**
		 * @return QQNodeAuthAction
		 */
		static public function AuthAction() {
			return new QQNodeAuthAction('' . DB_TABLE_PREFIX_1 . 'auth_action', null, null);
		}
		/**
		 * @return QQNodeAuthGroup
		 */
		static public function AuthGroup() {
			return new QQNodeAuthGroup('' . DB_TABLE_PREFIX_1 . 'auth_group', null, null);
		}
		/**
		 * @return QQNodeAuthImplementedAction
		 */
		static public function AuthImplementedAction() {
			return new QQNodeAuthImplementedAction('' . DB_TABLE_PREFIX_1 . 'auth_implemented_action', null, null);
		}
		/**
		 * @return QQNodeAuthPrivilege
		 */
		static public function AuthPrivilege() {
			return new QQNodeAuthPrivilege('' . DB_TABLE_PREFIX_1 . 'auth_privilege', null, null);
		}
		/**
		 * @return QQNodeCamera
		 */
		static public function Camera() {
			return new QQNodeCamera('' . DB_TABLE_PREFIX_1 . 'camera', null, null);
		}
		/**
		 * @return QQNodeCameraPurpose
		 */
		static public function CameraPurpose() {
			return new QQNodeCameraPurpose('' . DB_TABLE_PREFIX_1 . 'camera_purpose', null, null);
		}
		/**
		 * @return QQNodeCodec
		 */
		static public function Codec() {
			return new QQNodeCodec('' . DB_TABLE_PREFIX_1 . 'codec', null, null);
		}
		/**
		 * @return QQNodeConfig
		 */
		static public function Config() {
			return new QQNodeConfig('' . DB_TABLE_PREFIX_1 . 'config', null, null);
		}
		/**
		 * @return QQNodeFile
		 */
		static public function File() {
			return new QQNodeFile('' . DB_TABLE_PREFIX_1 . 'file', null, null);
		}
		/**
		 * @return QQNodeFileType
		 */
		static public function FileType() {
			return new QQNodeFileType('' . DB_TABLE_PREFIX_1 . 'file_type', null, null);
		}
		/**
		 * @return QQNodeLibrary
		 */
		static public function Library() {
			return new QQNodeLibrary('' . DB_TABLE_PREFIX_1 . 'library', null, null);
		}
		/**
		 * @return QQNodeLocation
		 */
		static public function Location() {
			return new QQNodeLocation('' . DB_TABLE_PREFIX_1 . 'location', null, null);
		}
		/**
		 * @return QQNodeLog
		 */
		static public function Log() {
			return new QQNodeLog('' . DB_TABLE_PREFIX_1 . 'log', null, null);
		}
		/**
		 * @return QQNodeNews
		 */
		static public function News() {
			return new QQNodeNews('' . DB_TABLE_PREFIX_1 . 'news', null, null);
		}
		/**
		 * @return QQNodeOrganisation
		 */
		static public function Organisation() {
			return new QQNodeOrganisation('' . DB_TABLE_PREFIX_1 . 'organisation', null, null);
		}
		/**
		 * @return QQNodePerson
		 */
		static public function Person() {
			return new QQNodePerson('' . DB_TABLE_PREFIX_1 . 'person', null, null);
		}
		/**
		 * @return QQNodeProject
		 */
		static public function Project() {
			return new QQNodeProject('' . DB_TABLE_PREFIX_1 . 'project', null, null);
		}
		/**
		 * @return QQNodeProjectType
		 */
		static public function ProjectType() {
			return new QQNodeProjectType('' . DB_TABLE_PREFIX_1 . 'project_type', null, null);
		}
		/**
		 * @return QQNodeRecording
		 */
		static public function Recording() {
			return new QQNodeRecording('' . DB_TABLE_PREFIX_1 . 'recording', null, null);
		}
		/**
		 * @return QQNodeRecordingType
		 */
		static public function RecordingType() {
			return new QQNodeRecordingType('' . DB_TABLE_PREFIX_1 . 'recording_type', null, null);
		}
		/**
		 * @return QQNodeSession
		 */
		static public function Session() {
			return new QQNodeSession('' . DB_TABLE_PREFIX_1 . 'session', null, null);
		}
		/**
		 * @return QQNodeSessionKey
		 */
		static public function SessionKey() {
			return new QQNodeSessionKey('' . DB_TABLE_PREFIX_1 . 'session_key', null, null);
		}
		/**
		 * @return QQNodeTape
		 */
		static public function Tape() {
			return new QQNodeTape('' . DB_TABLE_PREFIX_1 . 'tape', null, null);
		}
		/**
		 * @return QQNodeTapeLocation
		 */
		static public function TapeLocation() {
			return new QQNodeTapeLocation('' . DB_TABLE_PREFIX_1 . 'tape_location', null, null);
		}
		/**
		 * @return QQNodeTemplate
		 */
		static public function Template() {
			return new QQNodeTemplate('' . DB_TABLE_PREFIX_1 . 'template', null, null);
		}
		/**
		 * @return QQNodeTemplateConfig
		 */
		static public function TemplateConfig() {
			return new QQNodeTemplateConfig('' . DB_TABLE_PREFIX_1 . 'template_config', null, null);
		}
		/**
		 * @return QQNodeUser
		 */
		static public function User() {
			return new QQNodeUser('' . DB_TABLE_PREFIX_1 . 'user', null, null);
		}
	}
?>
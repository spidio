<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the SessionKey class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single SessionKey object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a SessionKeyMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read SessionKey $SessionKey the actual SessionKey data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $UserIdControl
	 * property-read QLabel $UserIdLabel
	 * property QListBox $SessionIdControl
	 * property-read QLabel $SessionIdLabel
	 * property QTextBox $IpAddressControl
	 * property-read QLabel $IpAddressLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QDateTimePicker $ChangedControl
	 * property-read QLabel $ChangedLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class SessionKeyMetaControlGen extends QBaseClass {
		// General Variables
		protected $objSessionKey;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of SessionKey's individual data fields
		protected $txtId;
		protected $lstUser;
		protected $lstSession;
		protected $txtIpAddress;
		protected $calAdded;
		protected $calChanged;
		protected $lblOptlock;

		// Controls that allow the viewing of SessionKey's individual data fields
		protected $lblId;
		protected $lblUserId;
		protected $lblSessionId;
		protected $lblIpAddress;
		protected $lblAdded;
		protected $lblChanged;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * SessionKeyMetaControl to edit a single SessionKey object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single SessionKey object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionKeyMetaControl
		 * @param SessionKey $objSessionKey new or existing SessionKey object
		 */
		 public function __construct($objParentObject, SessionKey $objSessionKey) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this SessionKeyMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked SessionKey object
			$this->objSessionKey = $objSessionKey;

			// Figure out if we're Editing or Creating New
			if ($this->objSessionKey->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionKeyMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing SessionKey object creation - defaults to CreateOrEdit
 		 * @return SessionKeyMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objSessionKey = SessionKey::Load($strId);

				// SessionKey was found -- return it!
				if ($objSessionKey)
					return new SessionKeyMetaControl($objParentObject, $objSessionKey);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a SessionKey object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new SessionKeyMetaControl($objParentObject, new SessionKey());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionKeyMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing SessionKey object creation - defaults to CreateOrEdit
		 * @return SessionKeyMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return SessionKeyMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionKeyMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing SessionKey object creation - defaults to CreateOrEdit
		 * @return SessionKeyMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return SessionKeyMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objSessionKey->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = SessionKey::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objSessionKey->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstUser
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstUser_Create($strControlId = null) {
			$this->lstUser = new QListBox($this->objParentObject, $strControlId);
			$this->lstUser->Name = QApplication::Translate('User');
			$this->lstUser->Required = true;
			if (!$this->blnEditMode)
				$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
			$objUserArray = User::LoadAll();
			if ($objUserArray) foreach ($objUserArray as $objUser) {
				$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
				if (($this->objSessionKey->User) && ($this->objSessionKey->User->Id == $objUser->Id))
					$objListItem->Selected = true;
				$this->lstUser->AddItem($objListItem);
			}
			return $this->lstUser;
		}

		/**
		 * Create and setup QLabel lblUserId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblUserId_Create($strControlId = null) {
			$this->lblUserId = new QLabel($this->objParentObject, $strControlId);
			$this->lblUserId->Name = QApplication::Translate('User');
			$this->lblUserId->Text = ($this->objSessionKey->User) ? $this->objSessionKey->User->__toString() : null;
			$this->lblUserId->Required = true;
			return $this->lblUserId;
		}

		/**
		 * Create and setup QListBox lstSession
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstSession_Create($strControlId = null) {
			$this->lstSession = new QListBox($this->objParentObject, $strControlId);
			$this->lstSession->Name = QApplication::Translate('Session');
			$this->lstSession->Required = true;
			if (!$this->blnEditMode)
				$this->lstSession->AddItem(QApplication::Translate('- Select One -'), null);
			$objSessionArray = Session::LoadAll();
			if ($objSessionArray) foreach ($objSessionArray as $objSession) {
				$objListItem = new QListItem($objSession->__toString(), $objSession->Id);
				if (($this->objSessionKey->Session) && ($this->objSessionKey->Session->Id == $objSession->Id))
					$objListItem->Selected = true;
				$this->lstSession->AddItem($objListItem);
			}
			return $this->lstSession;
		}

		/**
		 * Create and setup QLabel lblSessionId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblSessionId_Create($strControlId = null) {
			$this->lblSessionId = new QLabel($this->objParentObject, $strControlId);
			$this->lblSessionId->Name = QApplication::Translate('Session');
			$this->lblSessionId->Text = ($this->objSessionKey->Session) ? $this->objSessionKey->Session->__toString() : null;
			$this->lblSessionId->Required = true;
			return $this->lblSessionId;
		}

		/**
		 * Create and setup QTextBox txtIpAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtIpAddress_Create($strControlId = null) {
			$this->txtIpAddress = new QTextBox($this->objParentObject, $strControlId);
			$this->txtIpAddress->Name = QApplication::Translate('Ip Address');
			$this->txtIpAddress->Text = $this->objSessionKey->IpAddress;
			$this->txtIpAddress->Required = true;
			$this->txtIpAddress->MaxLength = SessionKey::IpAddressMaxLength;
			return $this->txtIpAddress;
		}

		/**
		 * Create and setup QLabel lblIpAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblIpAddress_Create($strControlId = null) {
			$this->lblIpAddress = new QLabel($this->objParentObject, $strControlId);
			$this->lblIpAddress->Name = QApplication::Translate('Ip Address');
			$this->lblIpAddress->Text = $this->objSessionKey->IpAddress;
			$this->lblIpAddress->Required = true;
			return $this->lblIpAddress;
		}

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objSessionKey->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objSessionKey->Added) ? $this->objSessionKey->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calChanged
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calChanged_Create($strControlId = null) {
			$this->calChanged = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calChanged->Name = QApplication::Translate('Changed');
			$this->calChanged->DateTime = $this->objSessionKey->Changed;
			$this->calChanged->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calChanged->Required = true;
			return $this->calChanged;
		}

		/**
		 * Create and setup QLabel lblChanged
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblChanged_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblChanged = new QLabel($this->objParentObject, $strControlId);
			$this->lblChanged->Name = QApplication::Translate('Changed');
			$this->strChangedDateTimeFormat = $strDateTimeFormat;
			$this->lblChanged->Text = sprintf($this->objSessionKey->Changed) ? $this->objSessionKey->__toString($this->strChangedDateTimeFormat) : null;
			$this->lblChanged->Required = true;
			return $this->lblChanged;
		}

		protected $strChangedDateTimeFormat;

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objSessionKey->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}



		/**
		 * Refresh this MetaControl with Data from the local SessionKey object.
		 * @param boolean $blnReload reload SessionKey from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objSessionKey->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objSessionKey->Id;
			if ($this->lblId) $this->lblId->Text = $this->objSessionKey->Id;

			if ($this->lstUser) {
					$this->lstUser->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
				$objUserArray = User::LoadAll();
				if ($objUserArray) foreach ($objUserArray as $objUser) {
					$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
					if (($this->objSessionKey->User) && ($this->objSessionKey->User->Id == $objUser->Id))
						$objListItem->Selected = true;
					$this->lstUser->AddItem($objListItem);
				}
			}
			if ($this->lblUserId) $this->lblUserId->Text = ($this->objSessionKey->User) ? $this->objSessionKey->User->__toString() : null;

			if ($this->lstSession) {
					$this->lstSession->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstSession->AddItem(QApplication::Translate('- Select One -'), null);
				$objSessionArray = Session::LoadAll();
				if ($objSessionArray) foreach ($objSessionArray as $objSession) {
					$objListItem = new QListItem($objSession->__toString(), $objSession->Id);
					if (($this->objSessionKey->Session) && ($this->objSessionKey->Session->Id == $objSession->Id))
						$objListItem->Selected = true;
					$this->lstSession->AddItem($objListItem);
				}
			}
			if ($this->lblSessionId) $this->lblSessionId->Text = ($this->objSessionKey->Session) ? $this->objSessionKey->Session->__toString() : null;

			if ($this->txtIpAddress) $this->txtIpAddress->Text = $this->objSessionKey->IpAddress;
			if ($this->lblIpAddress) $this->lblIpAddress->Text = $this->objSessionKey->IpAddress;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objSessionKey->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objSessionKey->Added) ? $this->objSessionKey->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->calChanged) $this->calChanged->DateTime = $this->objSessionKey->Changed;
			if ($this->lblChanged) $this->lblChanged->Text = sprintf($this->objSessionKey->Changed) ? $this->objSessionKey->__toString($this->strChangedDateTimeFormat) : null;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objSessionKey->Optlock;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC SESSIONKEY OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's SessionKey instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveSessionKey() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objSessionKey->Id = $this->txtId->Text;
				if ($this->lstUser) $this->objSessionKey->UserId = $this->lstUser->SelectedValue;
				if ($this->lstSession) $this->objSessionKey->SessionId = $this->lstSession->SelectedValue;
				if ($this->txtIpAddress) $this->objSessionKey->IpAddress = $this->txtIpAddress->Text;
				if ($this->calAdded) $this->objSessionKey->Added = $this->calAdded->DateTime;
				if ($this->calChanged) $this->objSessionKey->Changed = $this->calChanged->DateTime;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the SessionKey object
				$this->objSessionKey->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's SessionKey instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteSessionKey() {
			$this->objSessionKey->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'SessionKey': return $this->objSessionKey;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to SessionKey fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'UserIdControl':
					if (!$this->lstUser) return $this->lstUser_Create();
					return $this->lstUser;
				case 'UserIdLabel':
					if (!$this->lblUserId) return $this->lblUserId_Create();
					return $this->lblUserId;
				case 'SessionIdControl':
					if (!$this->lstSession) return $this->lstSession_Create();
					return $this->lstSession;
				case 'SessionIdLabel':
					if (!$this->lblSessionId) return $this->lblSessionId_Create();
					return $this->lblSessionId;
				case 'IpAddressControl':
					if (!$this->txtIpAddress) return $this->txtIpAddress_Create();
					return $this->txtIpAddress;
				case 'IpAddressLabel':
					if (!$this->lblIpAddress) return $this->lblIpAddress_Create();
					return $this->lblIpAddress;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'ChangedControl':
					if (!$this->calChanged) return $this->calChanged_Create();
					return $this->calChanged;
				case 'ChangedLabel':
					if (!$this->lblChanged) return $this->lblChanged_Create();
					return $this->lblChanged;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to SessionKey fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'UserIdControl':
						return ($this->lstUser = QType::Cast($mixValue, 'QControl'));
					case 'SessionIdControl':
						return ($this->lstSession = QType::Cast($mixValue, 'QControl'));
					case 'IpAddressControl':
						return ($this->txtIpAddress = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'ChangedControl':
						return ($this->calChanged = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
<?php
	/**
	 * The CodecEnum class defined here contains
	 * code for the CodecEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_codec_enum" table
	 * in the database.
	 * 
	 * To use, you should use the CodecEnum subclass which
	 * extends this CodecEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the CodecEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class CodecEnumGen extends QBaseClass {
		const video = 1;
		const audio = 2;
		const other = 3;

		const MaxId = 3;

		public static $NameArray = array(
			1 => 'video',
			2 => 'audio',
			3 => 'other');

		public static $TokenArray = array(
			1 => 'video',
			2 => 'audio',
			3 => 'other');

		public static $ReverseNameArray = array(
			'video' => 1,
			'audio' => 2,
			'other' => 3);

		public static $ReverseTokenArray = array(
			'video' => 1,
			'audio' => 2,
			'other' => 3);

		public static function ToString($intCodecEnumId) {
			switch ($intCodecEnumId) {
				case 1: return 'video';
				case 2: return 'audio';
				case 3: return 'other';
				default:
					throw new QCallerException(sprintf('Invalid intCodecEnumId: %s', $intCodecEnumId));
			}
		}

		public static function ToToken($intCodecEnumId) {
			switch ($intCodecEnumId) {
				case 1: return 'video';
				case 2: return 'audio';
				case 3: return 'other';
				default:
					throw new QCallerException(sprintf('Invalid intCodecEnumId: %s', $intCodecEnumId));
			}
		}

	}
?>
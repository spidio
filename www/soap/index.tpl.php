<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php _p($this->SoftwareName); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php _p(QApplication::$EncodingType); ?>" />
	<style type="text/css">
	body {
		text-align: center;
	}
	
	.RequestPanel {
		text-align: center;
		display: inline-table;
	}
	
	.ResponsePanel {
		text-align: center;
		display: inline-table;
	}
	</style>
	<meta name="author" content="<?php _p($this->CopyrightAuthor); ?>" />
	<meta name="copyright" content="Copyright (c) <?php _p($this->CopyrightYears); ?> <?php _p($this->CopyrightAuthor); ?>" />
</head>

<body>
	<h1><?php _p($this->SoftwareName); ?></h1>
	<p><?php _p($this->TextIntro, false); ?></p>
<?php $this->RenderBegin(); ?>
	<p>
		<?php $this->clbWsdlUrl->Render(); ?>
		<?php $this->txtWsdlUrl->Render(); ?>
		<?php $this->btnWsdlUrl->Render(); ?>
		<?php $this->btnReset->Render(); ?>
	</p>
	<p>
		<?php $this->lstService->Render(); ?>
		<?php $this->lstFunctions->Render(); ?>
	</p>
	<div><?php $this->pnlRequest->Render(); ?></div>
	<div>&nbsp;</div>
	<div><?php $this->pnlResponse->Render(); ?></div>
	<h6>
		<?php _p($this->TextCopyright, false); ?><br />
		<?php _p($this->TextPoweredBy, false); ?>
	</h6>
	<p><?php $this->objDefaultWaitIcon->Render(); ?></p>
<?php $this->RenderEnd(); ?>
</body>
</html>
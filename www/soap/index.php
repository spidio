<?php

// We are running Soapidio Client.
if (!defined('RUNNING_SOAPIDIO_CLIENT')) {
	define('RUNNING_SOAPIDIO_CLIENT', true);
}

// Include prepend.inc to load Qcodo.
require('./../includes/prepend.inc.php');

// Update version.
QApplication::UpdateVersionInfo();

// PHP have no WSDL caching to do.
ini_set('soap.wsdl_cache_enabled', false);

class SoapidioClientForm extends QForm {
	protected $objSoapClient;
	protected $strSoapServiceNameArray;
	protected $strSoapFunctionNameArray;
	
	protected $blnCookieSet = false;
	protected $strCookieName = 'spidio_sid';
	protected $strCookieValue;
	protected $blnAttachedSession = false;
	
	protected $pnlRequest;
	protected $pnlResponse;
	
	protected $txtWsdlUrl;
	protected $clbWsdlUrl;
	protected $btnWsdlUrl;
	protected $strWsdlUrlBase;
	protected $btnReset;
	
	protected $lstService;
	protected $lstFunctions;
	
	protected $strCopyrightAuthor;
	protected $strCopyrightYears;
	protected $strSoftwareName;
	protected $strSoftwareVersion;
	
	protected $strTextIntro;
	protected $strTextCopyright;
	protected $strTextPoweredBy;
	
	public function Form_Create() {
		$this->strCopyrightAuthor = QApplication::GetConfig('copyright_author');
		$this->strCopyrightYears = QApplication::GetConfig('copyright_years');
		$this->strSoftwareName = QApplication::GetConfig('software_webservice_name');
		$this->strSoftwareVersion = sprintf('%s.%s', QApplication::GetConfig('software_webservice_version'), QApplication::GetConfig('software_webservice_revision'));
		
		$this->strTextIntro = sprintf(QApplication::Translate('Welcome to %s version %s.<br />You can use this client to access the Soapidio web services.'), $this->strSoftwareName, $this->strSoftwareVersion);
		$this->strTextCopyright = sprintf(QApplication::Translate('%s is copyrighted &copy; %s by %s.'), QApplication::GetConfig('site_title'), $this->strCopyrightYears, $this->strCopyrightAuthor);
		$this->strTextPoweredBy = sprintf(QApplication::Translate('Powered by <a href="%s">%s</a>, version %s.%s, copyrighted &copy; %s by %s.'), QApplication::GetConfig('software_webservice_website'), QApplication::GetConfig('software_webservice_name'), QApplication::GetConfig('software_webservice_version'), QApplication::GetConfig('software_webservice_revision'), QApplication::GetConfig('software_webservice_years'), QApplication::GetConfig('software_webservice_author'));
		
		$this->txtWsdlUrl = new QTextBox($this);
		$this->SetWsdlUrl();	
		$this->txtWsdlUrl->AddAction(new QEnterKeyEvent(), new QAjaxAction('btnWsdlUrl_Click'));
		$this->txtWsdlUrl->AddAction(new QEnterKeyEvent(), new QTerminateAction());
		
		$this->clbWsdlUrl = new QControlLabel($this->txtWsdlUrl);
		$this->clbWsdlUrl->ForControl = $this->txtWsdlUrl;
		$this->clbWsdlUrl->Text = QApplication::Translate('Webservice base URL');
		
		$this->btnWsdlUrl = new QButton($this);
		$this->btnWsdlUrl->Text = QApplication::Translate('Load base URL');
		$this->btnWsdlUrl->AddAction(new QClickEvent(), new QAjaxAction('btnWsdlUrl_Click'));
		
		$this->btnReset = new QButton($this);
		$this->btnReset->Text = QApplication::Translate('Reset');
		$this->btnReset->AddAction(new QClickEvent(), new QAjaxAction('btnReset_Click'));
		
		$this->pnlRequest = new SoapidioRequestPanel($this);
		$this->pnlResponse = new SoapidioResponsePanel($this);
		
		$this->lstService = new QListBox($this);
		$this->lstService_Fill();
		$this->lstService->AddAction(new QChangeEvent(), new QAjaxAction('lstService_Change'));
		
		$this->lstFunctions = new QListBox($this);
		$this->lstFunctions_Fill();
		$this->lstFunctions->AddAction(new QChangeEvent(), new QAjaxAction('lstFunctions_Change'));
		
		$this->lstFunctions_Change();
		
		$this->objDefaultWaitIcon = new QWaitIcon($this);
	}
	
	public function Form_PreRender() {
		$strCookieName = strtolower(sprintf('%s_sid', QApplication::GetConfig('software_webservice_name')));
		if (CookieStore::ExistCookie($strCookieName)) {
			$strCookieValue = CookieStore::UpdateCookie($strCookieName)->Value;
			
			$this->SetSoapClient('auth');
			if ($this->objSoapClient->ValidSession($strCookieValue)) {
				$this->SetSoapCookie($strCookieValue);
				
				if (!$this->IsPostBack()) {
					$this->Login($strCookieValue);
				}
			} else if ($this->lstService->SelectedValue == 'auth') {
				$this->Logout();
				$this->pnlResponse->ResetControls();
			}
		} else {
			$this->Logout(false);
			
			if ($this->lstService->SelectedValue != 'auth') {
				$this->pnlResponse->ResetControls();
			}
		}
	}
	
	public function btnWsdlUrl_Click() {
		if (!is_null($this->txtWsdlUrl->Text)) {
			$this->lstService_Fill();
			$this->lstService->SelectedIndex = ($this->blnCookieSet) ? 0 : 1;
			$this->lstService_Change();
		}
	}
	
	public function btnReset_Click() {
		$this->SetWsdlUrl();
		$this->btnWsdlUrl_Click();
	}
	
	public function lstService_Change() {
		if ($this->lstService->SelectedValue == '#refresh#') {
			$this->lstService_Fill();
		} else {
			$this->pnlRequest->RemoveChildControls(true);
			
			if (!CookieStore::ExistCookie(strtolower(sprintf('%s_sid', QApplication::GetConfig('software_webservice_name'))))) {
				$this->Logout();
			}
			
			if (!is_null($this->lstService->SelectedValue) && !is_null($this->txtWsdlUrl->Text)) {
				$this->lstFunctions_Fill();
				$this->lstFunctions_Change();
			} else {
				$this->lstFunctions->Enabled = false;
				$this->lstFunctions->RemoveAllItems();
				$this->lstFunctions->AddItem(QApplication::Translate('- Functions -'), null, true);
				
				$this->pnlRequest->SetFunctionDeclaration($this->lstFunctions->SelectedValue);
				$this->pnlResponse->RemoveChildControls(true);
			}
		}
	}
	
	public function lstService_Fill() {
		if ($this->blnCookieSet) {
			$this->lstService->RemoveAllItems();
			$this->lstService->AddItem(QApplication::Translate('- Services -'), null, true);
			if (QApplication::GetConfig('development_soapclient', QType::Boolean)) {
				$this->lstService->AddItem(QApplication::Translate('(Refresh)'), '#refresh#');
			}
			
			$this->FillSoapServiceArray();
			foreach ($this->strSoapServiceNameArray as $strSoapServiceName) {
				if ($strSoapServiceName == 'Development' && !QApplication::GetConfig('development_soapclient', QType::Boolean)) {
					continue;
				}
				
				$this->lstService->AddItem($strSoapServiceName, strtolower($strSoapServiceName));
			}
			
			$this->lstFunctions->RemoveAllItems();
			$this->lstFunctions->Enabled = false;
			$this->lstFunctions->AddItem(QApplication::Translate('- Functions -'), null, true);
			$this->pnlRequest->SetFunctionDeclaration(null);
		} else {
			$this->lstService->RemoveAllItems();
			$this->lstService->AddItem(QApplication::Translate('- Services -'), null);
			$this->lstService->AddItem('Auth', 'auth', true);
			$this->SetSoapClient('auth');
		}
	}
	
	public function lstFunctions_Change() {
		if ($this->lstFunctions->SelectedValue == '#refresh#') {
			$this->lstFunctions_Fill();
			$this->lstFunctions_Change();
		} else {
			if (!CookieStore::ExistCookie(strtolower(sprintf('%s_sid', QApplication::GetConfig('software_webservice_name')))) && $this->lstService->SelectedValue != 'auth') {
				$this->Logout();
			}
			
			$this->pnlRequest->SetFunctionDeclaration($this->lstFunctions->SelectedValue);
		}
	}
	
	public function lstFunctions_Fill() {
		$this->lstFunctions->RemoveAllItems();
		$this->lstFunctions->Enabled = true;
		
		if ($this->blnCookieSet) {
			if (is_null($this->txtWsdlUrl->Text)) {
				throw new QUndefinedPropertyException('GET', get_class($this), 'WsdlUrl');
			}
			
			$this->lstFunctions->AddItem(QApplication::Translate('- Functions -'), null, true);
			if (QApplication::GetConfig('development_soapclient', QType::Boolean)) {
				$this->lstFunctions->AddItem(QApplication::Translate('(Refresh)'), '#refresh#');
			}
			
			$this->SetSoapClient($this->lstService->SelectedValue);
			foreach ($this->strSoapFunctionNameArray as $strFunctionDeclaration) {
				$intStart = strpos($strFunctionDeclaration, ' ') + 1;
				$intLength = strpos($strFunctionDeclaration, '(') - $intStart;
				
				$strFunctionName = substr($strFunctionDeclaration, $intStart, $intLength);
				
				if ($this->lstService->SelectedValue == 'auth' && (
					($strFunctionName == 'CheckSession' && !QApplication::GetConfig('development_soapclient', QType::Boolean)) ||
					($strFunctionName == 'ValidSession' && !QApplication::GetConfig('development_soapclient', QType::Boolean)) ||
					($strFunctionName == 'GetAuthenticationType' && !QApplication::GetConfig('development_soapclient', QType::Boolean)) ||
					($strFunctionName == 'Login') ||
					($strFunctionName == 'Logout' && $this->blnAttachedSession) ||
					($strFunctionName == 'AttachSession' && $this->blnAttachedSession && !QApplication::GetConfig('development_soapclient', QType::Boolean)) ||
					($strFunctionName == 'DetachSession' && !$this->blnAttachedSession)
				)) {
					continue;
				}
				
				$this->lstFunctions->AddItem($strFunctionName, $strFunctionDeclaration);
			}
		} else {
			$this->lstFunctions->AddItem(QApplication::Translate('- Functions -'), null);
			if (QApplication::GetConfig('development_soapclient', QType::Boolean)) {
				$this->lstFunctions->AddItem(QApplication::Translate('(Refresh)'), '#refresh#');
			}
			
			foreach ($this->strSoapFunctionNameArray as $strFunctionDeclaration) {
				$intStart = strpos($strFunctionDeclaration, ' ') + 1;
				$intLength = strpos($strFunctionDeclaration, '(') - $intStart;
				
				$strFunctionName = substr($strFunctionDeclaration, $intStart, $intLength);
				
				if ($strFunctionName != 'CheckSession' && $strFunctionName != 'ValidSession' && $strFunctionName != 'Logout' && $strFunctionName != 'DetachSession') {
					$this->lstFunctions->AddItem($strFunctionName, $strFunctionDeclaration, ($strFunctionName == 'Login'));
				}	
			}
		}
	}
	
	public function pnlRequest_DoRequest($strFunctionName, $strFuunctionDeclaration, $mixArgumentArray) {
		if (!CookieStore::ExistCookie(strtolower(sprintf('%s_sid', QApplication::GetConfig('software_webservice_name')))) && $this->lstService->SelectedValue != 'auth') {
			$this->Logout();
		} else {
			$this->SetSoapClient($this->lstService->SelectedValue);
			try {
				$mixResult = call_user_func_array(array($this->objSoapClient, $strFunctionName), $mixArgumentArray);
			} catch (SoapFault $e) {
				if ($e->getMessage() == 'NeedLogin') {
					$this->Logout();
					QApplication::Redirect(dirname($_SERVER['PHP_SELF']));
				} else {
					throw $e;
				}
			}
			
			if ($strFunctionName == 'Login' && $this->lstService->SelectedValue == 'auth' && !$this->blnCookieSet) {
				if (!empty($mixResult)) {
					$this->Login(UUID::GetObjectFromSoapObject($mixResult));
				} else {
					$this->pnlResponse->SetFunctionResult($this->lstService->SelectedName, $strFunctionName, $strFuunctionDeclaration, $mixArgumentArray, QApplication::Translate('Incorrect combination'));
				}
			} else if ($strFunctionName == 'AttachSession' && $this->lstService->SelectedValue == 'auth') {
				if (!empty($mixResult)) {
					$this->Login(UUID::GetObjectFromSoapObject($mixResult));
					$this->blnAttachedSession = true;
				} else {
					$this->pnlResponse->SetFunctionResult($this->lstService->SelectedName, $strFunctionName, $strFuunctionDeclaration, $mixArgumentArray, QApplication::Translate('Incorrect session'));
				}
			} else if (($strFunctionName == 'Logout' || $strFunctionName == 'DetachSession') && $this->lstService->SelectedValue == 'auth' && $this->blnCookieSet) {
				$this->Logout();
				$this->blnAttachedSession = false;
				$this->pnlResponse->ResetControls();
			} else if ($strFunctionName == 'ResetSession' && $this->lstService->SelectedValue == 'auth') {
				$this->Logout();
				QApplication::Redirect(dirname($_SERVER['PHP_SELF']));
			} else {
				$this->pnlResponse->SetFunctionResult($this->lstService->SelectedName, $strFunctionName, $strFuunctionDeclaration, $mixArgumentArray, $mixResult);
			}
		}
	}
	
	protected function FillSoapServiceArray() {
		if ($this->blnCookieSet) {
			$this->SetSoapClient('api');
			
			try {
				$this->strSoapServiceNameArray = $this->objSoapClient->GetApis();
			} catch (SoapFault $e) {
				if ($e->getMessage() == 'NeedLogin') {
					$this->Logout();
					QApplication::Redirect(dirname($_SERVER['PHP_SELF']));
				}
			}
		}
	}
	
	protected function Login($strSessionId) {
		$this->pnlResponse->ResetControls();
		$this->SetSoapCookie($strSessionId);
		CookieStore::UpdateCookie(strtolower(sprintf('%s_sid', QApplication::GetConfig('software_webservice_name'))), $strSessionId);
		$this->lstService_Fill();
	}
	
	protected function Logout($blnFillListBoxes = true) {	
		$this->SetSoapCookie(null);
		CookieStore::DeleteCookie(strtolower(sprintf('%s_sid', QApplication::GetConfig('software_webservice_name'))));
		
		if ($blnFillListBoxes) {
			$this->lstService_Fill();
			$this->lstFunctions_Fill();
			$this->lstFunctions_Change();
		}
	}
	
	protected function SetSoapClient($strService) {
		if (is_null($this->txtWsdlUrl->Text)) {
			throw new QUndefinedPropertyException('GET', get_class($this), 'WsdlUrl');
		}
		
		$this->objSoapClient = new SoapClient(sprintf('%s%s?wsdl', $this->txtWsdlUrl->Text, $strService), array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));
		$this->objSoapClient->__setCookie($this->strCookieName, $this->strCookieValue);
		$this->strSoapFunctionNameArray = $this->objSoapClient->__getFunctions();
	}
	
	protected function SetSoapCookie($strCookieValue) {
		if (is_null($strCookieValue)) {
			$this->blnCookieSet = false;
		} else {
			$this->blnCookieSet = true;
		}
		
		$this->strCookieValue = $strCookieValue;
		$this->objSoapClient->__setCookie($this->strCookieName, $this->strCookieValue);
	}
	
	protected function SetWsdlUrl() {
		$this->txtWsdlUrl->Text = sprintf('http%s://%s%s', (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS']) ? 's' : '',	$_SERVER['HTTP_HOST'],	str_replace(basename(__FILE__), '', QApplication::$ScriptName));
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'CopyrightAuthor': return $this->strCopyrightAuthor;
			case 'CopyrightYears': return $this->strCopyrightYears;
			case 'SoftwareName': return $this->strSoftwareName;
			case 'TextCopyright': return $this->strTextCopyright;
			case 'TextIntro': return $this->strTextIntro;
			case 'TextPoweredBy': return $this->strTextPoweredBy;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
//			case 'SoftwareName':
//				try {
//					return ($this->strSoftwareName = QType::Cast($mixValue, QType::String));
//				} catch (QCallerException $objExc) {
//					$objExc->IncrementOffset();
//					throw $objExc;
//				}
			
			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

SoapidioClientForm::Run('SoapidioClientForm');

?>
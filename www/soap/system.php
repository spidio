<?php

// Load Spidio SOAP foundation.
require('prepend.inc.php');

class SoapidioSystem extends SoapidioService {
	/**
	 * Returns CoreSystem version number.
	 *
	 * @return string
	 */
	public function GetCoreSystemVersion() {
		require(sprintf('%s/.versioninfo.php', __INCLUDES__));
		new CoreVersion();
		
		return sprintf('%s.%s', QApplication::GetConfig('software_coresystem_version'), 
			QApplication::GetConfig('software_coresystem_revision'));
	}
	
	/**
	 * Returns Webservices version number.
	 *
	 * @return string
	 */
	public function GetWebServiceVersion() {
		return sprintf('%s.%s', QApplication::GetConfig('software_webservice_version'), 
			QApplication::GetConfig('software_webservice_revision'));
	}
}

SoapidioSystem::Run('SoapidioSystem', 'http://spidio.sourceforge.net/namespaces/system');

?>
<?php
	/**
	 * This is the abstract Panel class for the List All functionality
	 * of the FileType class.  This code-generated class
	 * contains a datagrid to display an HTML page that can
	 * list a collection of FileType objects.  It includes
	 * functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QPanel which extends this FileTypeListPanelBase
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent re-
	 * code generation.
	 * 
	 * @package Spidio
	 * @subpackage Drafts
	 * 
	 */
	class FileTypeListPanel extends QPanel {
		// Local instance of the Meta DataGrid to list FileTypes
		public $dtgFileTypes;

		// Other public QControls in this panel
		public $btnCreateNew;
		public $pxyEdit;

		// Callback Method Names
		protected $strSetEditPanelMethod;
		protected $strCloseEditPanelMethod;
		
		public function __construct($objParentObject, $strSetEditPanelMethod, $strCloseEditPanelMethod, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Record Method Callbacks
			$this->strSetEditPanelMethod = $strSetEditPanelMethod;
			$this->strCloseEditPanelMethod = $strCloseEditPanelMethod;

			// Setup the Template
			$this->Template = 'FileTypeListPanel.tpl.php';

			// Instantiate the Meta DataGrid
			$this->dtgFileTypes = new FileTypeDataGrid($this);

			// Style the DataGrid (if desired)
			$this->dtgFileTypes->CssClass = 'datagrid';
			$this->dtgFileTypes->AlternateRowStyle->CssClass = 'alternate';

			// Add Pagination (if desired)
			$this->dtgFileTypes->Paginator = new QPaginator($this->dtgFileTypes);
			$this->dtgFileTypes->ItemsPerPage = 8;

			// Use the MetaDataGrid functionality to add Columns for this datagrid

			// Create an Edit Column
			$this->pxyEdit = new QControlProxy($this);
			$this->pxyEdit->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'pxyEdit_Click'));
			$this->dtgFileTypes->MetaAddEditProxyColumn($this->pxyEdit, 'Edit', 'Edit');

			// Create the Other Columns (note that you can use strings for ' . DB_TABLE_PREFIX_1 . 'file_type's properties, or you
			// can traverse down QQN::' . DB_TABLE_PREFIX_1 . 'file_type() to display fields that are down the hierarchy)
			$this->dtgFileTypes->MetaAddColumn('Id');
			$this->dtgFileTypes->MetaAddColumn('Name');
			$this->dtgFileTypes->MetaAddColumn('Extension');
			$this->dtgFileTypes->MetaAddColumn('Optlock');
			$this->dtgFileTypes->MetaAddColumn('Owner');
			$this->dtgFileTypes->MetaAddColumn('Group');
			$this->dtgFileTypes->MetaAddColumn('Perms');
			$this->dtgFileTypes->MetaAddTypeColumn('Status', 'AuthStatusEnum');

			// Setup the Create New button
			$this->btnCreateNew = new QButton($this);
			$this->btnCreateNew->Text = QApplication::Translate('Create a New') . ' ' . QApplication::Translate('FileType');
			$this->btnCreateNew->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCreateNew_Click'));
		}

		public function pxyEdit_Click($strFormId, $strControlId, $strParameter) {
			$strParameterArray = explode(',', $strParameter);
			$objEditPanel = new FileTypeEditPanel($this, $this->strCloseEditPanelMethod, $strParameterArray[0]);

			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}

		public function btnCreateNew_Click($strFormId, $strControlId, $strParameter) {
			$objEditPanel = new FileTypeEditPanel($this, $this->strCloseEditPanelMethod, null);
			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}
	}
?>
<?php
	require(__DATAGEN_META_CONTROLS__ . '/AuthPrivilegeDataGridGen.class.php');

	/**
	 * This is the "Meta" DataGrid customizable subclass for the List functionality
	 * of the AuthPrivilege class.  This code-generated class extends
	 * from the generated Meta DataGrid class which contains a QDataGrid class which
	 * can be used by any QForm or QPanel, listing a collection of AuthPrivilege
	 * objects.  It includes functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create an instance of this DataGrid in a QForm or QPanel.
	 *
	 * This file is intended to be modified.  Subsequent code regenerations will NOT modify
	 * or overwrite this file.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * 
	 */
	class AuthPrivilegeDataGrid extends AuthPrivilegeDataGridGen {
	}
?>
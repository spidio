<?php

require('includes/prepend.inc.php');

array_shift($_SERVER['argv']);
$_SERVER['argc'] = count($_SERVER['argv']);

if (!$_SERVER['argc']) {
	die('No output filename provided, halt execution.' . PHP_EOL);
} else {
	$strFileName = $_SERVER['argv'][0];
	$intBytesWritten = 0;
	
	$objFlatRecursiveDirectoryIterator = new FlatRecursiveDirectoryIterator(realpath(sprintf('%s/../www', dirname(__FILE__))));
	$resFilePointer = fopen($strFileName, 'w');
	foreach ($objFlatRecursiveDirectoryIterator as $strEntry) {
		// Exclude .DS_Store, _devtools and includes/qcodo
		if (strpos($strEntry, '.DS_Store') === false &&
			strpos($strEntry, '.gitignore') === false &&
			strpos($strEntry, '.git') === false &&
			strpos($strEntry, '_devtools') === false &&
			strpos($strEntry, 'qcodo') === false) {
			$intBytesWritten += fwrite($resFilePointer, $strEntry . PHP_EOL);
		}
	}
	fclose($resFilePointer);
	
	die('Output file written:' . PHP_EOL . 
		' Filename: ' . $strFileName . PHP_EOL .
		' Path:     ' . realpath('.') . PHP_EOL .
		' Written:  ' . $intBytesWritten . ' bytes' . PHP_EOL 
	);
}

?>
<?php

class FlatRecursiveDirectoryIterator extends RecursiveIteratorIterator {
	public function __construct($strPath) {
		parent::__construct(new RecursiveDirectoryIterator($strPath, RecursiveDirectoryIterator::KEY_AS_FILENAME));
	}
	
	public function current() {
		$strToReturn = $this->getSubIterator(0)->getPath() . '/';
		
		for ($l = 0; $l < $this->getDepth(); $l++) {
			$strToReturn .= $this->getSubIterator($l)->getFilename() . '/';
		}
		
		return $strToReturn . $this->getSubIterator($l)->getFilename();
	}
	
	public function __call($strFunction, $mixParameterArray) {
		return call_user_func_array(array($this->getSubIterator(), $strFunction), $mixParameterArray);
	}
}

?>
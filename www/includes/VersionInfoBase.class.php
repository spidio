<?php

abstract class VersionInfoBase extends QBaseClass {
	protected $strName;
	protected $strVersion;
	
	protected $strAuthor;
	protected $strWebsite;
	
	public function __construct() {
		$this->SetVersionInfo();
		$this->UpdateVersionInfo();
	}
	
	/**
	 * Compare two Spidio version strings by $strOperator.
	 *
	 * @param string $strVersion1
	 * @param string $strVersion2
	 * @param string $strOperator
	 * @return string
	 */
	static public function CompareVersionString($strVersion1, $strVersion2, $strOperator) {
		$strTranslationArray = array(
			'D' => 1,	// Development
			'A' => 2,	// Alpha
			'B' => 3,	// Beta
			'R' => 4,	// Release Candidate
			'F'	=> 5,	// Final
			'P' => 6	// Patch
		);
		
		$strVersion1Array = str_split($strVersion1);
		for ($i = 0; $i < count($strVersion1Array); $i++) {
			if (!is_numeric($strVersion1Array[$i])) {
				$strReleaseState1 = $strTranslationArray[$strVersion1Array[$i]];
				unset($strVersion1Array[$i]);
			}
		}
		$strVersion1Array[] = $strReleaseState1;
		
		$strVersion2Array = str_split($strVersion2);
		for ($i = 0; $i < count($strVersion2Array); $i++) {
			if (!is_numeric($strVersion2Array[$i])) {
				$strReleaseState2 = $strTranslationArray[$strVersion2Array[$i]];
				unset($strVersion2Array[$i]);
			}
		}
		$strVersion2Array[] = $strReleaseState2;
		
		return version_compare(implode('.', $strVersion1Array), implode('.', $strVersion2Array), $strOperator);
	}
	
	protected abstract function SetVersionInfo();
	protected abstract function UpdateVersionInfo();
	
	public function __get($strName) {
		switch ($strName) {
			case 'Name': return $this->strName;
			case 'Version': return $this->strVersion;
			case 'Author': return $this->strAuthor;
			case 'Website': return $this->strWebsite;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
			case 'Name':
				try {
					return ($this->strName = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case "Version":
				try {
					return ($this->strVersion = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Author':
				try {
					return ($this->strAuthor = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Website':
				try {
					return ($this->strWebsite = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
				
			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
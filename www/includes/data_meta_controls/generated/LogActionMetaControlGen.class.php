<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the LogAction class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single LogAction object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a LogActionMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read LogAction $LogAction the actual LogAction data class being edited
	 * property QLabel $IdControl
	 * property-read QLabel $IdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $ConstantControl
	 * property-read QLabel $ConstantLabel
	 * property QTextBox $ExplainationControl
	 * property-read QLabel $ExplainationLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class LogActionMetaControlGen extends QBaseClass {
		// General Variables
		protected $objLogAction;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of LogAction's individual data fields
		protected $lblId;
		protected $txtName;
		protected $txtConstant;
		protected $txtExplaination;

		// Controls that allow the viewing of LogAction's individual data fields
		protected $lblName;
		protected $lblConstant;
		protected $lblExplaination;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * LogActionMetaControl to edit a single LogAction object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single LogAction object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogActionMetaControl
		 * @param LogAction $objLogAction new or existing LogAction object
		 */
		 public function __construct($objParentObject, LogAction $objLogAction) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this LogActionMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked LogAction object
			$this->objLogAction = $objLogAction;

			// Figure out if we're Editing or Creating New
			if ($this->objLogAction->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogActionMetaControl
		 * @param integer $intId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing LogAction object creation - defaults to CreateOrEdit
 		 * @return LogActionMetaControl
		 */
		public static function Create($objParentObject, $intId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($intId)) {
				$objLogAction = LogAction::Load($intId);

				// LogAction was found -- return it!
				if ($objLogAction)
					return new LogActionMetaControl($objParentObject, $objLogAction);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a LogAction object with PK arguments: ' . $intId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new LogActionMetaControl($objParentObject, new LogAction());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogActionMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing LogAction object creation - defaults to CreateOrEdit
		 * @return LogActionMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intId = QApplication::PathInfo(0);
			return LogActionMetaControl::Create($objParentObject, $intId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogActionMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing LogAction object creation - defaults to CreateOrEdit
		 * @return LogActionMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intId = QApplication::QueryString('intId');
			return LogActionMetaControl::Create($objParentObject, $intId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			if ($this->blnEditMode)
				$this->lblId->Text = $this->objLogAction->Id;
			else
				$this->lblId->Text = 'N/A';
			return $this->lblId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objLogAction->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = LogAction::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objLogAction->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtConstant
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtConstant_Create($strControlId = null) {
			$this->txtConstant = new QTextBox($this->objParentObject, $strControlId);
			$this->txtConstant->Name = QApplication::Translate('Constant');
			$this->txtConstant->Text = $this->objLogAction->Constant;
			$this->txtConstant->Required = true;
			$this->txtConstant->MaxLength = LogAction::ConstantMaxLength;
			return $this->txtConstant;
		}

		/**
		 * Create and setup QLabel lblConstant
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblConstant_Create($strControlId = null) {
			$this->lblConstant = new QLabel($this->objParentObject, $strControlId);
			$this->lblConstant->Name = QApplication::Translate('Constant');
			$this->lblConstant->Text = $this->objLogAction->Constant;
			$this->lblConstant->Required = true;
			return $this->lblConstant;
		}

		/**
		 * Create and setup QTextBox txtExplaination
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtExplaination_Create($strControlId = null) {
			$this->txtExplaination = new QTextBox($this->objParentObject, $strControlId);
			$this->txtExplaination->Name = QApplication::Translate('Explaination');
			$this->txtExplaination->Text = $this->objLogAction->Explaination;
			$this->txtExplaination->Required = true;
			$this->txtExplaination->TextMode = QTextMode::MultiLine;
			return $this->txtExplaination;
		}

		/**
		 * Create and setup QLabel lblExplaination
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblExplaination_Create($strControlId = null) {
			$this->lblExplaination = new QLabel($this->objParentObject, $strControlId);
			$this->lblExplaination->Name = QApplication::Translate('Explaination');
			$this->lblExplaination->Text = $this->objLogAction->Explaination;
			$this->lblExplaination->Required = true;
			return $this->lblExplaination;
		}



		/**
		 * Refresh this MetaControl with Data from the local LogAction object.
		 * @param boolean $blnReload reload LogAction from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objLogAction->Reload();

			if ($this->lblId) if ($this->blnEditMode) $this->lblId->Text = $this->objLogAction->Id;

			if ($this->txtName) $this->txtName->Text = $this->objLogAction->Name;
			if ($this->lblName) $this->lblName->Text = $this->objLogAction->Name;

			if ($this->txtConstant) $this->txtConstant->Text = $this->objLogAction->Constant;
			if ($this->lblConstant) $this->lblConstant->Text = $this->objLogAction->Constant;

			if ($this->txtExplaination) $this->txtExplaination->Text = $this->objLogAction->Explaination;
			if ($this->lblExplaination) $this->lblExplaination->Text = $this->objLogAction->Explaination;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC LOGACTION OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's LogAction instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveLogAction() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtName) $this->objLogAction->Name = $this->txtName->Text;
				if ($this->txtConstant) $this->objLogAction->Constant = $this->txtConstant->Text;
				if ($this->txtExplaination) $this->objLogAction->Explaination = $this->txtExplaination->Text;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the LogAction object
				$this->objLogAction->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's LogAction instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteLogAction() {
			$this->objLogAction->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'LogAction': return $this->objLogAction;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to LogAction fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'ConstantControl':
					if (!$this->txtConstant) return $this->txtConstant_Create();
					return $this->txtConstant;
				case 'ConstantLabel':
					if (!$this->lblConstant) return $this->lblConstant_Create();
					return $this->lblConstant;
				case 'ExplainationControl':
					if (!$this->txtExplaination) return $this->txtExplaination_Create();
					return $this->txtExplaination;
				case 'ExplainationLabel':
					if (!$this->lblExplaination) return $this->lblExplaination_Create();
					return $this->lblExplaination;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to LogAction fields
					case 'IdControl':
						return ($this->lblId = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'ConstantControl':
						return ($this->txtConstant = QType::Cast($mixValue, 'QControl'));
					case 'ExplainationControl':
						return ($this->txtExplaination = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
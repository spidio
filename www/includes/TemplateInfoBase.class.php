<?php

abstract class TemplateInfoBase extends VersionInfoBase {
	protected $strCopyright;
	protected $strEmail;
	protected $objConfigArray;
	
	public function __construct(&$objTemplateForm) {
		parent::__construct();
		$this->ProcessConfiguration($objTemplateForm);
	}
	
	protected function CreateConfiguration($strName, $strValue, $blnIsDynamic = false) {
		$objReturn = new TemplateConfig();
		$objReturn->Name = sprintf('%s_%s', strtolower($this->strName), $strName);
		$objReturn->Value = $strValue;
		$objReturn->IsDynamic = $blnIsDynamic;
		return $objReturn;
	}
	
	protected function ProcessConfiguration(&$objTemplateForm) {
		foreach ($this->objConfigArray as $objConfig) {
			$objConfigDatabase = TemplateConfig::Load($objConfig->Name);
			if (empty($objConfigDatabase)) {
				$objConfig->Save();
			} else {
				$objConfig = $objConfigDatabase;
			}
			
			$strMemberName = sprintf('strConfig%s', ucfirst(substr($objConfig->Name, strpos($objConfig->Name, '_') + 1)));
			$objTemplateForm->$strMemberName = QType::Cast($objConfig->Value, QType::String);
		}
	}
	
	protected function ProcessHttpHeaders() {
		foreach ($this->strHeaderArray as $strHeader) {
			header($strHeader);
		}
	}
	
	protected function UpdateVersionInfo() {
		if (!($objTemplate = Template::LoadByName(strtolower($this->strName)))) {
			$objTemplate = new Template();
			
			do {
				$objTemplate->Id = (string) new UUID();
			} while (Template::QueryCount(QQ::Equal(QQN::Template()->Id, $objTemplate->Id)));
			
			$objTemplate->Name = strtolower($this->strName);
			$objTemplate->Version = $this->strVersion;
			$objTemplate->Copyright = $this->strCopyright;
			$objTemplate->Author = $this->strAuthor;
			$objTemplate->Email = $this->strEmail;
			$objTemplate->Website = $this->strWebsite;
			
			$objTemplate->Save();
		} else if (VersionInfoBase::CompareVersionString($this->strVersion, $objTemplate->Version, '>')) {
			$objTemplate->Version = $this->strVersion;
			$objTemplate->Save();
		}
	}
	
	protected function UninstallTemplate($objTemplateForm) {
		foreach ($this->objConfigArray as $objConfig) {
			$objConfig = TemplateQApplication::GetConfig($objConfig->Name);
			if (!empty($objConfig)) {
				$objConfig->Delete();
			}
		}
		
		$objTemplate = Template::LoadByName(strtolower($this->strName));
		$objTemplate->Delete();
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Copyright': return $this->strCopyright;
			case 'Email': return $this->strEmail;
			case 'ConfigArray': return $this->objConfigArray;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
			case 'Copyright':
				try {
					return ($this->strCopyright = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Email':
				try {
					return ($this->strEmail = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'ConfigArray':
				try {
					return ($this->objConfigArray = QType::Cast($mixValue, QType::ArrayType));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
				
			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
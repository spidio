<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the TemplateConfig class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single TemplateConfig object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a TemplateConfigMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read TemplateConfig $TemplateConfig the actual TemplateConfig data class being edited
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $ValueControl
	 * property-read QLabel $ValueLabel
	 * property QCheckBox $IsDynamicControl
	 * property-read QLabel $IsDynamicLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class TemplateConfigMetaControlGen extends QBaseClass {
		// General Variables
		protected $objTemplateConfig;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of TemplateConfig's individual data fields
		protected $txtName;
		protected $txtValue;
		protected $chkIsDynamic;
		protected $lblOptlock;

		// Controls that allow the viewing of TemplateConfig's individual data fields
		protected $lblName;
		protected $lblValue;
		protected $lblIsDynamic;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * TemplateConfigMetaControl to edit a single TemplateConfig object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single TemplateConfig object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateConfigMetaControl
		 * @param TemplateConfig $objTemplateConfig new or existing TemplateConfig object
		 */
		 public function __construct($objParentObject, TemplateConfig $objTemplateConfig) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this TemplateConfigMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked TemplateConfig object
			$this->objTemplateConfig = $objTemplateConfig;

			// Figure out if we're Editing or Creating New
			if ($this->objTemplateConfig->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateConfigMetaControl
		 * @param string $strName primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing TemplateConfig object creation - defaults to CreateOrEdit
 		 * @return TemplateConfigMetaControl
		 */
		public static function Create($objParentObject, $strName = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strName)) {
				$objTemplateConfig = TemplateConfig::Load($strName);

				// TemplateConfig was found -- return it!
				if ($objTemplateConfig)
					return new TemplateConfigMetaControl($objParentObject, $objTemplateConfig);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a TemplateConfig object with PK arguments: ' . $strName);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new TemplateConfigMetaControl($objParentObject, new TemplateConfig());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateConfigMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing TemplateConfig object creation - defaults to CreateOrEdit
		 * @return TemplateConfigMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strName = QApplication::PathInfo(0);
			return TemplateConfigMetaControl::Create($objParentObject, $strName, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateConfigMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing TemplateConfig object creation - defaults to CreateOrEdit
		 * @return TemplateConfigMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strName = QApplication::QueryString('strName');
			return TemplateConfigMetaControl::Create($objParentObject, $strName, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objTemplateConfig->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = TemplateConfig::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objTemplateConfig->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtValue
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtValue_Create($strControlId = null) {
			$this->txtValue = new QTextBox($this->objParentObject, $strControlId);
			$this->txtValue->Name = QApplication::Translate('Value');
			$this->txtValue->Text = $this->objTemplateConfig->Value;
			$this->txtValue->Required = true;
			$this->txtValue->MaxLength = TemplateConfig::ValueMaxLength;
			return $this->txtValue;
		}

		/**
		 * Create and setup QLabel lblValue
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblValue_Create($strControlId = null) {
			$this->lblValue = new QLabel($this->objParentObject, $strControlId);
			$this->lblValue->Name = QApplication::Translate('Value');
			$this->lblValue->Text = $this->objTemplateConfig->Value;
			$this->lblValue->Required = true;
			return $this->lblValue;
		}

		/**
		 * Create and setup QCheckBox chkIsDynamic
		 * @param string $strControlId optional ControlId to use
		 * @return QCheckBox
		 */
		public function chkIsDynamic_Create($strControlId = null) {
			$this->chkIsDynamic = new QCheckBox($this->objParentObject, $strControlId);
			$this->chkIsDynamic->Name = QApplication::Translate('Is Dynamic');
			$this->chkIsDynamic->Checked = $this->objTemplateConfig->IsDynamic;
			return $this->chkIsDynamic;
		}

		/**
		 * Create and setup QLabel lblIsDynamic
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblIsDynamic_Create($strControlId = null) {
			$this->lblIsDynamic = new QLabel($this->objParentObject, $strControlId);
			$this->lblIsDynamic->Name = QApplication::Translate('Is Dynamic');
			$this->lblIsDynamic->Text = ($this->objTemplateConfig->IsDynamic) ? QApplication::Translate('Yes') : QApplication::Translate('No');
			return $this->lblIsDynamic;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objTemplateConfig->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}



		/**
		 * Refresh this MetaControl with Data from the local TemplateConfig object.
		 * @param boolean $blnReload reload TemplateConfig from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objTemplateConfig->Reload();

			if ($this->txtName) $this->txtName->Text = $this->objTemplateConfig->Name;
			if ($this->lblName) $this->lblName->Text = $this->objTemplateConfig->Name;

			if ($this->txtValue) $this->txtValue->Text = $this->objTemplateConfig->Value;
			if ($this->lblValue) $this->lblValue->Text = $this->objTemplateConfig->Value;

			if ($this->chkIsDynamic) $this->chkIsDynamic->Checked = $this->objTemplateConfig->IsDynamic;
			if ($this->lblIsDynamic) $this->lblIsDynamic->Text = ($this->objTemplateConfig->IsDynamic) ? QApplication::Translate('Yes') : QApplication::Translate('No');

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objTemplateConfig->Optlock;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC TEMPLATECONFIG OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's TemplateConfig instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveTemplateConfig() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtName) $this->objTemplateConfig->Name = $this->txtName->Text;
				if ($this->txtValue) $this->objTemplateConfig->Value = $this->txtValue->Text;
				if ($this->chkIsDynamic) $this->objTemplateConfig->IsDynamic = $this->chkIsDynamic->Checked;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the TemplateConfig object
				$this->objTemplateConfig->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's TemplateConfig instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteTemplateConfig() {
			$this->objTemplateConfig->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'TemplateConfig': return $this->objTemplateConfig;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to TemplateConfig fields -- will be created dynamically if not yet created
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'ValueControl':
					if (!$this->txtValue) return $this->txtValue_Create();
					return $this->txtValue;
				case 'ValueLabel':
					if (!$this->lblValue) return $this->lblValue_Create();
					return $this->lblValue;
				case 'IsDynamicControl':
					if (!$this->chkIsDynamic) return $this->chkIsDynamic_Create();
					return $this->chkIsDynamic;
				case 'IsDynamicLabel':
					if (!$this->lblIsDynamic) return $this->lblIsDynamic_Create();
					return $this->lblIsDynamic;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to TemplateConfig fields
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'ValueControl':
						return ($this->txtValue = QType::Cast($mixValue, 'QControl'));
					case 'IsDynamicControl':
						return ($this->chkIsDynamic = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
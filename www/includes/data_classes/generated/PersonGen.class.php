<?php
	/**
	 * The abstract PersonGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Person subclass which
	 * extends this PersonGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Person class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $AddressId the value for strAddressId (Not Null)
	 * @property string $Name the value for strName (Not Null)
	 * @property string $Email the value for strEmail (Not Null)
	 * @property integer $Phone the value for intPhone 
	 * @property integer $PhoneA the value for intPhoneA 
	 * @property integer $PhoneC the value for intPhoneC 
	 * @property integer $Mobile the value for intMobile 
	 * @property integer $MobileA the value for intMobileA 
	 * @property integer $MobileC the value for intMobileC 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Address $Address the value for the Address object referenced by strAddressId (Not Null)
	 * @property-read Organisation $_OrganisationAsPerson the value for the private _objOrganisationAsPerson (Read-Only) if set due to an expansion on the spidio_organisation.person_id reverse relationship
	 * @property-read Organisation[] $_OrganisationAsPersonArray the value for the private _objOrganisationAsPersonArray (Read-Only) if set due to an ExpandAsArray on the spidio_organisation.person_id reverse relationship
	 * @property-read TapeLocation $_TapeLocationAsPerson the value for the private _objTapeLocationAsPerson (Read-Only) if set due to an expansion on the spidio_tape_location.person_id reverse relationship
	 * @property-read TapeLocation[] $_TapeLocationAsPersonArray the value for the private _objTapeLocationAsPersonArray (Read-Only) if set due to an ExpandAsArray on the spidio_tape_location.person_id reverse relationship
	 * @property-read User $_UserAsPerson the value for the private _objUserAsPerson (Read-Only) if set due to an expansion on the spidio_user.person_id reverse relationship
	 * @property-read User[] $_UserAsPersonArray the value for the private _objUserAsPersonArray (Read-Only) if set due to an ExpandAsArray on the spidio_user.person_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class PersonGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_person.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_person.address_id
		 * @var string strAddressId
		 */
		protected $strAddressId;
		const AddressIdMaxLength = 36;
		const AddressIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.email
		 * @var string strEmail
		 */
		protected $strEmail;
		const EmailMaxLength = 255;
		const EmailDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.phone
		 * @var integer intPhone
		 */
		protected $intPhone;
		const PhoneDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.phone_a
		 * @var integer intPhoneA
		 */
		protected $intPhoneA;
		const PhoneADefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.phone_c
		 * @var integer intPhoneC
		 */
		protected $intPhoneC;
		const PhoneCDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.mobile
		 * @var integer intMobile
		 */
		protected $intMobile;
		const MobileDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.mobile_a
		 * @var integer intMobileA
		 */
		protected $intMobileA;
		const MobileADefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.mobile_c
		 * @var integer intMobileC
		 */
		protected $intMobileC;
		const MobileCDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_person.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single OrganisationAsPerson object
		 * (of type Organisation), if this Person object was restored with
		 * an expansion on the spidio_organisation association table.
		 * @var Organisation _objOrganisationAsPerson;
		 */
		private $_objOrganisationAsPerson;

		/**
		 * Private member variable that stores a reference to an array of OrganisationAsPerson objects
		 * (of type Organisation[]), if this Person object was restored with
		 * an ExpandAsArray on the spidio_organisation association table.
		 * @var Organisation[] _objOrganisationAsPersonArray;
		 */
		private $_objOrganisationAsPersonArray = array();

		/**
		 * Private member variable that stores a reference to a single TapeLocationAsPerson object
		 * (of type TapeLocation), if this Person object was restored with
		 * an expansion on the spidio_tape_location association table.
		 * @var TapeLocation _objTapeLocationAsPerson;
		 */
		private $_objTapeLocationAsPerson;

		/**
		 * Private member variable that stores a reference to an array of TapeLocationAsPerson objects
		 * (of type TapeLocation[]), if this Person object was restored with
		 * an ExpandAsArray on the spidio_tape_location association table.
		 * @var TapeLocation[] _objTapeLocationAsPersonArray;
		 */
		private $_objTapeLocationAsPersonArray = array();

		/**
		 * Private member variable that stores a reference to a single UserAsPerson object
		 * (of type User), if this Person object was restored with
		 * an expansion on the spidio_user association table.
		 * @var User _objUserAsPerson;
		 */
		private $_objUserAsPerson;

		/**
		 * Private member variable that stores a reference to an array of UserAsPerson objects
		 * (of type User[]), if this Person object was restored with
		 * an ExpandAsArray on the spidio_user association table.
		 * @var User[] _objUserAsPersonArray;
		 */
		private $_objUserAsPersonArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_person.address_id.
		 *
		 * NOTE: Always use the Address property getter to correctly retrieve this Address object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Address objAddress
		 */
		protected $objAddress;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Person from PK Info
		 * @param string $strId
		 * @return Person
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Person::QuerySingle(
				QQ::Equal(QQN::Person()->Id, $strId)
			);
		}

		/**
		 * Load all People
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Person[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Person::QueryArray to perform the LoadAll query
			try {
				return Person::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all People
		 * @return int
		 */
		public static function CountAll() {
			// Call Person::QueryCount to perform the CountAll query
			return Person::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Create/Build out the QueryBuilder object with Person-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'person');
			Person::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'person');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Person object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Person the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Person::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Person object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Person::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Person objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Person[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Person::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Person::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Person objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Person::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'person_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Person-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Person::GetSelectFields($objQueryBuilder);
				Person::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Person::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Person
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'person';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'address_id', $strAliasPrefix . 'address_id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'email', $strAliasPrefix . 'email');
			$objBuilder->AddSelectItem($strTableName, 'phone', $strAliasPrefix . 'phone');
			$objBuilder->AddSelectItem($strTableName, 'phone_a', $strAliasPrefix . 'phone_a');
			$objBuilder->AddSelectItem($strTableName, 'phone_c', $strAliasPrefix . 'phone_c');
			$objBuilder->AddSelectItem($strTableName, 'mobile', $strAliasPrefix . 'mobile');
			$objBuilder->AddSelectItem($strTableName, 'mobile_a', $strAliasPrefix . 'mobile_a');
			$objBuilder->AddSelectItem($strTableName, 'mobile_c', $strAliasPrefix . 'mobile_c');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Person from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Person::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Person
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'person__';


				$strAlias = $strAliasPrefix . 'organisationasperson__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objOrganisationAsPersonArray)) {
						$objPreviousChildItem = $objPreviousItem->_objOrganisationAsPersonArray[$intPreviousChildItemCount - 1];
						$objChildItem = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasperson__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objOrganisationAsPersonArray[] = $objChildItem;
					} else
						$objPreviousItem->_objOrganisationAsPersonArray[] = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'tapelocationasperson__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objTapeLocationAsPersonArray)) {
						$objPreviousChildItem = $objPreviousItem->_objTapeLocationAsPersonArray[$intPreviousChildItemCount - 1];
						$objChildItem = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationasperson__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objTapeLocationAsPersonArray[] = $objChildItem;
					} else
						$objPreviousItem->_objTapeLocationAsPersonArray[] = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'userasperson__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objUserAsPersonArray)) {
						$objPreviousChildItem = $objPreviousItem->_objUserAsPersonArray[$intPreviousChildItemCount - 1];
						$objChildItem = User::InstantiateDbRow($objDbRow, $strAliasPrefix . 'userasperson__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objUserAsPersonArray[] = $objChildItem;
					} else
						$objPreviousItem->_objUserAsPersonArray[] = User::InstantiateDbRow($objDbRow, $strAliasPrefix . 'userasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'person__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the Person object
			$objToReturn = new Person();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'address_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'address_id'] : $strAliasPrefix . 'address_id';
			$objToReturn->strAddressId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'email', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'email'] : $strAliasPrefix . 'email';
			$objToReturn->strEmail = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'phone', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'phone'] : $strAliasPrefix . 'phone';
			$objToReturn->intPhone = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'phone_a', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'phone_a'] : $strAliasPrefix . 'phone_a';
			$objToReturn->intPhoneA = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'phone_c', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'phone_c'] : $strAliasPrefix . 'phone_c';
			$objToReturn->intPhoneC = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'mobile', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'mobile'] : $strAliasPrefix . 'mobile';
			$objToReturn->intMobile = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'mobile_a', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'mobile_a'] : $strAliasPrefix . 'mobile_a';
			$objToReturn->intMobileA = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'mobile_c', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'mobile_c'] : $strAliasPrefix . 'mobile_c';
			$objToReturn->intMobileC = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'person__';

			// Check for Address Early Binding
			$strAlias = $strAliasPrefix . 'address_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objAddress = Address::InstantiateDbRow($objDbRow, $strAliasPrefix . 'address_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			// Check for OrganisationAsPerson Virtual Binding
			$strAlias = $strAliasPrefix . 'organisationasperson__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objOrganisationAsPersonArray[] = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objOrganisationAsPerson = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for TapeLocationAsPerson Virtual Binding
			$strAlias = $strAliasPrefix . 'tapelocationasperson__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objTapeLocationAsPersonArray[] = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objTapeLocationAsPerson = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for UserAsPerson Virtual Binding
			$strAlias = $strAliasPrefix . 'userasperson__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objUserAsPersonArray[] = User::InstantiateDbRow($objDbRow, $strAliasPrefix . 'userasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objUserAsPerson = User::InstantiateDbRow($objDbRow, $strAliasPrefix . 'userasperson__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of People from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Person[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Person::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Person::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Person object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Person
		*/
		public static function LoadById($strId) {
			return Person::QuerySingle(
				QQ::Equal(QQN::Person()->Id, $strId)
			);
		}
			
		/**
		 * Load a single Person object,
		 * by Name, Email Index(es)
		 * @param string $strName
		 * @param string $strEmail
		 * @return Person
		*/
		public static function LoadByNameEmail($strName, $strEmail) {
			return Person::QuerySingle(
				QQ::AndCondition(
				QQ::Equal(QQN::Person()->Name, $strName),
				QQ::Equal(QQN::Person()->Email, $strEmail)
				)
			);
		}
			
		/**
		 * Load an array of Person objects,
		 * by AddressId Index(es)
		 * @param string $strAddressId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Person[]
		*/
		public static function LoadArrayByAddressId($strAddressId, $objOptionalClauses = null) {
			// Call Person::QueryArray to perform the LoadArrayByAddressId query
			try {
				return Person::QueryArray(
					QQ::Equal(QQN::Person()->AddressId, $strAddressId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count People
		 * by AddressId Index(es)
		 * @param string $strAddressId
		 * @return int
		*/
		public static function CountByAddressId($strAddressId) {
			// Call Person::QueryCount to perform the CountByAddressId query
			return Person::QueryCount(
				QQ::Equal(QQN::Person()->AddressId, $strAddressId)
			);
		}
			
		/**
		 * Load an array of Person objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Person[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call Person::QueryArray to perform the LoadArrayByOwner query
			try {
				return Person::QueryArray(
					QQ::Equal(QQN::Person()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count People
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call Person::QueryCount to perform the CountByOwner query
			return Person::QueryCount(
				QQ::Equal(QQN::Person()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of Person objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Person[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call Person::QueryArray to perform the LoadArrayByStatus query
			try {
				return Person::QueryArray(
					QQ::Equal(QQN::Person()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count People
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call Person::QueryCount to perform the CountByStatus query
			return Person::QueryCount(
				QQ::Equal(QQN::Person()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Person
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'person` (
							`id`,
							`address_id`,
							`name`,
							`email`,
							`phone`,
							`phone_a`,
							`phone_c`,
							`mobile`,
							`mobile_a`,
							`mobile_c`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strAddressId) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->strEmail) . ',
							' . $objDatabase->SqlVariable($this->intPhone) . ',
							' . $objDatabase->SqlVariable($this->intPhoneA) . ',
							' . $objDatabase->SqlVariable($this->intPhoneC) . ',
							' . $objDatabase->SqlVariable($this->intMobile) . ',
							' . $objDatabase->SqlVariable($this->intMobileA) . ',
							' . $objDatabase->SqlVariable($this->intMobileC) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'person`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Person');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'person`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`address_id` = ' . $objDatabase->SqlVariable($this->strAddressId) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`email` = ' . $objDatabase->SqlVariable($this->strEmail) . ',
							`phone` = ' . $objDatabase->SqlVariable($this->intPhone) . ',
							`phone_a` = ' . $objDatabase->SqlVariable($this->intPhoneA) . ',
							`phone_c` = ' . $objDatabase->SqlVariable($this->intPhoneC) . ',
							`mobile` = ' . $objDatabase->SqlVariable($this->intMobile) . ',
							`mobile_a` = ' . $objDatabase->SqlVariable($this->intMobileA) . ',
							`mobile_c` = ' . $objDatabase->SqlVariable($this->intMobileC) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'person`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Person
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Person with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'person`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all People
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'person`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'person table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'person`');
		}

		/**
		 * Reload this Person from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Person object.');

			// Reload the Object
			$objReloaded = Person::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->AddressId = $objReloaded->AddressId;
			$this->strName = $objReloaded->strName;
			$this->strEmail = $objReloaded->strEmail;
			$this->intPhone = $objReloaded->intPhone;
			$this->intPhoneA = $objReloaded->intPhoneA;
			$this->intPhoneC = $objReloaded->intPhoneC;
			$this->intMobile = $objReloaded->intMobile;
			$this->intMobileA = $objReloaded->intMobileA;
			$this->intMobileC = $objReloaded->intMobileC;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'AddressId':
					/**
					 * Gets the value for strAddressId (Not Null)
					 * @return string
					 */
					return $this->strAddressId;

				case 'Name':
					/**
					 * Gets the value for strName (Not Null)
					 * @return string
					 */
					return $this->strName;

				case 'Email':
					/**
					 * Gets the value for strEmail (Not Null)
					 * @return string
					 */
					return $this->strEmail;

				case 'Phone':
					/**
					 * Gets the value for intPhone 
					 * @return integer
					 */
					return $this->intPhone;

				case 'PhoneA':
					/**
					 * Gets the value for intPhoneA 
					 * @return integer
					 */
					return $this->intPhoneA;

				case 'PhoneC':
					/**
					 * Gets the value for intPhoneC 
					 * @return integer
					 */
					return $this->intPhoneC;

				case 'Mobile':
					/**
					 * Gets the value for intMobile 
					 * @return integer
					 */
					return $this->intMobile;

				case 'MobileA':
					/**
					 * Gets the value for intMobileA 
					 * @return integer
					 */
					return $this->intMobileA;

				case 'MobileC':
					/**
					 * Gets the value for intMobileC 
					 * @return integer
					 */
					return $this->intMobileC;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Address':
					/**
					 * Gets the value for the Address object referenced by strAddressId (Not Null)
					 * @return Address
					 */
					try {
						if ((!$this->objAddress) && (!is_null($this->strAddressId)))
							$this->objAddress = Address::Load($this->strAddressId);
						return $this->objAddress;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_OrganisationAsPerson':
					/**
					 * Gets the value for the private _objOrganisationAsPerson (Read-Only)
					 * if set due to an expansion on the spidio_organisation.person_id reverse relationship
					 * @return Organisation
					 */
					return $this->_objOrganisationAsPerson;

				case '_OrganisationAsPersonArray':
					/**
					 * Gets the value for the private _objOrganisationAsPersonArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_organisation.person_id reverse relationship
					 * @return Organisation[]
					 */
					return (array) $this->_objOrganisationAsPersonArray;

				case '_TapeLocationAsPerson':
					/**
					 * Gets the value for the private _objTapeLocationAsPerson (Read-Only)
					 * if set due to an expansion on the spidio_tape_location.person_id reverse relationship
					 * @return TapeLocation
					 */
					return $this->_objTapeLocationAsPerson;

				case '_TapeLocationAsPersonArray':
					/**
					 * Gets the value for the private _objTapeLocationAsPersonArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_tape_location.person_id reverse relationship
					 * @return TapeLocation[]
					 */
					return (array) $this->_objTapeLocationAsPersonArray;

				case '_UserAsPerson':
					/**
					 * Gets the value for the private _objUserAsPerson (Read-Only)
					 * if set due to an expansion on the spidio_user.person_id reverse relationship
					 * @return User
					 */
					return $this->_objUserAsPerson;

				case '_UserAsPersonArray':
					/**
					 * Gets the value for the private _objUserAsPersonArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_user.person_id reverse relationship
					 * @return User[]
					 */
					return (array) $this->_objUserAsPersonArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AddressId':
					/**
					 * Sets the value for strAddressId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objAddress = null;
						return ($this->strAddressId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Email':
					/**
					 * Sets the value for strEmail (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strEmail = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Phone':
					/**
					 * Sets the value for intPhone 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPhone = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PhoneA':
					/**
					 * Sets the value for intPhoneA 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPhoneA = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PhoneC':
					/**
					 * Sets the value for intPhoneC 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPhoneC = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Mobile':
					/**
					 * Sets the value for intMobile 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intMobile = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'MobileA':
					/**
					 * Sets the value for intMobileA 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intMobileA = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'MobileC':
					/**
					 * Sets the value for intMobileC 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intMobileC = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Address':
					/**
					 * Sets the value for the Address object referenced by strAddressId (Not Null)
					 * @param Address $mixValue
					 * @return Address
					 */
					if (is_null($mixValue)) {
						$this->strAddressId = null;
						$this->objAddress = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Address object
						try {
							$mixValue = QType::Cast($mixValue, 'Address');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Address object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Address for this Person');

						// Update Local Member Variables
						$this->objAddress = $mixValue;
						$this->strAddressId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for OrganisationAsPerson
		//-------------------------------------------------------------------

		/**
		 * Gets all associated OrganisationsAsPerson as an array of Organisation objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		*/ 
		public function GetOrganisationAsPersonArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Organisation::LoadArrayByPersonId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated OrganisationsAsPerson
		 * @return int
		*/ 
		public function CountOrganisationsAsPerson() {
			if ((is_null($this->strId)))
				return 0;

			return Organisation::CountByPersonId($this->strId);
		}

		/**
		 * Associates a OrganisationAsPerson
		 * @param Organisation $objOrganisation
		 * @return void
		*/ 
		public function AssociateOrganisationAsPerson(Organisation $objOrganisation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateOrganisationAsPerson on this unsaved Person.');
			if ((is_null($objOrganisation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateOrganisationAsPerson on this Person with an unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_organisation`
				SET
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objOrganisation->Id) . '
			');
		}

		/**
		 * Unassociates a OrganisationAsPerson
		 * @param Organisation $objOrganisation
		 * @return void
		*/ 
		public function UnassociateOrganisationAsPerson(Organisation $objOrganisation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsPerson on this unsaved Person.');
			if ((is_null($objOrganisation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsPerson on this Person with an unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_organisation`
				SET
					`person_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objOrganisation->Id) . ' AND
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all OrganisationsAsPerson
		 * @return void
		*/ 
		public function UnassociateAllOrganisationsAsPerson() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsPerson on this unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_organisation`
				SET
					`person_id` = null
				WHERE
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated OrganisationAsPerson
		 * @param Organisation $objOrganisation
		 * @return void
		*/ 
		public function DeleteAssociatedOrganisationAsPerson(Organisation $objOrganisation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsPerson on this unsaved Person.');
			if ((is_null($objOrganisation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsPerson on this Person with an unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_organisation`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objOrganisation->Id) . ' AND
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated OrganisationsAsPerson
		 * @return void
		*/ 
		public function DeleteAllOrganisationsAsPerson() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsPerson on this unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_organisation`
				WHERE
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for TapeLocationAsPerson
		//-------------------------------------------------------------------

		/**
		 * Gets all associated TapeLocationsAsPerson as an array of TapeLocation objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/ 
		public function GetTapeLocationAsPersonArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return TapeLocation::LoadArrayByPersonId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated TapeLocationsAsPerson
		 * @return int
		*/ 
		public function CountTapeLocationsAsPerson() {
			if ((is_null($this->strId)))
				return 0;

			return TapeLocation::CountByPersonId($this->strId);
		}

		/**
		 * Associates a TapeLocationAsPerson
		 * @param TapeLocation $objTapeLocation
		 * @return void
		*/ 
		public function AssociateTapeLocationAsPerson(TapeLocation $objTapeLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateTapeLocationAsPerson on this unsaved Person.');
			if ((is_null($objTapeLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateTapeLocationAsPerson on this Person with an unsaved TapeLocation.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape_location`
				SET
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTapeLocation->Id) . '
			');
		}

		/**
		 * Unassociates a TapeLocationAsPerson
		 * @param TapeLocation $objTapeLocation
		 * @return void
		*/ 
		public function UnassociateTapeLocationAsPerson(TapeLocation $objTapeLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsPerson on this unsaved Person.');
			if ((is_null($objTapeLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsPerson on this Person with an unsaved TapeLocation.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape_location`
				SET
					`person_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTapeLocation->Id) . ' AND
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all TapeLocationsAsPerson
		 * @return void
		*/ 
		public function UnassociateAllTapeLocationsAsPerson() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsPerson on this unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape_location`
				SET
					`person_id` = null
				WHERE
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated TapeLocationAsPerson
		 * @param TapeLocation $objTapeLocation
		 * @return void
		*/ 
		public function DeleteAssociatedTapeLocationAsPerson(TapeLocation $objTapeLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsPerson on this unsaved Person.');
			if ((is_null($objTapeLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsPerson on this Person with an unsaved TapeLocation.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_tape_location`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTapeLocation->Id) . ' AND
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated TapeLocationsAsPerson
		 * @return void
		*/ 
		public function DeleteAllTapeLocationsAsPerson() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsPerson on this unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_tape_location`
				WHERE
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for UserAsPerson
		//-------------------------------------------------------------------

		/**
		 * Gets all associated UsersAsPerson as an array of User objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return User[]
		*/ 
		public function GetUserAsPersonArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return User::LoadArrayByPersonId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated UsersAsPerson
		 * @return int
		*/ 
		public function CountUsersAsPerson() {
			if ((is_null($this->strId)))
				return 0;

			return User::CountByPersonId($this->strId);
		}

		/**
		 * Associates a UserAsPerson
		 * @param User $objUser
		 * @return void
		*/ 
		public function AssociateUserAsPerson(User $objUser) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateUserAsPerson on this unsaved Person.');
			if ((is_null($objUser->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateUserAsPerson on this Person with an unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_user`
				SET
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objUser->Id) . '
			');
		}

		/**
		 * Unassociates a UserAsPerson
		 * @param User $objUser
		 * @return void
		*/ 
		public function UnassociateUserAsPerson(User $objUser) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateUserAsPerson on this unsaved Person.');
			if ((is_null($objUser->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateUserAsPerson on this Person with an unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_user`
				SET
					`person_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objUser->Id) . ' AND
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all UsersAsPerson
		 * @return void
		*/ 
		public function UnassociateAllUsersAsPerson() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateUserAsPerson on this unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_user`
				SET
					`person_id` = null
				WHERE
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated UserAsPerson
		 * @param User $objUser
		 * @return void
		*/ 
		public function DeleteAssociatedUserAsPerson(User $objUser) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateUserAsPerson on this unsaved Person.');
			if ((is_null($objUser->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateUserAsPerson on this Person with an unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_user`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objUser->Id) . ' AND
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated UsersAsPerson
		 * @return void
		*/ 
		public function DeleteAllUsersAsPerson() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateUserAsPerson on this unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Person::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_user`
				WHERE
					`person_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Person"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Address" type="xsd1:Address"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="Email" type="xsd:string"/>';
			$strToReturn .= '<element name="Phone" type="xsd:int"/>';
			$strToReturn .= '<element name="PhoneA" type="xsd:int"/>';
			$strToReturn .= '<element name="PhoneC" type="xsd:int"/>';
			$strToReturn .= '<element name="Mobile" type="xsd:int"/>';
			$strToReturn .= '<element name="MobileA" type="xsd:int"/>';
			$strToReturn .= '<element name="MobileC" type="xsd:int"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Person', $strComplexTypeArray)) {
				$strComplexTypeArray['Person'] = Person::GetSoapComplexTypeXml();
				Address::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Person::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Person();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'Address')) &&
				($objSoapObject->Address))
				$objToReturn->Address = Address::GetObjectFromSoapObject($objSoapObject->Address);
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'Email'))
				$objToReturn->strEmail = $objSoapObject->Email;
			if (property_exists($objSoapObject, 'Phone'))
				$objToReturn->intPhone = $objSoapObject->Phone;
			if (property_exists($objSoapObject, 'PhoneA'))
				$objToReturn->intPhoneA = $objSoapObject->PhoneA;
			if (property_exists($objSoapObject, 'PhoneC'))
				$objToReturn->intPhoneC = $objSoapObject->PhoneC;
			if (property_exists($objSoapObject, 'Mobile'))
				$objToReturn->intMobile = $objSoapObject->Mobile;
			if (property_exists($objSoapObject, 'MobileA'))
				$objToReturn->intMobileA = $objSoapObject->MobileA;
			if (property_exists($objSoapObject, 'MobileC'))
				$objToReturn->intMobileC = $objSoapObject->MobileC;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Person::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objAddress)
				$objObject->objAddress = Address::GetSoapObjectFromObject($objObject->objAddress, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strAddressId = null;
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodePerson extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Person';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'person';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'AddressId':
					return new QQNode('address_id', 'AddressId', 'string', $this);
				case 'Address':
					return new QQNodeAddress('address_id', 'Address', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Email':
					return new QQNode('email', 'Email', 'string', $this);
				case 'Phone':
					return new QQNode('phone', 'Phone', 'integer', $this);
				case 'PhoneA':
					return new QQNode('phone_a', 'PhoneA', 'integer', $this);
				case 'PhoneC':
					return new QQNode('phone_c', 'PhoneC', 'integer', $this);
				case 'Mobile':
					return new QQNode('mobile', 'Mobile', 'integer', $this);
				case 'MobileA':
					return new QQNode('mobile_a', 'MobileA', 'integer', $this);
				case 'MobileC':
					return new QQNode('mobile_c', 'MobileC', 'integer', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'OrganisationAsPerson':
					return new QQReverseReferenceNodeOrganisation($this, 'organisationasperson', 'reverse_reference', 'person_id');
				case 'TapeLocationAsPerson':
					return new QQReverseReferenceNodeTapeLocation($this, 'tapelocationasperson', 'reverse_reference', 'person_id');
				case 'UserAsPerson':
					return new QQReverseReferenceNodeUser($this, 'userasperson', 'reverse_reference', 'person_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodePerson extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Person';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'person';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'AddressId':
					return new QQNode('address_id', 'AddressId', 'string', $this);
				case 'Address':
					return new QQNodeAddress('address_id', 'Address', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Email':
					return new QQNode('email', 'Email', 'string', $this);
				case 'Phone':
					return new QQNode('phone', 'Phone', 'integer', $this);
				case 'PhoneA':
					return new QQNode('phone_a', 'PhoneA', 'integer', $this);
				case 'PhoneC':
					return new QQNode('phone_c', 'PhoneC', 'integer', $this);
				case 'Mobile':
					return new QQNode('mobile', 'Mobile', 'integer', $this);
				case 'MobileA':
					return new QQNode('mobile_a', 'MobileA', 'integer', $this);
				case 'MobileC':
					return new QQNode('mobile_c', 'MobileC', 'integer', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'OrganisationAsPerson':
					return new QQReverseReferenceNodeOrganisation($this, 'organisationasperson', 'reverse_reference', 'person_id');
				case 'TapeLocationAsPerson':
					return new QQReverseReferenceNodeTapeLocation($this, 'tapelocationasperson', 'reverse_reference', 'person_id');
				case 'UserAsPerson':
					return new QQReverseReferenceNodeUser($this, 'userasperson', 'reverse_reference', 'person_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
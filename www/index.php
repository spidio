<?php

// Include prepend.inc to load Qcodo
require('includes/prepend.inc.php');

// Enable BBCode support
QApplication::InitializeBBCode();

// Update version information if needed
QApplication::UpdateVersionInfo();

// Load the requested page
switch (QApplication::PathInfo(0)) {
	default:
	case QApplication::GetConfig('pagename_index'):
		QApplication::LoadForm(QApplication::GetConfig('pagename_index'));
		break;
	case QApplication::GetConfig('pagename_login'):
		QApplication::LoadForm(QApplication::GetConfig('pagename_login'));
		break;
	case QApplication::GetConfig('pagename_logout'):
		QApplication::LoadForm(QApplication::GetConfig('pagename_logout'));
		break;
	case QApplication::GetConfig('pagename_recent'):
		QApplication::LoadForm(QApplication::GetConfig('pagename_recent'));
		break;
}

?>
<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Session class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Session object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a SessionMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Session $Session the actual Session data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $UserIdControl
	 * property-read QLabel $UserIdLabel
	 * property QTextBox $IpAddressControl
	 * property-read QLabel $IpAddressLabel
	 * property QCheckBox $AutoLoginControl
	 * property-read QLabel $AutoLoginLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QDateTimePicker $ChangedControl
	 * property-read QLabel $ChangedLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QListBox $SessionKeyAsSessionControl
	 * property-read QLabel $SessionKeyAsSessionLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class SessionMetaControlGen extends QBaseClass {
		// General Variables
		protected $objSession;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Session's individual data fields
		protected $txtId;
		protected $lstUser;
		protected $txtIpAddress;
		protected $chkAutoLogin;
		protected $calAdded;
		protected $calChanged;
		protected $lblOptlock;

		// Controls that allow the viewing of Session's individual data fields
		protected $lblId;
		protected $lblUserId;
		protected $lblIpAddress;
		protected $lblAutoLogin;
		protected $lblAdded;
		protected $lblChanged;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References
		protected $lstSessionKeyAsSession;

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References
		protected $lblSessionKeyAsSession;


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * SessionMetaControl to edit a single Session object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Session object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionMetaControl
		 * @param Session $objSession new or existing Session object
		 */
		 public function __construct($objParentObject, Session $objSession) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this SessionMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Session object
			$this->objSession = $objSession;

			// Figure out if we're Editing or Creating New
			if ($this->objSession->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Session object creation - defaults to CreateOrEdit
 		 * @return SessionMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objSession = Session::Load($strId);

				// Session was found -- return it!
				if ($objSession)
					return new SessionMetaControl($objParentObject, $objSession);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Session object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new SessionMetaControl($objParentObject, new Session());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Session object creation - defaults to CreateOrEdit
		 * @return SessionMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return SessionMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this SessionMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Session object creation - defaults to CreateOrEdit
		 * @return SessionMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return SessionMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objSession->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Session::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objSession->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstUser
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstUser_Create($strControlId = null) {
			$this->lstUser = new QListBox($this->objParentObject, $strControlId);
			$this->lstUser->Name = QApplication::Translate('User');
			$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
			$objUserArray = User::LoadAll();
			if ($objUserArray) foreach ($objUserArray as $objUser) {
				$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
				if (($this->objSession->User) && ($this->objSession->User->Id == $objUser->Id))
					$objListItem->Selected = true;
				$this->lstUser->AddItem($objListItem);
			}
			return $this->lstUser;
		}

		/**
		 * Create and setup QLabel lblUserId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblUserId_Create($strControlId = null) {
			$this->lblUserId = new QLabel($this->objParentObject, $strControlId);
			$this->lblUserId->Name = QApplication::Translate('User');
			$this->lblUserId->Text = ($this->objSession->User) ? $this->objSession->User->__toString() : null;
			return $this->lblUserId;
		}

		/**
		 * Create and setup QTextBox txtIpAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtIpAddress_Create($strControlId = null) {
			$this->txtIpAddress = new QTextBox($this->objParentObject, $strControlId);
			$this->txtIpAddress->Name = QApplication::Translate('Ip Address');
			$this->txtIpAddress->Text = $this->objSession->IpAddress;
			$this->txtIpAddress->Required = true;
			$this->txtIpAddress->MaxLength = Session::IpAddressMaxLength;
			return $this->txtIpAddress;
		}

		/**
		 * Create and setup QLabel lblIpAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblIpAddress_Create($strControlId = null) {
			$this->lblIpAddress = new QLabel($this->objParentObject, $strControlId);
			$this->lblIpAddress->Name = QApplication::Translate('Ip Address');
			$this->lblIpAddress->Text = $this->objSession->IpAddress;
			$this->lblIpAddress->Required = true;
			return $this->lblIpAddress;
		}

		/**
		 * Create and setup QCheckBox chkAutoLogin
		 * @param string $strControlId optional ControlId to use
		 * @return QCheckBox
		 */
		public function chkAutoLogin_Create($strControlId = null) {
			$this->chkAutoLogin = new QCheckBox($this->objParentObject, $strControlId);
			$this->chkAutoLogin->Name = QApplication::Translate('Auto Login');
			$this->chkAutoLogin->Checked = $this->objSession->AutoLogin;
			return $this->chkAutoLogin;
		}

		/**
		 * Create and setup QLabel lblAutoLogin
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAutoLogin_Create($strControlId = null) {
			$this->lblAutoLogin = new QLabel($this->objParentObject, $strControlId);
			$this->lblAutoLogin->Name = QApplication::Translate('Auto Login');
			$this->lblAutoLogin->Text = ($this->objSession->AutoLogin) ? QApplication::Translate('Yes') : QApplication::Translate('No');
			return $this->lblAutoLogin;
		}

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objSession->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objSession->Added) ? $this->objSession->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calChanged
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calChanged_Create($strControlId = null) {
			$this->calChanged = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calChanged->Name = QApplication::Translate('Changed');
			$this->calChanged->DateTime = $this->objSession->Changed;
			$this->calChanged->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calChanged->Required = true;
			return $this->calChanged;
		}

		/**
		 * Create and setup QLabel lblChanged
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblChanged_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblChanged = new QLabel($this->objParentObject, $strControlId);
			$this->lblChanged->Name = QApplication::Translate('Changed');
			$this->strChangedDateTimeFormat = $strDateTimeFormat;
			$this->lblChanged->Text = sprintf($this->objSession->Changed) ? $this->objSession->__toString($this->strChangedDateTimeFormat) : null;
			$this->lblChanged->Required = true;
			return $this->lblChanged;
		}

		protected $strChangedDateTimeFormat;

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objSession->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QListBox lstSessionKeyAsSession
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstSessionKeyAsSession_Create($strControlId = null) {
			$this->lstSessionKeyAsSession = new QListBox($this->objParentObject, $strControlId);
			$this->lstSessionKeyAsSession->Name = QApplication::Translate('Session Key As Session');
			$this->lstSessionKeyAsSession->AddItem(QApplication::Translate('- Select One -'), null);
			$objSessionKeyArray = SessionKey::LoadAll();
			if ($objSessionKeyArray) foreach ($objSessionKeyArray as $objSessionKey) {
				$objListItem = new QListItem($objSessionKey->__toString(), $objSessionKey->Id);
				if ($objSessionKey->SessionId == $this->objSession->Id)
					$objListItem->Selected = true;
				$this->lstSessionKeyAsSession->AddItem($objListItem);
			}
			// Because SessionKey's SessionKeyAsSession is not null, if a value is already selected, it cannot be changed.
			if ($this->lstSessionKeyAsSession->SelectedValue)
				$this->lstSessionKeyAsSession->Enabled = false;
			return $this->lstSessionKeyAsSession;
		}

		/**
		 * Create and setup QLabel lblSessionKeyAsSession
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblSessionKeyAsSession_Create($strControlId = null) {
			$this->lblSessionKeyAsSession = new QLabel($this->objParentObject, $strControlId);
			$this->lblSessionKeyAsSession->Name = QApplication::Translate('Session Key As Session');
			$this->lblSessionKeyAsSession->Text = ($this->objSession->SessionKeyAsSession) ? $this->objSession->SessionKeyAsSession->__toString() : null;
			return $this->lblSessionKeyAsSession;
		}



		/**
		 * Refresh this MetaControl with Data from the local Session object.
		 * @param boolean $blnReload reload Session from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objSession->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objSession->Id;
			if ($this->lblId) $this->lblId->Text = $this->objSession->Id;

			if ($this->lstUser) {
					$this->lstUser->RemoveAllItems();
				$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
				$objUserArray = User::LoadAll();
				if ($objUserArray) foreach ($objUserArray as $objUser) {
					$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
					if (($this->objSession->User) && ($this->objSession->User->Id == $objUser->Id))
						$objListItem->Selected = true;
					$this->lstUser->AddItem($objListItem);
				}
			}
			if ($this->lblUserId) $this->lblUserId->Text = ($this->objSession->User) ? $this->objSession->User->__toString() : null;

			if ($this->txtIpAddress) $this->txtIpAddress->Text = $this->objSession->IpAddress;
			if ($this->lblIpAddress) $this->lblIpAddress->Text = $this->objSession->IpAddress;

			if ($this->chkAutoLogin) $this->chkAutoLogin->Checked = $this->objSession->AutoLogin;
			if ($this->lblAutoLogin) $this->lblAutoLogin->Text = ($this->objSession->AutoLogin) ? QApplication::Translate('Yes') : QApplication::Translate('No');

			if ($this->calAdded) $this->calAdded->DateTime = $this->objSession->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objSession->Added) ? $this->objSession->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->calChanged) $this->calChanged->DateTime = $this->objSession->Changed;
			if ($this->lblChanged) $this->lblChanged->Text = sprintf($this->objSession->Changed) ? $this->objSession->__toString($this->strChangedDateTimeFormat) : null;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objSession->Optlock;

			if ($this->lstSessionKeyAsSession) {
				$this->lstSessionKeyAsSession->RemoveAllItems();
				$this->lstSessionKeyAsSession->AddItem(QApplication::Translate('- Select One -'), null);
				$objSessionKeyArray = SessionKey::LoadAll();
				if ($objSessionKeyArray) foreach ($objSessionKeyArray as $objSessionKey) {
					$objListItem = new QListItem($objSessionKey->__toString(), $objSessionKey->Id);
					if ($objSessionKey->SessionId == $this->objSession->Id)
						$objListItem->Selected = true;
					$this->lstSessionKeyAsSession->AddItem($objListItem);
				}
				// Because SessionKey's SessionKeyAsSession is not null, if a value is already selected, it cannot be changed.
				if ($this->lstSessionKeyAsSession->SelectedValue)
					$this->lstSessionKeyAsSession->Enabled = false;
				else
					$this->lstSessionKeyAsSession->Enabled = true;
			}
			if ($this->lblSessionKeyAsSession) $this->lblSessionKeyAsSession->Text = ($this->objSession->SessionKeyAsSession) ? $this->objSession->SessionKeyAsSession->__toString() : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC SESSION OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Session instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveSession() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objSession->Id = $this->txtId->Text;
				if ($this->lstUser) $this->objSession->UserId = $this->lstUser->SelectedValue;
				if ($this->txtIpAddress) $this->objSession->IpAddress = $this->txtIpAddress->Text;
				if ($this->chkAutoLogin) $this->objSession->AutoLogin = $this->chkAutoLogin->Checked;
				if ($this->calAdded) $this->objSession->Added = $this->calAdded->DateTime;
				if ($this->calChanged) $this->objSession->Changed = $this->calChanged->DateTime;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it
				if ($this->lstSessionKeyAsSession) $this->objSession->SessionKeyAsSession = SessionKey::Load($this->lstSessionKeyAsSession->SelectedValue);

				// Save the Session object
				$this->objSession->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Session instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteSession() {
			$this->objSession->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Session': return $this->objSession;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Session fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'UserIdControl':
					if (!$this->lstUser) return $this->lstUser_Create();
					return $this->lstUser;
				case 'UserIdLabel':
					if (!$this->lblUserId) return $this->lblUserId_Create();
					return $this->lblUserId;
				case 'IpAddressControl':
					if (!$this->txtIpAddress) return $this->txtIpAddress_Create();
					return $this->txtIpAddress;
				case 'IpAddressLabel':
					if (!$this->lblIpAddress) return $this->lblIpAddress_Create();
					return $this->lblIpAddress;
				case 'AutoLoginControl':
					if (!$this->chkAutoLogin) return $this->chkAutoLogin_Create();
					return $this->chkAutoLogin;
				case 'AutoLoginLabel':
					if (!$this->lblAutoLogin) return $this->lblAutoLogin_Create();
					return $this->lblAutoLogin;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'ChangedControl':
					if (!$this->calChanged) return $this->calChanged_Create();
					return $this->calChanged;
				case 'ChangedLabel':
					if (!$this->lblChanged) return $this->lblChanged_Create();
					return $this->lblChanged;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'SessionKeyAsSessionControl':
					if (!$this->lstSessionKeyAsSession) return $this->lstSessionKeyAsSession_Create();
					return $this->lstSessionKeyAsSession;
				case 'SessionKeyAsSessionLabel':
					if (!$this->lblSessionKeyAsSession) return $this->lblSessionKeyAsSession_Create();
					return $this->lblSessionKeyAsSession;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Session fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'UserIdControl':
						return ($this->lstUser = QType::Cast($mixValue, 'QControl'));
					case 'IpAddressControl':
						return ($this->txtIpAddress = QType::Cast($mixValue, 'QControl'));
					case 'AutoLoginControl':
						return ($this->chkAutoLogin = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'ChangedControl':
						return ($this->calChanged = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'SessionKeyAsSessionControl':
						return ($this->lstSessionKeyAsSession = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
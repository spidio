<?php
	require(__DATAGEN_CLASSES__ . '/CodecEnumGen.class.php');

	/**
	 * The CodecEnum class defined here contains any
	 * customized code for the CodecEnum enumerated type. 
	 * 
	 * It represents the enumerated values found in the "spidio_codec_enum" table in the database,
	 * and extends from the code generated abstract CodecEnumGen
	 * class, which contains all the values extracted from the database.
	 * 
	 * Type classes which are generally used to attach a type to data object.
	 * However, they may be used as simple database indepedant enumerated type.
	 * 
	 * @package Spidio
	 * @subpackage DataObjects
	 */
	abstract class CodecEnum extends CodecEnumGen {
		public static function ToString($intCodecEnumId) {
			switch ($intCodecEnumId) {
				case 1: return QApplication::Translate('Video');
				case 2: return QApplication::Translate('Audio');
				case 3: return QApplication::Translate('Other');
				default:
					return parent::ToString($intCodecEnumId);
			}
		}
	}
?>
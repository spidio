<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the Recording class.  It uses the code-generated
	 * RecordingMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a Recording columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both recording_edit.php AND
	 * recording_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class RecordingEditPanel extends QPanel {
		// Local instance of the RecordingMetaControl
		protected $mctRecording;

		// Controls for Recording's Data Fields
		public $txtId;
		public $lstProject;
		public $lstTape;
		public $lstCamera;
		public $lstCameraPurpose;
		public $lstTapeModeEnum;
		public $lstRecordingType;
		public $txtStart;
		public $txtEnd;
		public $chkWidescreen;
		public $calDate;
		public $txtScene;
		public $txtTake;
		public $txtAngle;
		public $calAdded;
		public $calChanged;
		public $txtComment;
		public $lblOptlock;
		public $txtOwner;
		public $txtGroup;
		public $txtPerms;
		public $lstStatusObject;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'RecordingEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the RecordingMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctRecording = RecordingMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on Recording's data fields
			$this->txtId = $this->mctRecording->txtId_Create();
			$this->lstProject = $this->mctRecording->lstProject_Create();
			$this->lstTape = $this->mctRecording->lstTape_Create();
			$this->lstCamera = $this->mctRecording->lstCamera_Create();
			$this->lstCameraPurpose = $this->mctRecording->lstCameraPurpose_Create();
			$this->lstTapeModeEnum = $this->mctRecording->lstTapeModeEnum_Create();
			$this->lstRecordingType = $this->mctRecording->lstRecordingType_Create();
			$this->txtStart = $this->mctRecording->txtStart_Create();
			$this->txtEnd = $this->mctRecording->txtEnd_Create();
			$this->chkWidescreen = $this->mctRecording->chkWidescreen_Create();
			$this->calDate = $this->mctRecording->calDate_Create();
			$this->txtScene = $this->mctRecording->txtScene_Create();
			$this->txtTake = $this->mctRecording->txtTake_Create();
			$this->txtAngle = $this->mctRecording->txtAngle_Create();
			$this->calAdded = $this->mctRecording->calAdded_Create();
			$this->calChanged = $this->mctRecording->calChanged_Create();
			$this->txtComment = $this->mctRecording->txtComment_Create();
			$this->lblOptlock = $this->mctRecording->lblOptlock_Create();
			$this->txtOwner = $this->mctRecording->txtOwner_Create();
			$this->txtGroup = $this->mctRecording->txtGroup_Create();
			$this->txtPerms = $this->mctRecording->txtPerms_Create();
			$this->lstStatusObject = $this->mctRecording->lstStatusObject_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('Recording') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctRecording->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the RecordingMetaControl
			$this->mctRecording->SaveRecording();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the RecordingMetaControl
			$this->mctRecording->DeleteRecording();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
<?php
	/**
	 * This is the abstract Panel class for the List All functionality
	 * of the TapeLocation class.  This code-generated class
	 * contains a datagrid to display an HTML page that can
	 * list a collection of TapeLocation objects.  It includes
	 * functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QPanel which extends this TapeLocationListPanelBase
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent re-
	 * code generation.
	 * 
	 * @package Spidio
	 * @subpackage Drafts
	 * 
	 */
	class TapeLocationListPanel extends QPanel {
		// Local instance of the Meta DataGrid to list TapeLocations
		public $dtgTapeLocations;

		// Other public QControls in this panel
		public $btnCreateNew;
		public $pxyEdit;

		// Callback Method Names
		protected $strSetEditPanelMethod;
		protected $strCloseEditPanelMethod;
		
		public function __construct($objParentObject, $strSetEditPanelMethod, $strCloseEditPanelMethod, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Record Method Callbacks
			$this->strSetEditPanelMethod = $strSetEditPanelMethod;
			$this->strCloseEditPanelMethod = $strCloseEditPanelMethod;

			// Setup the Template
			$this->Template = 'TapeLocationListPanel.tpl.php';

			// Instantiate the Meta DataGrid
			$this->dtgTapeLocations = new TapeLocationDataGrid($this);

			// Style the DataGrid (if desired)
			$this->dtgTapeLocations->CssClass = 'datagrid';
			$this->dtgTapeLocations->AlternateRowStyle->CssClass = 'alternate';

			// Add Pagination (if desired)
			$this->dtgTapeLocations->Paginator = new QPaginator($this->dtgTapeLocations);
			$this->dtgTapeLocations->ItemsPerPage = 8;

			// Use the MetaDataGrid functionality to add Columns for this datagrid

			// Create an Edit Column
			$this->pxyEdit = new QControlProxy($this);
			$this->pxyEdit->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'pxyEdit_Click'));
			$this->dtgTapeLocations->MetaAddEditProxyColumn($this->pxyEdit, 'Edit', 'Edit');

			// Create the Other Columns (note that you can use strings for ' . DB_TABLE_PREFIX_1 . 'tape_location's properties, or you
			// can traverse down QQN::' . DB_TABLE_PREFIX_1 . 'tape_location() to display fields that are down the hierarchy)
			$this->dtgTapeLocations->MetaAddColumn('Id');
			$this->dtgTapeLocations->MetaAddColumn(QQN::TapeLocation()->Tape);
			$this->dtgTapeLocations->MetaAddColumn(QQN::TapeLocation()->Location);
			$this->dtgTapeLocations->MetaAddColumn(QQN::TapeLocation()->Person);
			$this->dtgTapeLocations->MetaAddColumn('Date');
			$this->dtgTapeLocations->MetaAddColumn('Comment');
			$this->dtgTapeLocations->MetaAddColumn('Optlock');
			$this->dtgTapeLocations->MetaAddColumn('Owner');
			$this->dtgTapeLocations->MetaAddColumn('Group');
			$this->dtgTapeLocations->MetaAddColumn('Perms');
			$this->dtgTapeLocations->MetaAddTypeColumn('Status', 'AuthStatusEnum');

			// Setup the Create New button
			$this->btnCreateNew = new QButton($this);
			$this->btnCreateNew->Text = QApplication::Translate('Create a New') . ' ' . QApplication::Translate('TapeLocation');
			$this->btnCreateNew->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCreateNew_Click'));
		}

		public function pxyEdit_Click($strFormId, $strControlId, $strParameter) {
			$strParameterArray = explode(',', $strParameter);
			$objEditPanel = new TapeLocationEditPanel($this, $this->strCloseEditPanelMethod, $strParameterArray[0]);

			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}

		public function btnCreateNew_Click($strFormId, $strControlId, $strParameter) {
			$objEditPanel = new TapeLocationEditPanel($this, $this->strCloseEditPanelMethod, null);
			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}
	}
?>
<?php
	/**
	 * The AuthClassEnum class defined here contains
	 * code for the AuthClassEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_auth_class_enum" table
	 * in the database.
	 * 
	 * To use, you should use the AuthClassEnum subclass which
	 * extends this AuthClassEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthClassEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class AuthClassEnumGen extends QBaseClass {
		const Address = 1;
		const Camera = 2;
		const CameraPurpose = 3;
		const Codec = 4;
		const File = 5;
		const FileType = 6;
		const Library = 7;
		const Location = 8;
		const News = 9;
		const Organisation = 10;
		const Person = 11;
		const Project = 12;
		const ProjectType = 13;
		const Recording = 14;
		const RecordingType = 15;
		const Tape = 16;
		const TapeLocation = 17;
		const User = 18;

		const MaxId = 18;

		public static $NameArray = array(
			1 => 'Address',
			2 => 'Camera',
			3 => 'CameraPurpose',
			4 => 'Codec',
			5 => 'File',
			6 => 'FileType',
			7 => 'Library',
			8 => 'Location',
			9 => 'News',
			10 => 'Organisation',
			11 => 'Person',
			12 => 'Project',
			13 => 'ProjectType',
			14 => 'Recording',
			15 => 'RecordingType',
			16 => 'Tape',
			17 => 'TapeLocation',
			18 => 'User');

		public static $TokenArray = array(
			1 => 'Address',
			2 => 'Camera',
			3 => 'CameraPurpose',
			4 => 'Codec',
			5 => 'File',
			6 => 'FileType',
			7 => 'Library',
			8 => 'Location',
			9 => 'News',
			10 => 'Organisation',
			11 => 'Person',
			12 => 'Project',
			13 => 'ProjectType',
			14 => 'Recording',
			15 => 'RecordingType',
			16 => 'Tape',
			17 => 'TapeLocation',
			18 => 'User');

		public static $ReverseNameArray = array(
			'Address' => 1,
			'Camera' => 2,
			'CameraPurpose' => 3,
			'Codec' => 4,
			'File' => 5,
			'FileType' => 6,
			'Library' => 7,
			'Location' => 8,
			'News' => 9,
			'Organisation' => 10,
			'Person' => 11,
			'Project' => 12,
			'ProjectType' => 13,
			'Recording' => 14,
			'RecordingType' => 15,
			'Tape' => 16,
			'TapeLocation' => 17,
			'User' => 18);

		public static $ReverseTokenArray = array(
			'Address' => 1,
			'Camera' => 2,
			'CameraPurpose' => 3,
			'Codec' => 4,
			'File' => 5,
			'FileType' => 6,
			'Library' => 7,
			'Location' => 8,
			'News' => 9,
			'Organisation' => 10,
			'Person' => 11,
			'Project' => 12,
			'ProjectType' => 13,
			'Recording' => 14,
			'RecordingType' => 15,
			'Tape' => 16,
			'TapeLocation' => 17,
			'User' => 18);

		public static function ToString($intAuthClassEnumId) {
			switch ($intAuthClassEnumId) {
				case 1: return 'Address';
				case 2: return 'Camera';
				case 3: return 'CameraPurpose';
				case 4: return 'Codec';
				case 5: return 'File';
				case 6: return 'FileType';
				case 7: return 'Library';
				case 8: return 'Location';
				case 9: return 'News';
				case 10: return 'Organisation';
				case 11: return 'Person';
				case 12: return 'Project';
				case 13: return 'ProjectType';
				case 14: return 'Recording';
				case 15: return 'RecordingType';
				case 16: return 'Tape';
				case 17: return 'TapeLocation';
				case 18: return 'User';
				default:
					throw new QCallerException(sprintf('Invalid intAuthClassEnumId: %s', $intAuthClassEnumId));
			}
		}

		public static function ToToken($intAuthClassEnumId) {
			switch ($intAuthClassEnumId) {
				case 1: return 'Address';
				case 2: return 'Camera';
				case 3: return 'CameraPurpose';
				case 4: return 'Codec';
				case 5: return 'File';
				case 6: return 'FileType';
				case 7: return 'Library';
				case 8: return 'Location';
				case 9: return 'News';
				case 10: return 'Organisation';
				case 11: return 'Person';
				case 12: return 'Project';
				case 13: return 'ProjectType';
				case 14: return 'Recording';
				case 15: return 'RecordingType';
				case 16: return 'Tape';
				case 17: return 'TapeLocation';
				case 18: return 'User';
				default:
					throw new QCallerException(sprintf('Invalid intAuthClassEnumId: %s', $intAuthClassEnumId));
			}
		}

	}
?>
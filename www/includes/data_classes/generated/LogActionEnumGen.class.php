<?php
	/**
	 * The LogActionEnum class defined here contains
	 * code for the LogActionEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_log_action_enum" table
	 * in the database.
	 * 
	 * To use, you should use the LogActionEnum subclass which
	 * extends this LogActionEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the LogActionEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class LogActionEnumGen extends QBaseClass {
		const NUL = 1;
		const ADD = 2;
		const EDT = 3;
		const DEL = 4;

		const MaxId = 4;

		public static $NameArray = array(
			1 => 'NUL',
			2 => 'ADD',
			3 => 'EDT',
			4 => 'DEL');

		public static $TokenArray = array(
			1 => 'NUL',
			2 => 'ADD',
			3 => 'EDT',
			4 => 'DEL');

		public static $ReverseNameArray = array(
			'NUL' => 1,
			'ADD' => 2,
			'EDT' => 3,
			'DEL' => 4);

		public static $ReverseTokenArray = array(
			'NUL' => 1,
			'ADD' => 2,
			'EDT' => 3,
			'DEL' => 4);

		public static function ToString($intLogActionEnumId) {
			switch ($intLogActionEnumId) {
				case 1: return 'NUL';
				case 2: return 'ADD';
				case 3: return 'EDT';
				case 4: return 'DEL';
				default:
					throw new QCallerException(sprintf('Invalid intLogActionEnumId: %s', $intLogActionEnumId));
			}
		}

		public static function ToToken($intLogActionEnumId) {
			switch ($intLogActionEnumId) {
				case 1: return 'NUL';
				case 2: return 'ADD';
				case 3: return 'EDT';
				case 4: return 'DEL';
				default:
					throw new QCallerException(sprintf('Invalid intLogActionEnumId: %s', $intLogActionEnumId));
			}
		}

	}
?>
<?php

class CoreVersion extends VersionInfo {
	protected function SetVersionInfo() {
		// Software version
		$this->Version = '0D20';
		$this->Revision = '70';
		
		// Software name
		$this->Name = 'Spidio';
		$this->Website = 'https://sourceforge.net/projects/spidio';
		
		// Software copyright
		$this->Author = 'Spidio Project';
		$this->Years = '2007-2008';
	}
}

?>
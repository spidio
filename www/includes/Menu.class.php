<?php

if (!defined('__TEMPLATES__')) {
	trigger_error('Constant __TEMPLATES__ is not defined, please define it in configuration.inc.php.', E_USER_ERROR);
}

if (file_exists(sprintf('%s/%s/template_menu.php', __TEMPLATES__, QApplication::GetConfig('site_template')))) {
	require(sprintf('%s/%s/template_menu.php', __TEMPLATES__, QApplication::GetConfig('site_template')));
}

if (!class_exists('Menu')) {
	class Menu extends MenuBase {
		
	}
}

if (!class_exists('MenuItem')) {
	class MenuItem extends MenuItemBase {
		
	}
}

?>
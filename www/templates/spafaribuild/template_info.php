<?php

class TemplateInfo extends TemplateInfoBase {
	protected function SetVersionInfo() {
		$this->Name = 'Spafaribuild';
		$this->Version = '0D19';
		$this->Copyright = '2007 Spidio Project';
		
		$this->Author = 'Anner van Hardenbroek';
		$this->Email = 'dwlnetnl@users.sourceforge.net';
		$this->Website = 'https://sourceforge.net/projects/spidio';
		
		$this->ConfigArray = array($this->CreateConfiguration('validatoricons', '1'));
	}
}

?>
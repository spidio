<?php
	/**
	 * The abstract UserGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the User subclass which
	 * extends this UserGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the User class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $PersonId the value for strPersonId (Not Null)
	 * @property string $Name the value for strName (Unique)
	 * @property string $Password the value for strPassword (Not Null)
	 * @property boolean $Founder the value for blnFounder (Not Null)
	 * @property integer $GroupMemberships the value for intGroupMemberships (Not Null)
	 * @property string $LanguageCode the value for strLanguageCode (Not Null)
	 * @property string $CountryCode the value for strCountryCode 
	 * @property QDateTime $LastLoggedIn the value for dttLastLoggedIn 
	 * @property QDateTime $Added the value for dttAdded (Not Null)
	 * @property QDateTime $Changed the value for dttChanged (Not Null)
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Person $Person the value for the Person object referenced by strPersonId (Not Null)
	 * @property-read Log $_LogAsUser the value for the private _objLogAsUser (Read-Only) if set due to an expansion on the spidio_log.user_id reverse relationship
	 * @property-read Log[] $_LogAsUserArray the value for the private _objLogAsUserArray (Read-Only) if set due to an ExpandAsArray on the spidio_log.user_id reverse relationship
	 * @property-read News $_NewsAsUser the value for the private _objNewsAsUser (Read-Only) if set due to an expansion on the spidio_news.user_id reverse relationship
	 * @property-read News[] $_NewsAsUserArray the value for the private _objNewsAsUserArray (Read-Only) if set due to an ExpandAsArray on the spidio_news.user_id reverse relationship
	 * @property-read Session $_SessionAsUser the value for the private _objSessionAsUser (Read-Only) if set due to an expansion on the spidio_session.user_id reverse relationship
	 * @property-read Session[] $_SessionAsUserArray the value for the private _objSessionAsUserArray (Read-Only) if set due to an ExpandAsArray on the spidio_session.user_id reverse relationship
	 * @property-read SessionKey $_SessionKeyAsUser the value for the private _objSessionKeyAsUser (Read-Only) if set due to an expansion on the spidio_session_key.user_id reverse relationship
	 * @property-read SessionKey[] $_SessionKeyAsUserArray the value for the private _objSessionKeyAsUserArray (Read-Only) if set due to an ExpandAsArray on the spidio_session_key.user_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class UserGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_user.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_user.person_id
		 * @var string strPersonId
		 */
		protected $strPersonId;
		const PersonIdMaxLength = 36;
		const PersonIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.password
		 * @var string strPassword
		 */
		protected $strPassword;
		const PasswordMaxLength = 40;
		const PasswordDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.founder
		 * @var boolean blnFounder
		 */
		protected $blnFounder;
		const FounderDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.group_memberships
		 * @var integer intGroupMemberships
		 */
		protected $intGroupMemberships;
		const GroupMembershipsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.language_code
		 * @var string strLanguageCode
		 */
		protected $strLanguageCode;
		const LanguageCodeMaxLength = 2;
		const LanguageCodeDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.country_code
		 * @var string strCountryCode
		 */
		protected $strCountryCode;
		const CountryCodeMaxLength = 2;
		const CountryCodeDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.last_logged_in
		 * @var QDateTime dttLastLoggedIn
		 */
		protected $dttLastLoggedIn;
		const LastLoggedInDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.added
		 * @var QDateTime dttAdded
		 */
		protected $dttAdded;
		const AddedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.changed
		 * @var QDateTime dttChanged
		 */
		protected $dttChanged;
		const ChangedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_user.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single LogAsUser object
		 * (of type Log), if this User object was restored with
		 * an expansion on the spidio_log association table.
		 * @var Log _objLogAsUser;
		 */
		private $_objLogAsUser;

		/**
		 * Private member variable that stores a reference to an array of LogAsUser objects
		 * (of type Log[]), if this User object was restored with
		 * an ExpandAsArray on the spidio_log association table.
		 * @var Log[] _objLogAsUserArray;
		 */
		private $_objLogAsUserArray = array();

		/**
		 * Private member variable that stores a reference to a single NewsAsUser object
		 * (of type News), if this User object was restored with
		 * an expansion on the spidio_news association table.
		 * @var News _objNewsAsUser;
		 */
		private $_objNewsAsUser;

		/**
		 * Private member variable that stores a reference to an array of NewsAsUser objects
		 * (of type News[]), if this User object was restored with
		 * an ExpandAsArray on the spidio_news association table.
		 * @var News[] _objNewsAsUserArray;
		 */
		private $_objNewsAsUserArray = array();

		/**
		 * Private member variable that stores a reference to a single SessionAsUser object
		 * (of type Session), if this User object was restored with
		 * an expansion on the spidio_session association table.
		 * @var Session _objSessionAsUser;
		 */
		private $_objSessionAsUser;

		/**
		 * Private member variable that stores a reference to an array of SessionAsUser objects
		 * (of type Session[]), if this User object was restored with
		 * an ExpandAsArray on the spidio_session association table.
		 * @var Session[] _objSessionAsUserArray;
		 */
		private $_objSessionAsUserArray = array();

		/**
		 * Private member variable that stores a reference to a single SessionKeyAsUser object
		 * (of type SessionKey), if this User object was restored with
		 * an expansion on the spidio_session_key association table.
		 * @var SessionKey _objSessionKeyAsUser;
		 */
		private $_objSessionKeyAsUser;

		/**
		 * Private member variable that stores a reference to an array of SessionKeyAsUser objects
		 * (of type SessionKey[]), if this User object was restored with
		 * an ExpandAsArray on the spidio_session_key association table.
		 * @var SessionKey[] _objSessionKeyAsUserArray;
		 */
		private $_objSessionKeyAsUserArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_user.person_id.
		 *
		 * NOTE: Always use the Person property getter to correctly retrieve this Person object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Person objPerson
		 */
		protected $objPerson;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a User from PK Info
		 * @param string $strId
		 * @return User
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return User::QuerySingle(
				QQ::Equal(QQN::User()->Id, $strId)
			);
		}

		/**
		 * Load all Users
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return User[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call User::QueryArray to perform the LoadAll query
			try {
				return User::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Users
		 * @return int
		 */
		public static function CountAll() {
			// Call User::QueryCount to perform the CountAll query
			return User::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Create/Build out the QueryBuilder object with User-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'user');
			User::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'user');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single User object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return User the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = User::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new User object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return User::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of User objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return User[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = User::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return User::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of User objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = User::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'user_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with User-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				User::GetSelectFields($objQueryBuilder);
				User::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return User::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this User
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'user';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'person_id', $strAliasPrefix . 'person_id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'password', $strAliasPrefix . 'password');
			$objBuilder->AddSelectItem($strTableName, 'founder', $strAliasPrefix . 'founder');
			$objBuilder->AddSelectItem($strTableName, 'group_memberships', $strAliasPrefix . 'group_memberships');
			$objBuilder->AddSelectItem($strTableName, 'language_code', $strAliasPrefix . 'language_code');
			$objBuilder->AddSelectItem($strTableName, 'country_code', $strAliasPrefix . 'country_code');
			$objBuilder->AddSelectItem($strTableName, 'last_logged_in', $strAliasPrefix . 'last_logged_in');
			$objBuilder->AddSelectItem($strTableName, 'added', $strAliasPrefix . 'added');
			$objBuilder->AddSelectItem($strTableName, 'changed', $strAliasPrefix . 'changed');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a User from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this User::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return User
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'user__';


				$strAlias = $strAliasPrefix . 'logasuser__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objLogAsUserArray)) {
						$objPreviousChildItem = $objPreviousItem->_objLogAsUserArray[$intPreviousChildItemCount - 1];
						$objChildItem = Log::InstantiateDbRow($objDbRow, $strAliasPrefix . 'logasuser__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objLogAsUserArray[] = $objChildItem;
					} else
						$objPreviousItem->_objLogAsUserArray[] = Log::InstantiateDbRow($objDbRow, $strAliasPrefix . 'logasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'newsasuser__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objNewsAsUserArray)) {
						$objPreviousChildItem = $objPreviousItem->_objNewsAsUserArray[$intPreviousChildItemCount - 1];
						$objChildItem = News::InstantiateDbRow($objDbRow, $strAliasPrefix . 'newsasuser__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objNewsAsUserArray[] = $objChildItem;
					} else
						$objPreviousItem->_objNewsAsUserArray[] = News::InstantiateDbRow($objDbRow, $strAliasPrefix . 'newsasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'sessionasuser__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objSessionAsUserArray)) {
						$objPreviousChildItem = $objPreviousItem->_objSessionAsUserArray[$intPreviousChildItemCount - 1];
						$objChildItem = Session::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionasuser__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objSessionAsUserArray[] = $objChildItem;
					} else
						$objPreviousItem->_objSessionAsUserArray[] = Session::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'sessionkeyasuser__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objSessionKeyAsUserArray)) {
						$objPreviousChildItem = $objPreviousItem->_objSessionKeyAsUserArray[$intPreviousChildItemCount - 1];
						$objChildItem = SessionKey::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionkeyasuser__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objSessionKeyAsUserArray[] = $objChildItem;
					} else
						$objPreviousItem->_objSessionKeyAsUserArray[] = SessionKey::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionkeyasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'user__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the User object
			$objToReturn = new User();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'person_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'person_id'] : $strAliasPrefix . 'person_id';
			$objToReturn->strPersonId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'password', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'password'] : $strAliasPrefix . 'password';
			$objToReturn->strPassword = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'founder', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'founder'] : $strAliasPrefix . 'founder';
			$objToReturn->blnFounder = $objDbRow->GetColumn($strAliasName, 'Bit');
			$strAliasName = array_key_exists($strAliasPrefix . 'group_memberships', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group_memberships'] : $strAliasPrefix . 'group_memberships';
			$objToReturn->intGroupMemberships = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'language_code', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'language_code'] : $strAliasPrefix . 'language_code';
			$objToReturn->strLanguageCode = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'country_code', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'country_code'] : $strAliasPrefix . 'country_code';
			$objToReturn->strCountryCode = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'last_logged_in', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'last_logged_in'] : $strAliasPrefix . 'last_logged_in';
			$objToReturn->dttLastLoggedIn = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'added', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'added'] : $strAliasPrefix . 'added';
			$objToReturn->dttAdded = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'changed', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'changed'] : $strAliasPrefix . 'changed';
			$objToReturn->dttChanged = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'user__';

			// Check for Person Early Binding
			$strAlias = $strAliasPrefix . 'person_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objPerson = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'person_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			// Check for LogAsUser Virtual Binding
			$strAlias = $strAliasPrefix . 'logasuser__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objLogAsUserArray[] = Log::InstantiateDbRow($objDbRow, $strAliasPrefix . 'logasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objLogAsUser = Log::InstantiateDbRow($objDbRow, $strAliasPrefix . 'logasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for NewsAsUser Virtual Binding
			$strAlias = $strAliasPrefix . 'newsasuser__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objNewsAsUserArray[] = News::InstantiateDbRow($objDbRow, $strAliasPrefix . 'newsasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objNewsAsUser = News::InstantiateDbRow($objDbRow, $strAliasPrefix . 'newsasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for SessionAsUser Virtual Binding
			$strAlias = $strAliasPrefix . 'sessionasuser__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objSessionAsUserArray[] = Session::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objSessionAsUser = Session::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for SessionKeyAsUser Virtual Binding
			$strAlias = $strAliasPrefix . 'sessionkeyasuser__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objSessionKeyAsUserArray[] = SessionKey::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionkeyasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objSessionKeyAsUser = SessionKey::InstantiateDbRow($objDbRow, $strAliasPrefix . 'sessionkeyasuser__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of Users from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return User[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = User::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = User::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single User object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return User
		*/
		public static function LoadById($strId) {
			return User::QuerySingle(
				QQ::Equal(QQN::User()->Id, $strId)
			);
		}
			
		/**
		 * Load a single User object,
		 * by Name Index(es)
		 * @param string $strName
		 * @return User
		*/
		public static function LoadByName($strName) {
			return User::QuerySingle(
				QQ::Equal(QQN::User()->Name, $strName)
			);
		}
			
		/**
		 * Load an array of User objects,
		 * by PersonId Index(es)
		 * @param string $strPersonId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return User[]
		*/
		public static function LoadArrayByPersonId($strPersonId, $objOptionalClauses = null) {
			// Call User::QueryArray to perform the LoadArrayByPersonId query
			try {
				return User::QueryArray(
					QQ::Equal(QQN::User()->PersonId, $strPersonId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Users
		 * by PersonId Index(es)
		 * @param string $strPersonId
		 * @return int
		*/
		public static function CountByPersonId($strPersonId) {
			// Call User::QueryCount to perform the CountByPersonId query
			return User::QueryCount(
				QQ::Equal(QQN::User()->PersonId, $strPersonId)
			);
		}
			
		/**
		 * Load an array of User objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return User[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call User::QueryArray to perform the LoadArrayByOwner query
			try {
				return User::QueryArray(
					QQ::Equal(QQN::User()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Users
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call User::QueryCount to perform the CountByOwner query
			return User::QueryCount(
				QQ::Equal(QQN::User()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of User objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return User[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call User::QueryArray to perform the LoadArrayByStatus query
			try {
				return User::QueryArray(
					QQ::Equal(QQN::User()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Users
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call User::QueryCount to perform the CountByStatus query
			return User::QueryCount(
				QQ::Equal(QQN::User()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this User
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'user` (
							`id`,
							`person_id`,
							`name`,
							`password`,
							`founder`,
							`group_memberships`,
							`language_code`,
							`country_code`,
							`last_logged_in`,
							`added`,
							`changed`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strPersonId) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->strPassword) . ',
							' . $objDatabase->SqlVariable($this->blnFounder) . ',
							' . $objDatabase->SqlVariable($this->intGroupMemberships) . ',
							' . $objDatabase->SqlVariable($this->strLanguageCode) . ',
							' . $objDatabase->SqlVariable($this->strCountryCode) . ',
							' . $objDatabase->SqlVariable($this->dttLastLoggedIn) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'user`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('User');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'user`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`person_id` = ' . $objDatabase->SqlVariable($this->strPersonId) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`password` = ' . $objDatabase->SqlVariable($this->strPassword) . ',
							`founder` = ' . $objDatabase->SqlVariable($this->blnFounder) . ',
							`group_memberships` = ' . $objDatabase->SqlVariable($this->intGroupMemberships) . ',
							`language_code` = ' . $objDatabase->SqlVariable($this->strLanguageCode) . ',
							`country_code` = ' . $objDatabase->SqlVariable($this->strCountryCode) . ',
							`last_logged_in` = ' . $objDatabase->SqlVariable($this->dttLastLoggedIn) . ',
							`added` = ' . $objDatabase->SqlVariable($this->dttAdded) . ',
							`changed` = ' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'user`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this User
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this User with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'user`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Users
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'user`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'user table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'user`');
		}

		/**
		 * Reload this User from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved User object.');

			// Reload the Object
			$objReloaded = User::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->PersonId = $objReloaded->PersonId;
			$this->strName = $objReloaded->strName;
			$this->strPassword = $objReloaded->strPassword;
			$this->blnFounder = $objReloaded->blnFounder;
			$this->intGroupMemberships = $objReloaded->intGroupMemberships;
			$this->strLanguageCode = $objReloaded->strLanguageCode;
			$this->strCountryCode = $objReloaded->strCountryCode;
			$this->dttLastLoggedIn = $objReloaded->dttLastLoggedIn;
			$this->dttAdded = $objReloaded->dttAdded;
			$this->dttChanged = $objReloaded->dttChanged;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'PersonId':
					/**
					 * Gets the value for strPersonId (Not Null)
					 * @return string
					 */
					return $this->strPersonId;

				case 'Name':
					/**
					 * Gets the value for strName (Unique)
					 * @return string
					 */
					return $this->strName;

				case 'Password':
					/**
					 * Gets the value for strPassword (Not Null)
					 * @return string
					 */
					return $this->strPassword;

				case 'Founder':
					/**
					 * Gets the value for blnFounder (Not Null)
					 * @return boolean
					 */
					return $this->blnFounder;

				case 'GroupMemberships':
					/**
					 * Gets the value for intGroupMemberships (Not Null)
					 * @return integer
					 */
					return $this->intGroupMemberships;

				case 'LanguageCode':
					/**
					 * Gets the value for strLanguageCode (Not Null)
					 * @return string
					 */
					return $this->strLanguageCode;

				case 'CountryCode':
					/**
					 * Gets the value for strCountryCode 
					 * @return string
					 */
					return $this->strCountryCode;

				case 'LastLoggedIn':
					/**
					 * Gets the value for dttLastLoggedIn 
					 * @return QDateTime
					 */
					return $this->dttLastLoggedIn;

				case 'Added':
					/**
					 * Gets the value for dttAdded (Not Null)
					 * @return QDateTime
					 */
					return $this->dttAdded;

				case 'Changed':
					/**
					 * Gets the value for dttChanged (Not Null)
					 * @return QDateTime
					 */
					return $this->dttChanged;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Person':
					/**
					 * Gets the value for the Person object referenced by strPersonId (Not Null)
					 * @return Person
					 */
					try {
						if ((!$this->objPerson) && (!is_null($this->strPersonId)))
							$this->objPerson = Person::Load($this->strPersonId);
						return $this->objPerson;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_LogAsUser':
					/**
					 * Gets the value for the private _objLogAsUser (Read-Only)
					 * if set due to an expansion on the spidio_log.user_id reverse relationship
					 * @return Log
					 */
					return $this->_objLogAsUser;

				case '_LogAsUserArray':
					/**
					 * Gets the value for the private _objLogAsUserArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_log.user_id reverse relationship
					 * @return Log[]
					 */
					return (array) $this->_objLogAsUserArray;

				case '_NewsAsUser':
					/**
					 * Gets the value for the private _objNewsAsUser (Read-Only)
					 * if set due to an expansion on the spidio_news.user_id reverse relationship
					 * @return News
					 */
					return $this->_objNewsAsUser;

				case '_NewsAsUserArray':
					/**
					 * Gets the value for the private _objNewsAsUserArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_news.user_id reverse relationship
					 * @return News[]
					 */
					return (array) $this->_objNewsAsUserArray;

				case '_SessionAsUser':
					/**
					 * Gets the value for the private _objSessionAsUser (Read-Only)
					 * if set due to an expansion on the spidio_session.user_id reverse relationship
					 * @return Session
					 */
					return $this->_objSessionAsUser;

				case '_SessionAsUserArray':
					/**
					 * Gets the value for the private _objSessionAsUserArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_session.user_id reverse relationship
					 * @return Session[]
					 */
					return (array) $this->_objSessionAsUserArray;

				case '_SessionKeyAsUser':
					/**
					 * Gets the value for the private _objSessionKeyAsUser (Read-Only)
					 * if set due to an expansion on the spidio_session_key.user_id reverse relationship
					 * @return SessionKey
					 */
					return $this->_objSessionKeyAsUser;

				case '_SessionKeyAsUserArray':
					/**
					 * Gets the value for the private _objSessionKeyAsUserArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_session_key.user_id reverse relationship
					 * @return SessionKey[]
					 */
					return (array) $this->_objSessionKeyAsUserArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PersonId':
					/**
					 * Sets the value for strPersonId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objPerson = null;
						return ($this->strPersonId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Unique)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Password':
					/**
					 * Sets the value for strPassword (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strPassword = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Founder':
					/**
					 * Sets the value for blnFounder (Not Null)
					 * @param boolean $mixValue
					 * @return boolean
					 */
					try {
						return ($this->blnFounder = QType::Cast($mixValue, QType::Boolean));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'GroupMemberships':
					/**
					 * Sets the value for intGroupMemberships (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroupMemberships = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'LanguageCode':
					/**
					 * Sets the value for strLanguageCode (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strLanguageCode = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CountryCode':
					/**
					 * Sets the value for strCountryCode 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strCountryCode = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'LastLoggedIn':
					/**
					 * Sets the value for dttLastLoggedIn 
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttLastLoggedIn = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Added':
					/**
					 * Sets the value for dttAdded (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttAdded = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Changed':
					/**
					 * Sets the value for dttChanged (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttChanged = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Person':
					/**
					 * Sets the value for the Person object referenced by strPersonId (Not Null)
					 * @param Person $mixValue
					 * @return Person
					 */
					if (is_null($mixValue)) {
						$this->strPersonId = null;
						$this->objPerson = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Person object
						try {
							$mixValue = QType::Cast($mixValue, 'Person');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Person object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Person for this User');

						// Update Local Member Variables
						$this->objPerson = $mixValue;
						$this->strPersonId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for LogAsUser
		//-------------------------------------------------------------------

		/**
		 * Gets all associated LogsAsUser as an array of Log objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Log[]
		*/ 
		public function GetLogAsUserArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Log::LoadArrayByUserId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated LogsAsUser
		 * @return int
		*/ 
		public function CountLogsAsUser() {
			if ((is_null($this->strId)))
				return 0;

			return Log::CountByUserId($this->strId);
		}

		/**
		 * Associates a LogAsUser
		 * @param Log $objLog
		 * @return void
		*/ 
		public function AssociateLogAsUser(Log $objLog) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateLogAsUser on this unsaved User.');
			if ((is_null($objLog->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateLogAsUser on this User with an unsaved Log.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_log`
				SET
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objLog->Id) . '
			');
		}

		/**
		 * Unassociates a LogAsUser
		 * @param Log $objLog
		 * @return void
		*/ 
		public function UnassociateLogAsUser(Log $objLog) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLogAsUser on this unsaved User.');
			if ((is_null($objLog->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLogAsUser on this User with an unsaved Log.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_log`
				SET
					`user_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objLog->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all LogsAsUser
		 * @return void
		*/ 
		public function UnassociateAllLogsAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLogAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_log`
				SET
					`user_id` = null
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated LogAsUser
		 * @param Log $objLog
		 * @return void
		*/ 
		public function DeleteAssociatedLogAsUser(Log $objLog) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLogAsUser on this unsaved User.');
			if ((is_null($objLog->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLogAsUser on this User with an unsaved Log.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_log`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objLog->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated LogsAsUser
		 * @return void
		*/ 
		public function DeleteAllLogsAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLogAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_log`
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for NewsAsUser
		//-------------------------------------------------------------------

		/**
		 * Gets all associated NewsesAsUser as an array of News objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return News[]
		*/ 
		public function GetNewsAsUserArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return News::LoadArrayByUserId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated NewsesAsUser
		 * @return int
		*/ 
		public function CountNewsesAsUser() {
			if ((is_null($this->strId)))
				return 0;

			return News::CountByUserId($this->strId);
		}

		/**
		 * Associates a NewsAsUser
		 * @param News $objNews
		 * @return void
		*/ 
		public function AssociateNewsAsUser(News $objNews) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateNewsAsUser on this unsaved User.');
			if ((is_null($objNews->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateNewsAsUser on this User with an unsaved News.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_news`
				SET
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objNews->Id) . '
			');
		}

		/**
		 * Unassociates a NewsAsUser
		 * @param News $objNews
		 * @return void
		*/ 
		public function UnassociateNewsAsUser(News $objNews) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateNewsAsUser on this unsaved User.');
			if ((is_null($objNews->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateNewsAsUser on this User with an unsaved News.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_news`
				SET
					`user_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objNews->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all NewsesAsUser
		 * @return void
		*/ 
		public function UnassociateAllNewsesAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateNewsAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_news`
				SET
					`user_id` = null
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated NewsAsUser
		 * @param News $objNews
		 * @return void
		*/ 
		public function DeleteAssociatedNewsAsUser(News $objNews) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateNewsAsUser on this unsaved User.');
			if ((is_null($objNews->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateNewsAsUser on this User with an unsaved News.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_news`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objNews->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated NewsesAsUser
		 * @return void
		*/ 
		public function DeleteAllNewsesAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateNewsAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_news`
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for SessionAsUser
		//-------------------------------------------------------------------

		/**
		 * Gets all associated SessionsAsUser as an array of Session objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Session[]
		*/ 
		public function GetSessionAsUserArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Session::LoadArrayByUserId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated SessionsAsUser
		 * @return int
		*/ 
		public function CountSessionsAsUser() {
			if ((is_null($this->strId)))
				return 0;

			return Session::CountByUserId($this->strId);
		}

		/**
		 * Associates a SessionAsUser
		 * @param Session $objSession
		 * @return void
		*/ 
		public function AssociateSessionAsUser(Session $objSession) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateSessionAsUser on this unsaved User.');
			if ((is_null($objSession->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateSessionAsUser on this User with an unsaved Session.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_session`
				SET
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objSession->Id) . '
			');
		}

		/**
		 * Unassociates a SessionAsUser
		 * @param Session $objSession
		 * @return void
		*/ 
		public function UnassociateSessionAsUser(Session $objSession) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionAsUser on this unsaved User.');
			if ((is_null($objSession->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionAsUser on this User with an unsaved Session.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_session`
				SET
					`user_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objSession->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all SessionsAsUser
		 * @return void
		*/ 
		public function UnassociateAllSessionsAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_session`
				SET
					`user_id` = null
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated SessionAsUser
		 * @param Session $objSession
		 * @return void
		*/ 
		public function DeleteAssociatedSessionAsUser(Session $objSession) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionAsUser on this unsaved User.');
			if ((is_null($objSession->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionAsUser on this User with an unsaved Session.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_session`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objSession->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated SessionsAsUser
		 * @return void
		*/ 
		public function DeleteAllSessionsAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_session`
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for SessionKeyAsUser
		//-------------------------------------------------------------------

		/**
		 * Gets all associated SessionKeiesAsUser as an array of SessionKey objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return SessionKey[]
		*/ 
		public function GetSessionKeyAsUserArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return SessionKey::LoadArrayByUserId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated SessionKeiesAsUser
		 * @return int
		*/ 
		public function CountSessionKeiesAsUser() {
			if ((is_null($this->strId)))
				return 0;

			return SessionKey::CountByUserId($this->strId);
		}

		/**
		 * Associates a SessionKeyAsUser
		 * @param SessionKey $objSessionKey
		 * @return void
		*/ 
		public function AssociateSessionKeyAsUser(SessionKey $objSessionKey) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateSessionKeyAsUser on this unsaved User.');
			if ((is_null($objSessionKey->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateSessionKeyAsUser on this User with an unsaved SessionKey.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_session_key`
				SET
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objSessionKey->Id) . '
			');
		}

		/**
		 * Unassociates a SessionKeyAsUser
		 * @param SessionKey $objSessionKey
		 * @return void
		*/ 
		public function UnassociateSessionKeyAsUser(SessionKey $objSessionKey) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionKeyAsUser on this unsaved User.');
			if ((is_null($objSessionKey->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionKeyAsUser on this User with an unsaved SessionKey.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_session_key`
				SET
					`user_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objSessionKey->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all SessionKeiesAsUser
		 * @return void
		*/ 
		public function UnassociateAllSessionKeiesAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionKeyAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_session_key`
				SET
					`user_id` = null
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated SessionKeyAsUser
		 * @param SessionKey $objSessionKey
		 * @return void
		*/ 
		public function DeleteAssociatedSessionKeyAsUser(SessionKey $objSessionKey) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionKeyAsUser on this unsaved User.');
			if ((is_null($objSessionKey->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionKeyAsUser on this User with an unsaved SessionKey.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_session_key`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objSessionKey->Id) . ' AND
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated SessionKeiesAsUser
		 * @return void
		*/ 
		public function DeleteAllSessionKeiesAsUser() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateSessionKeyAsUser on this unsaved User.');

			// Get the Database Object for this Class
			$objDatabase = User::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_session_key`
				WHERE
					`user_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="User"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Person" type="xsd1:Person"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="Password" type="xsd:string"/>';
			$strToReturn .= '<element name="Founder" type="xsd:boolean"/>';
			$strToReturn .= '<element name="GroupMemberships" type="xsd:int"/>';
			$strToReturn .= '<element name="LanguageCode" type="xsd:string"/>';
			$strToReturn .= '<element name="CountryCode" type="xsd:string"/>';
			$strToReturn .= '<element name="LastLoggedIn" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Added" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Changed" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('User', $strComplexTypeArray)) {
				$strComplexTypeArray['User'] = User::GetSoapComplexTypeXml();
				Person::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, User::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new User();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'Person')) &&
				($objSoapObject->Person))
				$objToReturn->Person = Person::GetObjectFromSoapObject($objSoapObject->Person);
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'Password'))
				$objToReturn->strPassword = $objSoapObject->Password;
			if (property_exists($objSoapObject, 'Founder'))
				$objToReturn->blnFounder = $objSoapObject->Founder;
			if (property_exists($objSoapObject, 'GroupMemberships'))
				$objToReturn->intGroupMemberships = $objSoapObject->GroupMemberships;
			if (property_exists($objSoapObject, 'LanguageCode'))
				$objToReturn->strLanguageCode = $objSoapObject->LanguageCode;
			if (property_exists($objSoapObject, 'CountryCode'))
				$objToReturn->strCountryCode = $objSoapObject->CountryCode;
			if (property_exists($objSoapObject, 'LastLoggedIn'))
				$objToReturn->dttLastLoggedIn = new QDateTime($objSoapObject->LastLoggedIn);
			if (property_exists($objSoapObject, 'Added'))
				$objToReturn->dttAdded = new QDateTime($objSoapObject->Added);
			if (property_exists($objSoapObject, 'Changed'))
				$objToReturn->dttChanged = new QDateTime($objSoapObject->Changed);
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, User::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objPerson)
				$objObject->objPerson = Person::GetSoapObjectFromObject($objObject->objPerson, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strPersonId = null;
			if ($objObject->dttLastLoggedIn)
				$objObject->dttLastLoggedIn = $objObject->dttLastLoggedIn->__toString(QDateTime::FormatSoap);
			if ($objObject->dttAdded)
				$objObject->dttAdded = $objObject->dttAdded->__toString(QDateTime::FormatSoap);
			if ($objObject->dttChanged)
				$objObject->dttChanged = $objObject->dttChanged->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeUser extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'User';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'user';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'PersonId':
					return new QQNode('person_id', 'PersonId', 'string', $this);
				case 'Person':
					return new QQNodePerson('person_id', 'Person', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Password':
					return new QQNode('password', 'Password', 'string', $this);
				case 'Founder':
					return new QQNode('founder', 'Founder', 'boolean', $this);
				case 'GroupMemberships':
					return new QQNode('group_memberships', 'GroupMemberships', 'integer', $this);
				case 'LanguageCode':
					return new QQNode('language_code', 'LanguageCode', 'string', $this);
				case 'CountryCode':
					return new QQNode('country_code', 'CountryCode', 'string', $this);
				case 'LastLoggedIn':
					return new QQNode('last_logged_in', 'LastLoggedIn', 'QDateTime', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'LogAsUser':
					return new QQReverseReferenceNodeLog($this, 'logasuser', 'reverse_reference', 'user_id');
				case 'NewsAsUser':
					return new QQReverseReferenceNodeNews($this, 'newsasuser', 'reverse_reference', 'user_id');
				case 'SessionAsUser':
					return new QQReverseReferenceNodeSession($this, 'sessionasuser', 'reverse_reference', 'user_id');
				case 'SessionKeyAsUser':
					return new QQReverseReferenceNodeSessionKey($this, 'sessionkeyasuser', 'reverse_reference', 'user_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeUser extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'User';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'user';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'PersonId':
					return new QQNode('person_id', 'PersonId', 'string', $this);
				case 'Person':
					return new QQNodePerson('person_id', 'Person', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Password':
					return new QQNode('password', 'Password', 'string', $this);
				case 'Founder':
					return new QQNode('founder', 'Founder', 'boolean', $this);
				case 'GroupMemberships':
					return new QQNode('group_memberships', 'GroupMemberships', 'integer', $this);
				case 'LanguageCode':
					return new QQNode('language_code', 'LanguageCode', 'string', $this);
				case 'CountryCode':
					return new QQNode('country_code', 'CountryCode', 'string', $this);
				case 'LastLoggedIn':
					return new QQNode('last_logged_in', 'LastLoggedIn', 'QDateTime', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'LogAsUser':
					return new QQReverseReferenceNodeLog($this, 'logasuser', 'reverse_reference', 'user_id');
				case 'NewsAsUser':
					return new QQReverseReferenceNodeNews($this, 'newsasuser', 'reverse_reference', 'user_id');
				case 'SessionAsUser':
					return new QQReverseReferenceNodeSession($this, 'sessionasuser', 'reverse_reference', 'user_id');
				case 'SessionKeyAsUser':
					return new QQReverseReferenceNodeSessionKey($this, 'sessionkeyasuser', 'reverse_reference', 'user_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
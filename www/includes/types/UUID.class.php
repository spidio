<?php

class UUID extends QBaseClass {
	protected $strUUID;
	
	public function __construct($strUUID = null, $intVersion = 4) {
		QApplication::LoadExtension('uuid');
		
		if (is_null($strUUID)) {
			if (extension_loaded('uuid')) {
				switch ($intVersion) {
					case 1: $intType = UUID_TYPE_TIME;
					case 4: $intType = UUID_TYPE_RANDOM;
				}
				
				$strUUID = strtolower(uuid_create($intType));
			} else {
				/**
				 * Author: mimec
				 * Website: http://www.php.net/manual/en/function.uniqid.php#69164
				 */
				$strUUID = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
					mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff),
					mt_rand(0, 0x0fff) | 0x4000,
					mt_rand(0, 0x3fff) | 0x8000,
					mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff));
			}
		}
		
		$this->strUUID = $strUUID;
	}
	
	public function __toString() {
		return $this->strUUID;
	}
	
	/**
	 * Compare given UUID with this UUID.
	 *
	 * @param string|UUID $objUUID
	 * @return bool
	 */
	public function Compare($strUUID) {
		if ($strUUID instanceof UUID) {
			$strUUID = $strUUID->__toString();
		}
		
		if (extension_loaded('uuid')) {
			return (uuid_compare($this->strUUID, $strUUID) === 0);
		} else {
			return ($this->strUUID == $strUUID);
		}
	}
	
	// public function CompareLexico($objUUID)
	
	public function __get($strName) {
		switch ($strName) {
			case 'UUID': return $this->strUUID;
				
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	/* SOAP related function */
	public static function GetSoapComplexTypeXml() {
		$strToReturn = '<complexType name="UUID"><sequence>';
		$strToReturn .= '<element name="UUID" type="xsd:string"/>';
		$strToReturn .= '</sequence></complexType>';
		return $strToReturn;
	}

	public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
		if (!array_key_exists('UUID', $strComplexTypeArray)) {
			$strComplexTypeArray['UUID'] = UUID::GetSoapComplexTypeXml();
		}
	}

	public static function GetArrayFromSoapArray($objSoapArray) {
		$objArrayToReturn = array();

		foreach ($objSoapArray as $objSoapObject)
			array_push($objArrayToReturn, UUID::GetObjectFromSoapObject($objSoapObject));

		return $objArrayToReturn;
	}

	public static function GetObjectFromSoapObject($objSoapObject) {
		$objToReturn = new UUID();
		if (property_exists($objSoapObject, 'UUID'))
			$objToReturn->strUUID = $objSoapObject->UUID;
		return $objToReturn;
	}

	public static function GetSoapArrayFromArray($objArray) {
		if (!$objArray)
			return null;

		$objArrayToReturn = array();

		foreach ($objArray as $objObject)
			array_push($objArrayToReturn, UUID::GetSoapObjectFromObject($objObject, true));

		return unserialize(serialize($objArrayToReturn));
	}

	public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
		return $objObject;
	}
}

?>
<?php

interface AuthenticationSoap {
	public function Login($strUserName, $strPasswordSalt, $strSalt);
	public function Logout();
}

?>
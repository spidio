<?php

class SpidioTemplateForm extends TemplateForm {
	protected $objAuth;
	
	protected $strSiteTitle;
	protected $strSiteCopyrightAuthor;
	protected $strSiteCopyrightYears;
	
	protected $strUrlIndex;
	protected $strUrlLogout;
	protected $strUrlProfile;
	protected $strUrlRecent;
	protected $strUrlSearch;
	
	protected $strLogoFileName;
	
	protected $strUserName;
	protected $blnUserLoggedIn;
	protected $blnShowRecentChanges;
	
	protected $strPageTitle;
	
	protected $strTextCopyright;
	protected $strTextPoweredBy;
	
	protected $objMenu;
	protected $arrRecent;
	
	protected $txtSearch;
	protected $pnlSearch;
	protected $objSearchWaitIcon = null;
	protected $btnSearchCommit;
	protected $btnSearchReset;
	
	public function __construct() {
		parent::__construct();
		
		$this->objAuth = QApplication::GetAuth();
		
		$this->strSiteTitle = QApplication::GetConfig('site_title');
		$this->strSiteCopyrightAuthor = QApplication::GetConfig('copyright_author');
		$this->strSiteCopyrightYears = QApplication::GetConfig('copyright_years');
		
		$this->strUrlIndex = QApplication::MakeUrl(QApplication::GetConfig('pagename_index'));
		$this->strUrlLogout = QApplication::MakeUrl(QApplication::GetConfig('pagename_logout'));
		$this->strUrlProfile = QApplication::MakeUrl(QApplication::GetConfig('pagename_profile'));
		$this->strUrlRecent = QApplication::MakeUrl(QApplication::GetConfig('pagename_recent'));
		$this->strUrlSearch = QApplication::MakeUrl(QApplication::GetConfig('pagename_search'));
		
		$this->strLogoFileName = QApplication::GetConfig('site_logofile');
		
		$this->strTextCopyright = sprintf(QApplication::Translate('%s is copyrighted &copy; %s by %s.'), $this->strSiteTitle, $this->strSiteCopyrightYears, $this->strSiteCopyrightAuthor);
		$this->strTextPoweredBy = sprintf(QApplication::Translate('Powered by <a href="%s">%s</a>, version %s.%s, copyrighted &copy; %s by %s.'), QApplication::GetConfig('software_coresystem_website'), QApplication::GetConfig('software_coresystem_name'), QApplication::GetConfig('software_coresystem_version'), QApplication::GetConfig('software_coresystem_revision'), QApplication::GetConfig('software_coresystem_years'), QApplication::GetConfig('software_coresystem_author'));
		
		$this->strUserName = QApplication::MakeAcronymOfUser($this->Auth->User);
		$this->blnUserLoggedIn = QApplication::UserIsLoggedIn();
		$this->blnShowRecentChanges = QApplication::GetConfig('site_showrecentchanges');
		
		$this->objMenu = new Menu();
		$this->PopulateRecent();
	}
	
	protected function Form_Create() {
		$this->pnlSearch = new QPanel($this);
		$this->pnlSearch->Visible = false;
		$this->pnlSearch->CssClass = 'pnlSearch';
		$this->pnlSearch->Text = 'Spotsight search';
		
		$this->txtSearch = new QTextBox($this);
		$this->objSearchWaitIcon = new QWaitIcon($this->txtSearch);
		
		$this->txtSearch->AddAction(new QKeyPressEvent(), new QAjaxAction('txtSearch_Change', $this->objSearchWaitIcon));
		$this->txtSearch->AddAction(new QEscapeKeyEvent(), new QAjaxAction('txtSearch_Clear', null));
		$this->txtSearch->AddAction(new QEscapeKeyEvent(), new QTerminateAction());
		
		$this->btnSearchCommit = new QButton($this);
		$this->btnSearchCommit->Text = QApplication::Translate('Search!');
		$this->btnSearchCommit->AddAction(new QClickEvent(), new QAjaxAction('txtSearch_Change', $this->objSearchWaitIcon));
		
		$this->btnSearchReset = new QButton($this);
		$this->btnSearchReset->Text = QApplication::Translate('Reset');
		$this->btnSearchReset->AddAction(new QClickEvent(), new QAjaxAction('btnSearchReset_Click', $this->objSearchWaitIcon));
		
		if (!$this->Auth->IsLoggedIn()) {
			$this->txtSearch->Enabled = false;
			$this->btnSearchCommit->Enabled = false;
			$this->btnSearchReset->Enabled = false;
		}
	}
	
	public function Form_Run() {
		if (!$this->Auth->IsLoggedIn()) {
			QApplication::RedirectToLoginPage();
		}
	}
	
	public function txtSearch_Change() {
		$this->pnlSearch->Visible = true;
	}
	
	public function txtSearch_Clear() {
		$this->pnlSearch->Visible = false;
	}
	
	public function btnSearchReset_Click() {
		$this->txtSearch_Clear();
		$this->txtSearch->Text = '';
	}
	
	/**
	 * Print profiling HTML.
	 *
	 */
	public function GetProfilingHtml() {
		if (QApplication::GetConfig('development_db_profile')) {
			_p('<br />', false);
			QApplication::$Database[1]->OutputProfiling();
		}
	}
	
	/**
	 * Return resolved template imaages filename.
	 *
	 * @param string $strFileName
	 * @return string
	 */
	public function GetImagesFilePath($strFileName) {
		if (file_exists(sprintf('%s/%s/images/%s', __TEMPLATES__, QApplication::GetConfig('site_template'), $strFileName))) {
			return sprintf('%s/templates/%s/images/%s', __VIRTUAL_DIRECTORY__, QApplication::GetConfig('site_template'), $strFileName);
		} else {
			return sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __IMAGE_ASSETS__, $strFileName);
		}
	}
	
	/**
	 * Return resolved logo filename.
	 *
	 * @param string $strFileName
	 * @return string
	 */
	public function GetLogoFilePath() {
		if (file_exists(sprintf('./%s/%s', __IMAGE_ASSETS__, $this->strLogoFileName))) {
			return sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __IMAGE_ASSETS__, $this->strLogoFileName);
		} else {
			return sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __IMAGE_ASSETS__, 'logo_placeholder.gif');
		}
	}
	
	/**
	 * Return resolved template styles filename.
	 *
	 * @param string $strFileName
	 * @return string
	 */
	public static function GetStylesFilePath($strFileName) {
		if (file_exists(sprintf('%s/%s/styles/%s', __TEMPLATES__, QApplication::GetConfig('site_template'), $strFileName))) {
			return sprintf('%s/templates/%s/styles/%s', __VIRTUAL_DIRECTORY__, QApplication::GetConfig('site_template'), $strFileName);
		} else {
			return sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __CSS_ASSETS__, $strFileName);
		}
	}
	
	/**
	 * Populate recent in template with data from the database.
	 *
	 */
	public function PopulateRecent() {
		foreach (Log::LoadArrayOfRecentItems(QApplication::GetConfig('site_recentitems', QType::Integer)) as $objLog) {
			switch (LogItemEnum::ToString($objLog->LogItemEnumId)) {
				case 'ORGANISATION':
					switch (LogActionEnum::ToToken($objLog->LogActionEnumId)) {
						case 'ADD':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_organisation'), QApplication::GetConfig('pagename_view'), $objLog->ItemId),
								'Text'		=> sprintf(QApplication::Translate('%s added'), Organisation::Load($objLog->ItemId)->Name),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
						case 'EDT':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_organisation'), QApplication::GetConfig('pagename_view'), $objLog->ItemId),
								'Text'		=> sprintf(QApplication::Translate('%s edited'), Organisation::Load($objLog->ItemId)->Name),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
						case 'DEL':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_organisation')),
								'Text'		=> sprintf(QApplication::Translate('%s deleted'), Organisation::Load($objLog->ItemId)->Name),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
					}
					break;
				case 'PROJECT':
					switch (LogActionEnum::ToString($objLog->LogActionEnumId)) {
						case 'ADD':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_project'), QApplication::GetConfig('pagename_view'), $objLog->ItemId),
								'Text'		=> sprintf(QApplication::Translate('%s added'), Project::Load($objLog->ItemId)->Name),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
						case 'EDT':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_project'), QApplication::GetConfig('pagename_view'), $objLog->ItemId),
								'Text'		=> sprintf(QApplication::Translate('%s edited'), Project::Load($objLog->ItemId)->Name),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
						case 'DEL':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_project')),
								'Text'		=> sprintf(QApplication::Translate('%s deleted'), Project::Load($objLog->ItemId)->Name),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
					}
					break;
				case 'TAPE':
					switch (LogActionEnum::ToToken($objLog->LogActionEnumId)) {
						case 'ADD':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_tape'), QApplication::GetConfig('pagename_view'), $objLog->ItemId),
								'Text'		=> sprintf(QApplication::Translate('Tape #%s added'), Tape::Load($objLog->ItemId)->Number),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
						case 'EDT':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_tape'), QApplication::GetConfig('pagename_view'), $objLog->ItemId),
								'Text'		=> sprintf(QApplication::Translate('Tape #%s edited'), Tape::Load($objLog->ItemId)->Number),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
						case 'DEL':
							$this->arrRecent[] = array(
								'Url'		=> QApplication::MakeUrl(QApplication::GetConfig('pagename_tape')),
								'Text'		=> sprintf(QApplication::Translate('Tape #%s deleted'), Tape::Load($objLog->ItemId)->Number),
								'Details'	=> $this->RecentItemDetails($objLog->Date, $objLog->User)
							);
							break;
					}
					break;
			}
		}
	}
	
	/**
	 * Fill detail string for Recent Changes.
	 *
	 * @param QDateTime $dttDate
	 * @param User $objUser
	 * @return string
	 */
	protected function RecentItemDetails(QDateTime $dttDate, User $objUser) {
		$dttToday = QDateTime::Now(false);
		
		if ($dttDate->Difference($dttToday)->IsPositive()) {
			$strDate = sprintf('Today at %s', $dttDate->format(QApplication::GetConfig('display_format_time')));
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < QDateTimeSpan::SecondsPerDay) {
			$strDate = QApplication::Translate('Yesterday');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerDay * 2)) {
			$strDate = QApplication::Translate('Two days ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerDay * 7)) {
			$strDate = QApplication::Translate('A week ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerDay * 14)) {
			$strDate = QApplication::Translate('Two weeks ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerMonth)) {
			$strDate = QApplication::Translate('A month ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerMonth * 2)) {
			$strDate = QApplication::Translate('Two months ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerMonth * 6)) {
			$strDate = QApplication::Translate('Half a year ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerYear)) {
			$strDate = QApplication::Translate('A year ago');
		} else if (abs($dttToday->Difference($dttDate)->Seconds) < (QDateTimeSpan::SecondsPerYear * 2)) {
			$strDate = QApplication::Translate('Two years ago');
		} else {
			$strDate = QApplication::Translate('Long time ago');
		}
		
		return sprintf(QApplication::Translate('%s by %s'), $strDate, QApplication::MakeAcronymOfUser($objUser));
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'TextCopyright': return $this->strTextCopyright;
			case 'TextPoweredBy': return $this->strTextPoweredBy;
			case 'SiteTitle': return $this->strSiteTitle;
			case 'SiteCopyrightAuthor': return $this->strSiteCopyrightAuthor;
			case 'SiteCopyrightYears': return $this->strSiteCopyrightYears;
			case 'UrlIndex': return $this->strUrlIndex;
			case 'UrlLogout': return $this->strUrlLogout;
			case 'UrlProfile': return $this->strUrlProfile;
			case 'UrlRecent': return $this->strUrlRecent;
			case 'UrlSearch': return $this->strUrlSearch;
			case 'UserName': return $this->strUserName;
			case 'UserLoggedIn': return $this->blnUserLoggedIn;
			case 'ShowRecentChanges': return $this->blnShowRecentChanges;
			case 'LogoFileName': return $this->strLogoFileName;
			case 'PageTitle': return $this->strPageTitle;
			case 'Menu': return $this->objMenu;
			case 'Recent': return $this->arrRecent;
			case 'Auth': return $this->objAuth;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
			case 'PageTitle':
				try {
					return ($this->strPageTitle = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
<?php require('header.inc.php'); ?>

<div id="logindata">
	<?php _tf('You are logged out. Please click <a href="%s">here</a> to continue.', $this->UrlIndex); ?>
</div>

<?php require('footer.inc.php'); ?>
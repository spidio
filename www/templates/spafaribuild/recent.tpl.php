<?php require('header.inc.php'); ?>

<h1><?php _t($this->PageTitle); ?></h1>

<p><?php _p($this->TextIntro); ?></p>

<p>
	<?php $this->lstUser->Render(); ?>
	<?php $this->lstSession->Render(); ?>
	<?php $this->lstAddress->Render(); ?>
</p>

<p>
	<?php $this->lstType->Render(); ?>
	<?php $this->lstAction->Render(); ?>
	<?php $this->lstItem->Render(); ?>
</p>

<p>
	<?php $this->txtItems->RenderWithError(); ?>
	<?php $this->btnAdvanced->Render(); ?>
	<?php $this->btnRefresh->Render(); ?>
	<?php $this->btnReset->Render(); ?>
	<?php $this->objDefaultWaitIcon->Render(); ?>
</p>

<?php $this->dtgLog->Render(); ?>

<?php require('footer.inc.php'); ?>
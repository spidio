<?php
	/**
	 * This is the abstract Panel class for the List All functionality
	 * of the Person class.  This code-generated class
	 * contains a datagrid to display an HTML page that can
	 * list a collection of Person objects.  It includes
	 * functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QPanel which extends this PersonListPanelBase
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent re-
	 * code generation.
	 * 
	 * @package Spidio
	 * @subpackage Drafts
	 * 
	 */
	class PersonListPanel extends QPanel {
		// Local instance of the Meta DataGrid to list People
		public $dtgPeople;

		// Other public QControls in this panel
		public $btnCreateNew;
		public $pxyEdit;

		// Callback Method Names
		protected $strSetEditPanelMethod;
		protected $strCloseEditPanelMethod;
		
		public function __construct($objParentObject, $strSetEditPanelMethod, $strCloseEditPanelMethod, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Record Method Callbacks
			$this->strSetEditPanelMethod = $strSetEditPanelMethod;
			$this->strCloseEditPanelMethod = $strCloseEditPanelMethod;

			// Setup the Template
			$this->Template = 'PersonListPanel.tpl.php';

			// Instantiate the Meta DataGrid
			$this->dtgPeople = new PersonDataGrid($this);

			// Style the DataGrid (if desired)
			$this->dtgPeople->CssClass = 'datagrid';
			$this->dtgPeople->AlternateRowStyle->CssClass = 'alternate';

			// Add Pagination (if desired)
			$this->dtgPeople->Paginator = new QPaginator($this->dtgPeople);
			$this->dtgPeople->ItemsPerPage = 8;

			// Use the MetaDataGrid functionality to add Columns for this datagrid

			// Create an Edit Column
			$this->pxyEdit = new QControlProxy($this);
			$this->pxyEdit->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'pxyEdit_Click'));
			$this->dtgPeople->MetaAddEditProxyColumn($this->pxyEdit, 'Edit', 'Edit');

			// Create the Other Columns (note that you can use strings for ' . DB_TABLE_PREFIX_1 . 'person's properties, or you
			// can traverse down QQN::' . DB_TABLE_PREFIX_1 . 'person() to display fields that are down the hierarchy)
			$this->dtgPeople->MetaAddColumn('Id');
			$this->dtgPeople->MetaAddColumn(QQN::Person()->Address);
			$this->dtgPeople->MetaAddColumn('Name');
			$this->dtgPeople->MetaAddColumn('Email');
			$this->dtgPeople->MetaAddColumn('Phone');
			$this->dtgPeople->MetaAddColumn('PhoneA');
			$this->dtgPeople->MetaAddColumn('PhoneC');
			$this->dtgPeople->MetaAddColumn('Mobile');
			$this->dtgPeople->MetaAddColumn('MobileA');
			$this->dtgPeople->MetaAddColumn('MobileC');
			$this->dtgPeople->MetaAddColumn('Optlock');
			$this->dtgPeople->MetaAddColumn('Owner');
			$this->dtgPeople->MetaAddColumn('Group');
			$this->dtgPeople->MetaAddColumn('Perms');
			$this->dtgPeople->MetaAddTypeColumn('Status', 'AuthStatusEnum');

			// Setup the Create New button
			$this->btnCreateNew = new QButton($this);
			$this->btnCreateNew->Text = QApplication::Translate('Create a New') . ' ' . QApplication::Translate('Person');
			$this->btnCreateNew->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCreateNew_Click'));
		}

		public function pxyEdit_Click($strFormId, $strControlId, $strParameter) {
			$strParameterArray = explode(',', $strParameter);
			$objEditPanel = new PersonEditPanel($this, $this->strCloseEditPanelMethod, $strParameterArray[0]);

			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}

		public function btnCreateNew_Click($strFormId, $strControlId, $strParameter) {
			$objEditPanel = new PersonEditPanel($this, $this->strCloseEditPanelMethod, null);
			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}
	}
?>
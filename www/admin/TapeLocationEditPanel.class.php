<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the TapeLocation class.  It uses the code-generated
	 * TapeLocationMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a TapeLocation columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both tape_location_edit.php AND
	 * tape_location_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class TapeLocationEditPanel extends QPanel {
		// Local instance of the TapeLocationMetaControl
		protected $mctTapeLocation;

		// Controls for TapeLocation's Data Fields
		public $txtId;
		public $lstTape;
		public $lstLocation;
		public $lstPerson;
		public $calDate;
		public $txtComment;
		public $lblOptlock;
		public $txtOwner;
		public $txtGroup;
		public $txtPerms;
		public $lstStatusObject;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'TapeLocationEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the TapeLocationMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctTapeLocation = TapeLocationMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on TapeLocation's data fields
			$this->txtId = $this->mctTapeLocation->txtId_Create();
			$this->lstTape = $this->mctTapeLocation->lstTape_Create();
			$this->lstLocation = $this->mctTapeLocation->lstLocation_Create();
			$this->lstPerson = $this->mctTapeLocation->lstPerson_Create();
			$this->calDate = $this->mctTapeLocation->calDate_Create();
			$this->txtComment = $this->mctTapeLocation->txtComment_Create();
			$this->lblOptlock = $this->mctTapeLocation->lblOptlock_Create();
			$this->txtOwner = $this->mctTapeLocation->txtOwner_Create();
			$this->txtGroup = $this->mctTapeLocation->txtGroup_Create();
			$this->txtPerms = $this->mctTapeLocation->txtPerms_Create();
			$this->lstStatusObject = $this->mctTapeLocation->lstStatusObject_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('TapeLocation') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctTapeLocation->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the TapeLocationMetaControl
			$this->mctTapeLocation->SaveTapeLocation();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the TapeLocationMetaControl
			$this->mctTapeLocation->DeleteTapeLocation();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
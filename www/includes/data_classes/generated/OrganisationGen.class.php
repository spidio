<?php
	/**
	 * The abstract OrganisationGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Organisation subclass which
	 * extends this OrganisationGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Organisation class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $AddressId the value for strAddressId (Not Null)
	 * @property string $PersonId the value for strPersonId (Not Null)
	 * @property string $Name the value for strName (Unique)
	 * @property integer $Phone the value for intPhone 
	 * @property integer $PhoneA the value for intPhoneA 
	 * @property integer $PhoneC the value for intPhoneC 
	 * @property string $Email the value for strEmail 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Address $Address the value for the Address object referenced by strAddressId (Not Null)
	 * @property Person $Person the value for the Person object referenced by strPersonId (Not Null)
	 * @property-read Camera $_CameraAsOrganisation the value for the private _objCameraAsOrganisation (Read-Only) if set due to an expansion on the spidio_camera.organisation_id reverse relationship
	 * @property-read Camera[] $_CameraAsOrganisationArray the value for the private _objCameraAsOrganisationArray (Read-Only) if set due to an ExpandAsArray on the spidio_camera.organisation_id reverse relationship
	 * @property-read Project $_ProjectAsOrganisation the value for the private _objProjectAsOrganisation (Read-Only) if set due to an expansion on the spidio_project.organisation_id reverse relationship
	 * @property-read Project[] $_ProjectAsOrganisationArray the value for the private _objProjectAsOrganisationArray (Read-Only) if set due to an ExpandAsArray on the spidio_project.organisation_id reverse relationship
	 * @property-read Tape $_TapeAsOrganisation the value for the private _objTapeAsOrganisation (Read-Only) if set due to an expansion on the spidio_tape.organisation_id reverse relationship
	 * @property-read Tape[] $_TapeAsOrganisationArray the value for the private _objTapeAsOrganisationArray (Read-Only) if set due to an ExpandAsArray on the spidio_tape.organisation_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class OrganisationGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_organisation.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_organisation.address_id
		 * @var string strAddressId
		 */
		protected $strAddressId;
		const AddressIdMaxLength = 36;
		const AddressIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.person_id
		 * @var string strPersonId
		 */
		protected $strPersonId;
		const PersonIdMaxLength = 36;
		const PersonIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.phone
		 * @var integer intPhone
		 */
		protected $intPhone;
		const PhoneDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.phone_a
		 * @var integer intPhoneA
		 */
		protected $intPhoneA;
		const PhoneADefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.phone_c
		 * @var integer intPhoneC
		 */
		protected $intPhoneC;
		const PhoneCDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.email
		 * @var string strEmail
		 */
		protected $strEmail;
		const EmailMaxLength = 255;
		const EmailDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_organisation.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single CameraAsOrganisation object
		 * (of type Camera), if this Organisation object was restored with
		 * an expansion on the spidio_camera association table.
		 * @var Camera _objCameraAsOrganisation;
		 */
		private $_objCameraAsOrganisation;

		/**
		 * Private member variable that stores a reference to an array of CameraAsOrganisation objects
		 * (of type Camera[]), if this Organisation object was restored with
		 * an ExpandAsArray on the spidio_camera association table.
		 * @var Camera[] _objCameraAsOrganisationArray;
		 */
		private $_objCameraAsOrganisationArray = array();

		/**
		 * Private member variable that stores a reference to a single ProjectAsOrganisation object
		 * (of type Project), if this Organisation object was restored with
		 * an expansion on the spidio_project association table.
		 * @var Project _objProjectAsOrganisation;
		 */
		private $_objProjectAsOrganisation;

		/**
		 * Private member variable that stores a reference to an array of ProjectAsOrganisation objects
		 * (of type Project[]), if this Organisation object was restored with
		 * an ExpandAsArray on the spidio_project association table.
		 * @var Project[] _objProjectAsOrganisationArray;
		 */
		private $_objProjectAsOrganisationArray = array();

		/**
		 * Private member variable that stores a reference to a single TapeAsOrganisation object
		 * (of type Tape), if this Organisation object was restored with
		 * an expansion on the spidio_tape association table.
		 * @var Tape _objTapeAsOrganisation;
		 */
		private $_objTapeAsOrganisation;

		/**
		 * Private member variable that stores a reference to an array of TapeAsOrganisation objects
		 * (of type Tape[]), if this Organisation object was restored with
		 * an ExpandAsArray on the spidio_tape association table.
		 * @var Tape[] _objTapeAsOrganisationArray;
		 */
		private $_objTapeAsOrganisationArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_organisation.address_id.
		 *
		 * NOTE: Always use the Address property getter to correctly retrieve this Address object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Address objAddress
		 */
		protected $objAddress;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_organisation.person_id.
		 *
		 * NOTE: Always use the Person property getter to correctly retrieve this Person object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Person objPerson
		 */
		protected $objPerson;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Organisation from PK Info
		 * @param string $strId
		 * @return Organisation
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Organisation::QuerySingle(
				QQ::Equal(QQN::Organisation()->Id, $strId)
			);
		}

		/**
		 * Load all Organisations
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Organisation::QueryArray to perform the LoadAll query
			try {
				return Organisation::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Organisations
		 * @return int
		 */
		public static function CountAll() {
			// Call Organisation::QueryCount to perform the CountAll query
			return Organisation::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Create/Build out the QueryBuilder object with Organisation-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'organisation');
			Organisation::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'organisation');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Organisation object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Organisation the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Organisation::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Organisation object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Organisation::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Organisation objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Organisation[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Organisation::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Organisation::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Organisation objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Organisation::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'organisation_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Organisation-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Organisation::GetSelectFields($objQueryBuilder);
				Organisation::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Organisation::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Organisation
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'organisation';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'address_id', $strAliasPrefix . 'address_id');
			$objBuilder->AddSelectItem($strTableName, 'person_id', $strAliasPrefix . 'person_id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'phone', $strAliasPrefix . 'phone');
			$objBuilder->AddSelectItem($strTableName, 'phone_a', $strAliasPrefix . 'phone_a');
			$objBuilder->AddSelectItem($strTableName, 'phone_c', $strAliasPrefix . 'phone_c');
			$objBuilder->AddSelectItem($strTableName, 'email', $strAliasPrefix . 'email');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Organisation from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Organisation::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Organisation
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'organisation__';


				$strAlias = $strAliasPrefix . 'cameraasorganisation__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objCameraAsOrganisationArray)) {
						$objPreviousChildItem = $objPreviousItem->_objCameraAsOrganisationArray[$intPreviousChildItemCount - 1];
						$objChildItem = Camera::InstantiateDbRow($objDbRow, $strAliasPrefix . 'cameraasorganisation__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objCameraAsOrganisationArray[] = $objChildItem;
					} else
						$objPreviousItem->_objCameraAsOrganisationArray[] = Camera::InstantiateDbRow($objDbRow, $strAliasPrefix . 'cameraasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'projectasorganisation__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objProjectAsOrganisationArray)) {
						$objPreviousChildItem = $objPreviousItem->_objProjectAsOrganisationArray[$intPreviousChildItemCount - 1];
						$objChildItem = Project::InstantiateDbRow($objDbRow, $strAliasPrefix . 'projectasorganisation__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objProjectAsOrganisationArray[] = $objChildItem;
					} else
						$objPreviousItem->_objProjectAsOrganisationArray[] = Project::InstantiateDbRow($objDbRow, $strAliasPrefix . 'projectasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'tapeasorganisation__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objTapeAsOrganisationArray)) {
						$objPreviousChildItem = $objPreviousItem->_objTapeAsOrganisationArray[$intPreviousChildItemCount - 1];
						$objChildItem = Tape::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapeasorganisation__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objTapeAsOrganisationArray[] = $objChildItem;
					} else
						$objPreviousItem->_objTapeAsOrganisationArray[] = Tape::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapeasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'organisation__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the Organisation object
			$objToReturn = new Organisation();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'address_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'address_id'] : $strAliasPrefix . 'address_id';
			$objToReturn->strAddressId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'person_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'person_id'] : $strAliasPrefix . 'person_id';
			$objToReturn->strPersonId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'phone', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'phone'] : $strAliasPrefix . 'phone';
			$objToReturn->intPhone = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'phone_a', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'phone_a'] : $strAliasPrefix . 'phone_a';
			$objToReturn->intPhoneA = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'phone_c', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'phone_c'] : $strAliasPrefix . 'phone_c';
			$objToReturn->intPhoneC = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'email', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'email'] : $strAliasPrefix . 'email';
			$objToReturn->strEmail = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'organisation__';

			// Check for Address Early Binding
			$strAlias = $strAliasPrefix . 'address_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objAddress = Address::InstantiateDbRow($objDbRow, $strAliasPrefix . 'address_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Person Early Binding
			$strAlias = $strAliasPrefix . 'person_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objPerson = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'person_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			// Check for CameraAsOrganisation Virtual Binding
			$strAlias = $strAliasPrefix . 'cameraasorganisation__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objCameraAsOrganisationArray[] = Camera::InstantiateDbRow($objDbRow, $strAliasPrefix . 'cameraasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objCameraAsOrganisation = Camera::InstantiateDbRow($objDbRow, $strAliasPrefix . 'cameraasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for ProjectAsOrganisation Virtual Binding
			$strAlias = $strAliasPrefix . 'projectasorganisation__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objProjectAsOrganisationArray[] = Project::InstantiateDbRow($objDbRow, $strAliasPrefix . 'projectasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objProjectAsOrganisation = Project::InstantiateDbRow($objDbRow, $strAliasPrefix . 'projectasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for TapeAsOrganisation Virtual Binding
			$strAlias = $strAliasPrefix . 'tapeasorganisation__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objTapeAsOrganisationArray[] = Tape::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapeasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objTapeAsOrganisation = Tape::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapeasorganisation__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of Organisations from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Organisation[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Organisation::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Organisation::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Organisation object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Organisation
		*/
		public static function LoadById($strId) {
			return Organisation::QuerySingle(
				QQ::Equal(QQN::Organisation()->Id, $strId)
			);
		}
			
		/**
		 * Load a single Organisation object,
		 * by Name Index(es)
		 * @param string $strName
		 * @return Organisation
		*/
		public static function LoadByName($strName) {
			return Organisation::QuerySingle(
				QQ::Equal(QQN::Organisation()->Name, $strName)
			);
		}
			
		/**
		 * Load an array of Organisation objects,
		 * by AddressId Index(es)
		 * @param string $strAddressId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		*/
		public static function LoadArrayByAddressId($strAddressId, $objOptionalClauses = null) {
			// Call Organisation::QueryArray to perform the LoadArrayByAddressId query
			try {
				return Organisation::QueryArray(
					QQ::Equal(QQN::Organisation()->AddressId, $strAddressId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Organisations
		 * by AddressId Index(es)
		 * @param string $strAddressId
		 * @return int
		*/
		public static function CountByAddressId($strAddressId) {
			// Call Organisation::QueryCount to perform the CountByAddressId query
			return Organisation::QueryCount(
				QQ::Equal(QQN::Organisation()->AddressId, $strAddressId)
			);
		}
			
		/**
		 * Load an array of Organisation objects,
		 * by PersonId Index(es)
		 * @param string $strPersonId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		*/
		public static function LoadArrayByPersonId($strPersonId, $objOptionalClauses = null) {
			// Call Organisation::QueryArray to perform the LoadArrayByPersonId query
			try {
				return Organisation::QueryArray(
					QQ::Equal(QQN::Organisation()->PersonId, $strPersonId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Organisations
		 * by PersonId Index(es)
		 * @param string $strPersonId
		 * @return int
		*/
		public static function CountByPersonId($strPersonId) {
			// Call Organisation::QueryCount to perform the CountByPersonId query
			return Organisation::QueryCount(
				QQ::Equal(QQN::Organisation()->PersonId, $strPersonId)
			);
		}
			
		/**
		 * Load an array of Organisation objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call Organisation::QueryArray to perform the LoadArrayByOwner query
			try {
				return Organisation::QueryArray(
					QQ::Equal(QQN::Organisation()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Organisations
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call Organisation::QueryCount to perform the CountByOwner query
			return Organisation::QueryCount(
				QQ::Equal(QQN::Organisation()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of Organisation objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call Organisation::QueryArray to perform the LoadArrayByStatus query
			try {
				return Organisation::QueryArray(
					QQ::Equal(QQN::Organisation()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Organisations
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call Organisation::QueryCount to perform the CountByStatus query
			return Organisation::QueryCount(
				QQ::Equal(QQN::Organisation()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Organisation
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'organisation` (
							`id`,
							`address_id`,
							`person_id`,
							`name`,
							`phone`,
							`phone_a`,
							`phone_c`,
							`email`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strAddressId) . ',
							' . $objDatabase->SqlVariable($this->strPersonId) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->intPhone) . ',
							' . $objDatabase->SqlVariable($this->intPhoneA) . ',
							' . $objDatabase->SqlVariable($this->intPhoneC) . ',
							' . $objDatabase->SqlVariable($this->strEmail) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'organisation`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Organisation');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'organisation`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`address_id` = ' . $objDatabase->SqlVariable($this->strAddressId) . ',
							`person_id` = ' . $objDatabase->SqlVariable($this->strPersonId) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`phone` = ' . $objDatabase->SqlVariable($this->intPhone) . ',
							`phone_a` = ' . $objDatabase->SqlVariable($this->intPhoneA) . ',
							`phone_c` = ' . $objDatabase->SqlVariable($this->intPhoneC) . ',
							`email` = ' . $objDatabase->SqlVariable($this->strEmail) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'organisation`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Organisation
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Organisation with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'organisation`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Organisations
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'organisation`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'organisation table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'organisation`');
		}

		/**
		 * Reload this Organisation from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Organisation object.');

			// Reload the Object
			$objReloaded = Organisation::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->AddressId = $objReloaded->AddressId;
			$this->PersonId = $objReloaded->PersonId;
			$this->strName = $objReloaded->strName;
			$this->intPhone = $objReloaded->intPhone;
			$this->intPhoneA = $objReloaded->intPhoneA;
			$this->intPhoneC = $objReloaded->intPhoneC;
			$this->strEmail = $objReloaded->strEmail;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'AddressId':
					/**
					 * Gets the value for strAddressId (Not Null)
					 * @return string
					 */
					return $this->strAddressId;

				case 'PersonId':
					/**
					 * Gets the value for strPersonId (Not Null)
					 * @return string
					 */
					return $this->strPersonId;

				case 'Name':
					/**
					 * Gets the value for strName (Unique)
					 * @return string
					 */
					return $this->strName;

				case 'Phone':
					/**
					 * Gets the value for intPhone 
					 * @return integer
					 */
					return $this->intPhone;

				case 'PhoneA':
					/**
					 * Gets the value for intPhoneA 
					 * @return integer
					 */
					return $this->intPhoneA;

				case 'PhoneC':
					/**
					 * Gets the value for intPhoneC 
					 * @return integer
					 */
					return $this->intPhoneC;

				case 'Email':
					/**
					 * Gets the value for strEmail 
					 * @return string
					 */
					return $this->strEmail;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Address':
					/**
					 * Gets the value for the Address object referenced by strAddressId (Not Null)
					 * @return Address
					 */
					try {
						if ((!$this->objAddress) && (!is_null($this->strAddressId)))
							$this->objAddress = Address::Load($this->strAddressId);
						return $this->objAddress;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Person':
					/**
					 * Gets the value for the Person object referenced by strPersonId (Not Null)
					 * @return Person
					 */
					try {
						if ((!$this->objPerson) && (!is_null($this->strPersonId)))
							$this->objPerson = Person::Load($this->strPersonId);
						return $this->objPerson;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_CameraAsOrganisation':
					/**
					 * Gets the value for the private _objCameraAsOrganisation (Read-Only)
					 * if set due to an expansion on the spidio_camera.organisation_id reverse relationship
					 * @return Camera
					 */
					return $this->_objCameraAsOrganisation;

				case '_CameraAsOrganisationArray':
					/**
					 * Gets the value for the private _objCameraAsOrganisationArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_camera.organisation_id reverse relationship
					 * @return Camera[]
					 */
					return (array) $this->_objCameraAsOrganisationArray;

				case '_ProjectAsOrganisation':
					/**
					 * Gets the value for the private _objProjectAsOrganisation (Read-Only)
					 * if set due to an expansion on the spidio_project.organisation_id reverse relationship
					 * @return Project
					 */
					return $this->_objProjectAsOrganisation;

				case '_ProjectAsOrganisationArray':
					/**
					 * Gets the value for the private _objProjectAsOrganisationArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_project.organisation_id reverse relationship
					 * @return Project[]
					 */
					return (array) $this->_objProjectAsOrganisationArray;

				case '_TapeAsOrganisation':
					/**
					 * Gets the value for the private _objTapeAsOrganisation (Read-Only)
					 * if set due to an expansion on the spidio_tape.organisation_id reverse relationship
					 * @return Tape
					 */
					return $this->_objTapeAsOrganisation;

				case '_TapeAsOrganisationArray':
					/**
					 * Gets the value for the private _objTapeAsOrganisationArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_tape.organisation_id reverse relationship
					 * @return Tape[]
					 */
					return (array) $this->_objTapeAsOrganisationArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AddressId':
					/**
					 * Sets the value for strAddressId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objAddress = null;
						return ($this->strAddressId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PersonId':
					/**
					 * Sets the value for strPersonId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objPerson = null;
						return ($this->strPersonId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Unique)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Phone':
					/**
					 * Sets the value for intPhone 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPhone = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PhoneA':
					/**
					 * Sets the value for intPhoneA 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPhoneA = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PhoneC':
					/**
					 * Sets the value for intPhoneC 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPhoneC = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Email':
					/**
					 * Sets the value for strEmail 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strEmail = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Address':
					/**
					 * Sets the value for the Address object referenced by strAddressId (Not Null)
					 * @param Address $mixValue
					 * @return Address
					 */
					if (is_null($mixValue)) {
						$this->strAddressId = null;
						$this->objAddress = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Address object
						try {
							$mixValue = QType::Cast($mixValue, 'Address');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Address object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Address for this Organisation');

						// Update Local Member Variables
						$this->objAddress = $mixValue;
						$this->strAddressId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Person':
					/**
					 * Sets the value for the Person object referenced by strPersonId (Not Null)
					 * @param Person $mixValue
					 * @return Person
					 */
					if (is_null($mixValue)) {
						$this->strPersonId = null;
						$this->objPerson = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Person object
						try {
							$mixValue = QType::Cast($mixValue, 'Person');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Person object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Person for this Organisation');

						// Update Local Member Variables
						$this->objPerson = $mixValue;
						$this->strPersonId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for CameraAsOrganisation
		//-------------------------------------------------------------------

		/**
		 * Gets all associated CamerasAsOrganisation as an array of Camera objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Camera[]
		*/ 
		public function GetCameraAsOrganisationArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Camera::LoadArrayByOrganisationId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated CamerasAsOrganisation
		 * @return int
		*/ 
		public function CountCamerasAsOrganisation() {
			if ((is_null($this->strId)))
				return 0;

			return Camera::CountByOrganisationId($this->strId);
		}

		/**
		 * Associates a CameraAsOrganisation
		 * @param Camera $objCamera
		 * @return void
		*/ 
		public function AssociateCameraAsOrganisation(Camera $objCamera) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateCameraAsOrganisation on this unsaved Organisation.');
			if ((is_null($objCamera->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateCameraAsOrganisation on this Organisation with an unsaved Camera.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_camera`
				SET
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objCamera->Id) . '
			');
		}

		/**
		 * Unassociates a CameraAsOrganisation
		 * @param Camera $objCamera
		 * @return void
		*/ 
		public function UnassociateCameraAsOrganisation(Camera $objCamera) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateCameraAsOrganisation on this unsaved Organisation.');
			if ((is_null($objCamera->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateCameraAsOrganisation on this Organisation with an unsaved Camera.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_camera`
				SET
					`organisation_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objCamera->Id) . ' AND
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all CamerasAsOrganisation
		 * @return void
		*/ 
		public function UnassociateAllCamerasAsOrganisation() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateCameraAsOrganisation on this unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_camera`
				SET
					`organisation_id` = null
				WHERE
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated CameraAsOrganisation
		 * @param Camera $objCamera
		 * @return void
		*/ 
		public function DeleteAssociatedCameraAsOrganisation(Camera $objCamera) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateCameraAsOrganisation on this unsaved Organisation.');
			if ((is_null($objCamera->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateCameraAsOrganisation on this Organisation with an unsaved Camera.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_camera`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objCamera->Id) . ' AND
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated CamerasAsOrganisation
		 * @return void
		*/ 
		public function DeleteAllCamerasAsOrganisation() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateCameraAsOrganisation on this unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_camera`
				WHERE
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for ProjectAsOrganisation
		//-------------------------------------------------------------------

		/**
		 * Gets all associated ProjectsAsOrganisation as an array of Project objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Project[]
		*/ 
		public function GetProjectAsOrganisationArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Project::LoadArrayByOrganisationId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated ProjectsAsOrganisation
		 * @return int
		*/ 
		public function CountProjectsAsOrganisation() {
			if ((is_null($this->strId)))
				return 0;

			return Project::CountByOrganisationId($this->strId);
		}

		/**
		 * Associates a ProjectAsOrganisation
		 * @param Project $objProject
		 * @return void
		*/ 
		public function AssociateProjectAsOrganisation(Project $objProject) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateProjectAsOrganisation on this unsaved Organisation.');
			if ((is_null($objProject->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateProjectAsOrganisation on this Organisation with an unsaved Project.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_project`
				SET
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objProject->Id) . '
			');
		}

		/**
		 * Unassociates a ProjectAsOrganisation
		 * @param Project $objProject
		 * @return void
		*/ 
		public function UnassociateProjectAsOrganisation(Project $objProject) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateProjectAsOrganisation on this unsaved Organisation.');
			if ((is_null($objProject->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateProjectAsOrganisation on this Organisation with an unsaved Project.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_project`
				SET
					`organisation_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objProject->Id) . ' AND
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all ProjectsAsOrganisation
		 * @return void
		*/ 
		public function UnassociateAllProjectsAsOrganisation() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateProjectAsOrganisation on this unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_project`
				SET
					`organisation_id` = null
				WHERE
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated ProjectAsOrganisation
		 * @param Project $objProject
		 * @return void
		*/ 
		public function DeleteAssociatedProjectAsOrganisation(Project $objProject) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateProjectAsOrganisation on this unsaved Organisation.');
			if ((is_null($objProject->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateProjectAsOrganisation on this Organisation with an unsaved Project.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_project`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objProject->Id) . ' AND
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated ProjectsAsOrganisation
		 * @return void
		*/ 
		public function DeleteAllProjectsAsOrganisation() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateProjectAsOrganisation on this unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_project`
				WHERE
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for TapeAsOrganisation
		//-------------------------------------------------------------------

		/**
		 * Gets all associated TapesAsOrganisation as an array of Tape objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		*/ 
		public function GetTapeAsOrganisationArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Tape::LoadArrayByOrganisationId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated TapesAsOrganisation
		 * @return int
		*/ 
		public function CountTapesAsOrganisation() {
			if ((is_null($this->strId)))
				return 0;

			return Tape::CountByOrganisationId($this->strId);
		}

		/**
		 * Associates a TapeAsOrganisation
		 * @param Tape $objTape
		 * @return void
		*/ 
		public function AssociateTapeAsOrganisation(Tape $objTape) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateTapeAsOrganisation on this unsaved Organisation.');
			if ((is_null($objTape->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateTapeAsOrganisation on this Organisation with an unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape`
				SET
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTape->Id) . '
			');
		}

		/**
		 * Unassociates a TapeAsOrganisation
		 * @param Tape $objTape
		 * @return void
		*/ 
		public function UnassociateTapeAsOrganisation(Tape $objTape) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeAsOrganisation on this unsaved Organisation.');
			if ((is_null($objTape->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeAsOrganisation on this Organisation with an unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape`
				SET
					`organisation_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTape->Id) . ' AND
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all TapesAsOrganisation
		 * @return void
		*/ 
		public function UnassociateAllTapesAsOrganisation() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeAsOrganisation on this unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape`
				SET
					`organisation_id` = null
				WHERE
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated TapeAsOrganisation
		 * @param Tape $objTape
		 * @return void
		*/ 
		public function DeleteAssociatedTapeAsOrganisation(Tape $objTape) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeAsOrganisation on this unsaved Organisation.');
			if ((is_null($objTape->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeAsOrganisation on this Organisation with an unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_tape`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTape->Id) . ' AND
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated TapesAsOrganisation
		 * @return void
		*/ 
		public function DeleteAllTapesAsOrganisation() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeAsOrganisation on this unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Organisation::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_tape`
				WHERE
					`organisation_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Organisation"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Address" type="xsd1:Address"/>';
			$strToReturn .= '<element name="Person" type="xsd1:Person"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="Phone" type="xsd:int"/>';
			$strToReturn .= '<element name="PhoneA" type="xsd:int"/>';
			$strToReturn .= '<element name="PhoneC" type="xsd:int"/>';
			$strToReturn .= '<element name="Email" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Organisation', $strComplexTypeArray)) {
				$strComplexTypeArray['Organisation'] = Organisation::GetSoapComplexTypeXml();
				Address::AlterSoapComplexTypeArray($strComplexTypeArray);
				Person::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Organisation::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Organisation();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'Address')) &&
				($objSoapObject->Address))
				$objToReturn->Address = Address::GetObjectFromSoapObject($objSoapObject->Address);
			if ((property_exists($objSoapObject, 'Person')) &&
				($objSoapObject->Person))
				$objToReturn->Person = Person::GetObjectFromSoapObject($objSoapObject->Person);
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'Phone'))
				$objToReturn->intPhone = $objSoapObject->Phone;
			if (property_exists($objSoapObject, 'PhoneA'))
				$objToReturn->intPhoneA = $objSoapObject->PhoneA;
			if (property_exists($objSoapObject, 'PhoneC'))
				$objToReturn->intPhoneC = $objSoapObject->PhoneC;
			if (property_exists($objSoapObject, 'Email'))
				$objToReturn->strEmail = $objSoapObject->Email;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Organisation::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objAddress)
				$objObject->objAddress = Address::GetSoapObjectFromObject($objObject->objAddress, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strAddressId = null;
			if ($objObject->objPerson)
				$objObject->objPerson = Person::GetSoapObjectFromObject($objObject->objPerson, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strPersonId = null;
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeOrganisation extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Organisation';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'organisation';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'AddressId':
					return new QQNode('address_id', 'AddressId', 'string', $this);
				case 'Address':
					return new QQNodeAddress('address_id', 'Address', 'string', $this);
				case 'PersonId':
					return new QQNode('person_id', 'PersonId', 'string', $this);
				case 'Person':
					return new QQNodePerson('person_id', 'Person', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Phone':
					return new QQNode('phone', 'Phone', 'integer', $this);
				case 'PhoneA':
					return new QQNode('phone_a', 'PhoneA', 'integer', $this);
				case 'PhoneC':
					return new QQNode('phone_c', 'PhoneC', 'integer', $this);
				case 'Email':
					return new QQNode('email', 'Email', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'CameraAsOrganisation':
					return new QQReverseReferenceNodeCamera($this, 'cameraasorganisation', 'reverse_reference', 'organisation_id');
				case 'ProjectAsOrganisation':
					return new QQReverseReferenceNodeProject($this, 'projectasorganisation', 'reverse_reference', 'organisation_id');
				case 'TapeAsOrganisation':
					return new QQReverseReferenceNodeTape($this, 'tapeasorganisation', 'reverse_reference', 'organisation_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeOrganisation extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Organisation';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'organisation';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'AddressId':
					return new QQNode('address_id', 'AddressId', 'string', $this);
				case 'Address':
					return new QQNodeAddress('address_id', 'Address', 'string', $this);
				case 'PersonId':
					return new QQNode('person_id', 'PersonId', 'string', $this);
				case 'Person':
					return new QQNodePerson('person_id', 'Person', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Phone':
					return new QQNode('phone', 'Phone', 'integer', $this);
				case 'PhoneA':
					return new QQNode('phone_a', 'PhoneA', 'integer', $this);
				case 'PhoneC':
					return new QQNode('phone_c', 'PhoneC', 'integer', $this);
				case 'Email':
					return new QQNode('email', 'Email', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'CameraAsOrganisation':
					return new QQReverseReferenceNodeCamera($this, 'cameraasorganisation', 'reverse_reference', 'organisation_id');
				case 'ProjectAsOrganisation':
					return new QQReverseReferenceNodeProject($this, 'projectasorganisation', 'reverse_reference', 'organisation_id');
				case 'TapeAsOrganisation':
					return new QQReverseReferenceNodeTape($this, 'tapeasorganisation', 'reverse_reference', 'organisation_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Project class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Project object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a ProjectMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Project $Project the actual Project data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $ProjectTypeIdControl
	 * property-read QLabel $ProjectTypeIdLabel
	 * property QListBox $OrganisationIdControl
	 * property-read QLabel $OrganisationIdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QDateTimePicker $BeginControl
	 * property-read QLabel $BeginLabel
	 * property QDateTimePicker $EndControl
	 * property-read QLabel $EndLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class ProjectMetaControlGen extends QBaseClass {
		// General Variables
		protected $objProject;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Project's individual data fields
		protected $txtId;
		protected $lstProjectType;
		protected $lstOrganisation;
		protected $txtName;
		protected $calBegin;
		protected $calEnd;
		protected $calAdded;
		protected $txtComment;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Project's individual data fields
		protected $lblId;
		protected $lblProjectTypeId;
		protected $lblOrganisationId;
		protected $lblName;
		protected $lblBegin;
		protected $lblEnd;
		protected $lblAdded;
		protected $lblComment;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * ProjectMetaControl to edit a single Project object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Project object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectMetaControl
		 * @param Project $objProject new or existing Project object
		 */
		 public function __construct($objParentObject, Project $objProject) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this ProjectMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Project object
			$this->objProject = $objProject;

			// Figure out if we're Editing or Creating New
			if ($this->objProject->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Project object creation - defaults to CreateOrEdit
 		 * @return ProjectMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objProject = Project::Load($strId);

				// Project was found -- return it!
				if ($objProject)
					return new ProjectMetaControl($objParentObject, $objProject);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Project object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new ProjectMetaControl($objParentObject, new Project());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Project object creation - defaults to CreateOrEdit
		 * @return ProjectMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return ProjectMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Project object creation - defaults to CreateOrEdit
		 * @return ProjectMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return ProjectMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objProject->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Project::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objProject->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstProjectType
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstProjectType_Create($strControlId = null) {
			$this->lstProjectType = new QListBox($this->objParentObject, $strControlId);
			$this->lstProjectType->Name = QApplication::Translate('Project Type');
			$this->lstProjectType->Required = true;
			if (!$this->blnEditMode)
				$this->lstProjectType->AddItem(QApplication::Translate('- Select One -'), null);
			$objProjectTypeArray = ProjectType::LoadAll();
			if ($objProjectTypeArray) foreach ($objProjectTypeArray as $objProjectType) {
				$objListItem = new QListItem($objProjectType->__toString(), $objProjectType->Id);
				if (($this->objProject->ProjectType) && ($this->objProject->ProjectType->Id == $objProjectType->Id))
					$objListItem->Selected = true;
				$this->lstProjectType->AddItem($objListItem);
			}
			return $this->lstProjectType;
		}

		/**
		 * Create and setup QLabel lblProjectTypeId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblProjectTypeId_Create($strControlId = null) {
			$this->lblProjectTypeId = new QLabel($this->objParentObject, $strControlId);
			$this->lblProjectTypeId->Name = QApplication::Translate('Project Type');
			$this->lblProjectTypeId->Text = ($this->objProject->ProjectType) ? $this->objProject->ProjectType->__toString() : null;
			$this->lblProjectTypeId->Required = true;
			return $this->lblProjectTypeId;
		}

		/**
		 * Create and setup QListBox lstOrganisation
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstOrganisation_Create($strControlId = null) {
			$this->lstOrganisation = new QListBox($this->objParentObject, $strControlId);
			$this->lstOrganisation->Name = QApplication::Translate('Organisation');
			$this->lstOrganisation->Required = true;
			if (!$this->blnEditMode)
				$this->lstOrganisation->AddItem(QApplication::Translate('- Select One -'), null);
			$objOrganisationArray = Organisation::LoadAll();
			if ($objOrganisationArray) foreach ($objOrganisationArray as $objOrganisation) {
				$objListItem = new QListItem($objOrganisation->__toString(), $objOrganisation->Id);
				if (($this->objProject->Organisation) && ($this->objProject->Organisation->Id == $objOrganisation->Id))
					$objListItem->Selected = true;
				$this->lstOrganisation->AddItem($objListItem);
			}
			return $this->lstOrganisation;
		}

		/**
		 * Create and setup QLabel lblOrganisationId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOrganisationId_Create($strControlId = null) {
			$this->lblOrganisationId = new QLabel($this->objParentObject, $strControlId);
			$this->lblOrganisationId->Name = QApplication::Translate('Organisation');
			$this->lblOrganisationId->Text = ($this->objProject->Organisation) ? $this->objProject->Organisation->__toString() : null;
			$this->lblOrganisationId->Required = true;
			return $this->lblOrganisationId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objProject->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = Project::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objProject->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QDateTimePicker calBegin
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calBegin_Create($strControlId = null) {
			$this->calBegin = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calBegin->Name = QApplication::Translate('Begin');
			$this->calBegin->DateTime = $this->objProject->Begin;
			$this->calBegin->DateTimePickerType = QDateTimePickerType::Date;
			$this->calBegin->Required = true;
			return $this->calBegin;
		}

		/**
		 * Create and setup QLabel lblBegin
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblBegin_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblBegin = new QLabel($this->objParentObject, $strControlId);
			$this->lblBegin->Name = QApplication::Translate('Begin');
			$this->strBeginDateTimeFormat = $strDateTimeFormat;
			$this->lblBegin->Text = sprintf($this->objProject->Begin) ? $this->objProject->__toString($this->strBeginDateTimeFormat) : null;
			$this->lblBegin->Required = true;
			return $this->lblBegin;
		}

		protected $strBeginDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calEnd
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calEnd_Create($strControlId = null) {
			$this->calEnd = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calEnd->Name = QApplication::Translate('End');
			$this->calEnd->DateTime = $this->objProject->End;
			$this->calEnd->DateTimePickerType = QDateTimePickerType::Date;
			$this->calEnd->Required = true;
			return $this->calEnd;
		}

		/**
		 * Create and setup QLabel lblEnd
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblEnd_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblEnd = new QLabel($this->objParentObject, $strControlId);
			$this->lblEnd->Name = QApplication::Translate('End');
			$this->strEndDateTimeFormat = $strDateTimeFormat;
			$this->lblEnd->Text = sprintf($this->objProject->End) ? $this->objProject->__toString($this->strEndDateTimeFormat) : null;
			$this->lblEnd->Required = true;
			return $this->lblEnd;
		}

		protected $strEndDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objProject->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objProject->Added) ? $this->objProject->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objProject->Comment;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objProject->Comment;
			return $this->lblComment;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objProject->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objProject->Owner;
			$this->txtOwner->MaxLength = Project::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objProject->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objProject->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objProject->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objProject->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objProject->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objProject->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objProject->Status) ? AuthStatusEnum::$NameArray[$this->objProject->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Project object.
		 * @param boolean $blnReload reload Project from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objProject->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objProject->Id;
			if ($this->lblId) $this->lblId->Text = $this->objProject->Id;

			if ($this->lstProjectType) {
					$this->lstProjectType->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstProjectType->AddItem(QApplication::Translate('- Select One -'), null);
				$objProjectTypeArray = ProjectType::LoadAll();
				if ($objProjectTypeArray) foreach ($objProjectTypeArray as $objProjectType) {
					$objListItem = new QListItem($objProjectType->__toString(), $objProjectType->Id);
					if (($this->objProject->ProjectType) && ($this->objProject->ProjectType->Id == $objProjectType->Id))
						$objListItem->Selected = true;
					$this->lstProjectType->AddItem($objListItem);
				}
			}
			if ($this->lblProjectTypeId) $this->lblProjectTypeId->Text = ($this->objProject->ProjectType) ? $this->objProject->ProjectType->__toString() : null;

			if ($this->lstOrganisation) {
					$this->lstOrganisation->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstOrganisation->AddItem(QApplication::Translate('- Select One -'), null);
				$objOrganisationArray = Organisation::LoadAll();
				if ($objOrganisationArray) foreach ($objOrganisationArray as $objOrganisation) {
					$objListItem = new QListItem($objOrganisation->__toString(), $objOrganisation->Id);
					if (($this->objProject->Organisation) && ($this->objProject->Organisation->Id == $objOrganisation->Id))
						$objListItem->Selected = true;
					$this->lstOrganisation->AddItem($objListItem);
				}
			}
			if ($this->lblOrganisationId) $this->lblOrganisationId->Text = ($this->objProject->Organisation) ? $this->objProject->Organisation->__toString() : null;

			if ($this->txtName) $this->txtName->Text = $this->objProject->Name;
			if ($this->lblName) $this->lblName->Text = $this->objProject->Name;

			if ($this->calBegin) $this->calBegin->DateTime = $this->objProject->Begin;
			if ($this->lblBegin) $this->lblBegin->Text = sprintf($this->objProject->Begin) ? $this->objProject->__toString($this->strBeginDateTimeFormat) : null;

			if ($this->calEnd) $this->calEnd->DateTime = $this->objProject->End;
			if ($this->lblEnd) $this->lblEnd->Text = sprintf($this->objProject->End) ? $this->objProject->__toString($this->strEndDateTimeFormat) : null;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objProject->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objProject->Added) ? $this->objProject->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->txtComment) $this->txtComment->Text = $this->objProject->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objProject->Comment;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objProject->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objProject->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objProject->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objProject->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objProject->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objProject->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objProject->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objProject->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objProject->Status) ? AuthStatusEnum::$NameArray[$this->objProject->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC PROJECT OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Project instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveProject() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objProject->Id = $this->txtId->Text;
				if ($this->lstProjectType) $this->objProject->ProjectTypeId = $this->lstProjectType->SelectedValue;
				if ($this->lstOrganisation) $this->objProject->OrganisationId = $this->lstOrganisation->SelectedValue;
				if ($this->txtName) $this->objProject->Name = $this->txtName->Text;
				if ($this->calBegin) $this->objProject->Begin = $this->calBegin->DateTime;
				if ($this->calEnd) $this->objProject->End = $this->calEnd->DateTime;
				if ($this->calAdded) $this->objProject->Added = $this->calAdded->DateTime;
				if ($this->txtComment) $this->objProject->Comment = $this->txtComment->Text;
				if ($this->txtOwner) $this->objProject->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objProject->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objProject->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objProject->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Project object
				$this->objProject->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Project instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteProject() {
			$this->objProject->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Project': return $this->objProject;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Project fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'ProjectTypeIdControl':
					if (!$this->lstProjectType) return $this->lstProjectType_Create();
					return $this->lstProjectType;
				case 'ProjectTypeIdLabel':
					if (!$this->lblProjectTypeId) return $this->lblProjectTypeId_Create();
					return $this->lblProjectTypeId;
				case 'OrganisationIdControl':
					if (!$this->lstOrganisation) return $this->lstOrganisation_Create();
					return $this->lstOrganisation;
				case 'OrganisationIdLabel':
					if (!$this->lblOrganisationId) return $this->lblOrganisationId_Create();
					return $this->lblOrganisationId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'BeginControl':
					if (!$this->calBegin) return $this->calBegin_Create();
					return $this->calBegin;
				case 'BeginLabel':
					if (!$this->lblBegin) return $this->lblBegin_Create();
					return $this->lblBegin;
				case 'EndControl':
					if (!$this->calEnd) return $this->calEnd_Create();
					return $this->calEnd;
				case 'EndLabel':
					if (!$this->lblEnd) return $this->lblEnd_Create();
					return $this->lblEnd;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Project fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'ProjectTypeIdControl':
						return ($this->lstProjectType = QType::Cast($mixValue, 'QControl'));
					case 'OrganisationIdControl':
						return ($this->lstOrganisation = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'BeginControl':
						return ($this->calBegin = QType::Cast($mixValue, 'QControl'));
					case 'EndControl':
						return ($this->calEnd = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
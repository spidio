<?php
	/**
	 * This is the abstract Panel class for the List All functionality
	 * of the File class.  This code-generated class
	 * contains a datagrid to display an HTML page that can
	 * list a collection of File objects.  It includes
	 * functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QPanel which extends this FileListPanelBase
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent re-
	 * code generation.
	 * 
	 * @package Spidio
	 * @subpackage Drafts
	 * 
	 */
	class FileListPanel extends QPanel {
		// Local instance of the Meta DataGrid to list Files
		public $dtgFiles;

		// Other public QControls in this panel
		public $btnCreateNew;
		public $pxyEdit;

		// Callback Method Names
		protected $strSetEditPanelMethod;
		protected $strCloseEditPanelMethod;
		
		public function __construct($objParentObject, $strSetEditPanelMethod, $strCloseEditPanelMethod, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Record Method Callbacks
			$this->strSetEditPanelMethod = $strSetEditPanelMethod;
			$this->strCloseEditPanelMethod = $strCloseEditPanelMethod;

			// Setup the Template
			$this->Template = 'FileListPanel.tpl.php';

			// Instantiate the Meta DataGrid
			$this->dtgFiles = new FileDataGrid($this);

			// Style the DataGrid (if desired)
			$this->dtgFiles->CssClass = 'datagrid';
			$this->dtgFiles->AlternateRowStyle->CssClass = 'alternate';

			// Add Pagination (if desired)
			$this->dtgFiles->Paginator = new QPaginator($this->dtgFiles);
			$this->dtgFiles->ItemsPerPage = 8;

			// Use the MetaDataGrid functionality to add Columns for this datagrid

			// Create an Edit Column
			$this->pxyEdit = new QControlProxy($this);
			$this->pxyEdit->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'pxyEdit_Click'));
			$this->dtgFiles->MetaAddEditProxyColumn($this->pxyEdit, 'Edit', 'Edit');

			// Create the Other Columns (note that you can use strings for ' . DB_TABLE_PREFIX_1 . 'file's properties, or you
			// can traverse down QQN::' . DB_TABLE_PREFIX_1 . 'file() to display fields that are down the hierarchy)
			$this->dtgFiles->MetaAddColumn('Id');
			$this->dtgFiles->MetaAddColumn(QQN::File()->Recording);
			$this->dtgFiles->MetaAddColumn(QQN::File()->FileType);
			$this->dtgFiles->MetaAddColumn(QQN::File()->CodecIdVideoObject);
			$this->dtgFiles->MetaAddColumn(QQN::File()->CodecIdAudioObject);
			$this->dtgFiles->MetaAddColumn('Name');
			$this->dtgFiles->MetaAddColumn('CustomExtension');
			$this->dtgFiles->MetaAddColumn('Comment');
			$this->dtgFiles->MetaAddColumn('Added');
			$this->dtgFiles->MetaAddColumn('Changed');
			$this->dtgFiles->MetaAddColumn('Optlock');
			$this->dtgFiles->MetaAddColumn('Owner');
			$this->dtgFiles->MetaAddColumn('Group');
			$this->dtgFiles->MetaAddColumn('Perms');
			$this->dtgFiles->MetaAddTypeColumn('Status', 'AuthStatusEnum');

			// Setup the Create New button
			$this->btnCreateNew = new QButton($this);
			$this->btnCreateNew->Text = QApplication::Translate('Create a New') . ' ' . QApplication::Translate('File');
			$this->btnCreateNew->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCreateNew_Click'));
		}

		public function pxyEdit_Click($strFormId, $strControlId, $strParameter) {
			$strParameterArray = explode(',', $strParameter);
			$objEditPanel = new FileEditPanel($this, $this->strCloseEditPanelMethod, $strParameterArray[0]);

			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}

		public function btnCreateNew_Click($strFormId, $strControlId, $strParameter) {
			$objEditPanel = new FileEditPanel($this, $this->strCloseEditPanelMethod, null);
			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}
	}
?>
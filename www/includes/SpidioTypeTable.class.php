<?php

class SpidioTypeTable extends QTypeTable {
	protected $strPrefix;
	protected $intPrefixLength;
	protected $strPrefixConstant = 'DB_TABLE_PREFIX_%d';
	
	protected $intDatabaseIndex;
	
	public function __construct($strName, $strPrefix, $intDatabaseIndex) {
		$this->strPrefix = sprintf('%s_', $strPrefix);
		$this->intPrefixLength = strlen($this->strPrefix);
		$this->intDatabaseIndex = $intDatabaseIndex;
		parent::__construct($strName);
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Name':
				if (($this->intPrefixLength) &&
					(strlen($this->strName) > $this->intPrefixLength) &&
					(substr($this->strName, 0, $this->intPrefixLength - strlen($this->strName)) == $this->strPrefix) &&
					$this->intDatabaseIndex) {
					
					$strNameWithoutPrefix = substr($this->strName, $this->intPrefixLength);
					return sprintf('\' . %s . \'%s', sprintf($this->strPrefixConstant, $this->intDatabaseIndex), $strNameWithoutPrefix);
				} else {
					return $this->strName;
				}
			case 'NameWithoutPrefix':
				return $this->strName;
			case 'Prefix':
				return $this->strPrefix;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}

	public function __set($strName, $mixValue) {
		try {
			switch ($strName) {
				case 'Prefix':
					if ($this->strPrefix = QType::Cast($mixValue, QType::String)) {
						$this->intPrefixLength = strlen($this->strPrefix);
						return true;
					} else {
						return false;
					}
				default:
					return parent::__set($strName, $mixValue);
			}
		} catch (QCallerException $objExc) {
			$objExc->IncrementOffset();
			throw $objExc;
		}
	}
}

?>
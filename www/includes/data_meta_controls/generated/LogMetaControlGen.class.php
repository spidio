<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Log class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Log object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a LogMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Log $Log the actual Log data class being edited
	 * property QLabel $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $UserIdControl
	 * property-read QLabel $UserIdLabel
	 * property QListBox $LogItemEnumIdControl
	 * property-read QLabel $LogItemEnumIdLabel
	 * property QListBox $LogActionEnumIdControl
	 * property-read QLabel $LogActionEnumIdLabel
	 * property QTextBox $ItemIdControl
	 * property-read QLabel $ItemIdLabel
	 * property QDateTimePicker $DateControl
	 * property-read QLabel $DateLabel
	 * property QTextBox $SessionNameControl
	 * property-read QLabel $SessionNameLabel
	 * property QTextBox $IpAddressControl
	 * property-read QLabel $IpAddressLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class LogMetaControlGen extends QBaseClass {
		// General Variables
		protected $objLog;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Log's individual data fields
		protected $lblId;
		protected $lstUser;
		protected $lstLogItemEnum;
		protected $lstLogActionEnum;
		protected $txtItemId;
		protected $calDate;
		protected $txtSessionName;
		protected $txtIpAddress;

		// Controls that allow the viewing of Log's individual data fields
		protected $lblUserId;
		protected $lblLogItemEnumId;
		protected $lblLogActionEnumId;
		protected $lblItemId;
		protected $lblDate;
		protected $lblSessionName;
		protected $lblIpAddress;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * LogMetaControl to edit a single Log object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Log object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogMetaControl
		 * @param Log $objLog new or existing Log object
		 */
		 public function __construct($objParentObject, Log $objLog) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this LogMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Log object
			$this->objLog = $objLog;

			// Figure out if we're Editing or Creating New
			if ($this->objLog->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogMetaControl
		 * @param integer $intId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Log object creation - defaults to CreateOrEdit
 		 * @return LogMetaControl
		 */
		public static function Create($objParentObject, $intId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($intId)) {
				$objLog = Log::Load($intId);

				// Log was found -- return it!
				if ($objLog)
					return new LogMetaControl($objParentObject, $objLog);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Log object with PK arguments: ' . $intId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new LogMetaControl($objParentObject, new Log());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Log object creation - defaults to CreateOrEdit
		 * @return LogMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intId = QApplication::PathInfo(0);
			return LogMetaControl::Create($objParentObject, $intId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this LogMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Log object creation - defaults to CreateOrEdit
		 * @return LogMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intId = QApplication::QueryString('intId');
			return LogMetaControl::Create($objParentObject, $intId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			if ($this->blnEditMode)
				$this->lblId->Text = $this->objLog->Id;
			else
				$this->lblId->Text = 'N/A';
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstUser
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstUser_Create($strControlId = null) {
			$this->lstUser = new QListBox($this->objParentObject, $strControlId);
			$this->lstUser->Name = QApplication::Translate('User');
			$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
			$objUserArray = User::LoadAll();
			if ($objUserArray) foreach ($objUserArray as $objUser) {
				$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
				if (($this->objLog->User) && ($this->objLog->User->Id == $objUser->Id))
					$objListItem->Selected = true;
				$this->lstUser->AddItem($objListItem);
			}
			return $this->lstUser;
		}

		/**
		 * Create and setup QLabel lblUserId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblUserId_Create($strControlId = null) {
			$this->lblUserId = new QLabel($this->objParentObject, $strControlId);
			$this->lblUserId->Name = QApplication::Translate('User');
			$this->lblUserId->Text = ($this->objLog->User) ? $this->objLog->User->__toString() : null;
			return $this->lblUserId;
		}

		/**
		 * Create and setup QListBox lstLogItemEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstLogItemEnum_Create($strControlId = null) {
			$this->lstLogItemEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstLogItemEnum->Name = QApplication::Translate('Log Item Enum');
			$this->lstLogItemEnum->Required = true;
			foreach (LogItemEnum::$NameArray as $intId => $strValue)
				$this->lstLogItemEnum->AddItem(new QListItem($strValue, $intId, $this->objLog->LogItemEnumId == $intId));
			return $this->lstLogItemEnum;
		}

		/**
		 * Create and setup QLabel lblLogItemEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblLogItemEnumId_Create($strControlId = null) {
			$this->lblLogItemEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblLogItemEnumId->Name = QApplication::Translate('Log Item Enum');
			$this->lblLogItemEnumId->Text = ($this->objLog->LogItemEnumId) ? LogItemEnum::$NameArray[$this->objLog->LogItemEnumId] : null;
			$this->lblLogItemEnumId->Required = true;
			return $this->lblLogItemEnumId;
		}

		/**
		 * Create and setup QListBox lstLogActionEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstLogActionEnum_Create($strControlId = null) {
			$this->lstLogActionEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstLogActionEnum->Name = QApplication::Translate('Log Action Enum');
			$this->lstLogActionEnum->Required = true;
			foreach (LogActionEnum::$NameArray as $intId => $strValue)
				$this->lstLogActionEnum->AddItem(new QListItem($strValue, $intId, $this->objLog->LogActionEnumId == $intId));
			return $this->lstLogActionEnum;
		}

		/**
		 * Create and setup QLabel lblLogActionEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblLogActionEnumId_Create($strControlId = null) {
			$this->lblLogActionEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblLogActionEnumId->Name = QApplication::Translate('Log Action Enum');
			$this->lblLogActionEnumId->Text = ($this->objLog->LogActionEnumId) ? LogActionEnum::$NameArray[$this->objLog->LogActionEnumId] : null;
			$this->lblLogActionEnumId->Required = true;
			return $this->lblLogActionEnumId;
		}

		/**
		 * Create and setup QTextBox txtItemId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtItemId_Create($strControlId = null) {
			$this->txtItemId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtItemId->Name = QApplication::Translate('Item Id');
			$this->txtItemId->Text = $this->objLog->ItemId;
			$this->txtItemId->MaxLength = Log::ItemIdMaxLength;
			return $this->txtItemId;
		}

		/**
		 * Create and setup QLabel lblItemId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblItemId_Create($strControlId = null) {
			$this->lblItemId = new QLabel($this->objParentObject, $strControlId);
			$this->lblItemId->Name = QApplication::Translate('Item Id');
			$this->lblItemId->Text = $this->objLog->ItemId;
			return $this->lblItemId;
		}

		/**
		 * Create and setup QDateTimePicker calDate
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calDate_Create($strControlId = null) {
			$this->calDate = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calDate->Name = QApplication::Translate('Date');
			$this->calDate->DateTime = $this->objLog->Date;
			$this->calDate->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calDate->Required = true;
			return $this->calDate;
		}

		/**
		 * Create and setup QLabel lblDate
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblDate_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblDate = new QLabel($this->objParentObject, $strControlId);
			$this->lblDate->Name = QApplication::Translate('Date');
			$this->strDateDateTimeFormat = $strDateTimeFormat;
			$this->lblDate->Text = sprintf($this->objLog->Date) ? $this->objLog->__toString($this->strDateDateTimeFormat) : null;
			$this->lblDate->Required = true;
			return $this->lblDate;
		}

		protected $strDateDateTimeFormat;

		/**
		 * Create and setup QTextBox txtSessionName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtSessionName_Create($strControlId = null) {
			$this->txtSessionName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtSessionName->Name = QApplication::Translate('Session Name');
			$this->txtSessionName->Text = $this->objLog->SessionName;
			$this->txtSessionName->Required = true;
			$this->txtSessionName->MaxLength = Log::SessionNameMaxLength;
			return $this->txtSessionName;
		}

		/**
		 * Create and setup QLabel lblSessionName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblSessionName_Create($strControlId = null) {
			$this->lblSessionName = new QLabel($this->objParentObject, $strControlId);
			$this->lblSessionName->Name = QApplication::Translate('Session Name');
			$this->lblSessionName->Text = $this->objLog->SessionName;
			$this->lblSessionName->Required = true;
			return $this->lblSessionName;
		}

		/**
		 * Create and setup QTextBox txtIpAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtIpAddress_Create($strControlId = null) {
			$this->txtIpAddress = new QTextBox($this->objParentObject, $strControlId);
			$this->txtIpAddress->Name = QApplication::Translate('Ip Address');
			$this->txtIpAddress->Text = $this->objLog->IpAddress;
			$this->txtIpAddress->Required = true;
			$this->txtIpAddress->MaxLength = Log::IpAddressMaxLength;
			return $this->txtIpAddress;
		}

		/**
		 * Create and setup QLabel lblIpAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblIpAddress_Create($strControlId = null) {
			$this->lblIpAddress = new QLabel($this->objParentObject, $strControlId);
			$this->lblIpAddress->Name = QApplication::Translate('Ip Address');
			$this->lblIpAddress->Text = $this->objLog->IpAddress;
			$this->lblIpAddress->Required = true;
			return $this->lblIpAddress;
		}



		/**
		 * Refresh this MetaControl with Data from the local Log object.
		 * @param boolean $blnReload reload Log from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objLog->Reload();

			if ($this->lblId) if ($this->blnEditMode) $this->lblId->Text = $this->objLog->Id;

			if ($this->lstUser) {
					$this->lstUser->RemoveAllItems();
				$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
				$objUserArray = User::LoadAll();
				if ($objUserArray) foreach ($objUserArray as $objUser) {
					$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
					if (($this->objLog->User) && ($this->objLog->User->Id == $objUser->Id))
						$objListItem->Selected = true;
					$this->lstUser->AddItem($objListItem);
				}
			}
			if ($this->lblUserId) $this->lblUserId->Text = ($this->objLog->User) ? $this->objLog->User->__toString() : null;

			if ($this->lstLogItemEnum) $this->lstLogItemEnum->SelectedValue = $this->objLog->LogItemEnumId;
			if ($this->lblLogItemEnumId) $this->lblLogItemEnumId->Text = ($this->objLog->LogItemEnumId) ? LogItemEnum::$NameArray[$this->objLog->LogItemEnumId] : null;

			if ($this->lstLogActionEnum) $this->lstLogActionEnum->SelectedValue = $this->objLog->LogActionEnumId;
			if ($this->lblLogActionEnumId) $this->lblLogActionEnumId->Text = ($this->objLog->LogActionEnumId) ? LogActionEnum::$NameArray[$this->objLog->LogActionEnumId] : null;

			if ($this->txtItemId) $this->txtItemId->Text = $this->objLog->ItemId;
			if ($this->lblItemId) $this->lblItemId->Text = $this->objLog->ItemId;

			if ($this->calDate) $this->calDate->DateTime = $this->objLog->Date;
			if ($this->lblDate) $this->lblDate->Text = sprintf($this->objLog->Date) ? $this->objLog->__toString($this->strDateDateTimeFormat) : null;

			if ($this->txtSessionName) $this->txtSessionName->Text = $this->objLog->SessionName;
			if ($this->lblSessionName) $this->lblSessionName->Text = $this->objLog->SessionName;

			if ($this->txtIpAddress) $this->txtIpAddress->Text = $this->objLog->IpAddress;
			if ($this->lblIpAddress) $this->lblIpAddress->Text = $this->objLog->IpAddress;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC LOG OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Log instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveLog() {
			try {
				// Update any fields for controls that have been created
				if ($this->lstUser) $this->objLog->UserId = $this->lstUser->SelectedValue;
				if ($this->lstLogItemEnum) $this->objLog->LogItemEnumId = $this->lstLogItemEnum->SelectedValue;
				if ($this->lstLogActionEnum) $this->objLog->LogActionEnumId = $this->lstLogActionEnum->SelectedValue;
				if ($this->txtItemId) $this->objLog->ItemId = $this->txtItemId->Text;
				if ($this->calDate) $this->objLog->Date = $this->calDate->DateTime;
				if ($this->txtSessionName) $this->objLog->SessionName = $this->txtSessionName->Text;
				if ($this->txtIpAddress) $this->objLog->IpAddress = $this->txtIpAddress->Text;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Log object
				$this->objLog->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Log instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteLog() {
			$this->objLog->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Log': return $this->objLog;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Log fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'UserIdControl':
					if (!$this->lstUser) return $this->lstUser_Create();
					return $this->lstUser;
				case 'UserIdLabel':
					if (!$this->lblUserId) return $this->lblUserId_Create();
					return $this->lblUserId;
				case 'LogItemEnumIdControl':
					if (!$this->lstLogItemEnum) return $this->lstLogItemEnum_Create();
					return $this->lstLogItemEnum;
				case 'LogItemEnumIdLabel':
					if (!$this->lblLogItemEnumId) return $this->lblLogItemEnumId_Create();
					return $this->lblLogItemEnumId;
				case 'LogActionEnumIdControl':
					if (!$this->lstLogActionEnum) return $this->lstLogActionEnum_Create();
					return $this->lstLogActionEnum;
				case 'LogActionEnumIdLabel':
					if (!$this->lblLogActionEnumId) return $this->lblLogActionEnumId_Create();
					return $this->lblLogActionEnumId;
				case 'ItemIdControl':
					if (!$this->txtItemId) return $this->txtItemId_Create();
					return $this->txtItemId;
				case 'ItemIdLabel':
					if (!$this->lblItemId) return $this->lblItemId_Create();
					return $this->lblItemId;
				case 'DateControl':
					if (!$this->calDate) return $this->calDate_Create();
					return $this->calDate;
				case 'DateLabel':
					if (!$this->lblDate) return $this->lblDate_Create();
					return $this->lblDate;
				case 'SessionNameControl':
					if (!$this->txtSessionName) return $this->txtSessionName_Create();
					return $this->txtSessionName;
				case 'SessionNameLabel':
					if (!$this->lblSessionName) return $this->lblSessionName_Create();
					return $this->lblSessionName;
				case 'IpAddressControl':
					if (!$this->txtIpAddress) return $this->txtIpAddress_Create();
					return $this->txtIpAddress;
				case 'IpAddressLabel':
					if (!$this->lblIpAddress) return $this->lblIpAddress_Create();
					return $this->lblIpAddress;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Log fields
					case 'IdControl':
						return ($this->lblId = QType::Cast($mixValue, 'QControl'));
					case 'UserIdControl':
						return ($this->lstUser = QType::Cast($mixValue, 'QControl'));
					case 'LogItemEnumIdControl':
						return ($this->lstLogItemEnum = QType::Cast($mixValue, 'QControl'));
					case 'LogActionEnumIdControl':
						return ($this->lstLogActionEnum = QType::Cast($mixValue, 'QControl'));
					case 'ItemIdControl':
						return ($this->txtItemId = QType::Cast($mixValue, 'QControl'));
					case 'DateControl':
						return ($this->calDate = QType::Cast($mixValue, 'QControl'));
					case 'SessionNameControl':
						return ($this->txtSessionName = QType::Cast($mixValue, 'QControl'));
					case 'IpAddressControl':
						return ($this->txtIpAddress = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
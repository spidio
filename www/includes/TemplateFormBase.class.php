	<?php

abstract class TemplateFormBase extends QForm {
	protected $objInfo;
	
	public function __construct() {
		$objReflection = new ReflectionClass('TemplateForm');
		if (strpos($objReflection->getFileName(), __TEMPLATES__) !== false) {
			require_once sprintf('%s/template_info.php', dirname($objReflection->getFileName()));
			$this->objInfo = new TemplateInfo($this);
		}
	}
	
	protected function Render() {
		require('header.inc.php');
		parent::Render();
		require('footer.inc.php');
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Info': return $this->objInfo;
			
			default:
				try {
					if (strpos($strName, 'Config') !== false) {
						$strName = sprintf('str%s', $strName);
						if (property_exists($this, $strName)) {
							return $this->$strName;
						} else {
							return null;
						}
					}
					
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		if (strpos($strName, 'Config') !== false) {
			$this->$strName = $mixValue;
		} else {
			parent::__set($strName, $mixValue);
		}
	}
}

?>
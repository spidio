<?php
	/**
	 * This is the abstract Panel class for the List All functionality
	 * of the Codec class.  This code-generated class
	 * contains a datagrid to display an HTML page that can
	 * list a collection of Codec objects.  It includes
	 * functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QPanel which extends this CodecListPanelBase
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent re-
	 * code generation.
	 * 
	 * @package Spidio
	 * @subpackage Drafts
	 * 
	 */
	class CodecListPanel extends QPanel {
		// Local instance of the Meta DataGrid to list Codecs
		public $dtgCodecs;

		// Other public QControls in this panel
		public $btnCreateNew;
		public $pxyEdit;

		// Callback Method Names
		protected $strSetEditPanelMethod;
		protected $strCloseEditPanelMethod;
		
		public function __construct($objParentObject, $strSetEditPanelMethod, $strCloseEditPanelMethod, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Record Method Callbacks
			$this->strSetEditPanelMethod = $strSetEditPanelMethod;
			$this->strCloseEditPanelMethod = $strCloseEditPanelMethod;

			// Setup the Template
			$this->Template = 'CodecListPanel.tpl.php';

			// Instantiate the Meta DataGrid
			$this->dtgCodecs = new CodecDataGrid($this);

			// Style the DataGrid (if desired)
			$this->dtgCodecs->CssClass = 'datagrid';
			$this->dtgCodecs->AlternateRowStyle->CssClass = 'alternate';

			// Add Pagination (if desired)
			$this->dtgCodecs->Paginator = new QPaginator($this->dtgCodecs);
			$this->dtgCodecs->ItemsPerPage = 8;

			// Use the MetaDataGrid functionality to add Columns for this datagrid

			// Create an Edit Column
			$this->pxyEdit = new QControlProxy($this);
			$this->pxyEdit->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'pxyEdit_Click'));
			$this->dtgCodecs->MetaAddEditProxyColumn($this->pxyEdit, 'Edit', 'Edit');

			// Create the Other Columns (note that you can use strings for ' . DB_TABLE_PREFIX_1 . 'codec's properties, or you
			// can traverse down QQN::' . DB_TABLE_PREFIX_1 . 'codec() to display fields that are down the hierarchy)
			$this->dtgCodecs->MetaAddColumn('Id');
			$this->dtgCodecs->MetaAddColumn('Name');
			$this->dtgCodecs->MetaAddTypeColumn('CodecEnumId', 'CodecEnum');
			$this->dtgCodecs->MetaAddColumn('Fourcc');
			$this->dtgCodecs->MetaAddColumn('Optlock');
			$this->dtgCodecs->MetaAddColumn('Owner');
			$this->dtgCodecs->MetaAddColumn('Group');
			$this->dtgCodecs->MetaAddColumn('Perms');
			$this->dtgCodecs->MetaAddTypeColumn('Status', 'AuthStatusEnum');

			// Setup the Create New button
			$this->btnCreateNew = new QButton($this);
			$this->btnCreateNew->Text = QApplication::Translate('Create a New') . ' ' . QApplication::Translate('Codec');
			$this->btnCreateNew->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCreateNew_Click'));
		}

		public function pxyEdit_Click($strFormId, $strControlId, $strParameter) {
			$strParameterArray = explode(',', $strParameter);
			$objEditPanel = new CodecEditPanel($this, $this->strCloseEditPanelMethod, $strParameterArray[0]);

			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}

		public function btnCreateNew_Click($strFormId, $strControlId, $strParameter) {
			$objEditPanel = new CodecEditPanel($this, $this->strCloseEditPanelMethod, null);
			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}
	}
?>
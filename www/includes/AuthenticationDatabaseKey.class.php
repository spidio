<?php

interface AuthenticationDatabaseKey {
	function KeyLookup();
	function KeyRegister();
	function KeyDestroy();
	function KeyDestroyAll();
	function KeyGarbageCollect();
	function KeyGetName();
	function KeyExists();
}

?>
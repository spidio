<?php

abstract class SpidioAuth extends AuthenticationBase implements AuthenticationDatabaseSession, AuthenticationDatabaseKey {
	/**
	 * Session representation within Auth system.
	 *
	 * @var Session
	 */
	protected static $objSession;
	
	/**
	 * SessionKey representation within Auth system.
	 *
	 * @var SessionKey
	 */
	protected static $objSessionKey;
	
	/**
	 * Constructor.
	 *
	 */
	public function __construct() {
		// Set authentication scheme.
		$this->AuthenticationType = AuthenticationType::Sha1;
		
		// Load Session.
		$this->SessionLoad();
		
		// Start Session.
		$this->SessionStart();
		
		// Collect garbage for Sessions.
		$this->SessionGarbageCollect();
		
		// Lookup SessionKey.
		$this->KeyLookup();
		
		// Cleanup SessionKey table.
		$this->KeyGarbageCollect();
	}
	
	/**
	 * Return current User.
	 *
	 * @return User
	 */
	protected function GetUser() {
		return self::$objSession->User;
	}
	
	/**
	 * Check if user is logged in.
	 *
	 * @return bool
	 */
	public function IsLoggedIn() {
		return !is_null(self::$objSession->User);
	}
	
	/**
	 * Reset current Session.
	 *
	 */
	public function ResetSession() {
		$this->AddLogEntry(LogItemEnum::SESSIONRESET);
		if ($this->IsLoggedIn()) {
			$this->AddLogEntry(LogItemEnum::USERLOGOUTFORCE);
		}
		
		$this->SessionDestroy();
	}
	
	/**
	 * Check if user $strName exists.
	 *
	 * @param string $strName
	 * @return bool
	 */
	public function UserExists($strName) {
		return !is_null(User::LoadByName(ucfirst(strtolower($strName))));
	}
	
	/**
	 * Add Log entry.
	 *
	 * @param int $intLogItemEnum constant of LogItemEnum
	 * @param UUID $uuidItemId UUID object
	 */
	protected function AddLogEntry($intLogItemEnum, UUID $uuidItemId = null) {
		$objLog = new Log();
		
		if ($intLogItemEnum != LogItemEnum::SESSIONCREATED && $intLogItemEnum != LogItemEnum::SESSIONTIMEOUT) {
			$objLog->UserId = self::$objSession->UserId;
		} else {
			$objLog->UserId = null;
		}
		
		$objLog->LogItemEnumId = $intLogItemEnum;
		$objLog->LogActionEnumId = LogActionEnum::NUL;
		$objLog->Date = new QDateTime(QDateTime::Now);
		$objLog->ItemId = (is_null($uuidItemId)) ? null : (string) $uuidItemId;
		$objLog->SessionName = (string) $this->SessionGetName();
		$objLog->IpAddress = (string) $this->UserIpAddress();
		$objLog->Save();
	}
	
	abstract public function SessionLoad();	
	
	/**
	 * Register Session or update Session.
	 *
	 */
	public function SessionStart() {
		if (method_exists($this, 'SessionStartHookBegin')) {
			$this->SessionStartHookBegin();
		}
		
		// Register when no session is registered, otherwise update.
		if (is_null(self::$objSession)) {
			$this->SessionRegister();
		} else {
			$this->SessionUpdate();
		}
		
		if (method_exists($this, 'SessionStartHookEnd')) {
			$this->SessionStartHookEnd();
		}
	}
	
	/**
	 * Register Session for first time.
	 *
	 */
	protected function SessionRegister() {
		if (method_exists($this, 'SessionRegisterHookBegin')) {
			$this->SessionRegisterHookBegin();
		}
		
		// Create new Session.
		self::$objSession = new Session();
		
		// Fill fields with data.
		do {
			self::$objSession->Id = (string) new UUID();
		} while (Session::QueryCount(QQ::Equal(QQN::Session()->Id, self::$objSession->Id)) > 0);
		
		self::$objSession->UserId = null;
		self::$objSession->IpAddress = (string) $this->UserIpAddress();
		self::$objSession->AutoLogin = false;
		
		// Save the new Session.
		self::$objSession->Save();
		
		// Reload to load self::$objSession->Added value.
		self::$objSession->Reload();
		
		// Save Cookie
		$this->SetCookie('sid', self::$objSession->Id);
		
		// Add Log entry.
		$this->AddLogEntry(LogItemEnum::SESSIONCREATED);
				
		if (method_exists($this, 'SessionRegisterHookEnd')) {
			$this->SessionRegisterHookEnd();
		}
	}
	
	/**
	 * Update Session with fresh data.
	 *
	 */
	protected function SessionUpdate() {
		if (method_exists($this, 'SessionUpdateHookBegin')) {
			$this->SessionUpdateHookBegin();
		}
		
		// Set current IP address.
		self::$objSession->IpAddress = (string) $this->UserIpAddress();
		
		// save update.
		self::$objSession->Save();
		
		if (method_exists($this, 'SessionUpdateHookEnd')) {
			$this->SessionUpdateHookEnd();
		}
	}
	
	/**
	 * Register User to Session.
	 *
	 * @param User $objUser
	 */
	public function SessionRegisterUser(User $objUser) {
		if (method_exists($this, 'SessionRegisterUserHookBegin')) {
			$this->SessionRegisterUserHookBegin();
		}
		
		// Assign User object to Session.
		self::$objSession->User = $objUser;
		
		// Save Session.
		self::$objSession->Save();
		
		// Update i18n information
		$this->UpdateLanguage($objUser->LanguageCode, $objUser->CountryCode);
		
		if (method_exists($this, 'SessionRegisterUserHookEnd')) {
			$this->SessionRegisterUserHookEnd();
		}
	}
	
	/**
	 * Unregister User from Session.
	 *
	 */
	public function SessionUnRegisterUser() {
		if (method_exists($this, 'SessionUnRegisterUserHookBegin')) {
			$this->SessionUnRegisterUserHookBegin();
		}
		
		// Kill SessionKey related to this Session.
		$this->KeyDestroy();
		
		// Nullify User object this Session.
		self::$objSession->User = null;
		
		// Save Session.
		self::$objSession->Save();
		
		if (method_exists($this, 'SessionUnRegisterUserHookEnd')) {
			$this->SessionUnRegisterUserHookEnd();
		}
	}
	
	/**
	 * Delete Session.
	 *
	 */
	public function SessionDestroy() {
		if (method_exists($this, 'SessionDestroyHookBegin')) {
			$this->SessionDestroyHookBegin();
		}
		
		if (!is_null(self::$objSession->UserId)) {
			// Logout User.
			$this->SessionUnRegisterUser();
		}
		
		// Delete Session.
		self::$objSession->Delete();
		
		if (method_exists($this, 'SessionDestroyHookEnd')) {
			$this->SessionDestroyHookEnd();
		}
	}
	
	/**
	 * Delete old sessions from database.
	 *
	 */
	public function SessionGarbageCollect() {
		if (method_exists($this, 'SessionGarbageCollectHookBegin')) {
			$this->SessionGarbageCollectHookBegin();
		}
		
		// Define timeout time.
		$dttExpire = new QDateTime(QDateTime::Now);
		$dttExpire->AddSeconds(-$this->SessionGetTimeout());
		
		// Query table and delete the first 10 timed out sessions, delete only when there is no SessionKey assigned (checked via reverse relationship).
		foreach (Session::QueryArray(
			QQ::AndCondition(
				QQ::LessThan(QQN::Session()->Changed, $dttExpire),
				QQ::IsNull(QQN::Session()->SessionKeyAsSession->Id)
			),
			QQ::Clause(
				QQ::LimitInfo(10)
			)
		) as $objSession) {
			$objSession->Delete();
			$this->AddLogEntry(LogItemEnum::SESSIONTIMEOUT);
		}
		
		if (method_exists($this, 'SessionGarbageCollectHookEnd')) {
			$this->SessionGarbageCollectHookEnd();
		}
	}
	
	/**
	 * Get Session timeout time in seconds.
	 *
	 * @return int
	 */
	protected function SessionGetTimeout() {
		return $this->GetConfig('session_timeout', QType::Integer);
	}
	
	/**
	 * Return Session Id.
	 *
	 * @return UUID
	 */
	public function SessionGetName() {
		return new UUID(self::$objSession->Id);
	}
	
	/**
	 * Check if Session exists.
	 *
	 * @param string $strSessionId
	 * @return bool
	 */
	public function SessionExists($strSessionId) {
		return !is_null(Session::Load($strSessionId));
	}
	
	/**
	 * Check if Session exists and an User is logged.
	 *
	 * @param string $strSessionId
	 * @return bool
	 */
	public function SessionIsValid($strSessionId) {
		if (is_null($strSessionId)) {
			return false;
		} else {
			return !is_null(Session::Load($strSessionId)->UserId);
		}
	}
	
	/**
	 * Lookup key for Session.
	 *
	 */
	public function KeyLookup() {
		if (method_exists($this, 'KeyLookupHookBegin')) {
			$this->KeyLookupHookBegin();
		}
		
		// Load SessionKey from database
		if ($this->KeyExists()) {
			// Update SessionKey.
			self::$objSessionKey = SessionKey::Load($this->GetCookie('kid'));
			$this->KeyUpdate();
		}
		
		if (method_exists($this, 'KeyLookupHookEnd')) {
			$this->KeyLookupHookEnd();
		}
	}
	
	/**
	 * Register SessionKey for Session.
	 *
	 */
	public function KeyRegister() {
		if (method_exists($this, 'KeyRegisterHookBegin')) {
			$this->KeyRegisterHookBegin();
		}
		
		if (!SessionKey::LoadBySessionId(self::$objSession->Id)) {
			// Fill fields with data
			self::$objSessionKey = new SessionKey();
			do {
				self::$objSessionKey->Id = (string) new UUID();
			} while (Session::QueryCount(QQ::Equal(QQN::Session()->Id, self::$objSessionKey->Id)) > 0);
			
			self::$objSessionKey->User = self::$objSession->User;
			self::$objSessionKey->Session = self::$objSession;
			self::$objSessionKey->IpAddress = self::$objSession->IpAddress;
			
			// Save new SessionKey
			self::$objSessionKey->Save();
			
			// Update AutoLogin flag.
			self::$objSession->AutoLogin = true;
			
			// Save Session changed session fields.
			self::$objSession->Save();

			// Save Cookie.
			$dttExpire = new QDateTime(QDateTime::Now);
			$dttExpire->AddSeconds($this->GetConfig('sessionkey_timeout', QType::Integer));
			$this->SetCookie('kid', self::$objSessionKey->Id, $dttExpire);
		}
		
		if (method_exists($this, 'KeyDeleteHookEnd')) {
			$this->KeyDeleteHookEnd();
		}
	}
	
	/**
	 * Update SessionKey with fresh data.
	 *
	 */
	protected function KeyUpdate() {
		if (method_exists($this, 'KeyUpdateHookBegin')) {
			$this->KeyUpdateHookBegin();
		}
		
		$dttExpire = new QDateTime(QDateTime::Now);
		$dttExpire->AddSeconds(-$this->KeyGetChange());
		
		// Update key id when item is longer than config value for session id changing.
		if ($dttExpire->IsLaterThan(self::$objSessionKey->Changed)) {
			// Generate SessionKey id.
			do {
				self::$objSessionKey->Id = (string) new UUID();
			} while (SessionKey::QueryCount(QQ::Equal(QQN::SessionKey()->Id, self::$objSessionKey->Id)));
			
			// Save Cookie.
			$dttExpire = new QDateTime(QDateTime::Now);
			$dttExpire->AddSeconds($this->GetConfig('sessionkey_timeout', QType::Integer));
			$this->SetCookie('kid', self::$objSessionKey->Id, $dttExpire);
		}
		
		// Update Session in SessionKey when there is another Session for that SessionKey based on the database.
		if (self::$objSessionKey->Session->Id != $this->SessionGetName()) {
			self::$objSessionKey->Session = self::$objSession;
			
			self::$objSession->UserId = self::$objSessionKey->UserId;
			self::$objSession->AutoLogin = true;
			self::$objSession->Save();
			
			$this->AddLogEntry(LogItemEnum::USERLOGIN, new UUID(self::$objSessionKey->Id));
		}
		
		// Update IP address of SessionKey.
		self::$objSessionKey->IpAddress = self::$objSession->IpAddress;
		
		// Force update SessionKey because it is possible that the row is modified by another request from the same browser.
		self::$objSessionKey->Save(false, true);
		
		// Log in Session with user from SessionKey
		$this->SessionRegisterUser(self::$objSessionKey->User);		
		
		if (method_exists($this, 'KeyUpdateHookEnd')) {
			$this->KeyUpdateHookEnd();
		}
	}
	
	/**
	 * Delete SessionKey.
	 *
	 */
	public function KeyDestroy() {
		if (method_exists($this, 'KeyDeleteHookBegin')) {
			$this->KeyDeleteHookBegin();
		}
		
		// Delete SessionKey.
		if (!is_null(self::$objSessionKey)) {
			self::$objSessionKey->Delete();
		}
		
		if (method_exists($this, 'KeyDeleteHookEnd')) {
			$this->KeyDeleteHookEnd();
		}
	}
	
	/**
	 * Delete all sessionkeys for this User.
	 *
	 */
	public function KeyDestroyAll() {
		if (method_exists($this, 'KeyDeleteAllHookBegin')) {
			$this->KeyDeleteAllHookBegin();
		}
		
		$this->KeyDestroy();
		
		foreach (SessionKey::LoadArrayByUserId(self::$objSession->UserId) as $objSessionKey) {
			$objSessionKey->Delete();
		}
		
		if (method_exists($this, 'KeyDestroyAllHookEnd')) {
			$this->KeyDestroyAllHookEnd();
		}
	}
	
	/**
	 * Collect garbage of SessionKeyytable.
	 *
	 */
	public function KeyGarbageCollect() {
		if (method_exists($this, 'KeyGarbageCollectHookBegin')) {
			$this->KeyGarbageCollectHookBegin();
		}
		
		// Define timeout time.
		$dttExpire = new QDateTime(QDateTime::Now);
		$dttExpire->AddSeconds(-$this->KeyGetTimeout());
		
		// Query table and delete the first 10 timed out sessionkeys.
		foreach (SessionKey::QueryArray(
			QQ::LessThan(QQN::SessionKey()->Changed, $dttExpire),
			QQ::Clause(
				QQ::LimitInfo(10)
			)
		) as $objSessionKey) {
			$objSessionKey->Delete();
		}
		
		if (method_exists($this, 'KeyGarbageCollectHookEnd')) {
			$this->KeyGarbageCollectHookEnd();
		}
	}
	
	/**
	 * Return SessionKey Id.
	 *
	 * @return UUID
	 */
	public function KeyGetName() {
		return new UUID(self::$objSessionKey->Id);
	}
	
	/**
	 * Check if SessionKey exists.
	 *
	 * @return bool
	 */
	public function KeyExists() {
		if ($this->ExistCookie('kid')) {
			return (SessionKey::Load($this->GetCookie('kid')));
		} else {
			return false;
		}
	}
	
	/**
	 * Get SessionKey timeout time in seconds.
	 *
	 * @return int
	 */
	protected function KeyGetChange() {
		return $this->GetConfig('sessionkey_change', QType::Integer);
	}
	
	/**
	 * Get SessionKey timeout time in seconds.
	 *
	 * @return int
	 */
	protected function KeyGetTimeout() {
		return $this->GetConfig('sessionkey_timeout', QType::Integer);
	}
	
	/**
	 * Get Cookie value.
	 *
	 * @param string $strName
	 * @return string
	 */
	protected function GetCookie($strName) {
		$objCookie = CookieStore::UpdateCookie(sprintf('%s_%s', strtolower($this->GetConfig('software_coresystem_name')), $strName));
		return $objCookie->Value;
	}
	
	/**
	 * Check if Cookie exists.
	 *
	 * @param string $strName
	 * @return bool
	 */
	protected function ExistCookie($strName) {
		return CookieStore::ExistCookie(sprintf('%s_%s', strtolower($this->GetConfig('software_coresystem_name')), $strName));
	}
	
	/**
	 * Set or update cookie.
	 *
	 * @param string $strName
	 * @param string $strValue
	 * @param null|QDateTime $dttExpire
	 */
	protected function SetCookie($strName, $strValue, $dttExpire = null) {
		$objCookie = CookieStore::UpdateCookie(sprintf('%s_%s', strtolower($this->GetConfig('software_coresystem_name')), $strName));
		$objCookie->Value = $strValue;
		$objCookie->Path = sprintf('%s%s/', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__);
		
		if (!is_null($dttExpire)) {
			$objCookie->Expire = $dttExpire;
		}
		
		if (method_exists($this, 'SetCookieHook')) {
			$this->SetCookieHook($objCookie);
		}
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Session': return self::$objSession;
			case 'SessionKey': return self::$objSessionKey;
			case 'SessionId': return $this->SessionGetName();
			case 'SessionKeyId': return $this->KeyGetName();
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
			case 'Session':
				try {
					return (self::$objSession = QType::Cast($mixValue, 'Session'));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'SessionKey':
				try {
					return (self::$objSessionKey = QType::Cast($mixValue, 'SessionKey'));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
				
			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
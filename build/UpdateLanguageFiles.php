<?php

$objDirectoryIterator = new DirectoryIterator('./../www/includes/qcodo/i18n/');
while ($objDirectoryIterator->valid()) {
	if (preg_match('/^[a-z]{2}(_[a-z]{2})?.po$/', $objDirectoryIterator->getFilename())) {
		echo 'Updating: ' . $objDirectoryIterator->getFilename() . PHP_EOL;
		@exec('msgmerge ' . $objDirectoryIterator->getPathname() . ' ./../www/includes/qcodo/i18n/messages.pot -U --no-wrap -N -q');
	}
	
	$objDirectoryIterator->next();
}

?>
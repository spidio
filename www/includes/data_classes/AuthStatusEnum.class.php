<?php
	require(__DATAGEN_CLASSES__ . '/AuthStatusEnumGen.class.php');

	/**
	 * The AuthStatusEnum class defined here contains any
	 * customized code for the AuthStatusEnum enumerated type. 
	 * 
	 * It represents the enumerated values found in the "spidio_auth_status_enum" table in the database,
	 * and extends from the code generated abstract AuthStatusEnumGen
	 * class, which contains all the values extracted from the database.
	 * 
	 * Type classes which are generally used to attach a type to data object.
	 * However, they may be used as simple database indepedant enumerated type.
	 * 
	 * @package Spidio
	 * @subpackage DataObjects
	 */
	abstract class AuthStatusEnum extends AuthStatusEnumGen {
		public static function ToString($intStatusTypesId) {
			switch ($intStatusTypesId) {
				case 0: return QApplication::Translate('Created');
				case 1: return QApplication::Translate('Active');
				case 2: return QApplication::Translate('Inactive');
				case 4: return QApplication::Translate('Pending');
				case 8: return QApplication::Translate('Deleted');
				case 16: return QApplication::Translate('Cancelled');
				case 32: return QApplication::Translate('Received');
				case 64: return QApplication::Translate('Sent');
				default:
					return parent::ToString($intStatusTypesId);
			}
		}
	}
?>
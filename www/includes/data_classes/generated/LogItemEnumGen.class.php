<?php
	/**
	 * The LogItemEnum class defined here contains
	 * code for the LogItemEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_log_item_enum" table
	 * in the database.
	 * 
	 * To use, you should use the LogItemEnum subclass which
	 * extends this LogItemEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the LogItemEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class LogItemEnumGen extends QBaseClass {
		const USERLOGIN = 1;
		const USERLOGOUT = 2;
		const SESSIONTIMEOUT = 3;
		const SESSIONCREATED = 4;
		const USERLOGOUTFORCE = 5;
		const ORGANISATION = 6;
		const PROJECT = 7;
		const TAPE = 8;
		const SESSIONRESET = 9;

		const MaxId = 9;

		public static $NameArray = array(
			1 => 'USERLOGIN',
			2 => 'USERLOGOUT',
			3 => 'SESSIONTIMEOUT',
			4 => 'SESSIONCREATED',
			5 => 'USERLOGOUTFORCE',
			6 => 'ORGANISATION',
			7 => 'PROJECT',
			8 => 'TAPE',
			9 => 'SESSIONRESET');

		public static $TokenArray = array(
			1 => 'USERLOGIN',
			2 => 'USERLOGOUT',
			3 => 'SESSIONTIMEOUT',
			4 => 'SESSIONCREATED',
			5 => 'USERLOGOUTFORCE',
			6 => 'ORGANISATION',
			7 => 'PROJECT',
			8 => 'TAPE',
			9 => 'SESSIONRESET');

		public static $ReverseNameArray = array(
			'USERLOGIN' => 1,
			'USERLOGOUT' => 2,
			'SESSIONTIMEOUT' => 3,
			'SESSIONCREATED' => 4,
			'USERLOGOUTFORCE' => 5,
			'ORGANISATION' => 6,
			'PROJECT' => 7,
			'TAPE' => 8,
			'SESSIONRESET' => 9);

		public static $ReverseTokenArray = array(
			'USERLOGIN' => 1,
			'USERLOGOUT' => 2,
			'SESSIONTIMEOUT' => 3,
			'SESSIONCREATED' => 4,
			'USERLOGOUTFORCE' => 5,
			'ORGANISATION' => 6,
			'PROJECT' => 7,
			'TAPE' => 8,
			'SESSIONRESET' => 9);

		public static function ToString($intLogItemEnumId) {
			switch ($intLogItemEnumId) {
				case 1: return 'USERLOGIN';
				case 2: return 'USERLOGOUT';
				case 3: return 'SESSIONTIMEOUT';
				case 4: return 'SESSIONCREATED';
				case 5: return 'USERLOGOUTFORCE';
				case 6: return 'ORGANISATION';
				case 7: return 'PROJECT';
				case 8: return 'TAPE';
				case 9: return 'SESSIONRESET';
				default:
					throw new QCallerException(sprintf('Invalid intLogItemEnumId: %s', $intLogItemEnumId));
			}
		}

		public static function ToToken($intLogItemEnumId) {
			switch ($intLogItemEnumId) {
				case 1: return 'USERLOGIN';
				case 2: return 'USERLOGOUT';
				case 3: return 'SESSIONTIMEOUT';
				case 4: return 'SESSIONCREATED';
				case 5: return 'USERLOGOUTFORCE';
				case 6: return 'ORGANISATION';
				case 7: return 'PROJECT';
				case 8: return 'TAPE';
				case 9: return 'SESSIONRESET';
				default:
					throw new QCallerException(sprintf('Invalid intLogItemEnumId: %s', $intLogItemEnumId));
			}
		}

	}
?>
<?php

class SoapidioRequestPanel extends QPanel {
	protected $strFunctionDeclaration;
	protected $strFunctionName;
	protected $strFunctionArgumentsArray;
	
	protected $objControlsArray;
	
	/**
	 * Constructor for this control
	 * @param mixed $objParentObject Parent QForm or QControl that is responsible for rendering this control
	 * @param string $strControlId optional control ID
	 */
	public function __construct($objParentObject, $strControlId = null) {
		try {
			parent::__construct($objParentObject, $strControlId);
		} catch (QCallerException $objExc) { $objExc->IncrementOffset(); throw $objExc; }
		
		$this->strCssClass = 'RequestPanel';
	}
	
	/**
	 * Set Function declaration got from PHP Soap client.
	 *
	 * @param string $strFunctionDeclaration
	 */
	public function SetFunctionDeclaration($strFunctionDeclaration) {
		$this->strFunctionDeclaration = $strFunctionDeclaration;
		$this->ResetControls();
		
		if (!is_null($strFunctionDeclaration)) {
			$this->GenerateControlData();
		}
	}
	
	/**
	 * Delete all child controls and refresh control.
	 *
	 */
	public function ResetControls() {
		$this->RemoveChildControls(true);
		$this->objControlsArray = array();
		$this->Refresh();
	}
	/**
	
	 * Generate the required controls for this FunctionDeclaration.
	 *
	 */
	protected function GenerateControlData() {
		$intArgumentsBegin = strpos($this->strFunctionDeclaration, '(') + 1;
		$intArgumentsLength = strpos($this->strFunctionDeclaration, ')') - $intArgumentsBegin;
		$strArguments = substr($this->strFunctionDeclaration, $intArgumentsBegin, $intArgumentsLength);
		$this->strFunctionArgumentsArray = explode(', ', $strArguments);
		
		$intFunctionBegin = strpos($this->strFunctionDeclaration, ' ') + 1;
		$intFunctionLength = $intArgumentsBegin - $intFunctionBegin - 1;
		$this->strFunctionName = substr($this->strFunctionDeclaration, $intFunctionBegin, $intFunctionLength);
		
		$btnRequest = new QButton($this);
		$btnRequest->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnRequest_Click'));
		$btnRequest->Text = $this->strFunctionName;
		
		$this->objControlsArray[] = SoapidioRequestPanelElement::TableBegin;
		for ($i = 0; $i < count($this->strFunctionArgumentsArray); $i++) {
			$strFunctionArguments = explode(' ', $this->strFunctionArgumentsArray[$i]);
			if ($strArguments) {
				$this->objControlsArray[] = SoapidioRequestPanelElement::RowBegin;
				
				switch (QType::TypeFromDoc($strFunctionArguments[0])) {
					case QType::Boolean:
						$txtControl = new QCheckBox($this);
						$txtControl->Required = true;
						$txtControl->AddAction(new QEnterkeyEvent(), new QTerminateAction());
						
						$clbControl = new QControlLabel($txtControl);
						$clbControl->ForControl = $txtControl;
						$clbControl->Text = sprintf('%s: ', substr($strFunctionArguments[1], 4));
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $clbControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $txtControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						break;
					case QType::Integer:
						$txtControl = new QIntegerTextBox($this);
						$txtControl->Required = true;
						$txtControl->AddAction(new QEnterkeyEvent(), new QTerminateAction());
						
						$clbControl = new QControlLabel($txtControl);
						$clbControl->ForControl = $txtControl;
						$clbControl->Text = sprintf('%s: ', substr($strFunctionArguments[1], 4));
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $clbControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $txtControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						break;
					case QType::String:
						$txtControl = new QTextBox($this);
						$txtControl->Required = true;
						$txtControl->AddAction(new QEnterkeyEvent(), new QTerminateAction());
						
						$clbControl = new QControlLabel($txtControl);
						$clbControl->ForControl = $txtControl;
						$clbControl->Text = sprintf('%s: ', substr($strFunctionArguments[1], 4));
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $clbControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $txtControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						break;
					case 'Session':
						$lstControl = new QListBox($this);
						$lstControl->Required = true;
						$lstControl->AddAction(new QEnterkeyEvent(), new QTerminateAction());
						
						$dttExpire = new QDateTime(QDateTime::Now);
						$dttExpire->AddSeconds(-QApplication::GetConfig('session_timeout', QType::Integer));
						
						// Query table and delete first timed out sessions for this IpAddress, delete only when there is no SessionKey assigned (checked via reverse relationship).
						foreach (Session::QueryArray(
							QQ::AndCondition(
								QQ::Equal(QQN::Session()->IpAddress, (string) QApplication::GetClientAddress()),
								QQ::LessThan(QQN::Session()->Changed, $dttExpire),
								QQ::IsNull(QQN::Session()->SessionKeyAsSession->Id)
							)
						) as $objSession) {
							$objSession->Delete();
							QApplication::AddLogEntry(LogItemEnum::SESSIONTIMEOUT);
						}
						
						foreach (Session::QueryArray(
							QQ::AndCondition(
								QQ::Equal(QQN::Session()->IpAddress, (string) QApplication::GetClientAddress()),
								QQ::IsNotNull(QQN::Session()->UserId)
							)
						) as $objSession) {
							$lstControl->AddItem($objSession->__toString(), $objSession);
						}
						
						if ($lstControl->ItemCount == 0) {
							$lstControl->AddItem(QApplication::Translate('(no sessions available)'), null);
							$btnRequest->Enabled = false;
						}
						
						$clbControl = new QControlLabel($lstControl);
						$clbControl->ForControl = $lstControl;
						$clbControl->Text = sprintf('%s: ', substr($strFunctionArguments[1], 4));
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $clbControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnBegin;
						$this->objControlsArray[] = $lstControl;
						$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
						break;
				}
				
				$this->objControlsArray[] = SoapidioRequestPanelElement::RowEnd;
			}
		}
		
		$this->objControlsArray[] = SoapidioRequestPanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnSpan;
		$this->objControlsArray[] = $btnRequest;
		$this->objControlsArray[] = SoapidioRequestPanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioRequestPanelElement::RowEnd;
		$this->objControlsArray[] = SoapidioRequestPanelElement::TableEnd;
	}
	
	/**
	 * Action handler for request button. It execute on the parent form the pnlRequest_DoRequest() function.
	 *
	 */
	public function btnRequest_Click() {
		$mixArgumentArray = array();
		foreach ($this->objChildControlArray as $objControl) {
			if (!($objControl instanceof QButton)) {
				switch (get_class($objControl)) {
					case 'QCheckBox':
						$mixArgumentArray[] = $objControl->Checked;
						break;
					case 'QIntegerTextBox':
						$mixArgumentArray[] = $objControl->Text;
						break;
					case 'QTextBox':
						$mixArgumentArray[] = $objControl->Text;
						break;
					case 'QListBox':
						$mixArgumentArray[] = $objControl->SelectedValue;
						break;
				}
				
			}
		}
		
		$this->objForm->pnlRequest_DoRequest($this->strFunctionName, $this->strFunctionDeclaration, $mixArgumentArray);
	}
	
	/**
	 * Return HTML for this control.
	 *
	 * @return string
	 */
	protected function GetControlHtml() {
		$strReturn = '';
		
		for ($i = 0; $i < count($this->objControlsArray); $i++) {
			if ($this->objControlsArray[$i] instanceof QControl) {
				$strReturn .= $this->objControlsArray[$i]->GetControlHtml();
			} else if (is_int($this->objControlsArray[$i])) {
				switch ($this->objControlsArray[$i]) {
					case SoapidioRequestPanelElement::TableBegin:
						$strReturn .= sprintf('<table class="%s">', $this->strCssClass);
						break;
					case SoapidioRequestPanelElement::TableEnd:
						$strReturn .= '</table>';
						break;
					
					case SoapidioRequestPanelElement::RowBegin:
						$strReturn .= '<tr>';
						break;
					case SoapidioRequestPanelElement::RowEnd:
						$strReturn .= '</tr>';
						break;
						
					case SoapidioRequestPanelElement::ColumnBegin:
						$strReturn .= '<td>';
						break;
					case SoapidioRequestPanelElement::ColumnEnd:
						$strReturn .= '</td>';
						break;
					case SoapidioRequestPanelElement::ColumnSpan:
						$strReturn .= '<td colspan="2">';
						break;
				}
			} else {
				throw new QCallerException('Non valid item in array');
			}
		}
		
		return $strReturn;
	}
	
	/**
	 * Parse $_POST data for all child controls.
	 *
	 */
	public function ParsePostData() {
		foreach ($this->objChildControlArray as $objControl) {
			$objControl->ParsePostData();
		}
	}
}

abstract class SoapidioRequestPanelElement {
	const TableBegin = 1;
	const TableEnd = 2;
	
	const RowBegin = 4;
	const RowEnd = 8;
	
	const ColumnSpan = 16;
	const ColumnBegin = 32;
	const ColumnEnd = 64;
}

?>
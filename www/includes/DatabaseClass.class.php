<?php

abstract class DatabaseClass extends QBaseClass {
	/**
	 * Check permission for this item based on the requesting User and action.
	 *
	 * @param User $objUser
	 * @param string $strAuthActionName
	 * @return bool
	 */
	public function Can(User $objUser, $strAuthActionName) {
		// Get the AuthAction object.
		$objAuthAction = AuthAction::LoadByName($strAuthActionName);
		
		// If action isn't registered or action is requested on a table, an User can't have any sort of premission.
		if (is_null($objAuthAction)) {
			return false;
		}
		
		// Get ClassName from the name of this class.
		$strClassName = get_class($this);
		
		// Set $intAuthClassEnumId from $strClassName.
		$intAuthClassEnumId = AuthClassEnum::$ReverseNameArray[$strClassName];
		
		// Is this entry restored from the database?
		if ($this->__Restored) {
			// Get the AuthImpplementedAction object based on $intAuthClassEnumId and $objAuthAction->Id.
			$objAuthImplementedAction = AuthImplementedAction::Load($intAuthClassEnumId, $objAuthAction->Id);
			
			// If action doesn't apply to a given opject, an User can't have any sort of premission.
			if (is_null($objAuthImplementedAction)) {
				return false;
			}
			
			// Applies the AuthImplentedAction to objects? If that's the case throw an AuthException.
			if (!$objAuthImplementedAction->AuthAction->ApplyToObject) {
				throw new AuthException(sprintf('Action \'%s\' cannot be executed on %s item %s because it does not apply to objects', 
					$objAuthAction->Name, $strClassName, $this->Id));
			}
			
			// Matches the status of $objRow and the AuthImplementedAction's? If that's 
			// not the case the user has no authorization for the action on the object.
			if ($objAuthImplementedAction->AuthStatusEnumId != $this->Status && 
				!is_null($objAuthImplementedAction->AuthStatusEnumId)) {
				return false;
			}
			
			/**
			 * UNIX-style permissions:
			 * 
			 * $intPermissionsArray = array(
			 * 		'OwnerRead'		=> 256,
			 * 		'OwnerWrite'	=> 128,
			 * 		'OwnerDelete'	=> 64,
			 * 		'GroupRead'		=> 32,
			 * 		'GroupWrite'	=> 16,
			 * 		'GroupDelete'	=> 8,
			 * 		'OtherRead'		=> 4,
			 * 		'OtherWrite'	=> 2,
			 * 		'OtherDelete'	=> 1
			 * );
			 */
			
			// Load Privileges for 'self' role.
			if (AuthPrivilege::QueryCount(
					QQ::AndCondition(
						QQ::Equal(QQN::AuthPrivilege()->AuthActionId, $objAuthAction->Id),
						QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::_self)
					)
				) > 0 && $objUser->Id == $this->Id && $intAuthClassEnumId == AuthClassEnum::User) {
				return true;
			}
			
			// Is action an UNIX-style permission? Handle if it is availible on row level.
			if (($this->Owner == $objUser->Id || $this->Group & $objUser->GroupMemberships) && 
				($objAuthAction->Name == 'read' || $objAuthAction->Name == 'write' || $objAuthAction->Name == 'delete')) {
				switch ($objAuthAction->Name) {
					case 'read':
						return ($this->Perms & 256 || $this->Perms & 32 || $this->Perms & 4);
					case 'write':
						return ($this->Perms & 128 || $this->Perms & 16 || $this->Perms & 2);
					case 'delete':
						return ($this->Perms & 64 || $this->Perms & 8 || $this->Perms & 1);
				}
			}
			
			// Load Privileges for 'creator' role.
			if (AuthPrivilege::QueryCount(
					QQ::AndCondition(
						QQ::Equal(QQN::AuthPrivilege()->AuthClassEnumId, $intAuthClassEnumId),
						QQ::Equal(QQN::AuthPrivilege()->AuthActionId, $objAuthAction->Id),
						QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::creator)
					)
				) > 0 && $objUser->Id == $this->Owner) {
				return true;
			}
		}
		
		// Load Authentication module
		$objAuth = QApplication::GetAuth();
		
		// Collect garbage and handle it.
		foreach (AuthPrivilege::QueryArray(
			QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::session)
		) as $objPrivilege) {
			if (!$objAuth->SessionExists($objPrivilege->Who)) {
				$objPrivilege->Delete();
			}
		}
		
		// Load Privileges for other roles.
		$intPrivilegeCount = AuthPrivilege::QueryCount(
			QQ::AndCondition(
				QQ::Equal(QQN::AuthPrivilege()->AuthActionId, $objAuthAction->Id),
				QQ::Equal(QQN::AuthPrivilege()->AuthClassEnumId, $intAuthClassEnumId),
				QQ::OrCondition(
					QQ::AndCondition(
						QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::group),
						SQ::BitwiseAnd(QQN::AuthPrivilege()->Who, $objUser->GroupMemberships)
					),
					QQ::AndCondition(
						QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::user),
						QQ::Equal(QQN::AuthPrivilege()->Who, $objUser->Id)
					),
					QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::other),
					QQ::AndCondition(
						QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, AuthRoleEnum::session),
						QQ::Equal(QQN::AuthPrivilege()->Who, $objAuth->SessionGetName())
					)
				)
			)
		);
		
		return ($intPrivilegeCount > 0);
	}
}

?>
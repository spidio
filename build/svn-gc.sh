#!/bin/sh
# Copyright (c) 2008 Spidio Project
# Licensed under original BSD License.

if [ -f "svn-path" ]
then
	export BUILD=$PWD
	export SPIDIO_SVN=`cat svn-path`
	
	cd $SPIDIO_SVN
	echo --------------------------
	echo Running git-gc
	echo WC: $PWD
	echo --------------------------
	git-gc
	echo
	
	echo --------------------------
	echo Running git-fsck
	echo WC: $PWD
	echo --------------------------
	git-fsck
	echo
	cd $BUILD
fi
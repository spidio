<?php
	/**
	 * The abstract ProjectGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Project subclass which
	 * extends this ProjectGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Project class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $ProjectTypeId the value for strProjectTypeId (Not Null)
	 * @property string $OrganisationId the value for strOrganisationId (Not Null)
	 * @property string $Name the value for strName (Unique)
	 * @property QDateTime $Begin the value for dttBegin (Not Null)
	 * @property QDateTime $End the value for dttEnd (Not Null)
	 * @property QDateTime $Added the value for dttAdded (Not Null)
	 * @property string $Comment the value for strComment 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property ProjectType $ProjectType the value for the ProjectType object referenced by strProjectTypeId (Not Null)
	 * @property Organisation $Organisation the value for the Organisation object referenced by strOrganisationId (Not Null)
	 * @property-read Recording $_RecordingAsProject the value for the private _objRecordingAsProject (Read-Only) if set due to an expansion on the spidio_recording.project_id reverse relationship
	 * @property-read Recording[] $_RecordingAsProjectArray the value for the private _objRecordingAsProjectArray (Read-Only) if set due to an ExpandAsArray on the spidio_recording.project_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class ProjectGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_project.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_project.project_type_id
		 * @var string strProjectTypeId
		 */
		protected $strProjectTypeId;
		const ProjectTypeIdMaxLength = 36;
		const ProjectTypeIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.organisation_id
		 * @var string strOrganisationId
		 */
		protected $strOrganisationId;
		const OrganisationIdMaxLength = 36;
		const OrganisationIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.begin
		 * @var QDateTime dttBegin
		 */
		protected $dttBegin;
		const BeginDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.end
		 * @var QDateTime dttEnd
		 */
		protected $dttEnd;
		const EndDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.added
		 * @var QDateTime dttAdded
		 */
		protected $dttAdded;
		const AddedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_project.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single RecordingAsProject object
		 * (of type Recording), if this Project object was restored with
		 * an expansion on the spidio_recording association table.
		 * @var Recording _objRecordingAsProject;
		 */
		private $_objRecordingAsProject;

		/**
		 * Private member variable that stores a reference to an array of RecordingAsProject objects
		 * (of type Recording[]), if this Project object was restored with
		 * an ExpandAsArray on the spidio_recording association table.
		 * @var Recording[] _objRecordingAsProjectArray;
		 */
		private $_objRecordingAsProjectArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_project.project_type_id.
		 *
		 * NOTE: Always use the ProjectType property getter to correctly retrieve this ProjectType object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var ProjectType objProjectType
		 */
		protected $objProjectType;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_project.organisation_id.
		 *
		 * NOTE: Always use the Organisation property getter to correctly retrieve this Organisation object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Organisation objOrganisation
		 */
		protected $objOrganisation;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Project from PK Info
		 * @param string $strId
		 * @return Project
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Project::QuerySingle(
				QQ::Equal(QQN::Project()->Id, $strId)
			);
		}

		/**
		 * Load all Projects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Project[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Project::QueryArray to perform the LoadAll query
			try {
				return Project::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Projects
		 * @return int
		 */
		public static function CountAll() {
			// Call Project::QueryCount to perform the CountAll query
			return Project::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Create/Build out the QueryBuilder object with Project-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'project');
			Project::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'project');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Project object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Project the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Project::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Project object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Project::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Project objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Project[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Project::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Project::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Project objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Project::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'project_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Project-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Project::GetSelectFields($objQueryBuilder);
				Project::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Project::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Project
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'project';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'project_type_id', $strAliasPrefix . 'project_type_id');
			$objBuilder->AddSelectItem($strTableName, 'organisation_id', $strAliasPrefix . 'organisation_id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'begin', $strAliasPrefix . 'begin');
			$objBuilder->AddSelectItem($strTableName, 'end', $strAliasPrefix . 'end');
			$objBuilder->AddSelectItem($strTableName, 'added', $strAliasPrefix . 'added');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Project from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Project::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Project
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'project__';


				$strAlias = $strAliasPrefix . 'recordingasproject__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objRecordingAsProjectArray)) {
						$objPreviousChildItem = $objPreviousItem->_objRecordingAsProjectArray[$intPreviousChildItemCount - 1];
						$objChildItem = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingasproject__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objRecordingAsProjectArray[] = $objChildItem;
					} else
						$objPreviousItem->_objRecordingAsProjectArray[] = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingasproject__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'project__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the Project object
			$objToReturn = new Project();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'project_type_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'project_type_id'] : $strAliasPrefix . 'project_type_id';
			$objToReturn->strProjectTypeId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'organisation_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'organisation_id'] : $strAliasPrefix . 'organisation_id';
			$objToReturn->strOrganisationId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'begin', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'begin'] : $strAliasPrefix . 'begin';
			$objToReturn->dttBegin = $objDbRow->GetColumn($strAliasName, 'Date');
			$strAliasName = array_key_exists($strAliasPrefix . 'end', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'end'] : $strAliasPrefix . 'end';
			$objToReturn->dttEnd = $objDbRow->GetColumn($strAliasName, 'Date');
			$strAliasName = array_key_exists($strAliasPrefix . 'added', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'added'] : $strAliasPrefix . 'added';
			$objToReturn->dttAdded = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'project__';

			// Check for ProjectType Early Binding
			$strAlias = $strAliasPrefix . 'project_type_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objProjectType = ProjectType::InstantiateDbRow($objDbRow, $strAliasPrefix . 'project_type_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Organisation Early Binding
			$strAlias = $strAliasPrefix . 'organisation_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objOrganisation = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisation_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			// Check for RecordingAsProject Virtual Binding
			$strAlias = $strAliasPrefix . 'recordingasproject__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objRecordingAsProjectArray[] = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingasproject__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objRecordingAsProject = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingasproject__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of Projects from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Project[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Project::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Project::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Project object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Project
		*/
		public static function LoadById($strId) {
			return Project::QuerySingle(
				QQ::Equal(QQN::Project()->Id, $strId)
			);
		}
			
		/**
		 * Load a single Project object,
		 * by Name Index(es)
		 * @param string $strName
		 * @return Project
		*/
		public static function LoadByName($strName) {
			return Project::QuerySingle(
				QQ::Equal(QQN::Project()->Name, $strName)
			);
		}
			
		/**
		 * Load an array of Project objects,
		 * by OrganisationId Index(es)
		 * @param string $strOrganisationId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Project[]
		*/
		public static function LoadArrayByOrganisationId($strOrganisationId, $objOptionalClauses = null) {
			// Call Project::QueryArray to perform the LoadArrayByOrganisationId query
			try {
				return Project::QueryArray(
					QQ::Equal(QQN::Project()->OrganisationId, $strOrganisationId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Projects
		 * by OrganisationId Index(es)
		 * @param string $strOrganisationId
		 * @return int
		*/
		public static function CountByOrganisationId($strOrganisationId) {
			// Call Project::QueryCount to perform the CountByOrganisationId query
			return Project::QueryCount(
				QQ::Equal(QQN::Project()->OrganisationId, $strOrganisationId)
			);
		}
			
		/**
		 * Load an array of Project objects,
		 * by ProjectTypeId Index(es)
		 * @param string $strProjectTypeId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Project[]
		*/
		public static function LoadArrayByProjectTypeId($strProjectTypeId, $objOptionalClauses = null) {
			// Call Project::QueryArray to perform the LoadArrayByProjectTypeId query
			try {
				return Project::QueryArray(
					QQ::Equal(QQN::Project()->ProjectTypeId, $strProjectTypeId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Projects
		 * by ProjectTypeId Index(es)
		 * @param string $strProjectTypeId
		 * @return int
		*/
		public static function CountByProjectTypeId($strProjectTypeId) {
			// Call Project::QueryCount to perform the CountByProjectTypeId query
			return Project::QueryCount(
				QQ::Equal(QQN::Project()->ProjectTypeId, $strProjectTypeId)
			);
		}
			
		/**
		 * Load an array of Project objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Project[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call Project::QueryArray to perform the LoadArrayByOwner query
			try {
				return Project::QueryArray(
					QQ::Equal(QQN::Project()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Projects
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call Project::QueryCount to perform the CountByOwner query
			return Project::QueryCount(
				QQ::Equal(QQN::Project()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of Project objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Project[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call Project::QueryArray to perform the LoadArrayByStatus query
			try {
				return Project::QueryArray(
					QQ::Equal(QQN::Project()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Projects
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call Project::QueryCount to perform the CountByStatus query
			return Project::QueryCount(
				QQ::Equal(QQN::Project()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Project
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'project` (
							`id`,
							`project_type_id`,
							`organisation_id`,
							`name`,
							`begin`,
							`end`,
							`added`,
							`comment`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strProjectTypeId) . ',
							' . $objDatabase->SqlVariable($this->strOrganisationId) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->dttBegin) . ',
							' . $objDatabase->SqlVariable($this->dttEnd) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'project`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Project');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'project`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`project_type_id` = ' . $objDatabase->SqlVariable($this->strProjectTypeId) . ',
							`organisation_id` = ' . $objDatabase->SqlVariable($this->strOrganisationId) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`begin` = ' . $objDatabase->SqlVariable($this->dttBegin) . ',
							`end` = ' . $objDatabase->SqlVariable($this->dttEnd) . ',
							`added` = ' . $objDatabase->SqlVariable($this->dttAdded) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'project`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Project
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Project with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'project`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Projects
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'project`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'project table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'project`');
		}

		/**
		 * Reload this Project from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Project object.');

			// Reload the Object
			$objReloaded = Project::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->ProjectTypeId = $objReloaded->ProjectTypeId;
			$this->OrganisationId = $objReloaded->OrganisationId;
			$this->strName = $objReloaded->strName;
			$this->dttBegin = $objReloaded->dttBegin;
			$this->dttEnd = $objReloaded->dttEnd;
			$this->dttAdded = $objReloaded->dttAdded;
			$this->strComment = $objReloaded->strComment;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'ProjectTypeId':
					/**
					 * Gets the value for strProjectTypeId (Not Null)
					 * @return string
					 */
					return $this->strProjectTypeId;

				case 'OrganisationId':
					/**
					 * Gets the value for strOrganisationId (Not Null)
					 * @return string
					 */
					return $this->strOrganisationId;

				case 'Name':
					/**
					 * Gets the value for strName (Unique)
					 * @return string
					 */
					return $this->strName;

				case 'Begin':
					/**
					 * Gets the value for dttBegin (Not Null)
					 * @return QDateTime
					 */
					return $this->dttBegin;

				case 'End':
					/**
					 * Gets the value for dttEnd (Not Null)
					 * @return QDateTime
					 */
					return $this->dttEnd;

				case 'Added':
					/**
					 * Gets the value for dttAdded (Not Null)
					 * @return QDateTime
					 */
					return $this->dttAdded;

				case 'Comment':
					/**
					 * Gets the value for strComment 
					 * @return string
					 */
					return $this->strComment;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'ProjectType':
					/**
					 * Gets the value for the ProjectType object referenced by strProjectTypeId (Not Null)
					 * @return ProjectType
					 */
					try {
						if ((!$this->objProjectType) && (!is_null($this->strProjectTypeId)))
							$this->objProjectType = ProjectType::Load($this->strProjectTypeId);
						return $this->objProjectType;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Organisation':
					/**
					 * Gets the value for the Organisation object referenced by strOrganisationId (Not Null)
					 * @return Organisation
					 */
					try {
						if ((!$this->objOrganisation) && (!is_null($this->strOrganisationId)))
							$this->objOrganisation = Organisation::Load($this->strOrganisationId);
						return $this->objOrganisation;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_RecordingAsProject':
					/**
					 * Gets the value for the private _objRecordingAsProject (Read-Only)
					 * if set due to an expansion on the spidio_recording.project_id reverse relationship
					 * @return Recording
					 */
					return $this->_objRecordingAsProject;

				case '_RecordingAsProjectArray':
					/**
					 * Gets the value for the private _objRecordingAsProjectArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_recording.project_id reverse relationship
					 * @return Recording[]
					 */
					return (array) $this->_objRecordingAsProjectArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'ProjectTypeId':
					/**
					 * Sets the value for strProjectTypeId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objProjectType = null;
						return ($this->strProjectTypeId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'OrganisationId':
					/**
					 * Sets the value for strOrganisationId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objOrganisation = null;
						return ($this->strOrganisationId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Unique)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Begin':
					/**
					 * Sets the value for dttBegin (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttBegin = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'End':
					/**
					 * Sets the value for dttEnd (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttEnd = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Added':
					/**
					 * Sets the value for dttAdded (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttAdded = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'ProjectType':
					/**
					 * Sets the value for the ProjectType object referenced by strProjectTypeId (Not Null)
					 * @param ProjectType $mixValue
					 * @return ProjectType
					 */
					if (is_null($mixValue)) {
						$this->strProjectTypeId = null;
						$this->objProjectType = null;
						return null;
					} else {
						// Make sure $mixValue actually is a ProjectType object
						try {
							$mixValue = QType::Cast($mixValue, 'ProjectType');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED ProjectType object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved ProjectType for this Project');

						// Update Local Member Variables
						$this->objProjectType = $mixValue;
						$this->strProjectTypeId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Organisation':
					/**
					 * Sets the value for the Organisation object referenced by strOrganisationId (Not Null)
					 * @param Organisation $mixValue
					 * @return Organisation
					 */
					if (is_null($mixValue)) {
						$this->strOrganisationId = null;
						$this->objOrganisation = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Organisation object
						try {
							$mixValue = QType::Cast($mixValue, 'Organisation');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Organisation object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Organisation for this Project');

						// Update Local Member Variables
						$this->objOrganisation = $mixValue;
						$this->strOrganisationId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for RecordingAsProject
		//-------------------------------------------------------------------

		/**
		 * Gets all associated RecordingsAsProject as an array of Recording objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/ 
		public function GetRecordingAsProjectArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Recording::LoadArrayByProjectId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated RecordingsAsProject
		 * @return int
		*/ 
		public function CountRecordingsAsProject() {
			if ((is_null($this->strId)))
				return 0;

			return Recording::CountByProjectId($this->strId);
		}

		/**
		 * Associates a RecordingAsProject
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function AssociateRecordingAsProject(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateRecordingAsProject on this unsaved Project.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateRecordingAsProject on this Project with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`project_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . '
			');
		}

		/**
		 * Unassociates a RecordingAsProject
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function UnassociateRecordingAsProject(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsProject on this unsaved Project.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsProject on this Project with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`project_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . ' AND
					`project_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all RecordingsAsProject
		 * @return void
		*/ 
		public function UnassociateAllRecordingsAsProject() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsProject on this unsaved Project.');

			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`project_id` = null
				WHERE
					`project_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated RecordingAsProject
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function DeleteAssociatedRecordingAsProject(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsProject on this unsaved Project.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsProject on this Project with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_recording`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . ' AND
					`project_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated RecordingsAsProject
		 * @return void
		*/ 
		public function DeleteAllRecordingsAsProject() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsProject on this unsaved Project.');

			// Get the Database Object for this Class
			$objDatabase = Project::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_recording`
				WHERE
					`project_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Project"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="ProjectType" type="xsd1:ProjectType"/>';
			$strToReturn .= '<element name="Organisation" type="xsd1:Organisation"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="Begin" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="End" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Added" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Project', $strComplexTypeArray)) {
				$strComplexTypeArray['Project'] = Project::GetSoapComplexTypeXml();
				ProjectType::AlterSoapComplexTypeArray($strComplexTypeArray);
				Organisation::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Project::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Project();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'ProjectType')) &&
				($objSoapObject->ProjectType))
				$objToReturn->ProjectType = ProjectType::GetObjectFromSoapObject($objSoapObject->ProjectType);
			if ((property_exists($objSoapObject, 'Organisation')) &&
				($objSoapObject->Organisation))
				$objToReturn->Organisation = Organisation::GetObjectFromSoapObject($objSoapObject->Organisation);
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'Begin'))
				$objToReturn->dttBegin = new QDateTime($objSoapObject->Begin);
			if (property_exists($objSoapObject, 'End'))
				$objToReturn->dttEnd = new QDateTime($objSoapObject->End);
			if (property_exists($objSoapObject, 'Added'))
				$objToReturn->dttAdded = new QDateTime($objSoapObject->Added);
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Project::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objProjectType)
				$objObject->objProjectType = ProjectType::GetSoapObjectFromObject($objObject->objProjectType, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strProjectTypeId = null;
			if ($objObject->objOrganisation)
				$objObject->objOrganisation = Organisation::GetSoapObjectFromObject($objObject->objOrganisation, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strOrganisationId = null;
			if ($objObject->dttBegin)
				$objObject->dttBegin = $objObject->dttBegin->__toString(QDateTime::FormatSoap);
			if ($objObject->dttEnd)
				$objObject->dttEnd = $objObject->dttEnd->__toString(QDateTime::FormatSoap);
			if ($objObject->dttAdded)
				$objObject->dttAdded = $objObject->dttAdded->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeProject extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Project';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'project';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'ProjectTypeId':
					return new QQNode('project_type_id', 'ProjectTypeId', 'string', $this);
				case 'ProjectType':
					return new QQNodeProjectType('project_type_id', 'ProjectType', 'string', $this);
				case 'OrganisationId':
					return new QQNode('organisation_id', 'OrganisationId', 'string', $this);
				case 'Organisation':
					return new QQNodeOrganisation('organisation_id', 'Organisation', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Begin':
					return new QQNode('begin', 'Begin', 'QDateTime', $this);
				case 'End':
					return new QQNode('end', 'End', 'QDateTime', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'RecordingAsProject':
					return new QQReverseReferenceNodeRecording($this, 'recordingasproject', 'reverse_reference', 'project_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeProject extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Project';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'project';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'ProjectTypeId':
					return new QQNode('project_type_id', 'ProjectTypeId', 'string', $this);
				case 'ProjectType':
					return new QQNodeProjectType('project_type_id', 'ProjectType', 'string', $this);
				case 'OrganisationId':
					return new QQNode('organisation_id', 'OrganisationId', 'string', $this);
				case 'Organisation':
					return new QQNodeOrganisation('organisation_id', 'Organisation', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Begin':
					return new QQNode('begin', 'Begin', 'QDateTime', $this);
				case 'End':
					return new QQNode('end', 'End', 'QDateTime', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'RecordingAsProject':
					return new QQReverseReferenceNodeRecording($this, 'recordingasproject', 'reverse_reference', 'project_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
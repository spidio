<?php
	require(__DATAGEN_CLASSES__ . '/LogActionEnumGen.class.php');

	/**
	 * The LogActionEnum class defined here contains any
	 * customized code for the LogActionEnum enumerated type. 
	 * 
	 * It represents the enumerated values found in the "spidio_log_action_enum" table in the database,
	 * and extends from the code generated abstract LogActionEnumGen
	 * class, which contains all the values extracted from the database.
	 * 
	 * Type classes which are generally used to attach a type to data object.
	 * However, they may be used as simple database indepedant enumerated type.
	 * 
	 * @package Spidio
	 * @subpackage DataObjects
	 */
	abstract class LogActionEnum extends LogActionEnumGen {
		public static function ToTranslatedString($intLogActionEnumId) {
			switch ($intLogActionEnumId) {
				case 1: return QApplication::Translate('(null)');
				case 2: return QApplication::Translate('Add');
				case 3: return QApplication::Translate('Edit');
				case 4: return QApplication::Translate('Delete');
				default:
					return parent::ToString($intLogActionEnumId);
			}
		}
	}
?>
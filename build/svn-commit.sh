#!/bin/sh
# Copyright (c) 2008 Spidio Project
# Licensed under original BSD License.

if [ -f "svn-path" ]
then
	export BUILD=$PWD
	export SPIDIO_SVN=`cat svn-path`
	
	cd $SPIDIO_SVN
	echo --------------------------
	echo Running git-svn-rebase
	echo WC: $PWD
	echo --------------------------
	git-svn-rebase
	echo
	
	echo --------------------------
	echo Running git-pull
	echo WC: $PWD
	echo --------------------------
	git-pull # assuming there is an origin remote with an default revspec for fetch
	echo
	
	echo --------------------------
	echo Running git-svn dcommit
	echo WC: $PWD
	echo --------------------------
	git-svn dcommit
	echo
	cd $BUILD
fi
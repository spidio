<?php 
	// ClassPaths for the AuthClassEnum type class
		QApplicationBase::$ClassFile['authclassenum'] = __DATA_CLASSES__ . '/AuthClassEnum.class.php';
	// ClassPaths for the AuthPrivilegeTypeEnum type class
		QApplicationBase::$ClassFile['authprivilegetypeenum'] = __DATA_CLASSES__ . '/AuthPrivilegeTypeEnum.class.php';
	// ClassPaths for the AuthRoleEnum type class
		QApplicationBase::$ClassFile['authroleenum'] = __DATA_CLASSES__ . '/AuthRoleEnum.class.php';
	// ClassPaths for the AuthStatusEnum type class
		QApplicationBase::$ClassFile['authstatusenum'] = __DATA_CLASSES__ . '/AuthStatusEnum.class.php';
	// ClassPaths for the CodecEnum type class
		QApplicationBase::$ClassFile['codecenum'] = __DATA_CLASSES__ . '/CodecEnum.class.php';
	// ClassPaths for the LogActionEnum type class
		QApplicationBase::$ClassFile['logactionenum'] = __DATA_CLASSES__ . '/LogActionEnum.class.php';
	// ClassPaths for the LogItemEnum type class
		QApplicationBase::$ClassFile['logitemenum'] = __DATA_CLASSES__ . '/LogItemEnum.class.php';
	// ClassPaths for the TapeModeEnum type class
		QApplicationBase::$ClassFile['tapemodeenum'] = __DATA_CLASSES__ . '/TapeModeEnum.class.php';
?>
<?php

class CookieStore {
	static protected $objCookieArray = array();
	
	/**
	 * Initialize CookieStore.
	 *
	 */
	public static function Initialize() {
		self::FillStore();
		register_shutdown_function(array('CookieStore', 'SaveStore'));
	}
	
	/**
	 * Fill CookieStore from given array, $_COOKIE is standard used.
	 *
	 * @param array|null $arrCookies Array to fill from or null for $_COOKIE.
	 * @param bool $blnAssoc Given array is an associative array.
	 * @return bool
	 */
	public static function FillStore($arrCookies = null, $blnAssoc = false) {
		if (is_null($arrCookies)) {
			$arrCookies = $_COOKIE;
		}
		
		if (is_array($arrCookies)) {
			foreach ($arrCookies as $strKey => $strValue) {
				self::$objCookieArray[$strKey] = new Cookie($strKey, $strValue, null, false);
			}
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Save the CookieStore, and send cookies that are marked for save behind the scenes.
	 *
	 */
	public static function SaveStore() {
		foreach (self::$objCookieArray as $objCookie) {
			$objCookie->Save();
			$objCookie->GarbageCollect();
		}
	}
	
	/**
	 * Return internal array with Cookie objects.
	 *
	 * @return Cookie[]
	 */
	public static function GetStore() {
		return self::$objCookieArray;
	}
	
	/**
	 * Register new cookie or fetch existing (updated) cookie.
	 *
	 * @param string $strName
	 * @param string $strValue
	 * @param QDateTime $dttExpire
	 * @return Cookie
	 */
	public static function &UpdateCookie($strName, $strValue = '', $dttExpire = null) {
		if (self::ExistCookie($strName)) {
			if (!empty($strValue)) {
				self::$objCookieArray[$strName]->Value = $strValue;
			}
			
			if (!empty($dttExpire)) {
				self::$objCookieArray[$strName]->Expire = $dttExpire;
			}
			
			return self::$objCookieArray[$strName];
		} else {
			$objCookie = new Cookie($strName, $strValue, $dttExpire);
			self::$objCookieArray[$strName] = $objCookie;
			return $objCookie;
		}
	}
	
	/**
	 * Is Cookie availible in our CookieStore?
	 *
	 * @param string $strName
	 * @return bool
	 */
	public static function ExistCookie($strName) {
		return array_key_exists($strName, self::$objCookieArray);
	}
	
	/**
	 * Delete cookie from store.
	 *
	 * @param string $strName
	 */
	public static function DeleteCookie($strName) {
		if (self::ExistCookie($strName)) {
			self::$objCookieArray[$strName]->Delete();
		}
	}
}

?>
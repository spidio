<?php
	/**
	 * The AuthStatusEnum class defined here contains
	 * code for the AuthStatusEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_auth_status_enum" table
	 * in the database.
	 * 
	 * To use, you should use the AuthStatusEnum subclass which
	 * extends this AuthStatusEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthStatusEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class AuthStatusEnumGen extends QBaseClass {
		const ACTIVE = 1;
		const INACTIVE = 2;
		const PENDING = 4;
		const DELETED = 8;
		const CANCELLED = 16;
		const RECEIVED = 32;
		const SENT = 64;
		const STAGED = 128;

		const MaxId = 128;

		public static $NameArray = array(
			1 => 'ACTIVE',
			2 => 'INACTIVE',
			4 => 'PENDING',
			8 => 'DELETED',
			16 => 'CANCELLED',
			32 => 'RECEIVED',
			64 => 'SENT',
			128 => 'STAGED');

		public static $TokenArray = array(
			1 => 'ACTIVE',
			2 => 'INACTIVE',
			4 => 'PENDING',
			8 => 'DELETED',
			16 => 'CANCELLED',
			32 => 'RECEIVED',
			64 => 'SENT',
			128 => 'STAGED');

		public static $ReverseNameArray = array(
			'ACTIVE' => 1,
			'INACTIVE' => 2,
			'PENDING' => 4,
			'DELETED' => 8,
			'CANCELLED' => 16,
			'RECEIVED' => 32,
			'SENT' => 64,
			'STAGED' => 128);

		public static $ReverseTokenArray = array(
			'ACTIVE' => 1,
			'INACTIVE' => 2,
			'PENDING' => 4,
			'DELETED' => 8,
			'CANCELLED' => 16,
			'RECEIVED' => 32,
			'SENT' => 64,
			'STAGED' => 128);

		public static function ToString($intAuthStatusEnumId) {
			switch ($intAuthStatusEnumId) {
				case 1: return 'ACTIVE';
				case 2: return 'INACTIVE';
				case 4: return 'PENDING';
				case 8: return 'DELETED';
				case 16: return 'CANCELLED';
				case 32: return 'RECEIVED';
				case 64: return 'SENT';
				case 128: return 'STAGED';
				default:
					throw new QCallerException(sprintf('Invalid intAuthStatusEnumId: %s', $intAuthStatusEnumId));
			}
		}

		public static function ToToken($intAuthStatusEnumId) {
			switch ($intAuthStatusEnumId) {
				case 1: return 'ACTIVE';
				case 2: return 'INACTIVE';
				case 4: return 'PENDING';
				case 8: return 'DELETED';
				case 16: return 'CANCELLED';
				case 32: return 'RECEIVED';
				case 64: return 'SENT';
				case 128: return 'STAGED';
				default:
					throw new QCallerException(sprintf('Invalid intAuthStatusEnumId: %s', $intAuthStatusEnumId));
			}
		}

	}
?>
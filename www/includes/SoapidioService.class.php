<?php

class SoapidioService extends QSoapService {
	protected $objAuth;
	
	public function __construct($strClassName, $strNamespace) {
		$this->objAuth = QApplication::GetAuth();
		
		if (!$this->objAuth->IsLoggedIn() && !($this instanceof SoapidioAuth) && QApplication::$QueryString != 'wsdl') {
			$this->NeedLogin();
		} else {
			parent::__construct($strClassName, $strNamespace);
		}
	}
	
	protected function NeedLogin() {
		trigger_error('NeedLogin', E_USER_ERROR);
	}
}

?>
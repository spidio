<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Template class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Template object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a TemplateMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Template $Template the actual Template data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $VersionControl
	 * property-read QLabel $VersionLabel
	 * property QTextBox $AuthorControl
	 * property-read QLabel $AuthorLabel
	 * property QTextBox $EmailControl
	 * property-read QLabel $EmailLabel
	 * property QTextBox $CopyrightControl
	 * property-read QLabel $CopyrightLabel
	 * property QTextBox $WebsiteControl
	 * property-read QLabel $WebsiteLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class TemplateMetaControlGen extends QBaseClass {
		// General Variables
		protected $objTemplate;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Template's individual data fields
		protected $txtId;
		protected $txtName;
		protected $txtVersion;
		protected $txtAuthor;
		protected $txtEmail;
		protected $txtCopyright;
		protected $txtWebsite;
		protected $lstStatusObject;

		// Controls that allow the viewing of Template's individual data fields
		protected $lblId;
		protected $lblName;
		protected $lblVersion;
		protected $lblAuthor;
		protected $lblEmail;
		protected $lblCopyright;
		protected $lblWebsite;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * TemplateMetaControl to edit a single Template object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Template object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateMetaControl
		 * @param Template $objTemplate new or existing Template object
		 */
		 public function __construct($objParentObject, Template $objTemplate) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this TemplateMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Template object
			$this->objTemplate = $objTemplate;

			// Figure out if we're Editing or Creating New
			if ($this->objTemplate->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Template object creation - defaults to CreateOrEdit
 		 * @return TemplateMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objTemplate = Template::Load($strId);

				// Template was found -- return it!
				if ($objTemplate)
					return new TemplateMetaControl($objParentObject, $objTemplate);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Template object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new TemplateMetaControl($objParentObject, new Template());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Template object creation - defaults to CreateOrEdit
		 * @return TemplateMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return TemplateMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TemplateMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Template object creation - defaults to CreateOrEdit
		 * @return TemplateMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return TemplateMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objTemplate->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Template::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objTemplate->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objTemplate->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = Template::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objTemplate->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtVersion
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtVersion_Create($strControlId = null) {
			$this->txtVersion = new QTextBox($this->objParentObject, $strControlId);
			$this->txtVersion->Name = QApplication::Translate('Version');
			$this->txtVersion->Text = $this->objTemplate->Version;
			$this->txtVersion->Required = true;
			$this->txtVersion->MaxLength = Template::VersionMaxLength;
			return $this->txtVersion;
		}

		/**
		 * Create and setup QLabel lblVersion
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblVersion_Create($strControlId = null) {
			$this->lblVersion = new QLabel($this->objParentObject, $strControlId);
			$this->lblVersion->Name = QApplication::Translate('Version');
			$this->lblVersion->Text = $this->objTemplate->Version;
			$this->lblVersion->Required = true;
			return $this->lblVersion;
		}

		/**
		 * Create and setup QTextBox txtAuthor
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtAuthor_Create($strControlId = null) {
			$this->txtAuthor = new QTextBox($this->objParentObject, $strControlId);
			$this->txtAuthor->Name = QApplication::Translate('Author');
			$this->txtAuthor->Text = $this->objTemplate->Author;
			$this->txtAuthor->Required = true;
			$this->txtAuthor->MaxLength = Template::AuthorMaxLength;
			return $this->txtAuthor;
		}

		/**
		 * Create and setup QLabel lblAuthor
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthor_Create($strControlId = null) {
			$this->lblAuthor = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthor->Name = QApplication::Translate('Author');
			$this->lblAuthor->Text = $this->objTemplate->Author;
			$this->lblAuthor->Required = true;
			return $this->lblAuthor;
		}

		/**
		 * Create and setup QTextBox txtEmail
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtEmail_Create($strControlId = null) {
			$this->txtEmail = new QTextBox($this->objParentObject, $strControlId);
			$this->txtEmail->Name = QApplication::Translate('Email');
			$this->txtEmail->Text = $this->objTemplate->Email;
			$this->txtEmail->Required = true;
			$this->txtEmail->MaxLength = Template::EmailMaxLength;
			return $this->txtEmail;
		}

		/**
		 * Create and setup QLabel lblEmail
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblEmail_Create($strControlId = null) {
			$this->lblEmail = new QLabel($this->objParentObject, $strControlId);
			$this->lblEmail->Name = QApplication::Translate('Email');
			$this->lblEmail->Text = $this->objTemplate->Email;
			$this->lblEmail->Required = true;
			return $this->lblEmail;
		}

		/**
		 * Create and setup QTextBox txtCopyright
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtCopyright_Create($strControlId = null) {
			$this->txtCopyright = new QTextBox($this->objParentObject, $strControlId);
			$this->txtCopyright->Name = QApplication::Translate('Copyright');
			$this->txtCopyright->Text = $this->objTemplate->Copyright;
			$this->txtCopyright->Required = true;
			$this->txtCopyright->MaxLength = Template::CopyrightMaxLength;
			return $this->txtCopyright;
		}

		/**
		 * Create and setup QLabel lblCopyright
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCopyright_Create($strControlId = null) {
			$this->lblCopyright = new QLabel($this->objParentObject, $strControlId);
			$this->lblCopyright->Name = QApplication::Translate('Copyright');
			$this->lblCopyright->Text = $this->objTemplate->Copyright;
			$this->lblCopyright->Required = true;
			return $this->lblCopyright;
		}

		/**
		 * Create and setup QTextBox txtWebsite
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtWebsite_Create($strControlId = null) {
			$this->txtWebsite = new QTextBox($this->objParentObject, $strControlId);
			$this->txtWebsite->Name = QApplication::Translate('Website');
			$this->txtWebsite->Text = $this->objTemplate->Website;
			$this->txtWebsite->Required = true;
			$this->txtWebsite->MaxLength = Template::WebsiteMaxLength;
			return $this->txtWebsite;
		}

		/**
		 * Create and setup QLabel lblWebsite
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblWebsite_Create($strControlId = null) {
			$this->lblWebsite = new QLabel($this->objParentObject, $strControlId);
			$this->lblWebsite->Name = QApplication::Translate('Website');
			$this->lblWebsite->Text = $this->objTemplate->Website;
			$this->lblWebsite->Required = true;
			return $this->lblWebsite;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objTemplate->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objTemplate->Status) ? AuthStatusEnum::$NameArray[$this->objTemplate->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Template object.
		 * @param boolean $blnReload reload Template from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objTemplate->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objTemplate->Id;
			if ($this->lblId) $this->lblId->Text = $this->objTemplate->Id;

			if ($this->txtName) $this->txtName->Text = $this->objTemplate->Name;
			if ($this->lblName) $this->lblName->Text = $this->objTemplate->Name;

			if ($this->txtVersion) $this->txtVersion->Text = $this->objTemplate->Version;
			if ($this->lblVersion) $this->lblVersion->Text = $this->objTemplate->Version;

			if ($this->txtAuthor) $this->txtAuthor->Text = $this->objTemplate->Author;
			if ($this->lblAuthor) $this->lblAuthor->Text = $this->objTemplate->Author;

			if ($this->txtEmail) $this->txtEmail->Text = $this->objTemplate->Email;
			if ($this->lblEmail) $this->lblEmail->Text = $this->objTemplate->Email;

			if ($this->txtCopyright) $this->txtCopyright->Text = $this->objTemplate->Copyright;
			if ($this->lblCopyright) $this->lblCopyright->Text = $this->objTemplate->Copyright;

			if ($this->txtWebsite) $this->txtWebsite->Text = $this->objTemplate->Website;
			if ($this->lblWebsite) $this->lblWebsite->Text = $this->objTemplate->Website;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objTemplate->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objTemplate->Status) ? AuthStatusEnum::$NameArray[$this->objTemplate->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC TEMPLATE OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Template instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveTemplate() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objTemplate->Id = $this->txtId->Text;
				if ($this->txtName) $this->objTemplate->Name = $this->txtName->Text;
				if ($this->txtVersion) $this->objTemplate->Version = $this->txtVersion->Text;
				if ($this->txtAuthor) $this->objTemplate->Author = $this->txtAuthor->Text;
				if ($this->txtEmail) $this->objTemplate->Email = $this->txtEmail->Text;
				if ($this->txtCopyright) $this->objTemplate->Copyright = $this->txtCopyright->Text;
				if ($this->txtWebsite) $this->objTemplate->Website = $this->txtWebsite->Text;
				if ($this->lstStatusObject) $this->objTemplate->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Template object
				$this->objTemplate->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Template instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteTemplate() {
			$this->objTemplate->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Template': return $this->objTemplate;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Template fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'VersionControl':
					if (!$this->txtVersion) return $this->txtVersion_Create();
					return $this->txtVersion;
				case 'VersionLabel':
					if (!$this->lblVersion) return $this->lblVersion_Create();
					return $this->lblVersion;
				case 'AuthorControl':
					if (!$this->txtAuthor) return $this->txtAuthor_Create();
					return $this->txtAuthor;
				case 'AuthorLabel':
					if (!$this->lblAuthor) return $this->lblAuthor_Create();
					return $this->lblAuthor;
				case 'EmailControl':
					if (!$this->txtEmail) return $this->txtEmail_Create();
					return $this->txtEmail;
				case 'EmailLabel':
					if (!$this->lblEmail) return $this->lblEmail_Create();
					return $this->lblEmail;
				case 'CopyrightControl':
					if (!$this->txtCopyright) return $this->txtCopyright_Create();
					return $this->txtCopyright;
				case 'CopyrightLabel':
					if (!$this->lblCopyright) return $this->lblCopyright_Create();
					return $this->lblCopyright;
				case 'WebsiteControl':
					if (!$this->txtWebsite) return $this->txtWebsite_Create();
					return $this->txtWebsite;
				case 'WebsiteLabel':
					if (!$this->lblWebsite) return $this->lblWebsite_Create();
					return $this->lblWebsite;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Template fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'VersionControl':
						return ($this->txtVersion = QType::Cast($mixValue, 'QControl'));
					case 'AuthorControl':
						return ($this->txtAuthor = QType::Cast($mixValue, 'QControl'));
					case 'EmailControl':
						return ($this->txtEmail = QType::Cast($mixValue, 'QControl'));
					case 'CopyrightControl':
						return ($this->txtCopyright = QType::Cast($mixValue, 'QControl'));
					case 'WebsiteControl':
						return ($this->txtWebsite = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
<?php

class RecentChangesForm extends SpidioTemplateForm {
	protected $lstUser;
	protected $lstSession;
	protected $lstAddress;
	
	protected $lstType;
	protected $lstItem;
	protected $lstAction;
	
	protected $txtItems;
	protected $btnAdvanced;
	protected $blnAdvanced = false;
	protected $btnRefresh;
	protected $btnReset;
	protected $objDefaultWaitIcon;
	
	protected $dtgLog;
	
	protected $strTextIntro;
	
	protected function Form_Create() {
		parent::Form_Create();
		$this->PageTitle = QApplication::Translate('Recent changes/events');
		
		$this->strTextIntro = QApplication::Translate('On this page are events displayed. You can sort and filter this information and also delete it.');
		
		$this->dtgLog = new QDataGrid($this);
		$this->dtgLog->SetDataBinder('dtgLog_Bind');
		$this->dtgLog->UseAjax = true;
		
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Id'), '<?= $_ITEM->Id ?>', array('OrderByClause' => QQ::OrderBy(QQN::Log()->Id, false), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->Id))));
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Date'), '<?= QApplication::MakeDateTimeString($_ITEM->Date, "recent") ?>', array('OrderByClause' => QQ::OrderBy(QQN::Log()->Date, false), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->Date))));
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Type'), '<?= LogItemEnum::ToTranslatedString($_ITEM->LogItemEnumId) ?>', array('OrderByClause' => QQ::OrderBy(QQN::Log()->LogItemEnumId), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->LogItemEnumId, false))));
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Action'), '<?= ($_ITEM->LogActionEnumId == 1) ? null : LogActionEnum::ToTranslatedString($_ITEM->LogActionEnumId) ?>', array('OrderByClause' => QQ::OrderBy(QQN::Log()->LogActionEnumId), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->LogActionEnumId, false))));
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('User'), '<?= QApplication::MakeAcronymOfUser($_ITEM->User) ?>', 'HtmlEntities=false', array('OrderByClause' => QQ::OrderBy(QQN::Log()->User->Name), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->User->Name, false))));
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Information'), '<?= QApplication::GetStringOfLogItemIdBasedOnLogItemEnumId($_ITEM->ItemId, $_ITEM->LogItemEnumId) ?>'));
		$this->dtgLog->SortColumnIndex = 0;
		
		$objPaginator = new QPaginator($this->dtgLog);
		$this->dtgLog->Paginator = $objPaginator;
		$this->dtgLog->ItemsPerPage = 10;
		
		$this->lstUser = new QListBox($this);
		$this->lstUser->AddAction(new QChangeEvent(), new QAjaxAction('lstUser_Change'));
		$this->lstUser_Fill();
		
		$this->lstSession = new QListBox($this);
		$this->lstSession->Visible = (QApplication::GetConfig('recent_userlogins', QType::Boolean) || QApplication::GetConfig('development_recent_debug', QType::Boolean)) ? true : false;
		$this->lstSession->AddAction(new QChangeEvent(), new QAjaxAction('lstSession_Change'));
		$this->lstSession_Fill();
		
		$this->lstAddress = new QListBox($this);
		$this->lstAddress->Visible = (QApplication::GetConfig('recent_userlogins', QType::Boolean) || QApplication::GetConfig('development_recent_debug', QType::Boolean)) ? true : false;
		$this->lstAddress->AddAction(new QChangeEvent(), new QAjaxAction('lstAddress_Change'));
		$this->lstAddress_Fill();
		
		$this->lstType = new QListBox($this);
		$this->lstType->AddAction(new QChangeEvent(), new QAjaxAction('lstType_Change'));
		$this->lstType->AddItem(QApplication::Translate('Types'), null, true);
		$this->lstType->AddItem(QApplication::Translate('Organisation'), ListboxType::Organisation, null, QApplication::Translate('General'));
		$this->lstType->AddItem(QApplication::Translate('Project'), ListboxType::Project, null, QApplication::Translate('General'));
		$this->lstType->AddItem(QApplication::Translate('Tape'), ListboxType::Tape, null, QApplication::Translate('General'));
		$this->lstType->AddItem(QApplication::Translate('User login'), ListboxType::Login, null, QApplication::Translate('Session'));
		$this->lstType->AddItem(QApplication::Translate('User logout'), ListboxType::Logout, null, QApplication::Translate('Session'));
		$this->lstType->AddItem(QApplication::Translate('Forced user logout'), ListboxType::ForcedLogout, null, QApplication::Translate('Session'));
		$this->lstType->AddItem(QApplication::Translate('Session created'), ListboxType::Created, null, QApplication::Translate('Session'));
		$this->lstType->AddItem(QApplication::Translate('Session reset'), ListboxType::Reset, null, QApplication::Translate('Session'));
		$this->lstType->AddItem(QApplication::Translate('Session timeout'), ListboxType::Timeout, null, QApplication::Translate('Session'));
		$this->lstType->AddItem(QApplication::Translate('All except sessions'), ListboxType::ExceptSessions, null, QApplication::Translate('Combined'));
		$this->lstType->AddItem(QApplication::Translate('All session logs'), ListboxType::AllSessionLogs, null, QApplication::Translate('Combined'));
		
		$this->lstAction = new QListBox($this);
		$this->lstAction->Enabled = false;
		$this->lstAction->AddAction(new QChangeEvent(), new QAjaxAction('lstAction_Change'));
		$this->lstAction->AddItem(QApplication::Translate('- Actions -'), null);
		
		$this->lstItem = new QListBox($this);
		$this->lstItem->Enabled = false;
		$this->lstItem->AddAction(new QChangeEvent(), new QAjaxAction('lstItem_Change'));
		$this->lstItem->AddItem(QApplication::Translate('- Items -'), null);
		
		$this->txtItems = new QIntegerTextBox($this);
		$this->txtItems->CssClass = 'recent_txtItems';
		$this->txtItems->Text = 10;
		$this->txtItems->AddAction(new QKeyPressEvent(1), new QAjaxAction('txtItems_Change'));
		$this->txtItems->AddAction(new QEnterKeyEvent(), new QTerminateAction());
		$this->txtItems->AddAction(new QEscapeKeyEvent(), new QAjaxAction('btnReset_Click'));
		$this->txtItems->AddAction(new QEscapeKeyEvent(), new QAjaxAction('txtItems_Escape'));
		$this->txtItems->AddAction(new QEscapeKeyEvent(), new QTerminateAction());
		
		$this->btnAdvanced = new QButton($this);
		$this->btnAdvanced->Text = QApplication::Translate('Advanced');
		$this->btnAdvanced->AddAction(new QClickEvent(), new QAjaxAction('btnAdvanced_Click'));
		
		$this->btnRefresh = new QButton($this);
		$this->btnRefresh->Text = QApplication::Translate('Refresh');
		$this->btnRefresh->AddAction(new QClickEvent(), new QAjaxAction('btnRefresh_Click'));
		
		$this->btnReset = new QButton($this);
		$this->btnReset->Enabled = false;
		$this->btnReset->Text = QApplication::Translate('Reset');
		$this->btnReset->AddAction(new QClickEvent(), new QAjaxAction('btnReset_Click'));
		
		$this->objDefaultWaitIcon = new QWaitIcon($this);
		
		if (QApplication::GetConfig('development_recent_debug', QType::Boolean)) {
			$this->blnAdvanced = true;
			$this->dtgLog_ShowAdvancedColumns();
		}
	}
	
	public function dtgLog_Bind() {
		if ($this->lstUser->SelectedValue === false && $this->lstSession->SelectedValue == null && 
				$this->lstAddress->SelectedValue == null && $this->lstType->SelectedValue == null && 
				$this->lstItem->SelectedValue == null && $this->lstAction->SelectedValue == null) {
			
			if (QApplication::GetConfig('development_recent_debug', QType::Boolean)) {
				$this->dtgLog->TotalItemCount = Log::CountAll();
				$this->dtgLog->DataSource = Log::LoadAll(
					QQ::Clause(
						QQ::Expand(QQN::Log()->User->Person),
						$this->dtgLog->OrderByClause,
						$this->dtgLog->LimitClause
					)
				);
			} else if (QApplication::GetConfig('recent_userlogins', QType::Boolean) || $this->blnAdvanced) {
				$this->dtgLog->TotalItemCount = Log::QueryCount(
					QQ::AndCondition(
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESEt)
					)
				);
				$this->dtgLog->DataSource = Log::QueryArray(
					QQ::AndCondition(
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESEt)
					),
					QQ::Clause(
						QQ::Expand(QQN::Log()->User->Person),
						$this->dtgLog->OrderByClause,
						$this->dtgLog->LimitClause
					)
				);
			} else {
				$this->dtgLog->TotalItemCount = Log::QueryCount(
					QQ::AndCondition(
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGIN),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUT),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUTFORCE),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESEt)
					)
				);
				$this->dtgLog->DataSource = Log::QueryArray(
					QQ::AndCondition(
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGIN),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUT),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUTFORCE),
						QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESEt)
					),
					QQ::Clause(
						QQ::Expand(QQN::Log()->User->Person),
						$this->dtgLog->OrderByClause,
						$this->dtgLog->LimitClause
					)
				);
			}
		} else if ($this->lstUser->SelectedValue !== false || $this->lstSession->SelectedValue || 
				$this->lstAddress->SelectedValue || $this->lstType->SelectedValue || 
				$this->lstItem->SelectedValue || $this->lstAction->SelectedValue) {
			$objConditions = array();
			if ($this->lstUser->SelectedValue !== false)
				$objConditions[] = QQ::Equal(QQN::Log()->UserId, $this->lstUser->SelectedValue);
			
			if ($this->lstSession->SelectedValue)
				$objConditions[] = QQ::Equal(QQN::Log()->SessionName, $this->lstSession->SelectedValue);
			
			if ($this->lstAddress->SelectedValue)
				$objConditions[] = QQ::Equal(QQN::Log()->IpAddress, $this->lstAddress->SelectedValue);
			
			switch ($this->lstType->SelectedValue) {
				case ListboxType::Organisation:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::ORGANISATION);
					break;
				case ListboxType::Project:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::PROJECT);
					break;
				case ListboxType::Tape:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::TAPE);
					break;
				
				case ListboxType::Login:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGIN);
					break;
				case ListboxType::Logout:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUT);
					break;
				case ListboxType::ForcedLogout:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUTFORCE);
					break;
				case ListboxType::Created:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED);
					break;
				case ListboxType::Reset:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESET);
					break;
				case ListboxType::Timeout:
					$objConditions[] = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT);
					break;
					
				case ListboxType::ExceptSessions:
					$objConditions[] = QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGIN);
					$objConditions[] = QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUT);
					$objConditions[] = QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUTFORCE);
					$objConditions[] = QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED);
					$objConditions[] = QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESET);
					$objConditions[] = QQ::NotEqual(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT);
					break;
				case ListboxType::AllSessionLogs:
					$objConditions[] = QQ::OrCondition(
						QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGIN),
						QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUT),
						QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::USERLOGOUTFORCE),
						QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONCREATED),
						QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONRESET),
						QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::SESSIONTIMEOUT)
					);
					break;
			}
			
			if ($this->lstItem->SelectedValue) {
				switch ($this->lstType->SelectedValue) {
					case ListboxType::Organisation:
					case ListboxType::Project:
					case ListboxType::Tape:
						$objConditions[] = QQ::Equal(QQN::Log()->ItemId, $this->lstItem->SelectedValue);
						break;
				}
			}
			
			if ($this->lstAction->SelectedValue) {
				switch ($this->lstAction->SelectedValue) {
					case ListboxAction::Add:
						$objConditions[] = QQ::Equal(QQN::Log()->LogActionEnumId, LogActionEnum::ADD);
						break;
					case ListboxAction::Edit:
						$objConditions[] = QQ::Equal(QQN::Log()->LogActionEnumId, LogActionEnum::EDT);
						break;
					case ListboxAction::Delete:
						$objConditions[] = QQ::Equal(QQN::Log()->LogActionEnumId, LogActionEnum::DEL);
						break;
				}
			}
			
			$this->dtgLog->TotalItemCount = Log::QueryCount(new QQConditionAnd($objConditions));
			$this->dtgLog->DataSource = Log::QueryArray(
				new QQConditionAnd($objConditions),
				QQ::Clause(
					QQ::Expand(QQN::Log()->User),
					$this->dtgLog->OrderByClause,
					$this->dtgLog->LimitClause
				)
			);
		}
	}
	
	public function lstUser_Change() {
		if ($this->lstUser->SelectedValue === false && $this->lstSession->SelectedValue == null && 
				$this->lstAddress->SelectedValue == null && $this->lstType->SelectedValue == null && 
				$this->lstItem->SelectedValue == null && $this->lstAction->SelectedValue == null && $this->txtItems->Text == 10) {
			$this->btnReset->Enabled = false;
		} else {
			$this->btnReset->Enabled = true;
		}
		
		$this->dtgLog->PageNumber = 1;
		$this->dtgLog_Bind();
	}
	
	protected function lstUser_Fill($intSelectedValue = false) {
		$this->lstUser->RemoveAllItems();
		$this->lstUser->AddItem(QApplication::Translate('- Users -'), false, ($intSelectedValue === false));
		$this->lstUser->AddItem(QApplication::Translate('(nobody)'), null, ($intSelectedValue === null));
		foreach (User::LoadAll(QQ::Clause(QQ::OrderBy(QQN::User()->Name))) as $objUser) {
			$this->lstUser->AddItem($objUser->Name, $objUser->Id, ($intSelectedValue == $objUser->Id));
		}
	}
	
	public function lstSession_Change() {
		if ($this->lstUser->SelectedValue === false && $this->lstSession->SelectedValue == null && 
				$this->lstAddress->SelectedValue == null && $this->lstType->SelectedValue == null && 
				$this->lstItem->SelectedValue == null && $this->lstAction->SelectedValue == null && $this->txtItems->Text == 10) {
			$this->btnReset->Enabled = false;
		} else {
			$this->btnReset->Enabled = true;
		}

		if ($this->lstSession->SelectedValue && !$this->dtgLog->GetColumnByName(QApplication::Translate('Session')) && !$this->dtgLog->GetColumnByName(QApplication::Translate('Address'))) {
			 $this->dtgLog_ShowAdvancedColumns();
		} else if (!$this->blnAdvanced && $this->lstSession->SelectedValue == null && $this->lstAddress->SelectedValue == null) {
			 $this->dtgLog_HideAdvancedColumns();
		}
		
		$this->dtgLog->PageNumber = 1;
		$this->dtgLog_Bind();
	}
	
	protected function lstSession_Fill($strSelectedValue = null) {
		$this->lstSession->RemoveAllItems();
		$this->lstSession->AddItem(QApplication::Translate('- Sessions -'), null);
		foreach (Log::QueryArray(
			QQ::All(),
			QQ::Clause(
				QQ::GroupBy(QQN::Log()->SessionName)
			)
		) as $objLogSession) {
			$this->lstSession->AddItem($objLogSession->SessionName, $objLogSession->SessionName, ($strSelectedValue == $objLogSession->SessionName));
		}
	}
	
	public function lstAddress_Change() {
		if ($this->lstUser->SelectedValue === false && $this->lstSession->SelectedValue == null && 
				$this->lstAddress->SelectedValue == null && $this->lstType->SelectedValue == null && 
				$this->lstItem->SelectedValue == null && $this->lstAction->SelectedValue == null && $this->txtItems->Text == 10) {
			$this->btnReset->Enabled = false;
		} else {
			$this->btnReset->Enabled = true;
		}
		
		if ($this->lstAddress->SelectedValue && !$this->dtgLog->GetColumnByName(QApplication::Translate('Session')) && !$this->dtgLog->GetColumnByName(QApplication::Translate('Address'))) {
			$this->dtgLog_ShowAdvancedColumns();
		} else if (!$this->blnAdvanced && $this->lstSession->SelectedValue == null && $this->lstAddress->SelectedValue == null) {
			 $this->dtgLog_HideAdvancedColumns();
		}
		
		$this->dtgLog->PageNumber = 1;
		$this->dtgLog_Bind();
	}
	
	protected function lstAddress_Fill($strSelectedValue = null) {
		$this->lstAddress->RemoveAllItems();
		$this->lstAddress->AddItem(QApplication::Translate('- Addresses -'), null);
		
		foreach (Log::QueryArray(
			QQ::All(),
			QQ::Clause(
				QQ::GroupBy(QQN::Log()->IpAddress)
			)
		) as $objLogAddress) {
			$this->lstAddress->AddItem($objLogAddress->IpAddress, $objLogAddress->IpAddress, ($strSelectedValue == $objLogAddress->IpAddress));
		}
	}
	
	public function lstType_Change() {
		if ($this->lstUser->SelectedValue === false && $this->lstSession->SelectedValue == null && 
				$this->lstAddress->SelectedValue == null && $this->lstType->SelectedValue == null && 
				$this->lstItem->SelectedValue == null && $this->lstAction->SelectedValue == null && $this->txtItems->Text == 10) {
			$this->btnReset->Enabled = false;
		} else {
			$this->btnReset->Enabled = true;
		}
		
		switch ($this->lstType->SelectedValue) {
			default:
				$this->lstAction->Enabled = false;
				$this->lstAction->RemoveAllItems();
				$this->lstAction->AddItem(QApplication::Translate('- Actions -'), null);
				
				$this->lstItem->Enabled = false;
				$this->lstItem->RemoveAllItems();
				$this->lstItem->AddItem(QApplication::Translate('- Items -'), null);
				
				$this->dtgLog->PageNumber = 1;
				$this->dtgLog_Bind();
				break;
			
			case ListboxType::Organisation:
			case ListboxType::Project:
			case ListboxType::Tape:
				if (!$this->lstAction->SelectedValue) {
					$this->lstAction->Enabled = true;
					$this->lstAction->RemoveAllItems();
					$this->lstAction->AddItem(QApplication::Translate('- Actions -'), null);
					$this->lstAction->AddItem(QApplication::Translate('Add'), ListboxAction::Add);
					$this->lstAction->AddItem(QApplication::Translate('Edit'), ListboxAction::Edit);
					$this->lstAction->AddItem(QApplication::Translate('Delete'), ListboxAction::Delete);
				}
						
				$this->lstItem->Enabled = true;
				$this->lstItem_Fill();
				
				$this->dtgLog_Bind();
				break;
			
			case ListboxType::ExceptSessions:
				if (!$this->lstAction->SelectedValue) {
					$this->lstAction->Enabled = true;
					$this->lstAction->RemoveAllItems();
					$this->lstAction->AddItem(QApplication::Translate('- Actions -'), null);
					$this->lstAction->AddItem(QApplication::Translate('Add'), ListboxAction::Add);
					$this->lstAction->AddItem(QApplication::Translate('Edit'), ListboxAction::Edit);
					$this->lstAction->AddItem(QApplication::Translate('Delete'), ListboxAction::Delete);
				}
								
				$this->lstItem->Enabled = false;
				$this->lstItem->RemoveAllItems();
				$this->lstItem->AddItem(QApplication::Translate('- Items -'), null);
				
				$this->dtgLog->PageNumber = 1;
				$this->dtgLog_Bind();
				break;
		}
	}
	
	public function lstItem_Change() {
		$this->dtgLog->PageNumber = 1;
		$this->dtgLog_Bind();
	}
	
	public function lstItem_Fill($intSelectedValue = null) {
		$this->lstItem->RemoveAllItems();
		$this->lstItem->AddItem(QApplication::Translate('- Items -'), null);
		
		switch ($this->lstType->SelectedValue) {
			default:
				$objCondition = QQ::None();
				break;
			case ListboxType::Organisation:
				$objCondition = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::ORGANISATION);
				break;
			case ListboxType::Project:
				$objCondition = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::PROJECT);
				break;
			case ListboxType::Tape:
				$objCondition = QQ::Equal(QQN::Log()->LogItemEnumId, LogItemEnum::TAPE);
				break;
		}
		
		foreach (Log::QueryArray(
			QQ::AndCondition(
				$objCondition,
				QQ::OrCondition(
					QQ::Equal(QQN::Log()->LogActionEnumId, LogActionEnum::ADD),
					QQ::Equal(QQN::Log()->LogActionEnumId, LogActionEnum::EDT),
					QQ::Equal(QQN::Log()->LogActionEnumId, LogActionEnum::DEL)
				)
			),
			QQ::Clause(
				QQ::GroupBy(QQN::Log()->ItemId)
			)
		) as $objItem) {
			$this->lstItem->AddItem(QApplication::GetStringOfLogItemIdBasedOnLogItemEnumId($objItem->ItemId, $objItem->LogItemEnumId, false), $objItem->ItemId, ($intSelectedValue == $objItem->ItemId));
		}
	}
	
	public function lstAction_Change() {
		$this->dtgLog->PageNumber = 1;
		$this->dtgLog_Bind();
	}
	
	public function btnAdvanced_Click() {
		if (!QApplication::GetConfig('recent_userlogins', QType::Boolean) && !QApplication::GetConfig('development_recent_debug', QType::Boolean)) {
			if (!$this->blnAdvanced) {
				$this->lstSession->Visible = true;
				$this->lstAddress->Visible = true;
			} else {
				$this->lstSession->Visible = false;
				$this->lstAddress->Visible = false;
			}
		}
		
		if (!$this->dtgLog->GetColumnByName(QApplication::Translate('Session')) && !$this->dtgLog->GetColumnByName(QApplication::Translate('Address'))) {
			$this->dtgLog_ShowAdvancedColumns();
			$this->blnAdvanced = true;
		} else {
			$this->dtgLog_HideAdvancedColumns();
			$this->blnAdvanced = false;
		}
	}
	
	public function btnRefresh_Click() {
		$this->lstUser_fill($this->lstUser->SelectedValue);
		$this->lstSession_Fill($this->lstSession->SelectedValue);
		$this->lstAddress_Fill($this->lstAddress->SelectedValue);
		$this->lstItem_Fill($this->lstItem->SelectedValue);
		$this->dtgLog_Bind();
	}
	
	public function btnReset_Click() {
		$this->dtgLog->PageNumber = 1;
		$this->dtgLog->ItemsPerPage = 10;
		$this->dtgLog->SortColumnIndex = 0;
		
		$this->txtItems->Text = 10;
		
		$this->lstUser->SelectedIndex = 0;
		$this->lstUser_Change();
		
		$this->lstSession->SelectedIndex = 0;
		$this->lstSession_Change();
		
		$this->lstAddress->SelectedIndex = 0;
		$this->lstAddress_Change();
		
		$this->lstType->SelectedIndex = 0;
		$this->lstType_Change();
		
		$this->btnReset->Enabled = false;
	}
		
	public function txtItems_Change() {
		$this->dtgLog->ItemsPerPage = $this->txtItems->Text;
		
		if ($this->txtItems->Text != 10) {
			$this->btnReset->Enabled = true;
		} else {
			$this->btnReset->Enabled = false;
		}
	}
	
	public function txtItems_Escape() {
		$this->txtItems->Focus();
	}
	
	protected function dtgLog_ShowAdvancedColumns() {
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Session'), '<?= $_ITEM->SessionName ?>', array('OrderByClause' => QQ::OrderBy(QQN::Log()->SessionName), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->SessionName, false))));
		$this->btnAdvanced->Text = QApplication::Translate('Basic');
		$this->dtgLog->AddColumn(new QDataGridColumn(QApplication::Translate('Address'), '<?= QApplication::MakeAcronymOfAddress($_ITEM->IpAddress) ?>', 'HtmlEntities=false', array('OrderByClause' => QQ::OrderBy(QQN::Log()->IpAddress), 'ReverseOrderByClause' => QQ::OrderBy(QQN::Log()->IpAddress, false))));
	}
	
	protected function dtgLog_HideAdvancedColumns() {
		$this->dtgLog->RemoveColumnByName(QApplication::Translate('Session'));
		$this->dtgLog->RemoveColumnByName(QApplication::Translate('Address'));
		$this->btnAdvanced->Text = QApplication::Translate('Advanced');
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'TextIntro': return $this->strTextIntro;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

abstract class ListboxType {
	const Project = 1;
	const Organisation = 2;
	const Tape = 4;
	
	const Login = 8;
	const Logout = 16;
	const Timeout = 32;
	const Created = 64;
	const ForcedLogout = 128;
	const Reset = 256;
	
	const ExceptSessions = 512;
	const AllSessionLogs = 1024;
}

abstract class ListboxAction {
	const Add = 1;
	const Edit = 2;
	const Delete =4;
}

RecentChangesForm::Run('RecentChangesForm', QApplication::GetTemplatePath(__FILE__));

?>
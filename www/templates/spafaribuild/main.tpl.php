<?php require('header.inc.php'); ?>

<h1><?php _p($this->TextWelcome); ?></h1>

<p><?php _p($this->TextNews); ?></p>

<table class="GrayArea">
	<tr>
		<td class="MainNewsTitle"><?php $this->lstNews->Render(); ?>&nbsp;<?php $this->objDefaultWaitIcon->Render(); ?></td>
		<td class="MainNewsAuthor"><?php $this->lblNewsAuthor->Render(); ?></td>
		<td class="MainNewsDate"><?php _t('Added: '); $this->lblNewsAdded->Render(); ?><br />
			<?php _t('Changed: '); $this->lblNewsChanged->Render(); ?></td>
	</tr>
	<tr>
		<td colspan="3"><?php $this->lblNewsText->Render(); ?></td>
	</tr>
</table>

<h1><?php _t('Recent changes'); ?></h1>

<p><?php _t('The following items are the recent items of the sidebar, but more detailed.'); ?></p>

<?php $this->dtgRecentLog->Render(); ?>

<?php require('footer.inc.php'); ?>
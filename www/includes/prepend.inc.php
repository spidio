<?php
	if (!defined('__PREPEND_INCLUDED__')) {
		// Ensure prepend.inc is only executed once
		define('__PREPEND_INCLUDED__', 1);

		///////////////////////////////////
		// Define Server-specific constants
		///////////////////////////////////	
		/*
		 * This assumes that the configuration include file is in the same directory
		 * as this prepend include file.  For security reasons, you can feel free
		 * to move the configuration file anywhere you want.  But be sure to provide
		 * a relative or absolute path to the file.
		 */
		require(dirname(__FILE__) . '/configuration.inc.php');


		//////////////////////////////
		// Include the Qcodo Framework
		//////////////////////////////
		require(__QCODO_CORE__ . '/qcodo.inc.php');


		///////////////////////////////
		// Define the Application Class
		///////////////////////////////
		/**
		 * The Application class is an abstract class that statically provides
		 * information and global utilities for the entire web application.
		 *
		 * Custom constants for this webapp, as well as global variables and global
		 * methods should be declared in this abstract class (declared statically).
		 *
		 * This Application class should extend from the ApplicationBase class in
		 * the framework.
		 */
		abstract class QApplication extends QApplicationBase {
			/**
			 * This is called by the PHP5 Autoloader.  This method overrides the
			 * one in ApplicationBase.
			 *
			 * @return void
			 */
			public static function Autoload($strClassName) {
				// First use the Qcodo Autoloader
				if (!parent::Autoload($strClassName)) {
					if (file_exists($strFilePath = sprintf('%s/types/%s.class.php', __INCLUDES__, $strClassName))) {
						require($strFilePath);
						return true;
					}
				}
			}

			////////////////////////////
			// QApplication Customizations (e.g. EncodingType, etc.)
			////////////////////////////
			// public static $EncodingType = 'ISO-8859-1';
			public static $Auth;
			public static $BBCode;

			////////////////////////////
			// Additional Static Methods
			////////////////////////////
			/**
			 * Add Log entry.
			 *
			 * @param int $intLogItemEnum constant of LogItemEnum
			 * @param int $intLogActionEnum constant of LogActionEnum
			 * @param int $intItemId id of item where the log entry is about
			 */
			public static function AddLogEntry($intLogItemEnum, $intLogActionEnum = LogActionEnum::NUL, $uuidItemId = null) {
				$objLog = new Log();
				
				if ($intLogItemEnum != LogItemEnum::SESSIONCREATED && $intLogItemEnum != LogItemEnum::SESSIONTIMEOUT) {
					$objAuthentication = self::GetAuth();
					$objUser = $objAuthentication->Session->User;
					$objLog->UserId = $objUser->Id;
				} else {
					$objLog->UserId = null;
				}
				
				$objLog->LogItemEnumId = $intLogItemEnum;
				$objLog->LogActionEnumId = $intLogActionEnum;
				$objLog->Date = new QDateTime(QDateTime::Now);
				$objLog->ItemId = $uuidItemId;
				$objLog->SessionName = self::SessionName();
				$objLog->IpAddress = (string) new IpAddress(self::GetClientAddress());
				$objLog->Save();
			}
			
			/**
			 * Check permission for this item based on the requesting User and action.
			 *
			 * @param User $objUser
			 * @param string $strClassName
			 * @param string $strAuthActionName
			 * @return bool
			 */
			public static function Can(User $objUser, $strClassName, $strAuthActionName) {
				$objObject = new $strClassName();
				return $objObject->Can($objUser, $strAuthActionName);
			}
			
			/**
			 * Get Authentication object.
			 *
			 * @return object
			 */
			public static function &GetAuth() {
				if (empty(self::$Auth)) {
					self::InitializeAuth();
				}
				
				return self::$Auth;
			}
			
			/**
			 * Return IP address of the requesting client.
			 *
			 * @return IpAddress
			 */
			public static function GetClientAddress() {
				if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
					$strResult = $_SERVER['HTTP_X_FORWARDED_FOR'];
				} else if (array_key_exists('HTTP_CLIENT_IP', $_SERVER)) {
					$strResult = $_SERVER['HTTP_CLIENT_IP'];
				} else {
					$strResult = $_SERVER['REMOTE_ADDR'];
				}
				
				if (strstr($strResult, ',')) {
					$strResult = strtok($result, ',');
				}
				
				return new IpAddress($strResult);
			}
			
			/**
			 * Get configuration from database.
			 *
			 * @param string $strName
			 * @param string $strType
			 * @return mixed
			 */
			public static function GetConfig($strName, $strType = QType::String) {
//				$objCache = new QCache('config', $strName);
//				if (!($objConfig = unserialize($objCache->GetData($strName)))) {
					$objConfig = Config::Load($strName);
//					$objCache->SaveData(serialize($objConfig));
//				}
				
//				$objConfig->Reload();
				
				if (is_null($objConfig)) {
					return null;
				} else if ($strType == 'Config') {
					return $objConfig;
				} else {
					return QType::Cast($objConfig->Value, $strType);
				}
			}
			
			/**
			 * Get based on LogItemEnumId the responding string representation of $intId.
			 *
			 * @param int $intId
			 * @param int $intLogItemEnumId
			 * @param bool $blnFullName
			 * @return string
			 */
			public static function GetStringOfLogItemIdBasedOnLogItemEnumId($uuidItemId, $intLogItemEnumId, $blnFullName = true) {
				switch ($intLogItemEnumId) {
					case LogItemEnum::ORGANISATION:
						return strval(Organisation::Load($uuidItemId));
					case LogItemEnum::PROJECT:
						if ($blnFullName) {
							return strval(Project::Load($uuidItemId));
						} else {
							$objItem = Project::Load($uuidItemId);
							return sprintf('%s (%s)', $objItem->Name, $objItem->Organisation->Name);
						}
					case LogItemEnum::TAPE:
						return strval(Tape::Load($uuidItemId));
					case LogItemEnum::USERLOGIN:
						return $uuidItemId;
				}
			}
			
			/**
			 * Resolve path of template based on configuration.
			 *
			 * @param string $strFileName
			 * @return string
			 */
			public static function GetTemplatePath($strFileName) {
				if (strpos($strFileName, '.php') !== false) {
					$strFileName = basename($strFileName, '.php');
				}
				
				return sprintf('%s/%s/%s.tpl.php', __TEMPLATES__, QApplication::GetConfig('site_template'), $strFileName);
			}
			
			/**
			 * Initialize Auth services within QApplication.
			 *
			 */
			public static function InitializeAuth() {
				// Load SOAP version if running Soapidio.
				if (defined('RUNNING_SOAPIDIO')) {
					$strAdapterName = ucfirst(self::GetConfig('auth_adapter'));
					if (file_exists($strFileName = sprintf('%s/auth/%s/Soap.php', __INCLUDES__, $strAdapterName))) {
						require_once($strFileName);
						$strClassName = sprintf('%sAuthSoap', $strAdapterName);
						self::$Auth = new $strClassName();
					} else {
						throw new Exception(sprintf(self::Translate('Authentication module %s cannot be found. Application is terminated because module is required to run.'), $strAdapterName));
					}
				} else {
					$strAdapterName = ucfirst(self::GetConfig('auth_adapter'));
					if (file_exists($strFileName = sprintf('%s/auth/%s/User.php', __INCLUDES__, $strAdapterName))) {
						require_once($strFileName);
						$strClassName = sprintf('%sAuthUser', $strAdapterName);
						self::$Auth = new $strClassName();
					} else {
						throw new Exception(sprintf(self::Translate('Authentication module %s cannot be found. Application is terminated because module is required to run.'), $strAdapterName));
					}
				}
			}
			
			/**
			 * Initialize BBCode services within QApplication.
			 *
			 */
			public static function InitializeBBCode() {
				self::$BBCode = new BBCode();
			}
			
			/**
			 * Try to load $strName extension.
			 *
			 * @param string $strName Name of extension to load.
			 */
			public static function LoadExtension($strName) {
				if (!extension_loaded($strName)) {
					$strPrefix = (PHP_SHLIB_SUFFIX === 'dll') ? 'php_' : '';
					@dl(sprintf('%s%s.%s', $strPrefix, $strName, PHP_SHLIB_SUFFIX));
				}
			}
			
			/**
			 * Load a given form.
			 *
			 * @param string $strFileName
			 */
			public static function LoadForm($strFileName) {
				require(sprintf('%s/%s.php', __FORMS__, $strFileName));
			}
						
			/**
			 * Make acronym if configuration allows.
			 *
			 * @param User $objUser
			 * @return string
			 */
			public static function MakeAcronymOfUser($objUser) {
				if (!($objUser instanceof User)) {
					return '';
				}
				
				if (self::GetConfig('site_acronym_user', QType::Boolean)) {
					return sprintf('<acronym title="%s">%s</acronym>', $objUser->Person->Name, $objUser->Name);
				} else {
					return $objUser->Name;
				}
			}
			
			
			public static function MakeAcronymOfAddress($strAddress) {
				$objIpAddress = new IpAddress($strAddress);
				return $objIpAddress->GetAcronymOfAddress();
			}
			
			/**
			 * Make string of a value passed to a QDataTime object.
			 *
			 * @param string $mixValue
			 * @return string
			 */
			public static function MakeDateTimeString($mixValue, $strConfig = 'datetime') {
				$objDateTime = new QDateTime($mixValue);
				return $objDateTime->format(self::GetConfig(sprintf('display_format_%s', $strConfig)));
			}
			
			/**
			 * Make requestable url.
			 *
			 * @param string $strPage
			 * @param string $strMode
			 * @param int $intId
			 * @return string
			 */
			public static function MakeUrl($strPage, $strMode = '', $intId = 0) {
				if (!empty($strMode)) {
					if (!empty($intId)) {
						return sprintf('%s%s/%s/%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, $strPage, $strMode, $intId);
					} else {
						return sprintf('%s%s/%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, $strPage, $strMode);
					}
				} else {
					return sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, $strPage);
				}
			}
			
			/**
			 * Parse BBCode.
			 *
			 * @param string $strText
			 * @return string
			 */
			public static function ParseBBCode($strText) {
				return self::$BBCode->Parse($strText);
			}
			
			/**
			 * Redirect the browser to the login path. All subpaths of /main/... are ignored.
			 *
			 */
			public static function RedirectToLoginPage($strReturnPath = '') {
				if (!empty($strReturnPath)) {
					self::Redirect(sprintf('%s%s/%s/%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, QApplication::GetConfig('pagename_login'), QApplication::GetConfig('pagename_returnto'), $strReturnPath));
				}
				
				if (self::PathInfo(0) == QApplication::GetConfig('pagename_logout')) {
					self::Redirect(sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, QApplication::GetConfig('pagename_index')));
				} else if (self::PathInfo(0) != QApplication::GetConfig('pagename_login')) {
					if (is_null(self::$PathInfo) || self::PathInfo(0) == QApplication::GetConfig('pagename_index')) {
						self::Redirect(sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, QApplication::GetConfig('pagename_login')));
					} else {
						self::Redirect(sprintf('%s%s/%s/%s%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, QApplication::GetConfig('pagename_login'), QApplication::GetConfig('pagename_returnto'), self::$PathInfo));
					}
				}
			}
			
			/**
			 * Redirect the browser to the requested 'returnto' path (/page/returnto/page2).
			 * If the returnto page isn't requested, redirect to the index.
			 *
			 */
			public static function RedirectToReturnPage() {
				if (self::PathInfo(1) == QApplication::GetConfig('pagename_returnto')) {
					self::Redirect(sprintf('%s%s%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, substr(self::$PathInfo, strpos(self::$PathInfo, self::GetConfig('pagename_returnto')) + strlen(QApplication::GetConfig('pagename_returnto')))));
				} else {
					self::Redirect(sprintf('%s%s/%s', __VIRTUAL_DIRECTORY__, __SUBDIRECTORY__, QApplication::GetConfig('pagename_index')));
				}
			}
			
			/**
			 * Return session name.
			 * 
			 * @return string
			 *
			 */
			public static function SessionName() {
				if (empty(self::$Auth)) {
					self::InitializeAuth();
				}
				
				return self::$Auth->SessionGetName();
			}
			
			/**
			 * Update the version information.
			 *
			 */
			public static function UpdateVersionInfo() {
				if (defined('RUNNING_SOAPIDIO') || defined('RUNNING_SOAPIDIO_CLIENT')) {
					require sprintf('%s/.soapversion.php', __SOAPSYS__);
					new SoapVersion();
				} else {
					require sprintf('%s/.versioninfo.php', __INCLUDES__);
					new CoreVersion();
				}
			}
			
			
			public static function UserIsLoggedIn() {
				if (empty(self::$Auth)) {
					self::InitializeAuth();
				}
				
				return self::$Auth->IsLoggedIn();
			}
		}


		//////////////////////////
		// Custom Global Functions
		//////////////////////////
		// Include any other global functions (if any) here.
		function _tf() {
			if (func_num_args() > 1) {
				// Print, via Translation (if applicable)
				
				$mixArguments = func_get_args();
				$mixArguments[0] = QApplication::Translate(func_get_arg(0));
				
				call_user_func_array('printf', $mixArguments);
			} else {
				throw new QCallerException('You must provide at least 2 arguments');
			}
		}


		////////////////
		// Include Files
		////////////////
		// Include any other include files (if any) here...


		///////////////////////
		// Setup Error Handling
		///////////////////////
		/*
		 * Set Error/Exception Handling to the default
		 * Qcodo HandleError and HandlException functions
		 * (Only in non CLI mode)
		 *
		 * Feel free to change, if needed, to your own
		 * custom error handling script(s).
		 */
		if (array_key_exists('SERVER_PROTOCOL', $_SERVER)) {
			set_error_handler('QcodoHandleError');
			set_exception_handler('QcodoHandleException');
		}


		////////////////////////////////////////////////
		// Initialize the Application and DB Connections
		////////////////////////////////////////////////
		QApplication::Initialize();
		QApplication::InitializeDatabaseConnections();
		
		if (version_compare(PHP_VERSION, '5.2.0', '<')) {
			die('Please upgrade to at least PHP version 5.2.0.');
		}
		
		CookieStore::Initialize();

		//////////////////////////////////////////////
		// Setup Internationalization and Localization (if applicable)
		// Note, this is where you would implement code to do Language Setting discovery, as well, for example:
		// * Checking against $_GET['language_code']
		// * checking against session (example provided below)
		// * Checking the URL
		// * etc.
		//////////////////////////////////////////////
		// QApplication::$CountryCode = 'us';
		QApplication::$LanguageCode = QApplication::GetConfig('i18n_language');
		QI18n::Initialize();
	}
?>
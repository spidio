-- phpMyAdmin SQL Dump
-- version 2.11.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 30, 2007 at 03:48 AM
-- Server version: 5.0.45
-- PHP Version: 5.2.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `spidio_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `spidio_address`
--

CREATE TABLE IF NOT EXISTS `spidio_address` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `street` text collate utf8_bin NOT NULL,
  `code` varchar(10) collate utf8_bin NOT NULL,
  `town` varchar(255) collate utf8_bin NOT NULL,
  `country` char(3) collate utf8_bin NOT NULL COMMENT 'ISO 3166-1 alpha=3',
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_address`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_camera`
--

CREATE TABLE IF NOT EXISTS `spidio_camera` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `organisation_id` mediumint(8) unsigned NOT NULL,
  `brand` varchar(255) collate utf8_bin NOT NULL,
  `model` varchar(50) collate utf8_bin NOT NULL,
  `serial` varchar(30) collate utf8_bin default NULL,
  `comment` text collate utf8_bin NOT NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_organisation_id` (`organisation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_camera`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_camera_purpose`
--

CREATE TABLE IF NOT EXISTS `spidio_camera_purpose` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `comment` text collate utf8_bin NOT NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_camera_purpose`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_codec`
--

CREATE TABLE IF NOT EXISTS `spidio_codec` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `fourcc` char(4) collate utf8_bin default NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`fourcc`),
  KEY `FK_type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `spidio_codec`
--

INSERT INTO `spidio_codec` (`id`, `name`, `type`, `fourcc`, `optlock`) VALUES
(1, 'Digital Video', 1, 'DVCS', '0000-00-00 00:00:00'),
(2, 'Uncompressed Audio', 2, 'ARAW', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `spidio_codec_types`
--

CREATE TABLE IF NOT EXISTS `spidio_codec_types` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` char(5) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `spidio_codec_types`
--

INSERT INTO `spidio_codec_types` (`id`, `name`) VALUES
(2, 'audio'),
(3, 'other'),
(1, 'video');

-- --------------------------------------------------------

--
-- Table structure for table `spidio_config`
--

DROP TABLE IF EXISTS `spidio_config`;
CREATE TABLE IF NOT EXISTS `spidio_config` (
  `name` varchar(255) collate utf8_bin NOT NULL,
  `value` varchar(255) collate utf8_bin NOT NULL,
  `is_dynamic` tinyint(1) NOT NULL default '0',
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `spidio_config`
--

INSERT INTO `spidio_config` (`name`, `value`, `is_dynamic`, `optlock`) VALUES
('copyright_author', 'Spidio Project', 0, '2007-12-30 23:22:44'),
('copyright_years', '2007-2008', 0, '2007-12-30 23:22:48'),
('pagename_index', 'main', 0, '2007-12-30 01:44:11'),
('pagename_login', 'login', 0, '2007-12-30 01:44:32'),
('pagename_logout', 'logout', 0, '2007-12-30 12:46:46'),
('pagename_profile', 'profile', 0, '2007-12-30 22:32:08'),
('pagename_returnto', 'returnto', 0, '2007-12-30 02:00:31'),
('session_timeout', '1800', 0, '2007-12-30 23:32:00'),
('site_logofile', 'logo_placeholder.gif', 0, '2007-12-30 23:23:09'),
('site_template', 'spafaribuild', 0, '2007-12-30 23:22:25'),
('site_title', 'Spidio Development', 0, '0000-00-00 00:00:00'),
('software_name', 'Spidio', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `spidio_file`
--

CREATE TABLE IF NOT EXISTS `spidio_file` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `recording_id` mediumint(8) unsigned NOT NULL,
  `file_type_id` mediumint(8) unsigned NOT NULL,
  `codec_id_video` mediumint(8) unsigned default NULL,
  `codec_id_audio` mediumint(8) unsigned default NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `comment` text collate utf8_bin,
  `added` datetime NOT NULL,
  `changed` datetime NOT NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_recording_id` (`recording_id`),
  KEY `FK_codec_id_video` (`codec_id_video`),
  KEY `FK_codec_id_audio` (`codec_id_audio`),
  KEY `FK_file_type_id` (`file_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_file`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_file_extension`
--

CREATE TABLE IF NOT EXISTS `spidio_file_extension` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `extension` varchar(25) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `spidio_file_extension`
--

INSERT INTO `spidio_file_extension` (`id`, `name`, `extension`) VALUES
(1, 'Audio Video Interleave', 'avi'),
(2, 'Digital Video', 'dv'),
(3, 'Audio Video Interleave (Multiplexed)', 'avi');

-- --------------------------------------------------------

--
-- Table structure for table `spidio_location`
--

CREATE TABLE IF NOT EXISTS `spidio_location` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `address_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`),
  KEY `FK_address_id` (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_location`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_log`
--

CREATE TABLE IF NOT EXISTS `spidio_log` (
  `id` bigint(20) NOT NULL auto_increment,
  `user_id` mediumint(8) unsigned NOT NULL,
  `log_action_id` smallint(5) NOT NULL,
  `date` datetime NOT NULL,
  `address` varchar(49) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`),
  KEY `log_action_id` (`log_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_log_types`
--

CREATE TABLE IF NOT EXISTS `spidio_log_types` (
  `id` smallint(5) NOT NULL auto_increment,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `constant` varchar(15) collate utf8_bin NOT NULL,
  `explaination` text collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_log_types`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_organisation`
--

CREATE TABLE IF NOT EXISTS `spidio_organisation` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `address_id` mediumint(8) unsigned NOT NULL,
  `person_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `phone` bigint(20) unsigned default NULL,
  `phone_a` tinyint(5) unsigned default NULL,
  `phone_c` tinyint(3) unsigned default NULL,
  `email` varchar(255) collate utf8_bin default NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`),
  KEY `FK_address_id` (`address_id`),
  KEY `FK_person_id` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_organisation`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_person`
--

CREATE TABLE IF NOT EXISTS `spidio_person` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `address_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `email` varchar(255) collate utf8_bin NOT NULL,
  `phone` bigint(20) unsigned default NULL,
  `phone_a` tinyint(5) unsigned default NULL,
  `phone_c` tinyint(3) unsigned default NULL,
  `mobile` bigint(20) unsigned default NULL,
  `mobile_a` tinyint(5) unsigned default NULL,
  `mobile_c` tinyint(3) unsigned default NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`,`email`),
  KEY `FK_address_id` (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_person`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_project`
--

CREATE TABLE IF NOT EXISTS `spidio_project` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `project_type_id` smallint(5) unsigned NOT NULL,
  `organisation_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `begin` date NOT NULL,
  `end` date NOT NULL,
  `added` datetime NOT NULL,
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`),
  KEY `FK_organisation_id` (`organisation_id`),
  KEY `FK_project_type_id` (`project_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_project`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_project_type`
--

CREATE TABLE IF NOT EXISTS `spidio_project_type` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `comment` text collate utf8_bin NOT NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_project_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_recording`
--

CREATE TABLE IF NOT EXISTS `spidio_recording` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `project_id` mediumint(8) unsigned NOT NULL,
  `tape_id` mediumint(8) unsigned NOT NULL,
  `camera_id` smallint(5) unsigned default NULL,
  `camera_purpose_id` mediumint(8) unsigned default NULL,
  `tape_mode_type_id` tinyint(1) unsigned NOT NULL,
  `recording_type_id` smallint(5) unsigned NOT NULL,
  `start` tinyint(2) unsigned NOT NULL,
  `end` tinyint(2) unsigned NOT NULL,
  `widescreen` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `added` datetime NOT NULL,
  `take` tinyint(3) unsigned default NULL,
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_tape_id` (`tape_id`),
  KEY `FK_project_id` (`project_id`),
  KEY `FK_camera_id` (`camera_id`),
  KEY `FK_camera_purpose_id` (`camera_purpose_id`),
  KEY `FK_tape_mode_type_id` (`tape_mode_type_id`),
  KEY `FK_recording_asset_id` (`recording_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_recording`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_recording_type`
--

CREATE TABLE IF NOT EXISTS `spidio_recording_type` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_recording_type`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_session`
--

CREATE TABLE IF NOT EXISTS `spidio_session` (
  `id` varchar(40) collate utf8_bin NOT NULL,
  `session_data` longtext collate utf8_bin NOT NULL,
  `last_access` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `spidio_session`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_tape`
--

CREATE TABLE IF NOT EXISTS `spidio_tape` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `organisation_id` mediumint(8) unsigned NOT NULL,
  `tape_mode_type_id` tinyint(3) unsigned default NULL,
  `added` datetime NOT NULL,
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_organisation_id` (`organisation_id`),
  KEY `FK_tape_mode_type_id` (`tape_mode_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_tape`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_tape_location`
--

CREATE TABLE IF NOT EXISTS `spidio_tape_location` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `tape_id` mediumint(8) unsigned NOT NULL,
  `location_id` mediumint(8) unsigned NOT NULL,
  `person_id` mediumint(8) unsigned NOT NULL,
  `date` datetime NOT NULL,
  `comment` text collate utf8_bin,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `FK_location_id` (`location_id`),
  KEY `FK_person_id` (`person_id`),
  KEY `FK_tape_id` (`tape_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_tape_location`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_tape_mode_types`
--

CREATE TABLE IF NOT EXISTS `spidio_tape_mode_types` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` char(2) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `spidio_tape_mode_types`
--

INSERT INTO `spidio_tape_mode_types` (`id`, `name`) VALUES
(2, 'LP'),
(1, 'SP');

-- --------------------------------------------------------

--
-- Table structure for table `spidio_template`
--

CREATE TABLE IF NOT EXISTS `spidio_template` (
  `id` smallint(5) unsigned NOT NULL auto_increment,
  `name` varchar(25) collate utf8_bin NOT NULL,
  `version` varchar(10) collate utf8_bin NOT NULL,
  `author` varchar(255) collate utf8_bin NOT NULL,
  `email` varchar(255) collate utf8_bin NOT NULL,
  `copyright` varchar(255) collate utf8_bin NOT NULL,
  `website` varchar(255) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Dumping data for table `spidio_template`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_template_config`
--

CREATE TABLE IF NOT EXISTS `spidio_template_config` (
  `name` varchar(255) collate utf8_bin NOT NULL,
  `value` varchar(255) collate utf8_bin NOT NULL,
  `is_dynamic` tinyint(1) NOT NULL default '0',
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `spidio_template_config`
--


-- --------------------------------------------------------

--
-- Table structure for table `spidio_user`
--

CREATE TABLE IF NOT EXISTS `spidio_user` (
  `id` mediumint(8) unsigned NOT NULL auto_increment,
  `person_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) collate utf8_bin NOT NULL,
  `password` char(40) collate utf8_bin NOT NULL,
  `registered` datetime NOT NULL,
  `last_logged_in` datetime NOT NULL,
  `optlock` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `unique` (`name`),
  KEY `FK_person_id` (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `spidio_user`
--


--
-- Constraints for dumped tables
--

--
-- Constraints for table `spidio_camera`
--
ALTER TABLE `spidio_camera`
  ADD CONSTRAINT `spidio_camera_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `spidio_organisation` (`id`);

--
-- Constraints for table `spidio_codec`
--
ALTER TABLE `spidio_codec`
  ADD CONSTRAINT `spidio_codec_ibfk_1` FOREIGN KEY (`type`) REFERENCES `spidio_codec_types` (`id`);

--
-- Constraints for table `spidio_file`
--
ALTER TABLE `spidio_file`
  ADD CONSTRAINT `spidio_file_ibfk_1` FOREIGN KEY (`recording_id`) REFERENCES `spidio_recording` (`id`),
  ADD CONSTRAINT `spidio_file_ibfk_2` FOREIGN KEY (`file_type_id`) REFERENCES `spidio_file_extension` (`id`),
  ADD CONSTRAINT `spidio_file_ibfk_3` FOREIGN KEY (`codec_id_video`) REFERENCES `spidio_codec` (`id`),
  ADD CONSTRAINT `spidio_file_ibfk_4` FOREIGN KEY (`codec_id_audio`) REFERENCES `spidio_codec` (`id`);

--
-- Constraints for table `spidio_location`
--
ALTER TABLE `spidio_location`
  ADD CONSTRAINT `spidio_location_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `spidio_address` (`id`);

--
-- Constraints for table `spidio_log`
--
ALTER TABLE `spidio_log`
  ADD CONSTRAINT `spidio_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `spidio_user` (`id`),
  ADD CONSTRAINT `spidio_log_ibfk_2` FOREIGN KEY (`log_action_id`) REFERENCES `spidio_log_types` (`id`);

--
-- Constraints for table `spidio_organisation`
--
ALTER TABLE `spidio_organisation`
  ADD CONSTRAINT `spidio_organisation_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `spidio_address` (`id`),
  ADD CONSTRAINT `spidio_organisation_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `spidio_person` (`id`);

--
-- Constraints for table `spidio_person`
--
ALTER TABLE `spidio_person`
  ADD CONSTRAINT `spidio_person_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `spidio_address` (`id`);

--
-- Constraints for table `spidio_project`
--
ALTER TABLE `spidio_project`
  ADD CONSTRAINT `spidio_project_ibfk_1` FOREIGN KEY (`project_type_id`) REFERENCES `spidio_project_type` (`id`),
  ADD CONSTRAINT `spidio_project_ibfk_2` FOREIGN KEY (`organisation_id`) REFERENCES `spidio_organisation` (`id`);

--
-- Constraints for table `spidio_recording`
--
ALTER TABLE `spidio_recording`
  ADD CONSTRAINT `spidio_recording_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `spidio_project` (`id`),
  ADD CONSTRAINT `spidio_recording_ibfk_2` FOREIGN KEY (`tape_id`) REFERENCES `spidio_tape` (`id`),
  ADD CONSTRAINT `spidio_recording_ibfk_3` FOREIGN KEY (`camera_id`) REFERENCES `spidio_camera` (`id`),
  ADD CONSTRAINT `spidio_recording_ibfk_4` FOREIGN KEY (`camera_purpose_id`) REFERENCES `spidio_camera_purpose` (`id`),
  ADD CONSTRAINT `spidio_recording_ibfk_5` FOREIGN KEY (`tape_mode_type_id`) REFERENCES `spidio_tape_mode_types` (`id`),
  ADD CONSTRAINT `spidio_recording_ibfk_6` FOREIGN KEY (`recording_type_id`) REFERENCES `spidio_recording_type` (`id`);

--
-- Constraints for table `spidio_tape`
--
ALTER TABLE `spidio_tape`
  ADD CONSTRAINT `spidio_tape_ibfk_1` FOREIGN KEY (`organisation_id`) REFERENCES `spidio_organisation` (`id`),
  ADD CONSTRAINT `spidio_tape_ibfk_2` FOREIGN KEY (`tape_mode_type_id`) REFERENCES `spidio_tape_mode_types` (`id`);

--
-- Constraints for table `spidio_tape_location`
--
ALTER TABLE `spidio_tape_location`
  ADD CONSTRAINT `spidio_tape_location_ibfk_1` FOREIGN KEY (`tape_id`) REFERENCES `spidio_tape` (`id`),
  ADD CONSTRAINT `spidio_tape_location_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `spidio_location` (`id`),
  ADD CONSTRAINT `spidio_tape_location_ibfk_3` FOREIGN KEY (`person_id`) REFERENCES `spidio_person` (`id`);

--
-- Constraints for table `spidio_user`
--
ALTER TABLE `spidio_user`
  ADD CONSTRAINT `spidio_user_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `spidio_person` (`id`);

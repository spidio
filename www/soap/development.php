<?php

// Load Spidio SOAP foundation.
require('prepend.inc.php');

class SoapidioDevelopment extends SoapidioService {
	/**
	 * Generate UUID.
	 *
	 * @param string $strPassword
	 * @param string $strSalt
	 * @return string
	 */
	public function GeneratePasswordSalt($strPassword, $strSalt) {
		return $this->objAuth->GenerateSaltedPassword($strPassword, $strSalt);
	}
	
	/**
	 * Generate UUID.
	 *
	 * @param int $intVersion
	 * @return UUID
	 */
	public function GenerateUUID($intVersion) {
		return new UUID(null, $intVersion);
	}
	
	/**
	 * Get Session name.
	 *
	 * @return string
	 */
	public function GetSessionName() {
		return $this->objAuth->SessionGetName();
	}
	
	/**
	 * Get Session name.
	 *
	 * @return string
	 */
	public function GetVarQcodo() {
		$strQcodoVarArray = array(
			'QCODO_VERSION'					=>	QCODO_VERSION,
			'__SUBDIRECTORY__'				=>	__SUBDIRECTORY__,
			'__VIRTUAL_DIRECTORY__'			=>	__VIRTUAL_DIRECTORY__,
			'__INCLUDES__'					=>	__INCLUDES__,
			'__QCODO_CORE__'				=>	__QCODO_CORE__,
			'ERROR_PAGE_PATH'				=>	ERROR_PAGE_PATH,
			'PHP Include Path'				=>	get_include_path(),
			'QApplication::$DocumentRoot'	=>	QApplication::$DocumentRoot,
			'QApplication::$EncodingType'	=>	QApplication::$EncodingType,
			'QApplication::$PathInfo'		=>	QApplication::$PathInfo,
			'QApplication::$QueryString'	=>	QApplication::$QueryString,
			'QApplication::$RequestUri'		=>	QApplication::$RequestUri,
			'QApplication::$ScriptFilename'	=>	QApplication::$ScriptFilename,
			'QApplication::$ScriptName'		=>	QApplication::$ScriptName,
			'QApplication::$ServerAddress'	=>	QApplication::$ServerAddress
		);
		
		if (QApplication::$Database) {
			$strQcodoVarArray['QApplication::$Database'] = array();
			foreach (QApplication::$Database as $intKey => $objObject) {
				$strQcodoVarArray['QApplication::$Database'][$intKey] = unserialize(constant('DB_CONNECTION_' . $intKey));
			}
		}
		
		return sprintf('<pre style="text-align: left;">%s</pre>', print_r($strQcodoVarArray, true));
	}
	
	/**
	 * Return $_SERVER.
	 *
	 * @return string
	 */
	public function GetVarPhpServer() {
		return sprintf('<pre style="text-align: left;">%s</pre>', print_r($_SERVER, true));
	}
}

SoapidioDevelopment::Run('SoapidioDevelopment', 'http://spidio.sourceforge.net/namespaces/development');

?>
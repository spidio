#!/bin/sh
# Copyright (c) 2008 Spidio Project
# Licensed under original BSD License.

echo --------------------------
echo Running git-gc
echo WC: $PWD
echo --------------------------
git-gc
echo

echo --------------------------
echo Running git-fsck
echo WC: $PWD
echo --------------------------
git-fsck
echo

<?php
	/**
	 * The abstract FileGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the File subclass which
	 * extends this FileGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the File class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $RecordingId the value for strRecordingId (Not Null)
	 * @property string $FileTypeId the value for strFileTypeId (Not Null)
	 * @property string $CodecIdVideo the value for strCodecIdVideo 
	 * @property string $CodecIdAudio the value for strCodecIdAudio 
	 * @property string $Name the value for strName (Not Null)
	 * @property string $CustomExtension the value for strCustomExtension 
	 * @property string $Comment the value for strComment 
	 * @property QDateTime $Added the value for dttAdded (Not Null)
	 * @property QDateTime $Changed the value for dttChanged (Not Null)
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Recording $Recording the value for the Recording object referenced by strRecordingId (Not Null)
	 * @property FileType $FileType the value for the FileType object referenced by strFileTypeId (Not Null)
	 * @property Codec $CodecIdVideoObject the value for the Codec object referenced by strCodecIdVideo 
	 * @property Codec $CodecIdAudioObject the value for the Codec object referenced by strCodecIdAudio 
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class FileGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_file.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_file.recording_id
		 * @var string strRecordingId
		 */
		protected $strRecordingId;
		const RecordingIdMaxLength = 36;
		const RecordingIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.file_type_id
		 * @var string strFileTypeId
		 */
		protected $strFileTypeId;
		const FileTypeIdMaxLength = 36;
		const FileTypeIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.codec_id_video
		 * @var string strCodecIdVideo
		 */
		protected $strCodecIdVideo;
		const CodecIdVideoMaxLength = 36;
		const CodecIdVideoDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.codec_id_audio
		 * @var string strCodecIdAudio
		 */
		protected $strCodecIdAudio;
		const CodecIdAudioMaxLength = 36;
		const CodecIdAudioDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.custom_extension
		 * @var string strCustomExtension
		 */
		protected $strCustomExtension;
		const CustomExtensionMaxLength = 25;
		const CustomExtensionDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.added
		 * @var QDateTime dttAdded
		 */
		protected $dttAdded;
		const AddedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.changed
		 * @var QDateTime dttChanged
		 */
		protected $dttChanged;
		const ChangedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_file.recording_id.
		 *
		 * NOTE: Always use the Recording property getter to correctly retrieve this Recording object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Recording objRecording
		 */
		protected $objRecording;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_file.file_type_id.
		 *
		 * NOTE: Always use the FileType property getter to correctly retrieve this FileType object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var FileType objFileType
		 */
		protected $objFileType;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_file.codec_id_video.
		 *
		 * NOTE: Always use the CodecIdVideoObject property getter to correctly retrieve this Codec object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Codec objCodecIdVideoObject
		 */
		protected $objCodecIdVideoObject;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_file.codec_id_audio.
		 *
		 * NOTE: Always use the CodecIdAudioObject property getter to correctly retrieve this Codec object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Codec objCodecIdAudioObject
		 */
		protected $objCodecIdAudioObject;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a File from PK Info
		 * @param string $strId
		 * @return File
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return File::QuerySingle(
				QQ::Equal(QQN::File()->Id, $strId)
			);
		}

		/**
		 * Load all Files
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadAll query
			try {
				return File::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Files
		 * @return int
		 */
		public static function CountAll() {
			// Call File::QueryCount to perform the CountAll query
			return File::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = File::GetDatabase();

			// Create/Build out the QueryBuilder object with File-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'file');
			File::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'file');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single File object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return File the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = File::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new File object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return File::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of File objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return File[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = File::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return File::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of File objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = File::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = File::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'file_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with File-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				File::GetSelectFields($objQueryBuilder);
				File::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return File::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this File
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'file';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'recording_id', $strAliasPrefix . 'recording_id');
			$objBuilder->AddSelectItem($strTableName, 'file_type_id', $strAliasPrefix . 'file_type_id');
			$objBuilder->AddSelectItem($strTableName, 'codec_id_video', $strAliasPrefix . 'codec_id_video');
			$objBuilder->AddSelectItem($strTableName, 'codec_id_audio', $strAliasPrefix . 'codec_id_audio');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'custom_extension', $strAliasPrefix . 'custom_extension');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'added', $strAliasPrefix . 'added');
			$objBuilder->AddSelectItem($strTableName, 'changed', $strAliasPrefix . 'changed');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a File from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this File::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return File
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;


			// Create a new instance of the File object
			$objToReturn = new File();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'recording_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'recording_id'] : $strAliasPrefix . 'recording_id';
			$objToReturn->strRecordingId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'file_type_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'file_type_id'] : $strAliasPrefix . 'file_type_id';
			$objToReturn->strFileTypeId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'codec_id_video', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'codec_id_video'] : $strAliasPrefix . 'codec_id_video';
			$objToReturn->strCodecIdVideo = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'codec_id_audio', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'codec_id_audio'] : $strAliasPrefix . 'codec_id_audio';
			$objToReturn->strCodecIdAudio = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'custom_extension', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'custom_extension'] : $strAliasPrefix . 'custom_extension';
			$objToReturn->strCustomExtension = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'added', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'added'] : $strAliasPrefix . 'added';
			$objToReturn->dttAdded = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'changed', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'changed'] : $strAliasPrefix . 'changed';
			$objToReturn->dttChanged = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'file__';

			// Check for Recording Early Binding
			$strAlias = $strAliasPrefix . 'recording_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objRecording = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recording_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for FileType Early Binding
			$strAlias = $strAliasPrefix . 'file_type_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objFileType = FileType::InstantiateDbRow($objDbRow, $strAliasPrefix . 'file_type_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for CodecIdVideoObject Early Binding
			$strAlias = $strAliasPrefix . 'codec_id_video__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objCodecIdVideoObject = Codec::InstantiateDbRow($objDbRow, $strAliasPrefix . 'codec_id_video__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for CodecIdAudioObject Early Binding
			$strAlias = $strAliasPrefix . 'codec_id_audio__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objCodecIdAudioObject = Codec::InstantiateDbRow($objDbRow, $strAliasPrefix . 'codec_id_audio__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			return $objToReturn;
		}

		/**
		 * Instantiate an array of Files from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return File[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = File::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = File::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single File object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return File
		*/
		public static function LoadById($strId) {
			return File::QuerySingle(
				QQ::Equal(QQN::File()->Id, $strId)
			);
		}
			
		/**
		 * Load an array of File objects,
		 * by RecordingId Index(es)
		 * @param string $strRecordingId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/
		public static function LoadArrayByRecordingId($strRecordingId, $objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadArrayByRecordingId query
			try {
				return File::QueryArray(
					QQ::Equal(QQN::File()->RecordingId, $strRecordingId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Files
		 * by RecordingId Index(es)
		 * @param string $strRecordingId
		 * @return int
		*/
		public static function CountByRecordingId($strRecordingId) {
			// Call File::QueryCount to perform the CountByRecordingId query
			return File::QueryCount(
				QQ::Equal(QQN::File()->RecordingId, $strRecordingId)
			);
		}
			
		/**
		 * Load an array of File objects,
		 * by CodecIdVideo Index(es)
		 * @param string $strCodecIdVideo
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/
		public static function LoadArrayByCodecIdVideo($strCodecIdVideo, $objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadArrayByCodecIdVideo query
			try {
				return File::QueryArray(
					QQ::Equal(QQN::File()->CodecIdVideo, $strCodecIdVideo),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Files
		 * by CodecIdVideo Index(es)
		 * @param string $strCodecIdVideo
		 * @return int
		*/
		public static function CountByCodecIdVideo($strCodecIdVideo) {
			// Call File::QueryCount to perform the CountByCodecIdVideo query
			return File::QueryCount(
				QQ::Equal(QQN::File()->CodecIdVideo, $strCodecIdVideo)
			);
		}
			
		/**
		 * Load an array of File objects,
		 * by CodecIdAudio Index(es)
		 * @param string $strCodecIdAudio
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/
		public static function LoadArrayByCodecIdAudio($strCodecIdAudio, $objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadArrayByCodecIdAudio query
			try {
				return File::QueryArray(
					QQ::Equal(QQN::File()->CodecIdAudio, $strCodecIdAudio),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Files
		 * by CodecIdAudio Index(es)
		 * @param string $strCodecIdAudio
		 * @return int
		*/
		public static function CountByCodecIdAudio($strCodecIdAudio) {
			// Call File::QueryCount to perform the CountByCodecIdAudio query
			return File::QueryCount(
				QQ::Equal(QQN::File()->CodecIdAudio, $strCodecIdAudio)
			);
		}
			
		/**
		 * Load an array of File objects,
		 * by FileTypeId Index(es)
		 * @param string $strFileTypeId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/
		public static function LoadArrayByFileTypeId($strFileTypeId, $objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadArrayByFileTypeId query
			try {
				return File::QueryArray(
					QQ::Equal(QQN::File()->FileTypeId, $strFileTypeId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Files
		 * by FileTypeId Index(es)
		 * @param string $strFileTypeId
		 * @return int
		*/
		public static function CountByFileTypeId($strFileTypeId) {
			// Call File::QueryCount to perform the CountByFileTypeId query
			return File::QueryCount(
				QQ::Equal(QQN::File()->FileTypeId, $strFileTypeId)
			);
		}
			
		/**
		 * Load an array of File objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadArrayByOwner query
			try {
				return File::QueryArray(
					QQ::Equal(QQN::File()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Files
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call File::QueryCount to perform the CountByOwner query
			return File::QueryCount(
				QQ::Equal(QQN::File()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of File objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call File::QueryArray to perform the LoadArrayByStatus query
			try {
				return File::QueryArray(
					QQ::Equal(QQN::File()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Files
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call File::QueryCount to perform the CountByStatus query
			return File::QueryCount(
				QQ::Equal(QQN::File()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this File
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = File::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'file` (
							`id`,
							`recording_id`,
							`file_type_id`,
							`codec_id_video`,
							`codec_id_audio`,
							`name`,
							`custom_extension`,
							`comment`,
							`added`,
							`changed`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strRecordingId) . ',
							' . $objDatabase->SqlVariable($this->strFileTypeId) . ',
							' . $objDatabase->SqlVariable($this->strCodecIdVideo) . ',
							' . $objDatabase->SqlVariable($this->strCodecIdAudio) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->strCustomExtension) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'file`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('File');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'file`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`recording_id` = ' . $objDatabase->SqlVariable($this->strRecordingId) . ',
							`file_type_id` = ' . $objDatabase->SqlVariable($this->strFileTypeId) . ',
							`codec_id_video` = ' . $objDatabase->SqlVariable($this->strCodecIdVideo) . ',
							`codec_id_audio` = ' . $objDatabase->SqlVariable($this->strCodecIdAudio) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`custom_extension` = ' . $objDatabase->SqlVariable($this->strCustomExtension) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`added` = ' . $objDatabase->SqlVariable($this->dttAdded) . ',
							`changed` = ' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'file`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this File
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this File with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = File::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'file`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Files
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = File::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'file`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'file table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = File::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'file`');
		}

		/**
		 * Reload this File from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved File object.');

			// Reload the Object
			$objReloaded = File::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->RecordingId = $objReloaded->RecordingId;
			$this->FileTypeId = $objReloaded->FileTypeId;
			$this->CodecIdVideo = $objReloaded->CodecIdVideo;
			$this->CodecIdAudio = $objReloaded->CodecIdAudio;
			$this->strName = $objReloaded->strName;
			$this->strCustomExtension = $objReloaded->strCustomExtension;
			$this->strComment = $objReloaded->strComment;
			$this->dttAdded = $objReloaded->dttAdded;
			$this->dttChanged = $objReloaded->dttChanged;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'RecordingId':
					/**
					 * Gets the value for strRecordingId (Not Null)
					 * @return string
					 */
					return $this->strRecordingId;

				case 'FileTypeId':
					/**
					 * Gets the value for strFileTypeId (Not Null)
					 * @return string
					 */
					return $this->strFileTypeId;

				case 'CodecIdVideo':
					/**
					 * Gets the value for strCodecIdVideo 
					 * @return string
					 */
					return $this->strCodecIdVideo;

				case 'CodecIdAudio':
					/**
					 * Gets the value for strCodecIdAudio 
					 * @return string
					 */
					return $this->strCodecIdAudio;

				case 'Name':
					/**
					 * Gets the value for strName (Not Null)
					 * @return string
					 */
					return $this->strName;

				case 'CustomExtension':
					/**
					 * Gets the value for strCustomExtension 
					 * @return string
					 */
					return $this->strCustomExtension;

				case 'Comment':
					/**
					 * Gets the value for strComment 
					 * @return string
					 */
					return $this->strComment;

				case 'Added':
					/**
					 * Gets the value for dttAdded (Not Null)
					 * @return QDateTime
					 */
					return $this->dttAdded;

				case 'Changed':
					/**
					 * Gets the value for dttChanged (Not Null)
					 * @return QDateTime
					 */
					return $this->dttChanged;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Recording':
					/**
					 * Gets the value for the Recording object referenced by strRecordingId (Not Null)
					 * @return Recording
					 */
					try {
						if ((!$this->objRecording) && (!is_null($this->strRecordingId)))
							$this->objRecording = Recording::Load($this->strRecordingId);
						return $this->objRecording;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'FileType':
					/**
					 * Gets the value for the FileType object referenced by strFileTypeId (Not Null)
					 * @return FileType
					 */
					try {
						if ((!$this->objFileType) && (!is_null($this->strFileTypeId)))
							$this->objFileType = FileType::Load($this->strFileTypeId);
						return $this->objFileType;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CodecIdVideoObject':
					/**
					 * Gets the value for the Codec object referenced by strCodecIdVideo 
					 * @return Codec
					 */
					try {
						if ((!$this->objCodecIdVideoObject) && (!is_null($this->strCodecIdVideo)))
							$this->objCodecIdVideoObject = Codec::Load($this->strCodecIdVideo);
						return $this->objCodecIdVideoObject;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CodecIdAudioObject':
					/**
					 * Gets the value for the Codec object referenced by strCodecIdAudio 
					 * @return Codec
					 */
					try {
						if ((!$this->objCodecIdAudioObject) && (!is_null($this->strCodecIdAudio)))
							$this->objCodecIdAudioObject = Codec::Load($this->strCodecIdAudio);
						return $this->objCodecIdAudioObject;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'RecordingId':
					/**
					 * Sets the value for strRecordingId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objRecording = null;
						return ($this->strRecordingId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'FileTypeId':
					/**
					 * Sets the value for strFileTypeId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objFileType = null;
						return ($this->strFileTypeId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CodecIdVideo':
					/**
					 * Sets the value for strCodecIdVideo 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objCodecIdVideoObject = null;
						return ($this->strCodecIdVideo = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CodecIdAudio':
					/**
					 * Sets the value for strCodecIdAudio 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objCodecIdAudioObject = null;
						return ($this->strCodecIdAudio = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CustomExtension':
					/**
					 * Sets the value for strCustomExtension 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strCustomExtension = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Added':
					/**
					 * Sets the value for dttAdded (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttAdded = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Changed':
					/**
					 * Sets the value for dttChanged (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttChanged = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Recording':
					/**
					 * Sets the value for the Recording object referenced by strRecordingId (Not Null)
					 * @param Recording $mixValue
					 * @return Recording
					 */
					if (is_null($mixValue)) {
						$this->strRecordingId = null;
						$this->objRecording = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Recording object
						try {
							$mixValue = QType::Cast($mixValue, 'Recording');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Recording object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Recording for this File');

						// Update Local Member Variables
						$this->objRecording = $mixValue;
						$this->strRecordingId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'FileType':
					/**
					 * Sets the value for the FileType object referenced by strFileTypeId (Not Null)
					 * @param FileType $mixValue
					 * @return FileType
					 */
					if (is_null($mixValue)) {
						$this->strFileTypeId = null;
						$this->objFileType = null;
						return null;
					} else {
						// Make sure $mixValue actually is a FileType object
						try {
							$mixValue = QType::Cast($mixValue, 'FileType');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED FileType object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved FileType for this File');

						// Update Local Member Variables
						$this->objFileType = $mixValue;
						$this->strFileTypeId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'CodecIdVideoObject':
					/**
					 * Sets the value for the Codec object referenced by strCodecIdVideo 
					 * @param Codec $mixValue
					 * @return Codec
					 */
					if (is_null($mixValue)) {
						$this->strCodecIdVideo = null;
						$this->objCodecIdVideoObject = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Codec object
						try {
							$mixValue = QType::Cast($mixValue, 'Codec');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Codec object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved CodecIdVideoObject for this File');

						// Update Local Member Variables
						$this->objCodecIdVideoObject = $mixValue;
						$this->strCodecIdVideo = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'CodecIdAudioObject':
					/**
					 * Sets the value for the Codec object referenced by strCodecIdAudio 
					 * @param Codec $mixValue
					 * @return Codec
					 */
					if (is_null($mixValue)) {
						$this->strCodecIdAudio = null;
						$this->objCodecIdAudioObject = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Codec object
						try {
							$mixValue = QType::Cast($mixValue, 'Codec');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Codec object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved CodecIdAudioObject for this File');

						// Update Local Member Variables
						$this->objCodecIdAudioObject = $mixValue;
						$this->strCodecIdAudio = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="File"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Recording" type="xsd1:Recording"/>';
			$strToReturn .= '<element name="FileType" type="xsd1:FileType"/>';
			$strToReturn .= '<element name="CodecIdVideoObject" type="xsd1:Codec"/>';
			$strToReturn .= '<element name="CodecIdAudioObject" type="xsd1:Codec"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="CustomExtension" type="xsd:string"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Added" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Changed" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('File', $strComplexTypeArray)) {
				$strComplexTypeArray['File'] = File::GetSoapComplexTypeXml();
				Recording::AlterSoapComplexTypeArray($strComplexTypeArray);
				FileType::AlterSoapComplexTypeArray($strComplexTypeArray);
				Codec::AlterSoapComplexTypeArray($strComplexTypeArray);
				Codec::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, File::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new File();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'Recording')) &&
				($objSoapObject->Recording))
				$objToReturn->Recording = Recording::GetObjectFromSoapObject($objSoapObject->Recording);
			if ((property_exists($objSoapObject, 'FileType')) &&
				($objSoapObject->FileType))
				$objToReturn->FileType = FileType::GetObjectFromSoapObject($objSoapObject->FileType);
			if ((property_exists($objSoapObject, 'CodecIdVideoObject')) &&
				($objSoapObject->CodecIdVideoObject))
				$objToReturn->CodecIdVideoObject = Codec::GetObjectFromSoapObject($objSoapObject->CodecIdVideoObject);
			if ((property_exists($objSoapObject, 'CodecIdAudioObject')) &&
				($objSoapObject->CodecIdAudioObject))
				$objToReturn->CodecIdAudioObject = Codec::GetObjectFromSoapObject($objSoapObject->CodecIdAudioObject);
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'CustomExtension'))
				$objToReturn->strCustomExtension = $objSoapObject->CustomExtension;
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Added'))
				$objToReturn->dttAdded = new QDateTime($objSoapObject->Added);
			if (property_exists($objSoapObject, 'Changed'))
				$objToReturn->dttChanged = new QDateTime($objSoapObject->Changed);
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, File::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objRecording)
				$objObject->objRecording = Recording::GetSoapObjectFromObject($objObject->objRecording, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strRecordingId = null;
			if ($objObject->objFileType)
				$objObject->objFileType = FileType::GetSoapObjectFromObject($objObject->objFileType, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strFileTypeId = null;
			if ($objObject->objCodecIdVideoObject)
				$objObject->objCodecIdVideoObject = Codec::GetSoapObjectFromObject($objObject->objCodecIdVideoObject, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strCodecIdVideo = null;
			if ($objObject->objCodecIdAudioObject)
				$objObject->objCodecIdAudioObject = Codec::GetSoapObjectFromObject($objObject->objCodecIdAudioObject, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strCodecIdAudio = null;
			if ($objObject->dttAdded)
				$objObject->dttAdded = $objObject->dttAdded->__toString(QDateTime::FormatSoap);
			if ($objObject->dttChanged)
				$objObject->dttChanged = $objObject->dttChanged->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeFile extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'File';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'file';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'RecordingId':
					return new QQNode('recording_id', 'RecordingId', 'string', $this);
				case 'Recording':
					return new QQNodeRecording('recording_id', 'Recording', 'string', $this);
				case 'FileTypeId':
					return new QQNode('file_type_id', 'FileTypeId', 'string', $this);
				case 'FileType':
					return new QQNodeFileType('file_type_id', 'FileType', 'string', $this);
				case 'CodecIdVideo':
					return new QQNode('codec_id_video', 'CodecIdVideo', 'string', $this);
				case 'CodecIdVideoObject':
					return new QQNodeCodec('codec_id_video', 'CodecIdVideoObject', 'string', $this);
				case 'CodecIdAudio':
					return new QQNode('codec_id_audio', 'CodecIdAudio', 'string', $this);
				case 'CodecIdAudioObject':
					return new QQNodeCodec('codec_id_audio', 'CodecIdAudioObject', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'CustomExtension':
					return new QQNode('custom_extension', 'CustomExtension', 'string', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeFile extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'File';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'file';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'RecordingId':
					return new QQNode('recording_id', 'RecordingId', 'string', $this);
				case 'Recording':
					return new QQNodeRecording('recording_id', 'Recording', 'string', $this);
				case 'FileTypeId':
					return new QQNode('file_type_id', 'FileTypeId', 'string', $this);
				case 'FileType':
					return new QQNodeFileType('file_type_id', 'FileType', 'string', $this);
				case 'CodecIdVideo':
					return new QQNode('codec_id_video', 'CodecIdVideo', 'string', $this);
				case 'CodecIdVideoObject':
					return new QQNodeCodec('codec_id_video', 'CodecIdVideoObject', 'string', $this);
				case 'CodecIdAudio':
					return new QQNode('codec_id_audio', 'CodecIdAudio', 'string', $this);
				case 'CodecIdAudioObject':
					return new QQNodeCodec('codec_id_audio', 'CodecIdAudioObject', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'CustomExtension':
					return new QQNode('custom_extension', 'CustomExtension', 'string', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
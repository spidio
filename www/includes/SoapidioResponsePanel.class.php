<?php

class SoapidioResponsePanel extends QPanel {
	protected $strServiceName;
	
	protected $strFunctionName;
	protected $strFunctionDeclaration;
	protected $strFunctionArgumentsArray;
	protected $strFunctionReturnType;
	protected $mixArgumentArray;
	protected $mixResult;
	
	protected $objControlsArray;
	
	/**
	 * Constructor for this control
	 * @param mixed $objParentObject Parent QForm or QControl that is responsible for rendering this control
	 * @param string $strControlId optional control ID
	 */
	public function __construct($objParentObject, $strControlId = null) {
		try {
			parent::__construct($objParentObject, $strControlId);
		} catch (QCallerException $objExc) { $objExc->IncrementOffset(); throw $objExc; }
		
		$this->strCssClass = 'ResponsePanel';
	}
	
	/**
	 * Set Function data got from PHP Soap client.
	 *
	 * @param string $strServiceName
	 * @param string $strFunctionName
	 * @param string $strFunctionDeclaration
	 * @param mixed[] $mixArgumentArray
	 * @param mixed $mixResult
	 */
	public function SetFunctionResult($strServiceName, $strFunctionName, $strFunctionDeclaration, $mixArgumentArray, $mixResult) {
		$this->strServiceName = $strServiceName;
		$this->strFunctionName = $strFunctionName;
		$this->strFunctionDeclaration = $strFunctionDeclaration;
		$this->mixArgumentArray = $mixArgumentArray;
		$this->mixResult = $mixResult;
		
		$this->ResetControls();
		
		if (!is_null($strFunctionDeclaration)) {
			$this->GenerateControlData();
		}
	}
	
	/**
	 * Delete all child controls and refresh control.
	 *
	 */
	public function ResetControls() {
		$this->RemoveChildControls(true);
		$this->objControlsArray = array();
		$this->Refresh();
	}
	
	/**
	 * Generate the required controls for this FunctionResult.
	 *
	 */
	protected function GenerateControlData() {
		$intLength = strpos($this->strFunctionDeclaration, ' ');
		$this->strFunctionReturnType = substr($this->strFunctionDeclaration, 0, $intLength);
		
		$this->objControlsArray[] = SoapidioResponsePanelElement::TableBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
		$this->objControlsArray[] = QApplication::Translate('Serviee name:');
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnSpan2;
		$this->objControlsArray[] = $this->strServiceName;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
		$this->objControlsArray[] = QApplication::Translate('Function name:');
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnSpan2;
		$this->objControlsArray[] = $this->strFunctionName;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
		$this->objControlsArray[] = QApplication::Translate('Function declaration:');
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnSpan2;
		$this->objControlsArray[] = $this->strFunctionDeclaration;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
		$this->objControlsArray[] = QApplication::Translate('Function arguments:');
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		
		$intArgumentsBegin = strpos($this->strFunctionDeclaration, '(') + 1;
		$intArgumentsLength = strpos($this->strFunctionDeclaration, ')') - $intArgumentsBegin;
		$strArguments = substr($this->strFunctionDeclaration, $intArgumentsBegin, $intArgumentsLength);
		$this->strFunctionArgumentsArray = explode(', ', $strArguments);
		
		for ($i = 0; $i < count($this->strFunctionArgumentsArray); $i++) {
			if ($this->strFunctionArgumentsArray[$i]) {
				if (!array_key_exists($i, $this->mixArgumentArray)) {
					throw new QIndexOutOfRangeException($i, 'GenerateControlData()');
				}
				
				if ($i != 0) {
					$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
					$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
					$this->objControlsArray[] = SoapidioResponsePanelElement::NoBreakSpace;
					$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
				}
				
				$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
				$this->objControlsArray[] = $this->strFunctionArgumentsArray[$i];
				$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
				
				$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
				switch (gettype($this->mixArgumentArray[$i])) {
					case QType::ArrayType:
					case QType::Object:
						$this->objControlsArray[] = sprintf('<pre style="text-align: left">%s</pre>', print_r($this->mixArgumentArray[$i], true));
						break;
					case QType::Boolean:
						$this->objControlsArray[] = ($this->mixArgumentArray[$i]) ? 'true' : 'false';
						break;
					case QType::Integer:
					case QType::Float:
					case QType::String:
						if (empty($this->mixArgumentArray[$i])) {
							$this->objControlsArray[] = QApplication::Translate('<i>(empty)</i>');
						} else if (trim($this->mixArgumentArray[$i]) == '' && strlen($this->mixArgumentArray[$i]) > 1) {
							$this->objControlsArray[] = QApplication::Translate('<i>(spaces)</i>');
						} else if (trim($this->mixArgumentArray[$i]) == '' && strlen($this->mixArgumentArray[$i]) == 1) {
							$this->objControlsArray[] = QApplication::Translate('<i>(space)</i>');
						} else {
							$this->objControlsArray[] = $this->mixArgumentArray[$i];
						}
						break;
				}
				$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
			} else {
				$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnSpan2;
				$this->objControlsArray[] = QApplication::Translate('<i>(none)</i>');
				$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
			}
			
			$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		}
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::NoBreakSpace;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowBegin;
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnSpan3;
		
		
		if (strpos($this->strFunctionReturnType, 'ArrayOf') !== false) {
			$strType = str_replace('ArrayOf', '', $this->strFunctionReturnType);
			switch ($strType) {
				case 'String':
					$this->objControlsArray[] = sprintf('<pre style="text-align: left;">%s</pre>', print_r($this->mixResult, true));
					break;
				default:
					for ($i = 0; $i < count($this->mixResult); $i++) {
						$this->mixResult[$i] = call_user_func(array($strType, 'GetObjectFromSoapObject'), $this->mixResult[$i]);
					}
					
					$this->objControlsArray[] = sprintf('<pre style="text-align: left;">%s</pre>', print_r($this->mixResult, true));
					break;
			}
		} else {
			switch (QType::TypeFromDoc($this->strFunctionReturnType)) {
				case QType::ArrayType:
				case QType::Object:
					$this->objControlsArray[] = sprintf('<pre style="text-align: left;">%s</pre>', print_r($this->mixResult, true));
					break;
				case QType::Boolean:
					$this->objControlsArray[] = sprintf(QApplication::Translate('The function returned:<br/><i>%s</i>'), ($this->mixResult) ? 'true' : 'false');
					break;
				case QType::Integer:
				case QType::Float:
				case QType::String:
					$this->objControlsArray[] = sprintf(QApplication::Translate('The function returned:<br/><i>%s</i>'), $this->mixResult);
					break;
				default:
					$this->objControlsArray[] = sprintf(QApplication::Translate('The function returned:<br/><i>%s</i>'), call_user_func(array($this->strFunctionReturnType, 'GetObjectFromSoapObject'), $this->mixResult), true); 
					break;
			}
		}
		$this->objControlsArray[] = SoapidioResponsePanelElement::ColumnEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::RowEnd;
		$this->objControlsArray[] = SoapidioResponsePanelElement::TableEnd;
	}
	
	/**
	 * Return HTML for this control.
	 *
	 * @return string
	 */
	protected function GetControlHtml() {
		$strReturn = '';
		
		for ($i = 0; $i < count($this->objControlsArray); $i++) {
			if ($this->objControlsArray[$i] instanceof QControl) {
				$strReturn .= $this->objControlsArray[$i]->GetControlHtml();
			} else if (is_int($this->objControlsArray[$i])) {
				switch ($this->objControlsArray[$i]) {
					case SoapidioResponsePanelElement::TableBegin:
						$strReturn .= sprintf('<table class="%s">', $this->strCssClass);
						break;
					case SoapidioResponsePanelElement::TableEnd:
						$strReturn .= '</table>';
						break;
					
					case SoapidioResponsePanelElement::RowBegin:
						$strReturn .= '<tr>';
						break;
					case SoapidioResponsePanelElement::RowEnd:
						$strReturn .= '</tr>';
						break;
						
					case SoapidioResponsePanelElement::ColumnBegin:
						$strReturn .= '<td>';
						break;
					case SoapidioResponsePanelElement::ColumnEnd:
						$strReturn .= '</td>';
						break;
					case SoapidioResponsePanelElement::ColumnSpan2:
						$strReturn .= '<td colspan="2">';
						break;
					case SoapidioResponsePanelElement::ColumnSpan3:
						$strReturn .= '<td colspan="3">';
						break;
					
					case SoapidioResponsePanelElement::NoBreakSpace:
						$strReturn .= '&nbsp;';
						break;
				}
			} else if (is_string($this->objControlsArray[$i])) {
				$strReturn .= $this->objControlsArray[$i];
			} else {
				throw new QCallerException('Non valid item in array');
			}
		}
		
		return $strReturn;
	}
	
	/**
	 * Parse $_POST data for all child controls.
	 *
	 */
	public function ParsePostData() {
		foreach ($this->objChildControlArray as $objControl) {
			$objControl->ParsePostData();
		}
	}
}

abstract class SoapidioResponsePanelElement {
	const TableBegin = 1;
	const TableEnd = 2;
	
	const RowBegin = 4;
	const RowEnd = 8;
	
	const ColumnSpan2 = 16;
	const ColumnSpan3 = 32;
	const ColumnBegin = 64;
	const ColumnEnd = 128;
	
	const NoBreakSpace = 256;
}

?>
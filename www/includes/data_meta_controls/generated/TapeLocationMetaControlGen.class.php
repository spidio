<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the TapeLocation class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single TapeLocation object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a TapeLocationMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read TapeLocation $TapeLocation the actual TapeLocation data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $TapeIdControl
	 * property-read QLabel $TapeIdLabel
	 * property QListBox $LocationIdControl
	 * property-read QLabel $LocationIdLabel
	 * property QListBox $PersonIdControl
	 * property-read QLabel $PersonIdLabel
	 * property QDateTimePicker $DateControl
	 * property-read QLabel $DateLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class TapeLocationMetaControlGen extends QBaseClass {
		// General Variables
		protected $objTapeLocation;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of TapeLocation's individual data fields
		protected $txtId;
		protected $lstTape;
		protected $lstLocation;
		protected $lstPerson;
		protected $calDate;
		protected $txtComment;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of TapeLocation's individual data fields
		protected $lblId;
		protected $lblTapeId;
		protected $lblLocationId;
		protected $lblPersonId;
		protected $lblDate;
		protected $lblComment;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * TapeLocationMetaControl to edit a single TapeLocation object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single TapeLocation object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeLocationMetaControl
		 * @param TapeLocation $objTapeLocation new or existing TapeLocation object
		 */
		 public function __construct($objParentObject, TapeLocation $objTapeLocation) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this TapeLocationMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked TapeLocation object
			$this->objTapeLocation = $objTapeLocation;

			// Figure out if we're Editing or Creating New
			if ($this->objTapeLocation->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeLocationMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing TapeLocation object creation - defaults to CreateOrEdit
 		 * @return TapeLocationMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objTapeLocation = TapeLocation::Load($strId);

				// TapeLocation was found -- return it!
				if ($objTapeLocation)
					return new TapeLocationMetaControl($objParentObject, $objTapeLocation);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a TapeLocation object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new TapeLocationMetaControl($objParentObject, new TapeLocation());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeLocationMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing TapeLocation object creation - defaults to CreateOrEdit
		 * @return TapeLocationMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return TapeLocationMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeLocationMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing TapeLocation object creation - defaults to CreateOrEdit
		 * @return TapeLocationMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return TapeLocationMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objTapeLocation->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = TapeLocation::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objTapeLocation->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstTape
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstTape_Create($strControlId = null) {
			$this->lstTape = new QListBox($this->objParentObject, $strControlId);
			$this->lstTape->Name = QApplication::Translate('Tape');
			$this->lstTape->Required = true;
			if (!$this->blnEditMode)
				$this->lstTape->AddItem(QApplication::Translate('- Select One -'), null);
			$objTapeArray = Tape::LoadAll();
			if ($objTapeArray) foreach ($objTapeArray as $objTape) {
				$objListItem = new QListItem($objTape->__toString(), $objTape->Id);
				if (($this->objTapeLocation->Tape) && ($this->objTapeLocation->Tape->Id == $objTape->Id))
					$objListItem->Selected = true;
				$this->lstTape->AddItem($objListItem);
			}
			return $this->lstTape;
		}

		/**
		 * Create and setup QLabel lblTapeId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblTapeId_Create($strControlId = null) {
			$this->lblTapeId = new QLabel($this->objParentObject, $strControlId);
			$this->lblTapeId->Name = QApplication::Translate('Tape');
			$this->lblTapeId->Text = ($this->objTapeLocation->Tape) ? $this->objTapeLocation->Tape->__toString() : null;
			$this->lblTapeId->Required = true;
			return $this->lblTapeId;
		}

		/**
		 * Create and setup QListBox lstLocation
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstLocation_Create($strControlId = null) {
			$this->lstLocation = new QListBox($this->objParentObject, $strControlId);
			$this->lstLocation->Name = QApplication::Translate('Location');
			$this->lstLocation->Required = true;
			if (!$this->blnEditMode)
				$this->lstLocation->AddItem(QApplication::Translate('- Select One -'), null);
			$objLocationArray = Location::LoadAll();
			if ($objLocationArray) foreach ($objLocationArray as $objLocation) {
				$objListItem = new QListItem($objLocation->__toString(), $objLocation->Id);
				if (($this->objTapeLocation->Location) && ($this->objTapeLocation->Location->Id == $objLocation->Id))
					$objListItem->Selected = true;
				$this->lstLocation->AddItem($objListItem);
			}
			return $this->lstLocation;
		}

		/**
		 * Create and setup QLabel lblLocationId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblLocationId_Create($strControlId = null) {
			$this->lblLocationId = new QLabel($this->objParentObject, $strControlId);
			$this->lblLocationId->Name = QApplication::Translate('Location');
			$this->lblLocationId->Text = ($this->objTapeLocation->Location) ? $this->objTapeLocation->Location->__toString() : null;
			$this->lblLocationId->Required = true;
			return $this->lblLocationId;
		}

		/**
		 * Create and setup QListBox lstPerson
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstPerson_Create($strControlId = null) {
			$this->lstPerson = new QListBox($this->objParentObject, $strControlId);
			$this->lstPerson->Name = QApplication::Translate('Person');
			$this->lstPerson->Required = true;
			if (!$this->blnEditMode)
				$this->lstPerson->AddItem(QApplication::Translate('- Select One -'), null);
			$objPersonArray = Person::LoadAll();
			if ($objPersonArray) foreach ($objPersonArray as $objPerson) {
				$objListItem = new QListItem($objPerson->__toString(), $objPerson->Id);
				if (($this->objTapeLocation->Person) && ($this->objTapeLocation->Person->Id == $objPerson->Id))
					$objListItem->Selected = true;
				$this->lstPerson->AddItem($objListItem);
			}
			return $this->lstPerson;
		}

		/**
		 * Create and setup QLabel lblPersonId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblPersonId_Create($strControlId = null) {
			$this->lblPersonId = new QLabel($this->objParentObject, $strControlId);
			$this->lblPersonId->Name = QApplication::Translate('Person');
			$this->lblPersonId->Text = ($this->objTapeLocation->Person) ? $this->objTapeLocation->Person->__toString() : null;
			$this->lblPersonId->Required = true;
			return $this->lblPersonId;
		}

		/**
		 * Create and setup QDateTimePicker calDate
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calDate_Create($strControlId = null) {
			$this->calDate = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calDate->Name = QApplication::Translate('Date');
			$this->calDate->DateTime = $this->objTapeLocation->Date;
			$this->calDate->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calDate->Required = true;
			return $this->calDate;
		}

		/**
		 * Create and setup QLabel lblDate
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblDate_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblDate = new QLabel($this->objParentObject, $strControlId);
			$this->lblDate->Name = QApplication::Translate('Date');
			$this->strDateDateTimeFormat = $strDateTimeFormat;
			$this->lblDate->Text = sprintf($this->objTapeLocation->Date) ? $this->objTapeLocation->__toString($this->strDateDateTimeFormat) : null;
			$this->lblDate->Required = true;
			return $this->lblDate;
		}

		protected $strDateDateTimeFormat;

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objTapeLocation->Comment;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objTapeLocation->Comment;
			return $this->lblComment;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objTapeLocation->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objTapeLocation->Owner;
			$this->txtOwner->MaxLength = TapeLocation::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objTapeLocation->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objTapeLocation->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objTapeLocation->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objTapeLocation->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objTapeLocation->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objTapeLocation->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objTapeLocation->Status) ? AuthStatusEnum::$NameArray[$this->objTapeLocation->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local TapeLocation object.
		 * @param boolean $blnReload reload TapeLocation from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objTapeLocation->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objTapeLocation->Id;
			if ($this->lblId) $this->lblId->Text = $this->objTapeLocation->Id;

			if ($this->lstTape) {
					$this->lstTape->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstTape->AddItem(QApplication::Translate('- Select One -'), null);
				$objTapeArray = Tape::LoadAll();
				if ($objTapeArray) foreach ($objTapeArray as $objTape) {
					$objListItem = new QListItem($objTape->__toString(), $objTape->Id);
					if (($this->objTapeLocation->Tape) && ($this->objTapeLocation->Tape->Id == $objTape->Id))
						$objListItem->Selected = true;
					$this->lstTape->AddItem($objListItem);
				}
			}
			if ($this->lblTapeId) $this->lblTapeId->Text = ($this->objTapeLocation->Tape) ? $this->objTapeLocation->Tape->__toString() : null;

			if ($this->lstLocation) {
					$this->lstLocation->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstLocation->AddItem(QApplication::Translate('- Select One -'), null);
				$objLocationArray = Location::LoadAll();
				if ($objLocationArray) foreach ($objLocationArray as $objLocation) {
					$objListItem = new QListItem($objLocation->__toString(), $objLocation->Id);
					if (($this->objTapeLocation->Location) && ($this->objTapeLocation->Location->Id == $objLocation->Id))
						$objListItem->Selected = true;
					$this->lstLocation->AddItem($objListItem);
				}
			}
			if ($this->lblLocationId) $this->lblLocationId->Text = ($this->objTapeLocation->Location) ? $this->objTapeLocation->Location->__toString() : null;

			if ($this->lstPerson) {
					$this->lstPerson->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstPerson->AddItem(QApplication::Translate('- Select One -'), null);
				$objPersonArray = Person::LoadAll();
				if ($objPersonArray) foreach ($objPersonArray as $objPerson) {
					$objListItem = new QListItem($objPerson->__toString(), $objPerson->Id);
					if (($this->objTapeLocation->Person) && ($this->objTapeLocation->Person->Id == $objPerson->Id))
						$objListItem->Selected = true;
					$this->lstPerson->AddItem($objListItem);
				}
			}
			if ($this->lblPersonId) $this->lblPersonId->Text = ($this->objTapeLocation->Person) ? $this->objTapeLocation->Person->__toString() : null;

			if ($this->calDate) $this->calDate->DateTime = $this->objTapeLocation->Date;
			if ($this->lblDate) $this->lblDate->Text = sprintf($this->objTapeLocation->Date) ? $this->objTapeLocation->__toString($this->strDateDateTimeFormat) : null;

			if ($this->txtComment) $this->txtComment->Text = $this->objTapeLocation->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objTapeLocation->Comment;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objTapeLocation->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objTapeLocation->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objTapeLocation->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objTapeLocation->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objTapeLocation->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objTapeLocation->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objTapeLocation->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objTapeLocation->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objTapeLocation->Status) ? AuthStatusEnum::$NameArray[$this->objTapeLocation->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC TAPELOCATION OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's TapeLocation instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveTapeLocation() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objTapeLocation->Id = $this->txtId->Text;
				if ($this->lstTape) $this->objTapeLocation->TapeId = $this->lstTape->SelectedValue;
				if ($this->lstLocation) $this->objTapeLocation->LocationId = $this->lstLocation->SelectedValue;
				if ($this->lstPerson) $this->objTapeLocation->PersonId = $this->lstPerson->SelectedValue;
				if ($this->calDate) $this->objTapeLocation->Date = $this->calDate->DateTime;
				if ($this->txtComment) $this->objTapeLocation->Comment = $this->txtComment->Text;
				if ($this->txtOwner) $this->objTapeLocation->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objTapeLocation->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objTapeLocation->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objTapeLocation->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the TapeLocation object
				$this->objTapeLocation->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's TapeLocation instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteTapeLocation() {
			$this->objTapeLocation->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'TapeLocation': return $this->objTapeLocation;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to TapeLocation fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'TapeIdControl':
					if (!$this->lstTape) return $this->lstTape_Create();
					return $this->lstTape;
				case 'TapeIdLabel':
					if (!$this->lblTapeId) return $this->lblTapeId_Create();
					return $this->lblTapeId;
				case 'LocationIdControl':
					if (!$this->lstLocation) return $this->lstLocation_Create();
					return $this->lstLocation;
				case 'LocationIdLabel':
					if (!$this->lblLocationId) return $this->lblLocationId_Create();
					return $this->lblLocationId;
				case 'PersonIdControl':
					if (!$this->lstPerson) return $this->lstPerson_Create();
					return $this->lstPerson;
				case 'PersonIdLabel':
					if (!$this->lblPersonId) return $this->lblPersonId_Create();
					return $this->lblPersonId;
				case 'DateControl':
					if (!$this->calDate) return $this->calDate_Create();
					return $this->calDate;
				case 'DateLabel':
					if (!$this->lblDate) return $this->lblDate_Create();
					return $this->lblDate;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to TapeLocation fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'TapeIdControl':
						return ($this->lstTape = QType::Cast($mixValue, 'QControl'));
					case 'LocationIdControl':
						return ($this->lstLocation = QType::Cast($mixValue, 'QControl'));
					case 'PersonIdControl':
						return ($this->lstPerson = QType::Cast($mixValue, 'QControl'));
					case 'DateControl':
						return ($this->calDate = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
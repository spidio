<?php
	/**
	 * The abstract CameraPurposeGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the CameraPurpose subclass which
	 * extends this CameraPurposeGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the CameraPurpose class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $Name the value for strName (Unique)
	 * @property string $Comment the value for strComment (Not Null)
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property-read Recording $_RecordingAsCameraPurpose the value for the private _objRecordingAsCameraPurpose (Read-Only) if set due to an expansion on the spidio_recording.camera_purpose_id reverse relationship
	 * @property-read Recording[] $_RecordingAsCameraPurposeArray the value for the private _objRecordingAsCameraPurposeArray (Read-Only) if set due to an ExpandAsArray on the spidio_recording.camera_purpose_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class CameraPurposeGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_camera_purpose.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_camera_purpose.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single RecordingAsCameraPurpose object
		 * (of type Recording), if this CameraPurpose object was restored with
		 * an expansion on the spidio_recording association table.
		 * @var Recording _objRecordingAsCameraPurpose;
		 */
		private $_objRecordingAsCameraPurpose;

		/**
		 * Private member variable that stores a reference to an array of RecordingAsCameraPurpose objects
		 * (of type Recording[]), if this CameraPurpose object was restored with
		 * an ExpandAsArray on the spidio_recording association table.
		 * @var Recording[] _objRecordingAsCameraPurposeArray;
		 */
		private $_objRecordingAsCameraPurposeArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a CameraPurpose from PK Info
		 * @param string $strId
		 * @return CameraPurpose
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return CameraPurpose::QuerySingle(
				QQ::Equal(QQN::CameraPurpose()->Id, $strId)
			);
		}

		/**
		 * Load all CameraPurposes
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return CameraPurpose[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call CameraPurpose::QueryArray to perform the LoadAll query
			try {
				return CameraPurpose::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all CameraPurposes
		 * @return int
		 */
		public static function CountAll() {
			// Call CameraPurpose::QueryCount to perform the CountAll query
			return CameraPurpose::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Create/Build out the QueryBuilder object with CameraPurpose-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'camera_purpose');
			CameraPurpose::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'camera_purpose');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single CameraPurpose object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return CameraPurpose the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = CameraPurpose::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new CameraPurpose object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return CameraPurpose::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of CameraPurpose objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return CameraPurpose[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = CameraPurpose::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return CameraPurpose::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of CameraPurpose objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = CameraPurpose::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'camera_purpose_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with CameraPurpose-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				CameraPurpose::GetSelectFields($objQueryBuilder);
				CameraPurpose::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return CameraPurpose::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this CameraPurpose
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'camera_purpose';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a CameraPurpose from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this CameraPurpose::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return CameraPurpose
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'camera_purpose__';


				$strAlias = $strAliasPrefix . 'recordingascamerapurpose__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objRecordingAsCameraPurposeArray)) {
						$objPreviousChildItem = $objPreviousItem->_objRecordingAsCameraPurposeArray[$intPreviousChildItemCount - 1];
						$objChildItem = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingascamerapurpose__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objRecordingAsCameraPurposeArray[] = $objChildItem;
					} else
						$objPreviousItem->_objRecordingAsCameraPurposeArray[] = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingascamerapurpose__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'camera_purpose__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the CameraPurpose object
			$objToReturn = new CameraPurpose();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'camera_purpose__';




			// Check for RecordingAsCameraPurpose Virtual Binding
			$strAlias = $strAliasPrefix . 'recordingascamerapurpose__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objRecordingAsCameraPurposeArray[] = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingascamerapurpose__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objRecordingAsCameraPurpose = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingascamerapurpose__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of CameraPurposes from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return CameraPurpose[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = CameraPurpose::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = CameraPurpose::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single CameraPurpose object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return CameraPurpose
		*/
		public static function LoadById($strId) {
			return CameraPurpose::QuerySingle(
				QQ::Equal(QQN::CameraPurpose()->Id, $strId)
			);
		}
			
		/**
		 * Load a single CameraPurpose object,
		 * by Name Index(es)
		 * @param string $strName
		 * @return CameraPurpose
		*/
		public static function LoadByName($strName) {
			return CameraPurpose::QuerySingle(
				QQ::Equal(QQN::CameraPurpose()->Name, $strName)
			);
		}
			
		/**
		 * Load an array of CameraPurpose objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return CameraPurpose[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call CameraPurpose::QueryArray to perform the LoadArrayByOwner query
			try {
				return CameraPurpose::QueryArray(
					QQ::Equal(QQN::CameraPurpose()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count CameraPurposes
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call CameraPurpose::QueryCount to perform the CountByOwner query
			return CameraPurpose::QueryCount(
				QQ::Equal(QQN::CameraPurpose()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of CameraPurpose objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return CameraPurpose[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call CameraPurpose::QueryArray to perform the LoadArrayByStatus query
			try {
				return CameraPurpose::QueryArray(
					QQ::Equal(QQN::CameraPurpose()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count CameraPurposes
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call CameraPurpose::QueryCount to perform the CountByStatus query
			return CameraPurpose::QueryCount(
				QQ::Equal(QQN::CameraPurpose()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this CameraPurpose
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'camera_purpose` (
							`id`,
							`name`,
							`comment`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'camera_purpose`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('CameraPurpose');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'camera_purpose`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'camera_purpose`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this CameraPurpose
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this CameraPurpose with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'camera_purpose`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all CameraPurposes
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'camera_purpose`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'camera_purpose table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'camera_purpose`');
		}

		/**
		 * Reload this CameraPurpose from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved CameraPurpose object.');

			// Reload the Object
			$objReloaded = CameraPurpose::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->strName = $objReloaded->strName;
			$this->strComment = $objReloaded->strComment;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'Name':
					/**
					 * Gets the value for strName (Unique)
					 * @return string
					 */
					return $this->strName;

				case 'Comment':
					/**
					 * Gets the value for strComment (Not Null)
					 * @return string
					 */
					return $this->strComment;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////

				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_RecordingAsCameraPurpose':
					/**
					 * Gets the value for the private _objRecordingAsCameraPurpose (Read-Only)
					 * if set due to an expansion on the spidio_recording.camera_purpose_id reverse relationship
					 * @return Recording
					 */
					return $this->_objRecordingAsCameraPurpose;

				case '_RecordingAsCameraPurposeArray':
					/**
					 * Gets the value for the private _objRecordingAsCameraPurposeArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_recording.camera_purpose_id reverse relationship
					 * @return Recording[]
					 */
					return (array) $this->_objRecordingAsCameraPurposeArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Unique)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for RecordingAsCameraPurpose
		//-------------------------------------------------------------------

		/**
		 * Gets all associated RecordingsAsCameraPurpose as an array of Recording objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/ 
		public function GetRecordingAsCameraPurposeArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Recording::LoadArrayByCameraPurposeId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated RecordingsAsCameraPurpose
		 * @return int
		*/ 
		public function CountRecordingsAsCameraPurpose() {
			if ((is_null($this->strId)))
				return 0;

			return Recording::CountByCameraPurposeId($this->strId);
		}

		/**
		 * Associates a RecordingAsCameraPurpose
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function AssociateRecordingAsCameraPurpose(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateRecordingAsCameraPurpose on this unsaved CameraPurpose.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateRecordingAsCameraPurpose on this CameraPurpose with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`camera_purpose_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . '
			');
		}

		/**
		 * Unassociates a RecordingAsCameraPurpose
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function UnassociateRecordingAsCameraPurpose(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsCameraPurpose on this unsaved CameraPurpose.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsCameraPurpose on this CameraPurpose with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`camera_purpose_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . ' AND
					`camera_purpose_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all RecordingsAsCameraPurpose
		 * @return void
		*/ 
		public function UnassociateAllRecordingsAsCameraPurpose() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsCameraPurpose on this unsaved CameraPurpose.');

			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`camera_purpose_id` = null
				WHERE
					`camera_purpose_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated RecordingAsCameraPurpose
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function DeleteAssociatedRecordingAsCameraPurpose(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsCameraPurpose on this unsaved CameraPurpose.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsCameraPurpose on this CameraPurpose with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_recording`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . ' AND
					`camera_purpose_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated RecordingsAsCameraPurpose
		 * @return void
		*/ 
		public function DeleteAllRecordingsAsCameraPurpose() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsCameraPurpose on this unsaved CameraPurpose.');

			// Get the Database Object for this Class
			$objDatabase = CameraPurpose::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_recording`
				WHERE
					`camera_purpose_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="CameraPurpose"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('CameraPurpose', $strComplexTypeArray)) {
				$strComplexTypeArray['CameraPurpose'] = CameraPurpose::GetSoapComplexTypeXml();
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, CameraPurpose::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new CameraPurpose();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, CameraPurpose::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeCameraPurpose extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'CameraPurpose';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'camera_purpose';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'RecordingAsCameraPurpose':
					return new QQReverseReferenceNodeRecording($this, 'recordingascamerapurpose', 'reverse_reference', 'camera_purpose_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeCameraPurpose extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'CameraPurpose';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'camera_purpose';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'RecordingAsCameraPurpose':
					return new QQReverseReferenceNodeRecording($this, 'recordingascamerapurpose', 'reverse_reference', 'camera_purpose_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
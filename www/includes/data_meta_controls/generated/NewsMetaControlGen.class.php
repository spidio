<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the News class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single News object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a NewsMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read News $News the actual News data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $UserIdControl
	 * property-read QLabel $UserIdLabel
	 * property QTextBox $TitleControl
	 * property-read QLabel $TitleLabel
	 * property QTextBox $TextControl
	 * property-read QLabel $TextLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QDateTimePicker $ChangedControl
	 * property-read QLabel $ChangedLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class NewsMetaControlGen extends QBaseClass {
		// General Variables
		protected $objNews;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of News's individual data fields
		protected $txtId;
		protected $lstUser;
		protected $txtTitle;
		protected $txtText;
		protected $calAdded;
		protected $calChanged;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of News's individual data fields
		protected $lblId;
		protected $lblUserId;
		protected $lblTitle;
		protected $lblText;
		protected $lblAdded;
		protected $lblChanged;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * NewsMetaControl to edit a single News object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single News object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this NewsMetaControl
		 * @param News $objNews new or existing News object
		 */
		 public function __construct($objParentObject, News $objNews) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this NewsMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked News object
			$this->objNews = $objNews;

			// Figure out if we're Editing or Creating New
			if ($this->objNews->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this NewsMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing News object creation - defaults to CreateOrEdit
 		 * @return NewsMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objNews = News::Load($strId);

				// News was found -- return it!
				if ($objNews)
					return new NewsMetaControl($objParentObject, $objNews);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a News object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new NewsMetaControl($objParentObject, new News());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this NewsMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing News object creation - defaults to CreateOrEdit
		 * @return NewsMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return NewsMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this NewsMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing News object creation - defaults to CreateOrEdit
		 * @return NewsMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return NewsMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objNews->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = News::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objNews->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstUser
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstUser_Create($strControlId = null) {
			$this->lstUser = new QListBox($this->objParentObject, $strControlId);
			$this->lstUser->Name = QApplication::Translate('User');
			$this->lstUser->Required = true;
			if (!$this->blnEditMode)
				$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
			$objUserArray = User::LoadAll();
			if ($objUserArray) foreach ($objUserArray as $objUser) {
				$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
				if (($this->objNews->User) && ($this->objNews->User->Id == $objUser->Id))
					$objListItem->Selected = true;
				$this->lstUser->AddItem($objListItem);
			}
			return $this->lstUser;
		}

		/**
		 * Create and setup QLabel lblUserId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblUserId_Create($strControlId = null) {
			$this->lblUserId = new QLabel($this->objParentObject, $strControlId);
			$this->lblUserId->Name = QApplication::Translate('User');
			$this->lblUserId->Text = ($this->objNews->User) ? $this->objNews->User->__toString() : null;
			$this->lblUserId->Required = true;
			return $this->lblUserId;
		}

		/**
		 * Create and setup QTextBox txtTitle
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtTitle_Create($strControlId = null) {
			$this->txtTitle = new QTextBox($this->objParentObject, $strControlId);
			$this->txtTitle->Name = QApplication::Translate('Title');
			$this->txtTitle->Text = $this->objNews->Title;
			$this->txtTitle->Required = true;
			$this->txtTitle->MaxLength = News::TitleMaxLength;
			return $this->txtTitle;
		}

		/**
		 * Create and setup QLabel lblTitle
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblTitle_Create($strControlId = null) {
			$this->lblTitle = new QLabel($this->objParentObject, $strControlId);
			$this->lblTitle->Name = QApplication::Translate('Title');
			$this->lblTitle->Text = $this->objNews->Title;
			$this->lblTitle->Required = true;
			return $this->lblTitle;
		}

		/**
		 * Create and setup QTextBox txtText
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtText_Create($strControlId = null) {
			$this->txtText = new QTextBox($this->objParentObject, $strControlId);
			$this->txtText->Name = QApplication::Translate('Text');
			$this->txtText->Text = $this->objNews->Text;
			$this->txtText->Required = true;
			$this->txtText->TextMode = QTextMode::MultiLine;
			return $this->txtText;
		}

		/**
		 * Create and setup QLabel lblText
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblText_Create($strControlId = null) {
			$this->lblText = new QLabel($this->objParentObject, $strControlId);
			$this->lblText->Name = QApplication::Translate('Text');
			$this->lblText->Text = $this->objNews->Text;
			$this->lblText->Required = true;
			return $this->lblText;
		}

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objNews->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objNews->Added) ? $this->objNews->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calChanged
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calChanged_Create($strControlId = null) {
			$this->calChanged = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calChanged->Name = QApplication::Translate('Changed');
			$this->calChanged->DateTime = $this->objNews->Changed;
			$this->calChanged->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calChanged->Required = true;
			return $this->calChanged;
		}

		/**
		 * Create and setup QLabel lblChanged
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblChanged_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblChanged = new QLabel($this->objParentObject, $strControlId);
			$this->lblChanged->Name = QApplication::Translate('Changed');
			$this->strChangedDateTimeFormat = $strDateTimeFormat;
			$this->lblChanged->Text = sprintf($this->objNews->Changed) ? $this->objNews->__toString($this->strChangedDateTimeFormat) : null;
			$this->lblChanged->Required = true;
			return $this->lblChanged;
		}

		protected $strChangedDateTimeFormat;

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objNews->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objNews->Owner;
			$this->txtOwner->MaxLength = News::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objNews->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objNews->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objNews->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objNews->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objNews->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objNews->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objNews->Status) ? AuthStatusEnum::$NameArray[$this->objNews->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local News object.
		 * @param boolean $blnReload reload News from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objNews->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objNews->Id;
			if ($this->lblId) $this->lblId->Text = $this->objNews->Id;

			if ($this->lstUser) {
					$this->lstUser->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstUser->AddItem(QApplication::Translate('- Select One -'), null);
				$objUserArray = User::LoadAll();
				if ($objUserArray) foreach ($objUserArray as $objUser) {
					$objListItem = new QListItem($objUser->__toString(), $objUser->Id);
					if (($this->objNews->User) && ($this->objNews->User->Id == $objUser->Id))
						$objListItem->Selected = true;
					$this->lstUser->AddItem($objListItem);
				}
			}
			if ($this->lblUserId) $this->lblUserId->Text = ($this->objNews->User) ? $this->objNews->User->__toString() : null;

			if ($this->txtTitle) $this->txtTitle->Text = $this->objNews->Title;
			if ($this->lblTitle) $this->lblTitle->Text = $this->objNews->Title;

			if ($this->txtText) $this->txtText->Text = $this->objNews->Text;
			if ($this->lblText) $this->lblText->Text = $this->objNews->Text;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objNews->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objNews->Added) ? $this->objNews->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->calChanged) $this->calChanged->DateTime = $this->objNews->Changed;
			if ($this->lblChanged) $this->lblChanged->Text = sprintf($this->objNews->Changed) ? $this->objNews->__toString($this->strChangedDateTimeFormat) : null;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objNews->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objNews->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objNews->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objNews->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objNews->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objNews->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objNews->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objNews->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objNews->Status) ? AuthStatusEnum::$NameArray[$this->objNews->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC NEWS OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's News instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveNews() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objNews->Id = $this->txtId->Text;
				if ($this->lstUser) $this->objNews->UserId = $this->lstUser->SelectedValue;
				if ($this->txtTitle) $this->objNews->Title = $this->txtTitle->Text;
				if ($this->txtText) $this->objNews->Text = $this->txtText->Text;
				if ($this->calAdded) $this->objNews->Added = $this->calAdded->DateTime;
				if ($this->calChanged) $this->objNews->Changed = $this->calChanged->DateTime;
				if ($this->txtOwner) $this->objNews->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objNews->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objNews->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objNews->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the News object
				$this->objNews->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's News instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteNews() {
			$this->objNews->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'News': return $this->objNews;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to News fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'UserIdControl':
					if (!$this->lstUser) return $this->lstUser_Create();
					return $this->lstUser;
				case 'UserIdLabel':
					if (!$this->lblUserId) return $this->lblUserId_Create();
					return $this->lblUserId;
				case 'TitleControl':
					if (!$this->txtTitle) return $this->txtTitle_Create();
					return $this->txtTitle;
				case 'TitleLabel':
					if (!$this->lblTitle) return $this->lblTitle_Create();
					return $this->lblTitle;
				case 'TextControl':
					if (!$this->txtText) return $this->txtText_Create();
					return $this->txtText;
				case 'TextLabel':
					if (!$this->lblText) return $this->lblText_Create();
					return $this->lblText;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'ChangedControl':
					if (!$this->calChanged) return $this->calChanged_Create();
					return $this->calChanged;
				case 'ChangedLabel':
					if (!$this->lblChanged) return $this->lblChanged_Create();
					return $this->lblChanged;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to News fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'UserIdControl':
						return ($this->lstUser = QType::Cast($mixValue, 'QControl'));
					case 'TitleControl':
						return ($this->txtTitle = QType::Cast($mixValue, 'QControl'));
					case 'TextControl':
						return ($this->txtText = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'ChangedControl':
						return ($this->calChanged = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
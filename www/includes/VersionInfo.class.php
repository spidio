<?php

abstract class VersionInfo extends VersionInfoBase {
	protected $strRevision;
	protected $strYears;
	
	protected $strSystemNameArray = array('CoreVersion'	=>	'coresystem',
										'SoapVersion'	=>	'webservice'
									);
	
	protected function UpdateVersionInfo() {
		if (!($objVersion = QApplication::GetConfig(sprintf('software_%s_version', $this->strSystemNameArray[get_class($this)]), 'Config'))) {
			$objVersion = new Config();
			$objVersion->Name = sprintf('software_%s_version', $this->strSystemNameArray[get_class($this)]);
			$objVersion->Value = $this->strVersion;
			$objVersion->IsDynamic = false;
			$objVersion->Save();
		} else if ($this->CompareVersionString($this->strVersion, $objVersion->Value, '>')) {
			$objVersion->Value = $this->strVersion;
			$objVersion->Save();
		}
		
		if (!($objRevision = QApplication::GetConfig(sprintf('software_%s_revision', $this->strSystemNameArray[get_class($this)]), 'Config'))) {
			$objRevision = new Config();
			$objRevision->Name = sprintf('software_%s_revision', $this->strSystemNameArray[get_class($this)]);
			$objRevision->Value = $this->strRevision;
			$objRevision->IsDynamic = false;
			$objRevision->Save();
		} else if ($this->strRevision > $objRevision->Value) {
			$objRevision->Value = $this->strRevision;
			$objRevision->Save();
		}
		
		if (!($objName = QApplication::GetConfig(sprintf('software_%s_name', $this->strSystemNameArray[get_class($this)]), 'Config'))) {
			$objName = new Config();
			$objName->Name = sprintf('software_%s_name', $this->strSystemNameArray[get_class($this)]);
			$objName->Value = $this->strName;
			$objName->IsDynamic = false;
			$objName->Save();
		} else if ($this->strName != $objName->Value) {
			$objName->Value = $this->strName;
			$objName->Save();
		}
		
		if (!($objWebsite = QApplication::GetConfig(sprintf('software_%s_website', $this->strSystemNameArray[get_class($this)]), 'Config'))) {
			$objWebsite = new Config();
			$objWebsite->Name = sprintf('software_%s_website', $this->strSystemNameArray[get_class($this)]);
			$objWebsite->Value = $this->strWebsite;
			$objWebsite->IsDynamic = false;
			$objWebsite->Save();
		} else if ($this->strWebsite != $objWebsite->Value) {
			$objWebsite->Value = $this->strWebsite;
			$objWebsite->Save();
		}
		
		if (!($objAuthor = QApplication::GetConfig(sprintf('software_%s_author', $this->strSystemNameArray[get_class($this)]), 'Config'))) {
			$objAuthor = new Config();
			$objAuthor->Name = sprintf('software_%s_author', $this->strSystemNameArray[get_class($this)]);
			$objAuthor->Value = $this->strAuthor;
			$objAuthor->IsDynamic = false;
			$objAuthor->Save();
		} else if ($this->strAuthor != $objAuthor->Value) {
			$objAuthor->Value = $this->strAuthor;
			$objAuthor->Save();
		}
		
		if (!($objYears = QApplication::GetConfig(sprintf('software_%s_years', $this->strSystemNameArray[get_class($this)]), 'Config'))) {
			$objYears = new Config();
			$objYears->Name = sprintf('software_%s_years', $this->strSystemNameArray[get_class($this)]);
			$objYears->Value = $this->strYears;
			$objYears->IsDynamic = false;
			$objYears->Save();
		} else if ($this->strYears != $objYears->Value) {
			$objYears->Value = $this->strYears;
			$objYears->Save();
		}
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Revision': return $this->strRevision;
			case 'Years': return $this->strYears;
			case 'SystemName': return $this->strSystemNameArray[get_class($this)];
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
			case 'Revision':
				try {
					return ($this->strRevision = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Years':
				try {
					return ($this->strYears = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			
			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
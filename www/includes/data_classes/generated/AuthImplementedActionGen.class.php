<?php
	/**
	 * The abstract AuthImplementedActionGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the AuthImplementedAction subclass which
	 * extends this AuthImplementedActionGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthImplementedAction class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property integer $AuthClassEnumId the value for intAuthClassEnumId (PK)
	 * @property integer $AuthActionId the value for intAuthActionId (PK)
	 * @property integer $AuthStatusEnumId the value for intAuthStatusEnumId 
	 * @property AuthAction $AuthAction the value for the AuthAction object referenced by intAuthActionId (PK)
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class AuthImplementedActionGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_auth_implemented_action.auth_class_enum_id
		 * @var integer intAuthClassEnumId
		 */
		protected $intAuthClassEnumId;
		const AuthClassEnumIdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var integer __intAuthClassEnumId;
		 */
		protected $__intAuthClassEnumId;

		/**
		 * Protected member variable that maps to the database PK column spidio_auth_implemented_action.auth_action_id
		 * @var integer intAuthActionId
		 */
		protected $intAuthActionId;
		const AuthActionIdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var integer __intAuthActionId;
		 */
		protected $__intAuthActionId;

		/**
		 * Protected member variable that maps to the database column spidio_auth_implemented_action.auth_status_enum_id
		 * @var integer intAuthStatusEnumId
		 */
		protected $intAuthStatusEnumId;
		const AuthStatusEnumIdDefault = null;


		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_auth_implemented_action.auth_action_id.
		 *
		 * NOTE: Always use the AuthAction property getter to correctly retrieve this AuthAction object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var AuthAction objAuthAction
		 */
		protected $objAuthAction;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a AuthImplementedAction from PK Info
		 * @param integer $intAuthClassEnumId
		 * @param integer $intAuthActionId
		 * @return AuthImplementedAction
		 */
		public static function Load($intAuthClassEnumId, $intAuthActionId) {
			// Use QuerySingle to Perform the Query
			return AuthImplementedAction::QuerySingle(
				QQ::AndCondition(
				QQ::Equal(QQN::AuthImplementedAction()->AuthClassEnumId, $intAuthClassEnumId),
				QQ::Equal(QQN::AuthImplementedAction()->AuthActionId, $intAuthActionId)
				)
			);
		}

		/**
		 * Load all AuthImplementedActions
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthImplementedAction[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call AuthImplementedAction::QueryArray to perform the LoadAll query
			try {
				return AuthImplementedAction::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all AuthImplementedActions
		 * @return int
		 */
		public static function CountAll() {
			// Call AuthImplementedAction::QueryCount to perform the CountAll query
			return AuthImplementedAction::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = AuthImplementedAction::GetDatabase();

			// Create/Build out the QueryBuilder object with AuthImplementedAction-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'auth_implemented_action');
			AuthImplementedAction::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'auth_implemented_action');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single AuthImplementedAction object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return AuthImplementedAction the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthImplementedAction::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new AuthImplementedAction object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return AuthImplementedAction::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of AuthImplementedAction objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return AuthImplementedAction[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthImplementedAction::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return AuthImplementedAction::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of AuthImplementedAction objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthImplementedAction::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = AuthImplementedAction::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'auth_implemented_action_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with AuthImplementedAction-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				AuthImplementedAction::GetSelectFields($objQueryBuilder);
				AuthImplementedAction::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return AuthImplementedAction::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this AuthImplementedAction
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_implemented_action';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'auth_class_enum_id', $strAliasPrefix . 'auth_class_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'auth_action_id', $strAliasPrefix . 'auth_action_id');
			$objBuilder->AddSelectItem($strTableName, 'auth_status_enum_id', $strAliasPrefix . 'auth_status_enum_id');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a AuthImplementedAction from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this AuthImplementedAction::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return AuthImplementedAction
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;


			// Create a new instance of the AuthImplementedAction object
			$objToReturn = new AuthImplementedAction();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'auth_class_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_class_enum_id'] : $strAliasPrefix . 'auth_class_enum_id';
			$objToReturn->intAuthClassEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$objToReturn->__intAuthClassEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'auth_action_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_action_id'] : $strAliasPrefix . 'auth_action_id';
			$objToReturn->intAuthActionId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$objToReturn->__intAuthActionId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'auth_status_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_status_enum_id'] : $strAliasPrefix . 'auth_status_enum_id';
			$objToReturn->intAuthStatusEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'auth_implemented_action__';

			// Check for AuthAction Early Binding
			$strAlias = $strAliasPrefix . 'auth_action_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objAuthAction = AuthAction::InstantiateDbRow($objDbRow, $strAliasPrefix . 'auth_action_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			return $objToReturn;
		}

		/**
		 * Instantiate an array of AuthImplementedActions from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return AuthImplementedAction[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = AuthImplementedAction::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = AuthImplementedAction::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single AuthImplementedAction object,
		 * by AuthClassEnumId, AuthActionId Index(es)
		 * @param integer $intAuthClassEnumId
		 * @param integer $intAuthActionId
		 * @return AuthImplementedAction
		*/
		public static function LoadByAuthClassEnumIdAuthActionId($intAuthClassEnumId, $intAuthActionId) {
			return AuthImplementedAction::QuerySingle(
				QQ::AndCondition(
				QQ::Equal(QQN::AuthImplementedAction()->AuthClassEnumId, $intAuthClassEnumId),
				QQ::Equal(QQN::AuthImplementedAction()->AuthActionId, $intAuthActionId)
				)
			);
		}
			
		/**
		 * Load an array of AuthImplementedAction objects,
		 * by AuthStatusEnumId Index(es)
		 * @param integer $intAuthStatusEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthImplementedAction[]
		*/
		public static function LoadArrayByAuthStatusEnumId($intAuthStatusEnumId, $objOptionalClauses = null) {
			// Call AuthImplementedAction::QueryArray to perform the LoadArrayByAuthStatusEnumId query
			try {
				return AuthImplementedAction::QueryArray(
					QQ::Equal(QQN::AuthImplementedAction()->AuthStatusEnumId, $intAuthStatusEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthImplementedActions
		 * by AuthStatusEnumId Index(es)
		 * @param integer $intAuthStatusEnumId
		 * @return int
		*/
		public static function CountByAuthStatusEnumId($intAuthStatusEnumId) {
			// Call AuthImplementedAction::QueryCount to perform the CountByAuthStatusEnumId query
			return AuthImplementedAction::QueryCount(
				QQ::Equal(QQN::AuthImplementedAction()->AuthStatusEnumId, $intAuthStatusEnumId)
			);
		}
			
		/**
		 * Load an array of AuthImplementedAction objects,
		 * by AuthActionId Index(es)
		 * @param integer $intAuthActionId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthImplementedAction[]
		*/
		public static function LoadArrayByAuthActionId($intAuthActionId, $objOptionalClauses = null) {
			// Call AuthImplementedAction::QueryArray to perform the LoadArrayByAuthActionId query
			try {
				return AuthImplementedAction::QueryArray(
					QQ::Equal(QQN::AuthImplementedAction()->AuthActionId, $intAuthActionId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthImplementedActions
		 * by AuthActionId Index(es)
		 * @param integer $intAuthActionId
		 * @return int
		*/
		public static function CountByAuthActionId($intAuthActionId) {
			// Call AuthImplementedAction::QueryCount to perform the CountByAuthActionId query
			return AuthImplementedAction::QueryCount(
				QQ::Equal(QQN::AuthImplementedAction()->AuthActionId, $intAuthActionId)
			);
		}
			
		/**
		 * Load an array of AuthImplementedAction objects,
		 * by AuthClassEnumId Index(es)
		 * @param integer $intAuthClassEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthImplementedAction[]
		*/
		public static function LoadArrayByAuthClassEnumId($intAuthClassEnumId, $objOptionalClauses = null) {
			// Call AuthImplementedAction::QueryArray to perform the LoadArrayByAuthClassEnumId query
			try {
				return AuthImplementedAction::QueryArray(
					QQ::Equal(QQN::AuthImplementedAction()->AuthClassEnumId, $intAuthClassEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthImplementedActions
		 * by AuthClassEnumId Index(es)
		 * @param integer $intAuthClassEnumId
		 * @return int
		*/
		public static function CountByAuthClassEnumId($intAuthClassEnumId) {
			// Call AuthImplementedAction::QueryCount to perform the CountByAuthClassEnumId query
			return AuthImplementedAction::QueryCount(
				QQ::Equal(QQN::AuthImplementedAction()->AuthClassEnumId, $intAuthClassEnumId)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this AuthImplementedAction
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = AuthImplementedAction::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'auth_implemented_action` (
							`auth_class_enum_id`,
							`auth_action_id`,
							`auth_status_enum_id`
						) VALUES (
							' . $objDatabase->SqlVariable($this->intAuthClassEnumId) . ',
							' . $objDatabase->SqlVariable($this->intAuthActionId) . ',
							' . $objDatabase->SqlVariable($this->intAuthStatusEnumId) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'auth_implemented_action`
						SET
							`auth_class_enum_id` = ' . $objDatabase->SqlVariable($this->intAuthClassEnumId) . ',
							`auth_action_id` = ' . $objDatabase->SqlVariable($this->intAuthActionId) . ',
							`auth_status_enum_id` = ' . $objDatabase->SqlVariable($this->intAuthStatusEnumId) . '
						WHERE
							`auth_class_enum_id` = ' . $objDatabase->SqlVariable($this->__intAuthClassEnumId) . ' AND
							`auth_action_id` = ' . $objDatabase->SqlVariable($this->__intAuthActionId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__intAuthClassEnumId = $this->intAuthClassEnumId;
			$this->__intAuthActionId = $this->intAuthActionId;


			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this AuthImplementedAction
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->intAuthClassEnumId)) || (is_null($this->intAuthActionId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this AuthImplementedAction with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = AuthImplementedAction::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'auth_implemented_action`
				WHERE
					`auth_class_enum_id` = ' . $objDatabase->SqlVariable($this->intAuthClassEnumId) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intAuthActionId) . '');
		}

		/**
		 * Delete all AuthImplementedActions
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = AuthImplementedAction::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'auth_implemented_action`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'auth_implemented_action table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = AuthImplementedAction::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'auth_implemented_action`');
		}

		/**
		 * Reload this AuthImplementedAction from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved AuthImplementedAction object.');

			// Reload the Object
			$objReloaded = AuthImplementedAction::Load($this->intAuthClassEnumId, $this->intAuthActionId);

			// Update $this's local variables to match
			$this->AuthClassEnumId = $objReloaded->AuthClassEnumId;
			$this->__intAuthClassEnumId = $this->intAuthClassEnumId;
			$this->AuthActionId = $objReloaded->AuthActionId;
			$this->__intAuthActionId = $this->intAuthActionId;
			$this->AuthStatusEnumId = $objReloaded->AuthStatusEnumId;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'AuthClassEnumId':
					/**
					 * Gets the value for intAuthClassEnumId (PK)
					 * @return integer
					 */
					return $this->intAuthClassEnumId;

				case 'AuthActionId':
					/**
					 * Gets the value for intAuthActionId (PK)
					 * @return integer
					 */
					return $this->intAuthActionId;

				case 'AuthStatusEnumId':
					/**
					 * Gets the value for intAuthStatusEnumId 
					 * @return integer
					 */
					return $this->intAuthStatusEnumId;


				///////////////////
				// Member Objects
				///////////////////
				case 'AuthAction':
					/**
					 * Gets the value for the AuthAction object referenced by intAuthActionId (PK)
					 * @return AuthAction
					 */
					try {
						if ((!$this->objAuthAction) && (!is_null($this->intAuthActionId)))
							$this->objAuthAction = AuthAction::Load($this->intAuthActionId);
						return $this->objAuthAction;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'AuthClassEnumId':
					/**
					 * Sets the value for intAuthClassEnumId (PK)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intAuthClassEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AuthActionId':
					/**
					 * Sets the value for intAuthActionId (PK)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						$this->objAuthAction = null;
						return ($this->intAuthActionId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AuthStatusEnumId':
					/**
					 * Sets the value for intAuthStatusEnumId 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intAuthStatusEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'AuthAction':
					/**
					 * Sets the value for the AuthAction object referenced by intAuthActionId (PK)
					 * @param AuthAction $mixValue
					 * @return AuthAction
					 */
					if (is_null($mixValue)) {
						$this->intAuthActionId = null;
						$this->objAuthAction = null;
						return null;
					} else {
						// Make sure $mixValue actually is a AuthAction object
						try {
							$mixValue = QType::Cast($mixValue, 'AuthAction');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED AuthAction object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved AuthAction for this AuthImplementedAction');

						// Update Local Member Variables
						$this->objAuthAction = $mixValue;
						$this->intAuthActionId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="AuthImplementedAction"><sequence>';
			$strToReturn .= '<element name="AuthClassEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="AuthAction" type="xsd1:AuthAction"/>';
			$strToReturn .= '<element name="AuthStatusEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('AuthImplementedAction', $strComplexTypeArray)) {
				$strComplexTypeArray['AuthImplementedAction'] = AuthImplementedAction::GetSoapComplexTypeXml();
				AuthAction::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, AuthImplementedAction::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new AuthImplementedAction();
			if (property_exists($objSoapObject, 'AuthClassEnumId'))
				$objToReturn->intAuthClassEnumId = $objSoapObject->AuthClassEnumId;
			if ((property_exists($objSoapObject, 'AuthAction')) &&
				($objSoapObject->AuthAction))
				$objToReturn->AuthAction = AuthAction::GetObjectFromSoapObject($objSoapObject->AuthAction);
			if (property_exists($objSoapObject, 'AuthStatusEnumId'))
				$objToReturn->intAuthStatusEnumId = $objSoapObject->AuthStatusEnumId;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, AuthImplementedAction::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objAuthAction)
				$objObject->objAuthAction = AuthAction::GetSoapObjectFromObject($objObject->objAuthAction, false);
			else if (!$blnBindRelatedObjects)
				$objObject->intAuthActionId = null;
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeAuthImplementedAction extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'auth_class_enum_id';
		protected $strClassName = 'AuthImplementedAction';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_implemented_action';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'AuthClassEnumId':
					return new QQNode('auth_class_enum_id', 'AuthClassEnumId', 'integer', $this);
				case 'AuthActionId':
					return new QQNode('auth_action_id', 'AuthActionId', 'integer', $this);
				case 'AuthAction':
					return new QQNodeAuthAction('auth_action_id', 'AuthAction', 'integer', $this);
				case 'AuthStatusEnumId':
					return new QQNode('auth_status_enum_id', 'AuthStatusEnumId', 'integer', $this);

				case '_PrimaryKeyNode':
					return new QQNode('auth_class_enum_id', 'AuthClassEnumId', 'integer', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeAuthImplementedAction extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'auth_class_enum_id';
		protected $strClassName = 'AuthImplementedAction';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_implemented_action';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'AuthClassEnumId':
					return new QQNode('auth_class_enum_id', 'AuthClassEnumId', 'integer', $this);
				case 'AuthActionId':
					return new QQNode('auth_action_id', 'AuthActionId', 'integer', $this);
				case 'AuthAction':
					return new QQNodeAuthAction('auth_action_id', 'AuthAction', 'integer', $this);
				case 'AuthStatusEnumId':
					return new QQNode('auth_status_enum_id', 'AuthStatusEnumId', 'integer', $this);

				case '_PrimaryKeyNode':
					return new QQNode('auth_class_enum_id', 'AuthClassEnumId', 'integer', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
<?php

abstract class MenuBase {
	protected $objMenuItemArray;
	
	public function __construct() {
		$this->BuildMenu();
	}
	
	public function __toString() {
		$strReturn = '';
		for ($i = 0; $i < count($this->objMenuItemArray); $i++) {
			$strReturn .= sprintf('<a href="%s">%s</a>', $this->objMenuItemArray[$i]->Link, $this->objMenuItemArray[$i]->Text);
			
			if ($i < count($this->objMenuItemArray) - 1) {
				$strReturn .= ' &rsaquo; ';
			}
		}
		
		return $strReturn;
	}
	
	protected final function BuildMenu() {
	$this->objMenuItemArray = array(new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_index')), QApplication::Translate('Home')));
		
		switch (QApplication::PathInfo(0)) {
			default:
			case QApplication::GetConfig('pagename_index'):
				break;
			case QApplication::GetConfig('pagename_login'):
				$this->objMenuItemArray[] = new MenuItem(QApplication::GetConfig('pagename_login'), QApplication::Translate('Login'));
				break;
			case QApplication::GetConfig('pagename_logout'):
				$this->objMenuItemArray[] = new MenuItem(QApplication::GetConfig('pagename_logout'), QApplication::Translate('Logout'));
				break;
			case QApplication::GetConfig('pagename_profile'):
				$objAuth = QApplication::GetAuth();
				$this->objMenuItemArray[] = new MenuItem(QApplication::GetConfig('pagename_profile'), sprintf(QApplication::Translate('Profile of %s'), $objAuth->Session->User->Person->Name));
				break;
			case QApplication::GetConfig('pagename_recent'):
				$this->objMenuItemArray[] = new MenuItem(QApplication::GetConfig('pagename_recent'), QApplication::Translate('Recent changes'));
				break;
			case QApplication::GetConfig('pagename_search'):
				$this->objMenuItemArray[] = new MenuItem(QApplication::GetConfig('pagename_profile'), QApplication::Translate('Spotsight search'));
				break;
				
			case QApplication::GetConfig('pagename_organisation'):
				if (!is_null($objOrganisation = Organisation::LoadById(QApplication::PathInfo(2)))) {
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_organisation'), QApplication::GetConfig('pagename_view'), $objOrganisation->Id), $objOrganisation);
					
					switch (QApplication::PathInfo(1)) {
						default:
						case QApplication::GetConfig('pagename_view'):
							break;
						case QApplication::GetConfig('pagename_edit'):
							$this->objMenuItemArray[] = new MenuItem('', QApplication::Translate('Edit'));
							break;
						case QApplication::GetConfig('pagename_delete'):
							$this->objMenuItemArray[] = new MenuItem('', QApplication::Translate('Delete confirmation'));
							break;
					}
				} else {
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project')), QApplication::Translate('Organisations'));
					
					if (QApplication::PathInfo(1) == QApplication::GetConfig('pagename_add')) {
						$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project'), QApplication::GetConfig('pagename_add')), QApplication::Translate('Add entry'));
					}
				}
				break;
			case QApplication::GetConfig('pagename_project'):
				if (!is_null($objProject = Project::LoadById(QApplication::PathInfo(2)))) {
					$objOrganisation = $objProject->Organisation;
					
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_organisation'), QApplication::GetConfig('pagename_view'), $objOrganisation->Id), $objOrganisation);
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project'), QApplication::GetConfig('pagename_view'), $objProject->Id), $objProject->Name);
					
					switch (QApplication::PathInfo(1)) {
						default:
						case QApplication::GetConfig('pagename_view'):
							break;
						case QApplication::GetConfig('pagename_edit'):
							$this->objMenuItemArray[] = new MenuItem('', QApplication::Translate('Edit'));
							break;
						case QApplication::GetConfig('pagename_delete'):
							$this->objMenuItemArray[] = new MenuItem('', QApplication::Translate('Delete confirmation'));
							break;
					}
				} else {
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project')), QApplication::Translate('Projects'));
					
					if (QApplication::PathInfo(1) == QApplication::GetConfig('pagename_add')) {
						$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project'), QApplication::GetConfig('pagename_add')), QApplication::Translate('Add entry'));
					}
				}
				break;
			case QApplication::GetConfig('pagename_tape'):
				if (!is_null($objTape = Tape::LoadById(QApplication::PathInfo(2)))) {
					$objOrganisation = $objTape->Organisation;
					
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_organisation'), QApplication::GetConfig('pagename_view'), $objOrganisation->Id), $objOrganisation);
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_tape'), QApplication::GetConfig('pagename_view'), $objTape->Id), sprintf(QApplication::Translate('Tape #%s'), $objTape->Number));
					
					switch (QApplication::PathInfo(1)) {
						default:
						case QApplication::GetConfig('pagename_view'):
							break;
						case QApplication::GetConfig('pagename_edit'):
							$this->objMenuItemArray[] = new MenuItem('', QApplication::Translate('Edit'));
							break;
						case QApplication::GetConfig('pagename_delete'):
							$this->objMenuItemArray[] = new MenuItem('', QApplication::Translate('Delete confirmation'));
							break;
					}
				} else {
					$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project')), QApplication::Translate('Tapes'));
					
					if (QApplication::PathInfo(1) == QApplication::GetConfig('pagename_add')) {
						$this->objMenuItemArray[] = new MenuItem(QApplication::MakeUrl(QApplication::GetConfig('pagename_project'), QApplication::GetConfig('pagename_add')), QApplication::Translate('Add entry'));
					}
				}
				break;
		}
	}
		
	public function GetAsArray() {
		return $this->objMenuItemArray;
	}
}

abstract class MenuItemBase extends QBaseClass {
	protected $strLink;
	protected $strText;
	
	public function __construct($strLink, $strText) {
		$this->strLink = $strLink;
		$this->strText = $strText;
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Link': return $this->strLink;
			case 'Text': return $this->strText;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
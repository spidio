<?php

class SoapVersion extends VersionInfo {
	protected function SetVersionInfo() {
		// Software version
		$this->Version = '0B21';
		$this->Revision = '73';
		
		// Software name
		$this->Name = 'Soapidio';
		$this->Website = 'https://sourceforge.net/projects/spidio';
		
		// Software copyright
		$this->Author = 'Spidio Project';
		$this->Years = '2007-2008';
	}
}

?>
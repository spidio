<?php
	require(__DATAGEN_CLASSES__ . '/LogItemEnumGen.class.php');

	/**
	 * The LogItemEnum class defined here contains any
	 * customized code for the LogItemEnum enumerated type. 
	 * 
	 * It represents the enumerated values found in the "spidio_log_item_enum" table in the database,
	 * and extends from the code generated abstract LogItemEnumGen
	 * class, which contains all the values extracted from the database.
	 * 
	 * Type classes which are generally used to attach a type to data object.
	 * However, they may be used as simple database indepedant enumerated type.
	 * 
	 * @package Spidio
	 * @subpackage DataObjects
	 */
	abstract class LogItemEnum extends LogItemEnumGen {
		public static function ToTranslatedString($intLogItemEnumId) {
			switch ($intLogItemEnumId) {
				case 1: return QApplication::Translate('User login');
				case 2: return QApplication::Translate('User logout');
				case 3: return QApplication::Translate('Session timeout');
				case 4: return QApplication::Translate('Session created');
				case 5: return QApplication::Translate('Forced user logout');
				case 6: return QApplication::Translate('Organisation');
				case 7: return QApplication::Translate('Project');
				case 8: return QApplication::Translate('Tape');
				case 9: return QApplication::Translate('Session reset');
				default:
					return parent::ToString($intLogItemEnumId);
			}
		}
	}
?>
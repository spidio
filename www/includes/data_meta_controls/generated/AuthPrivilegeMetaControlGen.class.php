<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the AuthPrivilege class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single AuthPrivilege object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a AuthPrivilegeMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read AuthPrivilege $AuthPrivilege the actual AuthPrivilege data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $AuthRoleEnumIdControl
	 * property-read QLabel $AuthRoleEnumIdLabel
	 * property QTextBox $WhoControl
	 * property-read QLabel $WhoLabel
	 * property QListBox $AuthActionIdControl
	 * property-read QLabel $AuthActionIdLabel
	 * property QListBox $AuthPrivilegeTypeEnumIdControl
	 * property-read QLabel $AuthPrivilegeTypeEnumIdLabel
	 * property QListBox $AuthClassEnumIdControl
	 * property-read QLabel $AuthClassEnumIdLabel
	 * property QTextBox $RelatedIdControl
	 * property-read QLabel $RelatedIdLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class AuthPrivilegeMetaControlGen extends QBaseClass {
		// General Variables
		protected $objAuthPrivilege;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of AuthPrivilege's individual data fields
		protected $txtId;
		protected $lstAuthRoleEnum;
		protected $txtWho;
		protected $lstAuthAction;
		protected $lstAuthPrivilegeTypeEnum;
		protected $lstAuthClassEnum;
		protected $txtRelatedId;

		// Controls that allow the viewing of AuthPrivilege's individual data fields
		protected $lblId;
		protected $lblAuthRoleEnumId;
		protected $lblWho;
		protected $lblAuthActionId;
		protected $lblAuthPrivilegeTypeEnumId;
		protected $lblAuthClassEnumId;
		protected $lblRelatedId;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * AuthPrivilegeMetaControl to edit a single AuthPrivilege object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single AuthPrivilege object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthPrivilegeMetaControl
		 * @param AuthPrivilege $objAuthPrivilege new or existing AuthPrivilege object
		 */
		 public function __construct($objParentObject, AuthPrivilege $objAuthPrivilege) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this AuthPrivilegeMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked AuthPrivilege object
			$this->objAuthPrivilege = $objAuthPrivilege;

			// Figure out if we're Editing or Creating New
			if ($this->objAuthPrivilege->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthPrivilegeMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing AuthPrivilege object creation - defaults to CreateOrEdit
 		 * @return AuthPrivilegeMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objAuthPrivilege = AuthPrivilege::Load($strId);

				// AuthPrivilege was found -- return it!
				if ($objAuthPrivilege)
					return new AuthPrivilegeMetaControl($objParentObject, $objAuthPrivilege);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a AuthPrivilege object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new AuthPrivilegeMetaControl($objParentObject, new AuthPrivilege());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthPrivilegeMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing AuthPrivilege object creation - defaults to CreateOrEdit
		 * @return AuthPrivilegeMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return AuthPrivilegeMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthPrivilegeMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing AuthPrivilege object creation - defaults to CreateOrEdit
		 * @return AuthPrivilegeMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return AuthPrivilegeMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objAuthPrivilege->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = AuthPrivilege::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objAuthPrivilege->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstAuthRoleEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthRoleEnum_Create($strControlId = null) {
			$this->lstAuthRoleEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthRoleEnum->Name = QApplication::Translate('Auth Role Enum');
			$this->lstAuthRoleEnum->Required = true;
			foreach (AuthRoleEnum::$NameArray as $intId => $strValue)
				$this->lstAuthRoleEnum->AddItem(new QListItem($strValue, $intId, $this->objAuthPrivilege->AuthRoleEnumId == $intId));
			return $this->lstAuthRoleEnum;
		}

		/**
		 * Create and setup QLabel lblAuthRoleEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthRoleEnumId_Create($strControlId = null) {
			$this->lblAuthRoleEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthRoleEnumId->Name = QApplication::Translate('Auth Role Enum');
			$this->lblAuthRoleEnumId->Text = ($this->objAuthPrivilege->AuthRoleEnumId) ? AuthRoleEnum::$NameArray[$this->objAuthPrivilege->AuthRoleEnumId] : null;
			$this->lblAuthRoleEnumId->Required = true;
			return $this->lblAuthRoleEnumId;
		}

		/**
		 * Create and setup QTextBox txtWho
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtWho_Create($strControlId = null) {
			$this->txtWho = new QTextBox($this->objParentObject, $strControlId);
			$this->txtWho->Name = QApplication::Translate('Who');
			$this->txtWho->Text = $this->objAuthPrivilege->Who;
			$this->txtWho->MaxLength = AuthPrivilege::WhoMaxLength;
			return $this->txtWho;
		}

		/**
		 * Create and setup QLabel lblWho
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblWho_Create($strControlId = null) {
			$this->lblWho = new QLabel($this->objParentObject, $strControlId);
			$this->lblWho->Name = QApplication::Translate('Who');
			$this->lblWho->Text = $this->objAuthPrivilege->Who;
			return $this->lblWho;
		}

		/**
		 * Create and setup QListBox lstAuthAction
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthAction_Create($strControlId = null) {
			$this->lstAuthAction = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthAction->Name = QApplication::Translate('Auth Action');
			$this->lstAuthAction->Required = true;
			if (!$this->blnEditMode)
				$this->lstAuthAction->AddItem(QApplication::Translate('- Select One -'), null);
			$objAuthActionArray = AuthAction::LoadAll();
			if ($objAuthActionArray) foreach ($objAuthActionArray as $objAuthAction) {
				$objListItem = new QListItem($objAuthAction->__toString(), $objAuthAction->Id);
				if (($this->objAuthPrivilege->AuthAction) && ($this->objAuthPrivilege->AuthAction->Id == $objAuthAction->Id))
					$objListItem->Selected = true;
				$this->lstAuthAction->AddItem($objListItem);
			}
			return $this->lstAuthAction;
		}

		/**
		 * Create and setup QLabel lblAuthActionId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthActionId_Create($strControlId = null) {
			$this->lblAuthActionId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthActionId->Name = QApplication::Translate('Auth Action');
			$this->lblAuthActionId->Text = ($this->objAuthPrivilege->AuthAction) ? $this->objAuthPrivilege->AuthAction->__toString() : null;
			$this->lblAuthActionId->Required = true;
			return $this->lblAuthActionId;
		}

		/**
		 * Create and setup QListBox lstAuthPrivilegeTypeEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthPrivilegeTypeEnum_Create($strControlId = null) {
			$this->lstAuthPrivilegeTypeEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthPrivilegeTypeEnum->Name = QApplication::Translate('Auth Privilege Type Enum');
			$this->lstAuthPrivilegeTypeEnum->Required = true;
			foreach (AuthPrivilegeTypeEnum::$NameArray as $intId => $strValue)
				$this->lstAuthPrivilegeTypeEnum->AddItem(new QListItem($strValue, $intId, $this->objAuthPrivilege->AuthPrivilegeTypeEnumId == $intId));
			return $this->lstAuthPrivilegeTypeEnum;
		}

		/**
		 * Create and setup QLabel lblAuthPrivilegeTypeEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthPrivilegeTypeEnumId_Create($strControlId = null) {
			$this->lblAuthPrivilegeTypeEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthPrivilegeTypeEnumId->Name = QApplication::Translate('Auth Privilege Type Enum');
			$this->lblAuthPrivilegeTypeEnumId->Text = ($this->objAuthPrivilege->AuthPrivilegeTypeEnumId) ? AuthPrivilegeTypeEnum::$NameArray[$this->objAuthPrivilege->AuthPrivilegeTypeEnumId] : null;
			$this->lblAuthPrivilegeTypeEnumId->Required = true;
			return $this->lblAuthPrivilegeTypeEnumId;
		}

		/**
		 * Create and setup QListBox lstAuthClassEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthClassEnum_Create($strControlId = null) {
			$this->lstAuthClassEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthClassEnum->Name = QApplication::Translate('Auth Class Enum');
			$this->lstAuthClassEnum->Required = true;
			foreach (AuthClassEnum::$NameArray as $intId => $strValue)
				$this->lstAuthClassEnum->AddItem(new QListItem($strValue, $intId, $this->objAuthPrivilege->AuthClassEnumId == $intId));
			return $this->lstAuthClassEnum;
		}

		/**
		 * Create and setup QLabel lblAuthClassEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthClassEnumId_Create($strControlId = null) {
			$this->lblAuthClassEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthClassEnumId->Name = QApplication::Translate('Auth Class Enum');
			$this->lblAuthClassEnumId->Text = ($this->objAuthPrivilege->AuthClassEnumId) ? AuthClassEnum::$NameArray[$this->objAuthPrivilege->AuthClassEnumId] : null;
			$this->lblAuthClassEnumId->Required = true;
			return $this->lblAuthClassEnumId;
		}

		/**
		 * Create and setup QTextBox txtRelatedId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtRelatedId_Create($strControlId = null) {
			$this->txtRelatedId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtRelatedId->Name = QApplication::Translate('Related Id');
			$this->txtRelatedId->Text = $this->objAuthPrivilege->RelatedId;
			$this->txtRelatedId->MaxLength = AuthPrivilege::RelatedIdMaxLength;
			return $this->txtRelatedId;
		}

		/**
		 * Create and setup QLabel lblRelatedId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblRelatedId_Create($strControlId = null) {
			$this->lblRelatedId = new QLabel($this->objParentObject, $strControlId);
			$this->lblRelatedId->Name = QApplication::Translate('Related Id');
			$this->lblRelatedId->Text = $this->objAuthPrivilege->RelatedId;
			return $this->lblRelatedId;
		}



		/**
		 * Refresh this MetaControl with Data from the local AuthPrivilege object.
		 * @param boolean $blnReload reload AuthPrivilege from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objAuthPrivilege->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objAuthPrivilege->Id;
			if ($this->lblId) $this->lblId->Text = $this->objAuthPrivilege->Id;

			if ($this->lstAuthRoleEnum) $this->lstAuthRoleEnum->SelectedValue = $this->objAuthPrivilege->AuthRoleEnumId;
			if ($this->lblAuthRoleEnumId) $this->lblAuthRoleEnumId->Text = ($this->objAuthPrivilege->AuthRoleEnumId) ? AuthRoleEnum::$NameArray[$this->objAuthPrivilege->AuthRoleEnumId] : null;

			if ($this->txtWho) $this->txtWho->Text = $this->objAuthPrivilege->Who;
			if ($this->lblWho) $this->lblWho->Text = $this->objAuthPrivilege->Who;

			if ($this->lstAuthAction) {
					$this->lstAuthAction->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstAuthAction->AddItem(QApplication::Translate('- Select One -'), null);
				$objAuthActionArray = AuthAction::LoadAll();
				if ($objAuthActionArray) foreach ($objAuthActionArray as $objAuthAction) {
					$objListItem = new QListItem($objAuthAction->__toString(), $objAuthAction->Id);
					if (($this->objAuthPrivilege->AuthAction) && ($this->objAuthPrivilege->AuthAction->Id == $objAuthAction->Id))
						$objListItem->Selected = true;
					$this->lstAuthAction->AddItem($objListItem);
				}
			}
			if ($this->lblAuthActionId) $this->lblAuthActionId->Text = ($this->objAuthPrivilege->AuthAction) ? $this->objAuthPrivilege->AuthAction->__toString() : null;

			if ($this->lstAuthPrivilegeTypeEnum) $this->lstAuthPrivilegeTypeEnum->SelectedValue = $this->objAuthPrivilege->AuthPrivilegeTypeEnumId;
			if ($this->lblAuthPrivilegeTypeEnumId) $this->lblAuthPrivilegeTypeEnumId->Text = ($this->objAuthPrivilege->AuthPrivilegeTypeEnumId) ? AuthPrivilegeTypeEnum::$NameArray[$this->objAuthPrivilege->AuthPrivilegeTypeEnumId] : null;

			if ($this->lstAuthClassEnum) $this->lstAuthClassEnum->SelectedValue = $this->objAuthPrivilege->AuthClassEnumId;
			if ($this->lblAuthClassEnumId) $this->lblAuthClassEnumId->Text = ($this->objAuthPrivilege->AuthClassEnumId) ? AuthClassEnum::$NameArray[$this->objAuthPrivilege->AuthClassEnumId] : null;

			if ($this->txtRelatedId) $this->txtRelatedId->Text = $this->objAuthPrivilege->RelatedId;
			if ($this->lblRelatedId) $this->lblRelatedId->Text = $this->objAuthPrivilege->RelatedId;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC AUTHPRIVILEGE OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's AuthPrivilege instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveAuthPrivilege() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objAuthPrivilege->Id = $this->txtId->Text;
				if ($this->lstAuthRoleEnum) $this->objAuthPrivilege->AuthRoleEnumId = $this->lstAuthRoleEnum->SelectedValue;
				if ($this->txtWho) $this->objAuthPrivilege->Who = $this->txtWho->Text;
				if ($this->lstAuthAction) $this->objAuthPrivilege->AuthActionId = $this->lstAuthAction->SelectedValue;
				if ($this->lstAuthPrivilegeTypeEnum) $this->objAuthPrivilege->AuthPrivilegeTypeEnumId = $this->lstAuthPrivilegeTypeEnum->SelectedValue;
				if ($this->lstAuthClassEnum) $this->objAuthPrivilege->AuthClassEnumId = $this->lstAuthClassEnum->SelectedValue;
				if ($this->txtRelatedId) $this->objAuthPrivilege->RelatedId = $this->txtRelatedId->Text;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the AuthPrivilege object
				$this->objAuthPrivilege->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's AuthPrivilege instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteAuthPrivilege() {
			$this->objAuthPrivilege->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'AuthPrivilege': return $this->objAuthPrivilege;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to AuthPrivilege fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'AuthRoleEnumIdControl':
					if (!$this->lstAuthRoleEnum) return $this->lstAuthRoleEnum_Create();
					return $this->lstAuthRoleEnum;
				case 'AuthRoleEnumIdLabel':
					if (!$this->lblAuthRoleEnumId) return $this->lblAuthRoleEnumId_Create();
					return $this->lblAuthRoleEnumId;
				case 'WhoControl':
					if (!$this->txtWho) return $this->txtWho_Create();
					return $this->txtWho;
				case 'WhoLabel':
					if (!$this->lblWho) return $this->lblWho_Create();
					return $this->lblWho;
				case 'AuthActionIdControl':
					if (!$this->lstAuthAction) return $this->lstAuthAction_Create();
					return $this->lstAuthAction;
				case 'AuthActionIdLabel':
					if (!$this->lblAuthActionId) return $this->lblAuthActionId_Create();
					return $this->lblAuthActionId;
				case 'AuthPrivilegeTypeEnumIdControl':
					if (!$this->lstAuthPrivilegeTypeEnum) return $this->lstAuthPrivilegeTypeEnum_Create();
					return $this->lstAuthPrivilegeTypeEnum;
				case 'AuthPrivilegeTypeEnumIdLabel':
					if (!$this->lblAuthPrivilegeTypeEnumId) return $this->lblAuthPrivilegeTypeEnumId_Create();
					return $this->lblAuthPrivilegeTypeEnumId;
				case 'AuthClassEnumIdControl':
					if (!$this->lstAuthClassEnum) return $this->lstAuthClassEnum_Create();
					return $this->lstAuthClassEnum;
				case 'AuthClassEnumIdLabel':
					if (!$this->lblAuthClassEnumId) return $this->lblAuthClassEnumId_Create();
					return $this->lblAuthClassEnumId;
				case 'RelatedIdControl':
					if (!$this->txtRelatedId) return $this->txtRelatedId_Create();
					return $this->txtRelatedId;
				case 'RelatedIdLabel':
					if (!$this->lblRelatedId) return $this->lblRelatedId_Create();
					return $this->lblRelatedId;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to AuthPrivilege fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'AuthRoleEnumIdControl':
						return ($this->lstAuthRoleEnum = QType::Cast($mixValue, 'QControl'));
					case 'WhoControl':
						return ($this->txtWho = QType::Cast($mixValue, 'QControl'));
					case 'AuthActionIdControl':
						return ($this->lstAuthAction = QType::Cast($mixValue, 'QControl'));
					case 'AuthPrivilegeTypeEnumIdControl':
						return ($this->lstAuthPrivilegeTypeEnum = QType::Cast($mixValue, 'QControl'));
					case 'AuthClassEnumIdControl':
						return ($this->lstAuthClassEnum = QType::Cast($mixValue, 'QControl'));
					case 'RelatedIdControl':
						return ($this->txtRelatedId = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
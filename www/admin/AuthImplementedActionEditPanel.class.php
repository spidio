<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the AuthImplementedAction class.  It uses the code-generated
	 * AuthImplementedActionMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a AuthImplementedAction columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both auth_implemented_action_edit.php AND
	 * auth_implemented_action_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class AuthImplementedActionEditPanel extends QPanel {
		// Local instance of the AuthImplementedActionMetaControl
		protected $mctAuthImplementedAction;

		// Controls for AuthImplementedAction's Data Fields
		public $lstAuthClassEnum;
		public $lstAuthAction;
		public $lstAuthStatusEnum;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $intAuthClassEnumId = null, $intAuthActionId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'AuthImplementedActionEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the AuthImplementedActionMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctAuthImplementedAction = AuthImplementedActionMetaControl::Create($this, $intAuthClassEnumId, $intAuthActionId);

			// Call MetaControl's methods to create qcontrols based on AuthImplementedAction's data fields
			$this->lstAuthClassEnum = $this->mctAuthImplementedAction->lstAuthClassEnum_Create();
			$this->lstAuthAction = $this->mctAuthImplementedAction->lstAuthAction_Create();
			$this->lstAuthStatusEnum = $this->mctAuthImplementedAction->lstAuthStatusEnum_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('AuthImplementedAction') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctAuthImplementedAction->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the AuthImplementedActionMetaControl
			$this->mctAuthImplementedAction->SaveAuthImplementedAction();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the AuthImplementedActionMetaControl
			$this->mctAuthImplementedAction->DeleteAuthImplementedAction();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
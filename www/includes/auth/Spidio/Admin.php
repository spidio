<?php

// Load Framework
require_once('Framework.php');

class SpidioAuthAdmin extends SpidioAuth implements AuthenticationAdmin {
	/**
	 * Constructor.
	 *
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Assign session id.
	 *
	 * @param string $strSessionId
	 */
	public function AssignSession($strSessionId) {
		$this->strSessionId = $strSessionId;
	}
	
	/**
	 * Check if user exsits.
	 *
	 * @param string $strUserName
	 * @return bool
	 */
	public function UserExists($strUserName) {
		return QType::Cast(User::LoadByName(ucfirst(strtolower($strUserName))), QType::Boolean);
	}
}

?>
<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the Template class.  It uses the code-generated
	 * TemplateMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a Template columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both template_edit.php AND
	 * template_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class TemplateEditPanel extends QPanel {
		// Local instance of the TemplateMetaControl
		protected $mctTemplate;

		// Controls for Template's Data Fields
		public $txtId;
		public $txtName;
		public $txtVersion;
		public $txtAuthor;
		public $txtEmail;
		public $txtCopyright;
		public $txtWebsite;
		public $lstStatusObject;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'TemplateEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the TemplateMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctTemplate = TemplateMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on Template's data fields
			$this->txtId = $this->mctTemplate->txtId_Create();
			$this->txtName = $this->mctTemplate->txtName_Create();
			$this->txtVersion = $this->mctTemplate->txtVersion_Create();
			$this->txtAuthor = $this->mctTemplate->txtAuthor_Create();
			$this->txtEmail = $this->mctTemplate->txtEmail_Create();
			$this->txtCopyright = $this->mctTemplate->txtCopyright_Create();
			$this->txtWebsite = $this->mctTemplate->txtWebsite_Create();
			$this->lstStatusObject = $this->mctTemplate->lstStatusObject_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('Template') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctTemplate->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the TemplateMetaControl
			$this->mctTemplate->SaveTemplate();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the TemplateMetaControl
			$this->mctTemplate->DeleteTemplate();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
<?php
	/**
	 * The TapeModeEnum class defined here contains
	 * code for the TapeModeEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_tape_mode_enum" table
	 * in the database.
	 * 
	 * To use, you should use the TapeModeEnum subclass which
	 * extends this TapeModeEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the TapeModeEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class TapeModeEnumGen extends QBaseClass {
		const SP = 1;
		const LP = 2;

		const MaxId = 2;

		public static $NameArray = array(
			1 => 'SP',
			2 => 'LP');

		public static $TokenArray = array(
			1 => 'SP',
			2 => 'LP');

		public static $ReverseNameArray = array(
			'SP' => 1,
			'LP' => 2);

		public static $ReverseTokenArray = array(
			'SP' => 1,
			'LP' => 2);

		public static function ToString($intTapeModeEnumId) {
			switch ($intTapeModeEnumId) {
				case 1: return 'SP';
				case 2: return 'LP';
				default:
					throw new QCallerException(sprintf('Invalid intTapeModeEnumId: %s', $intTapeModeEnumId));
			}
		}

		public static function ToToken($intTapeModeEnumId) {
			switch ($intTapeModeEnumId) {
				case 1: return 'SP';
				case 2: return 'LP';
				default:
					throw new QCallerException(sprintf('Invalid intTapeModeEnumId: %s', $intTapeModeEnumId));
			}
		}

	}
?>
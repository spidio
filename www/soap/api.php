<?php

// Load Spidio SOAP foundation.
require('prepend.inc.php');

class SoapidioApi extends SoapidioService {
	/**
	 * Get array of all availible Soapidio API's.
	 *
	 * @return string[]
	 */
	public function GetApis() {
		$strApiNameArray = array();
		
		$objDirectory = opendir(dirname(__FILE__));
		$strFileNameExcludeArray = array('.', '..', '.htaccess', '.soapversion.php', '.DS_Store', 'index.php', 'index.tpl.php', 'prepend.inc.php', 'api.php');
		
		while ($strFileName = readdir($objDirectory)) {
			if (!in_array($strFileName, $strFileNameExcludeArray)) {
				$strServiceName = str_replace('.php', '', $strFileName);
				$strApiNameArray[] = ucfirst($strServiceName);
			}
		}
		
		return $strApiNameArray;
	}
}

SoapidioApi::Run('SoapidioApi', 'http://spidio.sourceforge.net/namespaces/api');

?>
#!/bin/sh
# Copyright (c) 2008 Spidio Project
# Licensed under original BSD License.

echo -------------------------------------
echo Running BuildTranslateInputFile.php
echo -------------------------------------
/usr/bin/env php -f BuildTranslateInputFile.php xgettext_files
echo

echo -----------------------------------
echo Running xgettext on www directory
echo -----------------------------------
echo Running xgettext
xgettext -L PHP --keyword='Translate' --keyword='_t' --keyword='_tf' -F --no-wrap --omit-header -f xgettext_files -p ./../www/includes/qcodo/i18n
echo Running UpdateMessagesPoHeader.php
/usr/bin/env php -f UpdateMessagesPoHeader.php
echo Renaming messages.po to messages.pot
mv ./../www/includes/qcodo/i18n/messages.po ./../www/includes/qcodo/i18n/messages.pot
echo

echo -------------------------------------
echo Running UpdateLanguageFiles.php
echo -------------------------------------
/usr/bin/env php -f UpdateLanguageFiles.php
echo

echo Done!
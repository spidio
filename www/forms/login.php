<?php

class LoginForm extends SpidioTemplateForm {
	protected $txtUserName;
	protected $clbUserName;
	protected $txtPassword;
	protected $clbPassword;
	protected $txtPasswordSalt;
	protected $txtSalt;
	protected $btnLogin;
	protected $btnLoginRemember;
	protected $lblErrorMessage;
	
	protected $strJavaScriptBase64;
	protected $strJavaScriptMd5;
	protected $strJavaScriptSha1;
	protected $strJavaScriptLogic;
	
	protected function Form_Create() {
		parent::Form_Create();
		$this->PageTitle = QApplication::Translate('Required to login');
		
		$this->txtPassword = new QTextBox($this, 'txtPassword');
		$this->txtPassword->TextMode = QTextMode::Password;
		$this->txtPassword->Required = true;
		$this->txtPassword->AddAction(new QEnterKeyEvent, new QJavaScriptAction('setPasswordSalt();'));
		$this->txtPassword->AddAction(new QEnterKeyEvent, new QAjaxAction('btnLogin_Click'));
		$this->txtPassword->AddAction(new QEnterKeyEvent, new QTerminateAction());
		
		$this->txtUserName = new QTextBox($this);
		$this->txtUserName->Required = true;
		$this->txtUserName->Focus();
		$this->txtUserName->AddAction(new QEnterKeyEvent, new QFocusControlAction($this->txtPassword));
		$this->txtUserName->AddAction(new QEnterKeyEvent, new QTerminateAction());
		
		$this->clbUserName = new QControlLabel($this);
		$this->clbUserName->ForControl = $this->txtUserName;
		$this->clbUserName->Text = QApplication::Translate('Username: ');
		
		$this->clbPassword = new QControlLabel($this);
		$this->clbPassword->ForControl = $this->txtPassword;
		$this->clbPassword->Text = QApplication::Translate('Password: ');
		
		$this->txtSalt = new QTextBox($this, 'txtSalt');
		$this->txtSalt->Display = false;
		
		$this->txtPasswordSalt = new QTextBox($this, 'txtPasswordSalt');
		$this->txtPasswordSalt->Display = false;
		
		$this->btnLogin = new QButton($this);
		$this->btnLogin->Text = QApplication::Translate('Login');
		$this->btnLogin->AddAction(new QClickEvent, new QJavaScriptAction('setPasswordSalt();'));
		$this->btnLogin->AddAction(new QClickEvent, new QAjaxAction('btnLogin_Click'));
		$this->btnLogin->AddAction(new QClickEvent, new QTerminateAction());
		
		$this->btnLoginRemember = new QButton($this);
		$this->btnLoginRemember->Text = QApplication::Translate('Login and Remember');
		$this->btnLoginRemember->AddAction(new QClickEvent, new QJavaScriptAction('setPasswordSalt();'));
		$this->btnLoginRemember->AddAction(new QClickEvent, new QAjaxAction('btnLoginRemember_Click'));
		$this->btnLoginRemember->AddAction(new QClickEvent, new QTerminateAction());
		
		$this->lblErrorMessage = new QLabel($this);
		
		$this->strJavaScriptBase64 = sprintf('%s%s/webtoolkit.base64.js', __VIRTUAL_DIRECTORY__ , __JS_ASSETS__);
		$this->strJavaScriptMd5 = sprintf('%s%s/webtoolkit.md5.js', __VIRTUAL_DIRECTORY__ , __JS_ASSETS__);
		$this->strJavaScriptSha1 = sprintf('%s%s/webtoolkit.sha1.js', __VIRTUAL_DIRECTORY__ , __JS_ASSETS__);
		$this->strJavaScriptLogic = $this->objAuth->GetAuthenticationJavaScript();
	}
	
	protected function Form_Exit() {
		if ($this->Auth->IsLoggedIn()) {
			QApplication::RedirectToReturnPage();
		}
	}
	
	protected function btnLogin_Click() {
		if (!$this->Auth->Login($this->txtUserName->Text, $this->txtPasswordSalt->Text, $this->txtSalt->Text)) {
			$this->lblErrorMessage->Text = QApplication::Translate('Incorrect combination.');
		}
	}
	
	protected function btnLoginRemember_Click() {
		if (!$this->Auth->Login($this->txtUserName->Text, $this->txtPasswordSalt->Text, $this->txtSalt->Text, true)) {
			$this->lblErrorMessage->Text = QApplication::Translate('Incorrect combination.');
		}
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'JavaScriptBase64': return $this->strJavaScriptBase64;
			case 'JavaScriptMd5': return $this->strJavaScriptMd5;
			case 'JavaScriptSha1': return $this->strJavaScriptSha1;
			case 'JavaScriptLogic': return $this->strJavaScriptLogic;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

LoginForm::Run('LoginForm', QApplication::GetTemplatePath(__FILE__));

?>
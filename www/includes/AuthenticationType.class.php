<?php

abstract class AuthenticationType {
	const Plain = 1;
	const Base64 = 2;
	const Md5 = 4;
	const Sha1 = 8;
}

?>
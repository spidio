<?php
	/**
	 * The abstract TapeLocationGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the TapeLocation subclass which
	 * extends this TapeLocationGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the TapeLocation class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $TapeId the value for strTapeId (Not Null)
	 * @property string $LocationId the value for strLocationId (Not Null)
	 * @property string $PersonId the value for strPersonId (Not Null)
	 * @property QDateTime $Date the value for dttDate (Not Null)
	 * @property string $Comment the value for strComment 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Tape $Tape the value for the Tape object referenced by strTapeId (Not Null)
	 * @property Location $Location the value for the Location object referenced by strLocationId (Not Null)
	 * @property Person $Person the value for the Person object referenced by strPersonId (Not Null)
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class TapeLocationGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_tape_location.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_tape_location.tape_id
		 * @var string strTapeId
		 */
		protected $strTapeId;
		const TapeIdMaxLength = 36;
		const TapeIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.location_id
		 * @var string strLocationId
		 */
		protected $strLocationId;
		const LocationIdMaxLength = 36;
		const LocationIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.person_id
		 * @var string strPersonId
		 */
		protected $strPersonId;
		const PersonIdMaxLength = 36;
		const PersonIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.date
		 * @var QDateTime dttDate
		 */
		protected $dttDate;
		const DateDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape_location.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_tape_location.tape_id.
		 *
		 * NOTE: Always use the Tape property getter to correctly retrieve this Tape object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Tape objTape
		 */
		protected $objTape;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_tape_location.location_id.
		 *
		 * NOTE: Always use the Location property getter to correctly retrieve this Location object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Location objLocation
		 */
		protected $objLocation;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_tape_location.person_id.
		 *
		 * NOTE: Always use the Person property getter to correctly retrieve this Person object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Person objPerson
		 */
		protected $objPerson;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a TapeLocation from PK Info
		 * @param string $strId
		 * @return TapeLocation
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return TapeLocation::QuerySingle(
				QQ::Equal(QQN::TapeLocation()->Id, $strId)
			);
		}

		/**
		 * Load all TapeLocations
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call TapeLocation::QueryArray to perform the LoadAll query
			try {
				return TapeLocation::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all TapeLocations
		 * @return int
		 */
		public static function CountAll() {
			// Call TapeLocation::QueryCount to perform the CountAll query
			return TapeLocation::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = TapeLocation::GetDatabase();

			// Create/Build out the QueryBuilder object with TapeLocation-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'tape_location');
			TapeLocation::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'tape_location');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single TapeLocation object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return TapeLocation the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = TapeLocation::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new TapeLocation object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return TapeLocation::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of TapeLocation objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return TapeLocation[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = TapeLocation::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return TapeLocation::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of TapeLocation objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = TapeLocation::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = TapeLocation::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'tape_location_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with TapeLocation-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				TapeLocation::GetSelectFields($objQueryBuilder);
				TapeLocation::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return TapeLocation::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this TapeLocation
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'tape_location';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'tape_id', $strAliasPrefix . 'tape_id');
			$objBuilder->AddSelectItem($strTableName, 'location_id', $strAliasPrefix . 'location_id');
			$objBuilder->AddSelectItem($strTableName, 'person_id', $strAliasPrefix . 'person_id');
			$objBuilder->AddSelectItem($strTableName, 'date', $strAliasPrefix . 'date');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a TapeLocation from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this TapeLocation::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return TapeLocation
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;


			// Create a new instance of the TapeLocation object
			$objToReturn = new TapeLocation();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'tape_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'tape_id'] : $strAliasPrefix . 'tape_id';
			$objToReturn->strTapeId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'location_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'location_id'] : $strAliasPrefix . 'location_id';
			$objToReturn->strLocationId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'person_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'person_id'] : $strAliasPrefix . 'person_id';
			$objToReturn->strPersonId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'date', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'date'] : $strAliasPrefix . 'date';
			$objToReturn->dttDate = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'tape_location__';

			// Check for Tape Early Binding
			$strAlias = $strAliasPrefix . 'tape_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objTape = Tape::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tape_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Location Early Binding
			$strAlias = $strAliasPrefix . 'location_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objLocation = Location::InstantiateDbRow($objDbRow, $strAliasPrefix . 'location_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Person Early Binding
			$strAlias = $strAliasPrefix . 'person_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objPerson = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'person_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			return $objToReturn;
		}

		/**
		 * Instantiate an array of TapeLocations from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return TapeLocation[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = TapeLocation::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = TapeLocation::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single TapeLocation object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return TapeLocation
		*/
		public static function LoadById($strId) {
			return TapeLocation::QuerySingle(
				QQ::Equal(QQN::TapeLocation()->Id, $strId)
			);
		}
			
		/**
		 * Load an array of TapeLocation objects,
		 * by LocationId Index(es)
		 * @param string $strLocationId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/
		public static function LoadArrayByLocationId($strLocationId, $objOptionalClauses = null) {
			// Call TapeLocation::QueryArray to perform the LoadArrayByLocationId query
			try {
				return TapeLocation::QueryArray(
					QQ::Equal(QQN::TapeLocation()->LocationId, $strLocationId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count TapeLocations
		 * by LocationId Index(es)
		 * @param string $strLocationId
		 * @return int
		*/
		public static function CountByLocationId($strLocationId) {
			// Call TapeLocation::QueryCount to perform the CountByLocationId query
			return TapeLocation::QueryCount(
				QQ::Equal(QQN::TapeLocation()->LocationId, $strLocationId)
			);
		}
			
		/**
		 * Load an array of TapeLocation objects,
		 * by PersonId Index(es)
		 * @param string $strPersonId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/
		public static function LoadArrayByPersonId($strPersonId, $objOptionalClauses = null) {
			// Call TapeLocation::QueryArray to perform the LoadArrayByPersonId query
			try {
				return TapeLocation::QueryArray(
					QQ::Equal(QQN::TapeLocation()->PersonId, $strPersonId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count TapeLocations
		 * by PersonId Index(es)
		 * @param string $strPersonId
		 * @return int
		*/
		public static function CountByPersonId($strPersonId) {
			// Call TapeLocation::QueryCount to perform the CountByPersonId query
			return TapeLocation::QueryCount(
				QQ::Equal(QQN::TapeLocation()->PersonId, $strPersonId)
			);
		}
			
		/**
		 * Load an array of TapeLocation objects,
		 * by TapeId Index(es)
		 * @param string $strTapeId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/
		public static function LoadArrayByTapeId($strTapeId, $objOptionalClauses = null) {
			// Call TapeLocation::QueryArray to perform the LoadArrayByTapeId query
			try {
				return TapeLocation::QueryArray(
					QQ::Equal(QQN::TapeLocation()->TapeId, $strTapeId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count TapeLocations
		 * by TapeId Index(es)
		 * @param string $strTapeId
		 * @return int
		*/
		public static function CountByTapeId($strTapeId) {
			// Call TapeLocation::QueryCount to perform the CountByTapeId query
			return TapeLocation::QueryCount(
				QQ::Equal(QQN::TapeLocation()->TapeId, $strTapeId)
			);
		}
			
		/**
		 * Load an array of TapeLocation objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call TapeLocation::QueryArray to perform the LoadArrayByOwner query
			try {
				return TapeLocation::QueryArray(
					QQ::Equal(QQN::TapeLocation()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count TapeLocations
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call TapeLocation::QueryCount to perform the CountByOwner query
			return TapeLocation::QueryCount(
				QQ::Equal(QQN::TapeLocation()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of TapeLocation objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call TapeLocation::QueryArray to perform the LoadArrayByStatus query
			try {
				return TapeLocation::QueryArray(
					QQ::Equal(QQN::TapeLocation()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count TapeLocations
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call TapeLocation::QueryCount to perform the CountByStatus query
			return TapeLocation::QueryCount(
				QQ::Equal(QQN::TapeLocation()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this TapeLocation
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = TapeLocation::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'tape_location` (
							`id`,
							`tape_id`,
							`location_id`,
							`person_id`,
							`date`,
							`comment`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strTapeId) . ',
							' . $objDatabase->SqlVariable($this->strLocationId) . ',
							' . $objDatabase->SqlVariable($this->strPersonId) . ',
							' . $objDatabase->SqlVariable($this->dttDate) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'tape_location`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('TapeLocation');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'tape_location`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`tape_id` = ' . $objDatabase->SqlVariable($this->strTapeId) . ',
							`location_id` = ' . $objDatabase->SqlVariable($this->strLocationId) . ',
							`person_id` = ' . $objDatabase->SqlVariable($this->strPersonId) . ',
							`date` = ' . $objDatabase->SqlVariable($this->dttDate) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'tape_location`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this TapeLocation
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this TapeLocation with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = TapeLocation::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'tape_location`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all TapeLocations
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = TapeLocation::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'tape_location`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'tape_location table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = TapeLocation::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'tape_location`');
		}

		/**
		 * Reload this TapeLocation from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved TapeLocation object.');

			// Reload the Object
			$objReloaded = TapeLocation::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->TapeId = $objReloaded->TapeId;
			$this->LocationId = $objReloaded->LocationId;
			$this->PersonId = $objReloaded->PersonId;
			$this->dttDate = $objReloaded->dttDate;
			$this->strComment = $objReloaded->strComment;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'TapeId':
					/**
					 * Gets the value for strTapeId (Not Null)
					 * @return string
					 */
					return $this->strTapeId;

				case 'LocationId':
					/**
					 * Gets the value for strLocationId (Not Null)
					 * @return string
					 */
					return $this->strLocationId;

				case 'PersonId':
					/**
					 * Gets the value for strPersonId (Not Null)
					 * @return string
					 */
					return $this->strPersonId;

				case 'Date':
					/**
					 * Gets the value for dttDate (Not Null)
					 * @return QDateTime
					 */
					return $this->dttDate;

				case 'Comment':
					/**
					 * Gets the value for strComment 
					 * @return string
					 */
					return $this->strComment;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Tape':
					/**
					 * Gets the value for the Tape object referenced by strTapeId (Not Null)
					 * @return Tape
					 */
					try {
						if ((!$this->objTape) && (!is_null($this->strTapeId)))
							$this->objTape = Tape::Load($this->strTapeId);
						return $this->objTape;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Location':
					/**
					 * Gets the value for the Location object referenced by strLocationId (Not Null)
					 * @return Location
					 */
					try {
						if ((!$this->objLocation) && (!is_null($this->strLocationId)))
							$this->objLocation = Location::Load($this->strLocationId);
						return $this->objLocation;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Person':
					/**
					 * Gets the value for the Person object referenced by strPersonId (Not Null)
					 * @return Person
					 */
					try {
						if ((!$this->objPerson) && (!is_null($this->strPersonId)))
							$this->objPerson = Person::Load($this->strPersonId);
						return $this->objPerson;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'TapeId':
					/**
					 * Sets the value for strTapeId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objTape = null;
						return ($this->strTapeId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'LocationId':
					/**
					 * Sets the value for strLocationId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objLocation = null;
						return ($this->strLocationId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'PersonId':
					/**
					 * Sets the value for strPersonId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objPerson = null;
						return ($this->strPersonId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Date':
					/**
					 * Sets the value for dttDate (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttDate = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Tape':
					/**
					 * Sets the value for the Tape object referenced by strTapeId (Not Null)
					 * @param Tape $mixValue
					 * @return Tape
					 */
					if (is_null($mixValue)) {
						$this->strTapeId = null;
						$this->objTape = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Tape object
						try {
							$mixValue = QType::Cast($mixValue, 'Tape');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Tape object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Tape for this TapeLocation');

						// Update Local Member Variables
						$this->objTape = $mixValue;
						$this->strTapeId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Location':
					/**
					 * Sets the value for the Location object referenced by strLocationId (Not Null)
					 * @param Location $mixValue
					 * @return Location
					 */
					if (is_null($mixValue)) {
						$this->strLocationId = null;
						$this->objLocation = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Location object
						try {
							$mixValue = QType::Cast($mixValue, 'Location');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Location object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Location for this TapeLocation');

						// Update Local Member Variables
						$this->objLocation = $mixValue;
						$this->strLocationId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Person':
					/**
					 * Sets the value for the Person object referenced by strPersonId (Not Null)
					 * @param Person $mixValue
					 * @return Person
					 */
					if (is_null($mixValue)) {
						$this->strPersonId = null;
						$this->objPerson = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Person object
						try {
							$mixValue = QType::Cast($mixValue, 'Person');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Person object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Person for this TapeLocation');

						// Update Local Member Variables
						$this->objPerson = $mixValue;
						$this->strPersonId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="TapeLocation"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Tape" type="xsd1:Tape"/>';
			$strToReturn .= '<element name="Location" type="xsd1:Location"/>';
			$strToReturn .= '<element name="Person" type="xsd1:Person"/>';
			$strToReturn .= '<element name="Date" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('TapeLocation', $strComplexTypeArray)) {
				$strComplexTypeArray['TapeLocation'] = TapeLocation::GetSoapComplexTypeXml();
				Tape::AlterSoapComplexTypeArray($strComplexTypeArray);
				Location::AlterSoapComplexTypeArray($strComplexTypeArray);
				Person::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, TapeLocation::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new TapeLocation();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'Tape')) &&
				($objSoapObject->Tape))
				$objToReturn->Tape = Tape::GetObjectFromSoapObject($objSoapObject->Tape);
			if ((property_exists($objSoapObject, 'Location')) &&
				($objSoapObject->Location))
				$objToReturn->Location = Location::GetObjectFromSoapObject($objSoapObject->Location);
			if ((property_exists($objSoapObject, 'Person')) &&
				($objSoapObject->Person))
				$objToReturn->Person = Person::GetObjectFromSoapObject($objSoapObject->Person);
			if (property_exists($objSoapObject, 'Date'))
				$objToReturn->dttDate = new QDateTime($objSoapObject->Date);
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, TapeLocation::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objTape)
				$objObject->objTape = Tape::GetSoapObjectFromObject($objObject->objTape, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strTapeId = null;
			if ($objObject->objLocation)
				$objObject->objLocation = Location::GetSoapObjectFromObject($objObject->objLocation, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strLocationId = null;
			if ($objObject->objPerson)
				$objObject->objPerson = Person::GetSoapObjectFromObject($objObject->objPerson, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strPersonId = null;
			if ($objObject->dttDate)
				$objObject->dttDate = $objObject->dttDate->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeTapeLocation extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'TapeLocation';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'tape_location';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'TapeId':
					return new QQNode('tape_id', 'TapeId', 'string', $this);
				case 'Tape':
					return new QQNodeTape('tape_id', 'Tape', 'string', $this);
				case 'LocationId':
					return new QQNode('location_id', 'LocationId', 'string', $this);
				case 'Location':
					return new QQNodeLocation('location_id', 'Location', 'string', $this);
				case 'PersonId':
					return new QQNode('person_id', 'PersonId', 'string', $this);
				case 'Person':
					return new QQNodePerson('person_id', 'Person', 'string', $this);
				case 'Date':
					return new QQNode('date', 'Date', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeTapeLocation extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'TapeLocation';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'tape_location';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'TapeId':
					return new QQNode('tape_id', 'TapeId', 'string', $this);
				case 'Tape':
					return new QQNodeTape('tape_id', 'Tape', 'string', $this);
				case 'LocationId':
					return new QQNode('location_id', 'LocationId', 'string', $this);
				case 'Location':
					return new QQNodeLocation('location_id', 'Location', 'string', $this);
				case 'PersonId':
					return new QQNode('person_id', 'PersonId', 'string', $this);
				case 'Person':
					return new QQNodePerson('person_id', 'Person', 'string', $this);
				case 'Date':
					return new QQNode('date', 'Date', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
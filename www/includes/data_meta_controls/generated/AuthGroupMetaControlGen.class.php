<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the AuthGroup class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single AuthGroup object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a AuthGroupMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read AuthGroup $AuthGroup the actual AuthGroup data class being edited
	 * property QIntegerTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QTextBox $ShortNameControl
	 * property-read QLabel $ShortNameLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class AuthGroupMetaControlGen extends QBaseClass {
		// General Variables
		protected $objAuthGroup;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of AuthGroup's individual data fields
		protected $txtId;
		protected $txtShortName;
		protected $txtName;

		// Controls that allow the viewing of AuthGroup's individual data fields
		protected $lblId;
		protected $lblShortName;
		protected $lblName;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * AuthGroupMetaControl to edit a single AuthGroup object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single AuthGroup object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthGroupMetaControl
		 * @param AuthGroup $objAuthGroup new or existing AuthGroup object
		 */
		 public function __construct($objParentObject, AuthGroup $objAuthGroup) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this AuthGroupMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked AuthGroup object
			$this->objAuthGroup = $objAuthGroup;

			// Figure out if we're Editing or Creating New
			if ($this->objAuthGroup->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthGroupMetaControl
		 * @param integer $intId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing AuthGroup object creation - defaults to CreateOrEdit
 		 * @return AuthGroupMetaControl
		 */
		public static function Create($objParentObject, $intId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($intId)) {
				$objAuthGroup = AuthGroup::Load($intId);

				// AuthGroup was found -- return it!
				if ($objAuthGroup)
					return new AuthGroupMetaControl($objParentObject, $objAuthGroup);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a AuthGroup object with PK arguments: ' . $intId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new AuthGroupMetaControl($objParentObject, new AuthGroup());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthGroupMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing AuthGroup object creation - defaults to CreateOrEdit
		 * @return AuthGroupMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intId = QApplication::PathInfo(0);
			return AuthGroupMetaControl::Create($objParentObject, $intId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthGroupMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing AuthGroup object creation - defaults to CreateOrEdit
		 * @return AuthGroupMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intId = QApplication::QueryString('intId');
			return AuthGroupMetaControl::Create($objParentObject, $intId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QIntegerTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objAuthGroup->Id;
			$this->txtId->Required = true;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null, $strFormat = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objAuthGroup->Id;
			$this->lblId->Required = true;
			$this->lblId->Format = $strFormat;
			return $this->lblId;
		}

		/**
		 * Create and setup QTextBox txtShortName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtShortName_Create($strControlId = null) {
			$this->txtShortName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtShortName->Name = QApplication::Translate('Short Name');
			$this->txtShortName->Text = $this->objAuthGroup->ShortName;
			$this->txtShortName->Required = true;
			$this->txtShortName->MaxLength = AuthGroup::ShortNameMaxLength;
			return $this->txtShortName;
		}

		/**
		 * Create and setup QLabel lblShortName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblShortName_Create($strControlId = null) {
			$this->lblShortName = new QLabel($this->objParentObject, $strControlId);
			$this->lblShortName->Name = QApplication::Translate('Short Name');
			$this->lblShortName->Text = $this->objAuthGroup->ShortName;
			$this->lblShortName->Required = true;
			return $this->lblShortName;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objAuthGroup->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = AuthGroup::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objAuthGroup->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}



		/**
		 * Refresh this MetaControl with Data from the local AuthGroup object.
		 * @param boolean $blnReload reload AuthGroup from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objAuthGroup->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objAuthGroup->Id;
			if ($this->lblId) $this->lblId->Text = $this->objAuthGroup->Id;

			if ($this->txtShortName) $this->txtShortName->Text = $this->objAuthGroup->ShortName;
			if ($this->lblShortName) $this->lblShortName->Text = $this->objAuthGroup->ShortName;

			if ($this->txtName) $this->txtName->Text = $this->objAuthGroup->Name;
			if ($this->lblName) $this->lblName->Text = $this->objAuthGroup->Name;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC AUTHGROUP OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's AuthGroup instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveAuthGroup() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objAuthGroup->Id = $this->txtId->Text;
				if ($this->txtShortName) $this->objAuthGroup->ShortName = $this->txtShortName->Text;
				if ($this->txtName) $this->objAuthGroup->Name = $this->txtName->Text;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the AuthGroup object
				$this->objAuthGroup->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's AuthGroup instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteAuthGroup() {
			$this->objAuthGroup->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'AuthGroup': return $this->objAuthGroup;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to AuthGroup fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'ShortNameControl':
					if (!$this->txtShortName) return $this->txtShortName_Create();
					return $this->txtShortName;
				case 'ShortNameLabel':
					if (!$this->lblShortName) return $this->lblShortName_Create();
					return $this->lblShortName;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to AuthGroup fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'ShortNameControl':
						return ($this->txtShortName = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
<?php
	// This is the "Pro" version of configuration.inc.php, without any comments, and restructured in a way
	// that should make sense for most pro-users of Qcodo
	
	// As always, feel free to use, change or ignore.

	define('SERVER_INSTANCE', 'test');

	switch (SERVER_INSTANCE) {
		case 'dev':
        	define('ALLOW_REMOTE_ADMIN', true);
			
			define ('__DOCROOT__', '/Users/dwlnetnl/Projecten/Spidio/www');
			define ('__VIRTUAL_DIRECTORY__', '/spidio');
			define ('__SUBDIRECTORY__', '');

			define('DB_CONNECTION_1', serialize(array(
				'adapter' => 'MySqli5',
				'server' => 'localhost',
				'port' => null,
				'database' => 'spidio_dev',
				'username' => 'spidio',
				'password' => '',
				'profiling' => false)));
			
			define('DB_TABLE_PREFIX_1', 'spidio_');
			break;
		case 'test':
        	define('ALLOW_REMOTE_ADMIN', true);
			
			define ('__DOCROOT__', '/file/path/to/www');
			define ('__VIRTUAL_DIRECTORY__', '');
			define ('__SUBDIRECTORY__', '/test/spidio');

			define('DB_CONNECTION_1', serialize(array(
				'adapter' => 'MySqli5',
				'server' => 'localhost',
				'port' => null,
				'database' => 'spidio_test',
				'username' => 'spidio',
				'password' => '',
				'profiling' => false)));
			
			define('DB_TABLE_PREFIX_1', 'spidio_');
			break;
		case 'stage':
		case 'prod':
/*        	define('ALLOW_REMOTE_ADMIN', false);
			
			define ('__DOCROOT__', '/home/qcodo/wwwroot');
			define ('__VIRTUAL_DIRECTORY__', '');
			define ('__SUBDIRECTORY__', '');

			define('DB_CONNECTION_1', serialize(array(
				'adapter' => 'MySqli5',
				'server' => 'localhost',
				'port' => null,
				'database' => 'spidio',
				'username' => 'spidio',
				'password' => 'spidio',
				'profiling' => false)));
			
			define('DB_TABLE_PREFIX_1', 'spidio_');
			break;
*/	}

	define ('__URL_REWRITE__', 'apache');

	define ('__DEVTOOLS_CLI__', __DOCROOT__ . __SUBDIRECTORY__ . '/../_devtools_cli');
	define ('__INCLUDES__', __DOCROOT__ .  __SUBDIRECTORY__ . '/includes');
	define ('__QCODO__', __INCLUDES__ . '/qcodo');
	define ('__QCODO_CORE__', __INCLUDES__ . '/qcodo/_core');
	define ('__DATA_CLASSES__', __INCLUDES__ . '/data_classes');
	define ('__DATAGEN_CLASSES__', __INCLUDES__ . '/data_classes/generated');
	define ('__DATA_META_CONTROLS__', __INCLUDES__ . '/data_meta_controls');
	define ('__DATAGEN_META_CONTROLS__', __INCLUDES__ . '/data_meta_controls/generated');

	define ('__DEVTOOLS__', __SUBDIRECTORY__ . '/_devtools');
	define ('__FORM_DRAFTS__', null);
	define ('__PANEL_DRAFTS__', __SUBDIRECTORY__ . '/admin');

	// We don't want "Examples", and we don't want to download them during qcodo_update
	define ('__EXAMPLES__', null);

	define ('__JS_ASSETS__', __SUBDIRECTORY__ . '/assets/js');
	define ('__CSS_ASSETS__', __SUBDIRECTORY__ . '/assets/css');
	define ('__IMAGE_ASSETS__', __SUBDIRECTORY__ . '/assets/images');
	define ('__PHP_ASSETS__', __SUBDIRECTORY__ . '/assets/php');
	
	define ('__TEMPLATES__',  __DOCROOT__ .  __SUBDIRECTORY__ . '/templates');
	define ('__FORMS__',  __DOCROOT__ .  __SUBDIRECTORY__ . '/forms');
	
	if ((function_exists('date_default_timezone_set')) && (!ini_get('date.timezone')))
		date_default_timezone_set('Europe/Amsterdam');

	define('ERROR_PAGE_PATH', __PHP_ASSETS__ . '/_core/error_page.php');
//	define('ERROR_LOG_PATH', __INCLUDES__ . '/error_log');

//	define('ERROR_FRIENDLY_PAGE_PATH', __PHP_ASSETS__ . '/friendly_error_page.php');
//	define('ERROR_FRIENDLY_AJAX_MESSAGE', 'Oops!  An error has occurred.\r\n\r\nThe error was logged, and we will take a look into this right away.');
	
	define('__SOAPSYS__', __DOCROOT__ . __SUBDIRECTORY__ . '/soap');
	
	define('MAGIC_COLUMN_NAME_CREATION_DATE', 'added');
	define('MAGIC_COLUMN_NAME_MODIFICATION_DATE', 'changed');
	
?>
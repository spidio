<?php

// Load Framework
require_once('Framework.php');

class SpidioAuthSoap extends SpidioAuth implements AuthenticationSoap {
	protected $strClientIpAddress;
	
	/**
	 * Log in user based on given credentals.
	 *
	 * @param string $strUserName
	 * @param string $strPasswordSalt
	 * @param string $strSalt
	 * @param bool $blnKeepLogin
	 * @return bool
	 */
	public function Login($strUserName, $strPasswordSalt, $strSalt) {
		if ($objUser = User::LoadByName(ucfirst(strtolower($strUserName)))) {
			if ($this->CheckPassword($objUser->Password, $strPasswordSalt, $strSalt)) {
				$this->SessionRegisterUser($objUser);
				$this->AddLogEntry(LogItemEnum::USERLOGIN);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Logout user and redirect to login page.
	 *
	 * @param bool $blnForce
	 * @return bool
	 */
	public function Logout() {
		if ($this->IsLoggedIn()) {
			$this->AddLogEntry(LogItemEnum::USERLOGOUT);
			$this->SessionUnRegisterUser();
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Load session id from cookie if it exists before for session updated.
	 *
	 */
	public function SessionLoad() {
		if ($this->ExistCookie('sid')) {
			// Load Session from database.
			self::$objSession = Session::Load($this->GetCookie('sid'));
		}
		
		if (!is_null(self::$objSession)) {
			// Check if Session is expired.
			$dttExpire = new QDateTime(QDateTime::Now);
			$dttExpire->AddSeconds(-$this->SessionGetTimeout());
			
			if ($dttExpire->IsLaterThan(self::$objSession->Changed)) {
				// Unregister User from session
				$this->SessionUnRegisterUser();
				
				// Add Log entry.
				$this->AddLogEntry(LogItemEnum::SESSIONTIMEOUT);
				if ($this->IsLoggedIn()) {
					$this->AddLogEntry(LogItemEnum::USERLOGOUTFORCE);
				}
			}
		}
	}
	
	/**
	 * Alter Cookie for Soapidio.
	 *
	 * @param Cookie $objCookie
	 */
	protected function SetCookieHook(Cookie $objCookie) {
		$objCookie->Path = sprintf('%s%s/', __VIRTUAL_DIRECTORY__, __SOAPSYS__);
	}
	
	/**
	 * Update Session IpAddress with the client IpAddress 
	 *
	 */
	protected function SessionUpdateHookBegin() {
		if (self::$objSession->IpAddress != (string) $this->UserIpAddress()) {
			$this->strClientIpAddress = self::$objSession->IpAddress;
		}
	}
	
	/**
	 * Update Session IpAddress with the client IpAddress 
	 *
	 */
	protected function SessionUpdateHookEnd() {
		if ($this->strClientIpAddress) {
			self::$objSession->IpAddress = $this->strClientIpAddress;
			self::$objSession->Save(false, true);
		}
	}
}

?>
<?php
	require(__DATAGEN_CLASSES__ . '/TapeModeEnumGen.class.php');

	/**
	 * The TapeModeEnum class defined here contains any
	 * customized code for the TapeModeEnum enumerated type. 
	 * 
	 * It represents the enumerated values found in the "spidio_tape_mode_enum" table in the database,
	 * and extends from the code generated abstract TapeModeEnumGen
	 * class, which contains all the values extracted from the database.
	 * 
	 * Type classes which are generally used to attach a type to data object.
	 * However, they may be used as simple database indepedant enumerated type.
	 * 
	 * @package Spidio
	 * @subpackage DataObjects
	 */
	abstract class TapeModeEnum extends TapeModeEnumGen {
		public static function ToString($intTapeModeEnumId) {
			switch ($intTapeModeEnumId) {
				case 1: return QApplication::Translate('SP');
				case 2: return QApplication::Translate('LP');
				default:
					return parent::ToString($intTapeModeEnumId);
			}
		}
	}
?>
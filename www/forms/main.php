<?php

class IndexForm extends SpidioTemplateForm {
	protected $lstNews;
	protected $lblNewsAuthor;
	protected $lblNewsText;
	
	protected $dttNewsAdded;
	protected $lblNewsAdded;
	protected $dttNewsChanged;
	protected $lblNewsChanged;
	
	protected $dtgRecentLog;
	
	protected $objDefaultWaitIcon;
	
	protected $strTextWelcome;
	protected $strTextNews;
	
	protected function Form_Create() {
		parent::Form_Create();
		$this->PageTitle = QApplication::Translate('Welcome page');
		
		$this->lstNews = new QListBox($this);
		$this->lstNews->AddAction(new QChangeEvent(), new QAjaxAction('lstNews_Change'));
		
		$objNewsArray = News::QueryArray(
			QQ::Equal(QQN::News()->Status, AuthStatusEnum::ACTIVE),
			QQ::Clause(
				QQ::OrderBy(QQN::News()->Added, 'DESC')
			)
		);
		if ($objNewsArray) foreach ($objNewsArray as $objNews) {
			$this->lstNews->AddItem($objNews->Title, $objNews->Id);
		} else {
			$this->lstNews->AddItem(QApplication::Translate('No news available'), null);
		}
		
		$this->lblNewsAuthor = new QLabel($this);
		$this->lblNewsAuthor->HtmlEntities = false;
		$this->lblNewsAuthor->Text = QApplication::MakeAcronymOfUser($objNewsArray[0]->User);
		
		$this->lblNewsText = new QLabel($this);
		$this->lblNewsText->HtmlEntities = false;
		$this->lblNewsText->Text = QApplication::ParseBBCode($objNewsArray[0]->Text);
		
		$this->dttNewsAdded = new QDateTime($objNewsArray[0]->Added);
		$this->lblNewsAdded = new QLabel($this);
		$this->lblNewsAdded->Text = $this->dttNewsAdded->format(QApplication::GetConfig('display_format_datetime'));
		
		$this->dttNewsChanged = new QDateTime($objNewsArray[0]->Changed);
		$this->lblNewsChanged = new QLabel($this);
		$this->lblNewsChanged->Text = $this->dttNewsChanged->format(QApplication::GetConfig('display_format_datetime'));
		
		$this->dtgRecentLog = new QDataGrid($this);
		$this->dtgRecentLog->SetDataBinder('dtgRecentLog_Bind');
		$this->dtgRecentLog->AddColumn(new QDataGridColumn('Type', '<?= LogItemEnum::ToTranslatedString($_ITEM->LogItemEnumId) ?>'));
		$this->dtgRecentLog->AddColumn(new QDataGridColumn('Action', '<?= LogActionEnum::ToTranslatedString($_ITEM->LogActionEnumId) ?>'));
		$this->dtgRecentLog->AddColumn(new QDataGridColumn('Item', '<?= QApplication::GetStringOfLogItemIdBasedOnLogItemEnumId($_ITEM->ItemId, $_ITEM->LogItemEnumId) ?>'));
		$this->dtgRecentLog->AddColumn(new QDataGridColumn('User', '<?= QApplication::MakeAcronymOfUser($_ITEM->User) ?>', 'HtmlEntities=false'));
		$this->dtgRecentLog->AddColumn(new QDataGridColumn('Date', '<?= QApplication::MakeDateTimeString($_ITEM->Date) ?>'));
		
		$this->objDefaultWaitIcon = new QWaitIcon($this);
		
		$this->strTextWelcome = sprintf(QApplication::Translate('Welcome %s'), $this->Auth->Session->User->Name);
		$this->strTextNews = sprintf(QApplication::Translate('Welcome to %s, the video library system of %s. Below is the current news displayed, you can use the listbox to access older news.'), $this->SiteTitle, $this->SiteCopyrightAuthor);
	}
	
	public function dtgRecentLog_Bind() {
		$this->dtgRecentLog->DataSource = Log::LoadArrayOfRecentItems(QApplication::GetConfig('site_recentitems_main', QType::Integer));
	}
	
	public function lstNews_Change($strFormId, $strControlId, $strParameter) {
		$objNews = News::LoadById($this->lstNews->SelectedValue);
		
		$this->lblNewsText->Text = QApplication::ParseBBCode($objNews->Text);
		$this->lblNewsAuthor->Text = QApplication::MakeAcronymOfUser($objNews->User);
		
		$this->dttNewsAdded = new QDateTime($objNews->Added);
		$this->lblNewsAdded->Text = $this->dttNewsAdded->format(QApplication::GetConfig('display_format_datetime'));
		
		$this->dttNewsChanged = new QDateTime($objNews->Changed);
		$this->lblNewsChanged->Text = $this->dttNewsChanged->format(QApplication::GetConfig('display_format_datetime'));
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'RecentLog': return $this->arrRecentLog;
			case 'TextWelcome': return $this->strTextWelcome;
			case 'TextNews': return $this->strTextNews;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

IndexForm::Run('IndexForm', QApplication::GetTemplatePath(__FILE__));

?>
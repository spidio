<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Organisation class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Organisation object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a OrganisationMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Organisation $Organisation the actual Organisation data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $AddressIdControl
	 * property-read QLabel $AddressIdLabel
	 * property QListBox $PersonIdControl
	 * property-read QLabel $PersonIdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QIntegerTextBox $PhoneControl
	 * property-read QLabel $PhoneLabel
	 * property QIntegerTextBox $PhoneAControl
	 * property-read QLabel $PhoneALabel
	 * property QIntegerTextBox $PhoneCControl
	 * property-read QLabel $PhoneCLabel
	 * property QTextBox $EmailControl
	 * property-read QLabel $EmailLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class OrganisationMetaControlGen extends QBaseClass {
		// General Variables
		protected $objOrganisation;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Organisation's individual data fields
		protected $txtId;
		protected $lstAddress;
		protected $lstPerson;
		protected $txtName;
		protected $txtPhone;
		protected $txtPhoneA;
		protected $txtPhoneC;
		protected $txtEmail;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Organisation's individual data fields
		protected $lblId;
		protected $lblAddressId;
		protected $lblPersonId;
		protected $lblName;
		protected $lblPhone;
		protected $lblPhoneA;
		protected $lblPhoneC;
		protected $lblEmail;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * OrganisationMetaControl to edit a single Organisation object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Organisation object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this OrganisationMetaControl
		 * @param Organisation $objOrganisation new or existing Organisation object
		 */
		 public function __construct($objParentObject, Organisation $objOrganisation) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this OrganisationMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Organisation object
			$this->objOrganisation = $objOrganisation;

			// Figure out if we're Editing or Creating New
			if ($this->objOrganisation->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this OrganisationMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Organisation object creation - defaults to CreateOrEdit
 		 * @return OrganisationMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objOrganisation = Organisation::Load($strId);

				// Organisation was found -- return it!
				if ($objOrganisation)
					return new OrganisationMetaControl($objParentObject, $objOrganisation);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Organisation object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new OrganisationMetaControl($objParentObject, new Organisation());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this OrganisationMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Organisation object creation - defaults to CreateOrEdit
		 * @return OrganisationMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return OrganisationMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this OrganisationMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Organisation object creation - defaults to CreateOrEdit
		 * @return OrganisationMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return OrganisationMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objOrganisation->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Organisation::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objOrganisation->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAddress_Create($strControlId = null) {
			$this->lstAddress = new QListBox($this->objParentObject, $strControlId);
			$this->lstAddress->Name = QApplication::Translate('Address');
			$this->lstAddress->Required = true;
			if (!$this->blnEditMode)
				$this->lstAddress->AddItem(QApplication::Translate('- Select One -'), null);
			$objAddressArray = Address::LoadAll();
			if ($objAddressArray) foreach ($objAddressArray as $objAddress) {
				$objListItem = new QListItem($objAddress->__toString(), $objAddress->Id);
				if (($this->objOrganisation->Address) && ($this->objOrganisation->Address->Id == $objAddress->Id))
					$objListItem->Selected = true;
				$this->lstAddress->AddItem($objListItem);
			}
			return $this->lstAddress;
		}

		/**
		 * Create and setup QLabel lblAddressId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAddressId_Create($strControlId = null) {
			$this->lblAddressId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAddressId->Name = QApplication::Translate('Address');
			$this->lblAddressId->Text = ($this->objOrganisation->Address) ? $this->objOrganisation->Address->__toString() : null;
			$this->lblAddressId->Required = true;
			return $this->lblAddressId;
		}

		/**
		 * Create and setup QListBox lstPerson
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstPerson_Create($strControlId = null) {
			$this->lstPerson = new QListBox($this->objParentObject, $strControlId);
			$this->lstPerson->Name = QApplication::Translate('Person');
			$this->lstPerson->Required = true;
			if (!$this->blnEditMode)
				$this->lstPerson->AddItem(QApplication::Translate('- Select One -'), null);
			$objPersonArray = Person::LoadAll();
			if ($objPersonArray) foreach ($objPersonArray as $objPerson) {
				$objListItem = new QListItem($objPerson->__toString(), $objPerson->Id);
				if (($this->objOrganisation->Person) && ($this->objOrganisation->Person->Id == $objPerson->Id))
					$objListItem->Selected = true;
				$this->lstPerson->AddItem($objListItem);
			}
			return $this->lstPerson;
		}

		/**
		 * Create and setup QLabel lblPersonId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblPersonId_Create($strControlId = null) {
			$this->lblPersonId = new QLabel($this->objParentObject, $strControlId);
			$this->lblPersonId->Name = QApplication::Translate('Person');
			$this->lblPersonId->Text = ($this->objOrganisation->Person) ? $this->objOrganisation->Person->__toString() : null;
			$this->lblPersonId->Required = true;
			return $this->lblPersonId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objOrganisation->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = Organisation::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objOrganisation->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QIntegerTextBox txtPhone
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPhone_Create($strControlId = null) {
			$this->txtPhone = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPhone->Name = QApplication::Translate('Phone');
			$this->txtPhone->Text = $this->objOrganisation->Phone;
			return $this->txtPhone;
		}

		/**
		 * Create and setup QLabel lblPhone
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPhone_Create($strControlId = null, $strFormat = null) {
			$this->lblPhone = new QLabel($this->objParentObject, $strControlId);
			$this->lblPhone->Name = QApplication::Translate('Phone');
			$this->lblPhone->Text = $this->objOrganisation->Phone;
			$this->lblPhone->Format = $strFormat;
			return $this->lblPhone;
		}

		/**
		 * Create and setup QIntegerTextBox txtPhoneA
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPhoneA_Create($strControlId = null) {
			$this->txtPhoneA = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPhoneA->Name = QApplication::Translate('Phone A');
			$this->txtPhoneA->Text = $this->objOrganisation->PhoneA;
			return $this->txtPhoneA;
		}

		/**
		 * Create and setup QLabel lblPhoneA
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPhoneA_Create($strControlId = null, $strFormat = null) {
			$this->lblPhoneA = new QLabel($this->objParentObject, $strControlId);
			$this->lblPhoneA->Name = QApplication::Translate('Phone A');
			$this->lblPhoneA->Text = $this->objOrganisation->PhoneA;
			$this->lblPhoneA->Format = $strFormat;
			return $this->lblPhoneA;
		}

		/**
		 * Create and setup QIntegerTextBox txtPhoneC
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPhoneC_Create($strControlId = null) {
			$this->txtPhoneC = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPhoneC->Name = QApplication::Translate('Phone C');
			$this->txtPhoneC->Text = $this->objOrganisation->PhoneC;
			return $this->txtPhoneC;
		}

		/**
		 * Create and setup QLabel lblPhoneC
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPhoneC_Create($strControlId = null, $strFormat = null) {
			$this->lblPhoneC = new QLabel($this->objParentObject, $strControlId);
			$this->lblPhoneC->Name = QApplication::Translate('Phone C');
			$this->lblPhoneC->Text = $this->objOrganisation->PhoneC;
			$this->lblPhoneC->Format = $strFormat;
			return $this->lblPhoneC;
		}

		/**
		 * Create and setup QTextBox txtEmail
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtEmail_Create($strControlId = null) {
			$this->txtEmail = new QTextBox($this->objParentObject, $strControlId);
			$this->txtEmail->Name = QApplication::Translate('Email');
			$this->txtEmail->Text = $this->objOrganisation->Email;
			$this->txtEmail->MaxLength = Organisation::EmailMaxLength;
			return $this->txtEmail;
		}

		/**
		 * Create and setup QLabel lblEmail
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblEmail_Create($strControlId = null) {
			$this->lblEmail = new QLabel($this->objParentObject, $strControlId);
			$this->lblEmail->Name = QApplication::Translate('Email');
			$this->lblEmail->Text = $this->objOrganisation->Email;
			return $this->lblEmail;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objOrganisation->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objOrganisation->Owner;
			$this->txtOwner->MaxLength = Organisation::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objOrganisation->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objOrganisation->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objOrganisation->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objOrganisation->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objOrganisation->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objOrganisation->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objOrganisation->Status) ? AuthStatusEnum::$NameArray[$this->objOrganisation->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Organisation object.
		 * @param boolean $blnReload reload Organisation from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objOrganisation->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objOrganisation->Id;
			if ($this->lblId) $this->lblId->Text = $this->objOrganisation->Id;

			if ($this->lstAddress) {
					$this->lstAddress->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstAddress->AddItem(QApplication::Translate('- Select One -'), null);
				$objAddressArray = Address::LoadAll();
				if ($objAddressArray) foreach ($objAddressArray as $objAddress) {
					$objListItem = new QListItem($objAddress->__toString(), $objAddress->Id);
					if (($this->objOrganisation->Address) && ($this->objOrganisation->Address->Id == $objAddress->Id))
						$objListItem->Selected = true;
					$this->lstAddress->AddItem($objListItem);
				}
			}
			if ($this->lblAddressId) $this->lblAddressId->Text = ($this->objOrganisation->Address) ? $this->objOrganisation->Address->__toString() : null;

			if ($this->lstPerson) {
					$this->lstPerson->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstPerson->AddItem(QApplication::Translate('- Select One -'), null);
				$objPersonArray = Person::LoadAll();
				if ($objPersonArray) foreach ($objPersonArray as $objPerson) {
					$objListItem = new QListItem($objPerson->__toString(), $objPerson->Id);
					if (($this->objOrganisation->Person) && ($this->objOrganisation->Person->Id == $objPerson->Id))
						$objListItem->Selected = true;
					$this->lstPerson->AddItem($objListItem);
				}
			}
			if ($this->lblPersonId) $this->lblPersonId->Text = ($this->objOrganisation->Person) ? $this->objOrganisation->Person->__toString() : null;

			if ($this->txtName) $this->txtName->Text = $this->objOrganisation->Name;
			if ($this->lblName) $this->lblName->Text = $this->objOrganisation->Name;

			if ($this->txtPhone) $this->txtPhone->Text = $this->objOrganisation->Phone;
			if ($this->lblPhone) $this->lblPhone->Text = $this->objOrganisation->Phone;

			if ($this->txtPhoneA) $this->txtPhoneA->Text = $this->objOrganisation->PhoneA;
			if ($this->lblPhoneA) $this->lblPhoneA->Text = $this->objOrganisation->PhoneA;

			if ($this->txtPhoneC) $this->txtPhoneC->Text = $this->objOrganisation->PhoneC;
			if ($this->lblPhoneC) $this->lblPhoneC->Text = $this->objOrganisation->PhoneC;

			if ($this->txtEmail) $this->txtEmail->Text = $this->objOrganisation->Email;
			if ($this->lblEmail) $this->lblEmail->Text = $this->objOrganisation->Email;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objOrganisation->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objOrganisation->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objOrganisation->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objOrganisation->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objOrganisation->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objOrganisation->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objOrganisation->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objOrganisation->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objOrganisation->Status) ? AuthStatusEnum::$NameArray[$this->objOrganisation->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC ORGANISATION OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Organisation instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveOrganisation() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objOrganisation->Id = $this->txtId->Text;
				if ($this->lstAddress) $this->objOrganisation->AddressId = $this->lstAddress->SelectedValue;
				if ($this->lstPerson) $this->objOrganisation->PersonId = $this->lstPerson->SelectedValue;
				if ($this->txtName) $this->objOrganisation->Name = $this->txtName->Text;
				if ($this->txtPhone) $this->objOrganisation->Phone = $this->txtPhone->Text;
				if ($this->txtPhoneA) $this->objOrganisation->PhoneA = $this->txtPhoneA->Text;
				if ($this->txtPhoneC) $this->objOrganisation->PhoneC = $this->txtPhoneC->Text;
				if ($this->txtEmail) $this->objOrganisation->Email = $this->txtEmail->Text;
				if ($this->txtOwner) $this->objOrganisation->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objOrganisation->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objOrganisation->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objOrganisation->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Organisation object
				$this->objOrganisation->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Organisation instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteOrganisation() {
			$this->objOrganisation->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Organisation': return $this->objOrganisation;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Organisation fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'AddressIdControl':
					if (!$this->lstAddress) return $this->lstAddress_Create();
					return $this->lstAddress;
				case 'AddressIdLabel':
					if (!$this->lblAddressId) return $this->lblAddressId_Create();
					return $this->lblAddressId;
				case 'PersonIdControl':
					if (!$this->lstPerson) return $this->lstPerson_Create();
					return $this->lstPerson;
				case 'PersonIdLabel':
					if (!$this->lblPersonId) return $this->lblPersonId_Create();
					return $this->lblPersonId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'PhoneControl':
					if (!$this->txtPhone) return $this->txtPhone_Create();
					return $this->txtPhone;
				case 'PhoneLabel':
					if (!$this->lblPhone) return $this->lblPhone_Create();
					return $this->lblPhone;
				case 'PhoneAControl':
					if (!$this->txtPhoneA) return $this->txtPhoneA_Create();
					return $this->txtPhoneA;
				case 'PhoneALabel':
					if (!$this->lblPhoneA) return $this->lblPhoneA_Create();
					return $this->lblPhoneA;
				case 'PhoneCControl':
					if (!$this->txtPhoneC) return $this->txtPhoneC_Create();
					return $this->txtPhoneC;
				case 'PhoneCLabel':
					if (!$this->lblPhoneC) return $this->lblPhoneC_Create();
					return $this->lblPhoneC;
				case 'EmailControl':
					if (!$this->txtEmail) return $this->txtEmail_Create();
					return $this->txtEmail;
				case 'EmailLabel':
					if (!$this->lblEmail) return $this->lblEmail_Create();
					return $this->lblEmail;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Organisation fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'AddressIdControl':
						return ($this->lstAddress = QType::Cast($mixValue, 'QControl'));
					case 'PersonIdControl':
						return ($this->lstPerson = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'PhoneControl':
						return ($this->txtPhone = QType::Cast($mixValue, 'QControl'));
					case 'PhoneAControl':
						return ($this->txtPhoneA = QType::Cast($mixValue, 'QControl'));
					case 'PhoneCControl':
						return ($this->txtPhoneC = QType::Cast($mixValue, 'QControl'));
					case 'EmailControl':
						return ($this->txtEmail = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
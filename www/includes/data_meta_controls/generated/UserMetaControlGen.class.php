<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the User class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single User object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a UserMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read User $User the actual User data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $PersonIdControl
	 * property-read QLabel $PersonIdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $PasswordControl
	 * property-read QLabel $PasswordLabel
	 * property QCheckBox $FounderControl
	 * property-read QLabel $FounderLabel
	 * property QIntegerTextBox $GroupMembershipsControl
	 * property-read QLabel $GroupMembershipsLabel
	 * property QTextBox $LanguageCodeControl
	 * property-read QLabel $LanguageCodeLabel
	 * property QTextBox $CountryCodeControl
	 * property-read QLabel $CountryCodeLabel
	 * property QDateTimePicker $LastLoggedInControl
	 * property-read QLabel $LastLoggedInLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QDateTimePicker $ChangedControl
	 * property-read QLabel $ChangedLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class UserMetaControlGen extends QBaseClass {
		// General Variables
		protected $objUser;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of User's individual data fields
		protected $txtId;
		protected $lstPerson;
		protected $txtName;
		protected $txtPassword;
		protected $chkFounder;
		protected $txtGroupMemberships;
		protected $txtLanguageCode;
		protected $txtCountryCode;
		protected $calLastLoggedIn;
		protected $calAdded;
		protected $calChanged;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of User's individual data fields
		protected $lblId;
		protected $lblPersonId;
		protected $lblName;
		protected $lblPassword;
		protected $lblFounder;
		protected $lblGroupMemberships;
		protected $lblLanguageCode;
		protected $lblCountryCode;
		protected $lblLastLoggedIn;
		protected $lblAdded;
		protected $lblChanged;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * UserMetaControl to edit a single User object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single User object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this UserMetaControl
		 * @param User $objUser new or existing User object
		 */
		 public function __construct($objParentObject, User $objUser) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this UserMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked User object
			$this->objUser = $objUser;

			// Figure out if we're Editing or Creating New
			if ($this->objUser->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this UserMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing User object creation - defaults to CreateOrEdit
 		 * @return UserMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objUser = User::Load($strId);

				// User was found -- return it!
				if ($objUser)
					return new UserMetaControl($objParentObject, $objUser);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a User object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new UserMetaControl($objParentObject, new User());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this UserMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing User object creation - defaults to CreateOrEdit
		 * @return UserMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return UserMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this UserMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing User object creation - defaults to CreateOrEdit
		 * @return UserMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return UserMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objUser->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = User::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objUser->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstPerson
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstPerson_Create($strControlId = null) {
			$this->lstPerson = new QListBox($this->objParentObject, $strControlId);
			$this->lstPerson->Name = QApplication::Translate('Person');
			$this->lstPerson->Required = true;
			if (!$this->blnEditMode)
				$this->lstPerson->AddItem(QApplication::Translate('- Select One -'), null);
			$objPersonArray = Person::LoadAll();
			if ($objPersonArray) foreach ($objPersonArray as $objPerson) {
				$objListItem = new QListItem($objPerson->__toString(), $objPerson->Id);
				if (($this->objUser->Person) && ($this->objUser->Person->Id == $objPerson->Id))
					$objListItem->Selected = true;
				$this->lstPerson->AddItem($objListItem);
			}
			return $this->lstPerson;
		}

		/**
		 * Create and setup QLabel lblPersonId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblPersonId_Create($strControlId = null) {
			$this->lblPersonId = new QLabel($this->objParentObject, $strControlId);
			$this->lblPersonId->Name = QApplication::Translate('Person');
			$this->lblPersonId->Text = ($this->objUser->Person) ? $this->objUser->Person->__toString() : null;
			$this->lblPersonId->Required = true;
			return $this->lblPersonId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objUser->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = User::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objUser->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtPassword
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtPassword_Create($strControlId = null) {
			$this->txtPassword = new QTextBox($this->objParentObject, $strControlId);
			$this->txtPassword->Name = QApplication::Translate('Password');
			$this->txtPassword->Text = $this->objUser->Password;
			$this->txtPassword->Required = true;
			$this->txtPassword->MaxLength = User::PasswordMaxLength;
			return $this->txtPassword;
		}

		/**
		 * Create and setup QLabel lblPassword
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblPassword_Create($strControlId = null) {
			$this->lblPassword = new QLabel($this->objParentObject, $strControlId);
			$this->lblPassword->Name = QApplication::Translate('Password');
			$this->lblPassword->Text = $this->objUser->Password;
			$this->lblPassword->Required = true;
			return $this->lblPassword;
		}

		/**
		 * Create and setup QCheckBox chkFounder
		 * @param string $strControlId optional ControlId to use
		 * @return QCheckBox
		 */
		public function chkFounder_Create($strControlId = null) {
			$this->chkFounder = new QCheckBox($this->objParentObject, $strControlId);
			$this->chkFounder->Name = QApplication::Translate('Founder');
			$this->chkFounder->Checked = $this->objUser->Founder;
			return $this->chkFounder;
		}

		/**
		 * Create and setup QLabel lblFounder
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblFounder_Create($strControlId = null) {
			$this->lblFounder = new QLabel($this->objParentObject, $strControlId);
			$this->lblFounder->Name = QApplication::Translate('Founder');
			$this->lblFounder->Text = ($this->objUser->Founder) ? QApplication::Translate('Yes') : QApplication::Translate('No');
			return $this->lblFounder;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroupMemberships
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroupMemberships_Create($strControlId = null) {
			$this->txtGroupMemberships = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroupMemberships->Name = QApplication::Translate('Group Memberships');
			$this->txtGroupMemberships->Text = $this->objUser->GroupMemberships;
			$this->txtGroupMemberships->Required = true;
			return $this->txtGroupMemberships;
		}

		/**
		 * Create and setup QLabel lblGroupMemberships
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroupMemberships_Create($strControlId = null, $strFormat = null) {
			$this->lblGroupMemberships = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroupMemberships->Name = QApplication::Translate('Group Memberships');
			$this->lblGroupMemberships->Text = $this->objUser->GroupMemberships;
			$this->lblGroupMemberships->Required = true;
			$this->lblGroupMemberships->Format = $strFormat;
			return $this->lblGroupMemberships;
		}

		/**
		 * Create and setup QTextBox txtLanguageCode
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtLanguageCode_Create($strControlId = null) {
			$this->txtLanguageCode = new QTextBox($this->objParentObject, $strControlId);
			$this->txtLanguageCode->Name = QApplication::Translate('Language Code');
			$this->txtLanguageCode->Text = $this->objUser->LanguageCode;
			$this->txtLanguageCode->Required = true;
			$this->txtLanguageCode->MaxLength = User::LanguageCodeMaxLength;
			return $this->txtLanguageCode;
		}

		/**
		 * Create and setup QLabel lblLanguageCode
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblLanguageCode_Create($strControlId = null) {
			$this->lblLanguageCode = new QLabel($this->objParentObject, $strControlId);
			$this->lblLanguageCode->Name = QApplication::Translate('Language Code');
			$this->lblLanguageCode->Text = $this->objUser->LanguageCode;
			$this->lblLanguageCode->Required = true;
			return $this->lblLanguageCode;
		}

		/**
		 * Create and setup QTextBox txtCountryCode
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtCountryCode_Create($strControlId = null) {
			$this->txtCountryCode = new QTextBox($this->objParentObject, $strControlId);
			$this->txtCountryCode->Name = QApplication::Translate('Country Code');
			$this->txtCountryCode->Text = $this->objUser->CountryCode;
			$this->txtCountryCode->MaxLength = User::CountryCodeMaxLength;
			return $this->txtCountryCode;
		}

		/**
		 * Create and setup QLabel lblCountryCode
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCountryCode_Create($strControlId = null) {
			$this->lblCountryCode = new QLabel($this->objParentObject, $strControlId);
			$this->lblCountryCode->Name = QApplication::Translate('Country Code');
			$this->lblCountryCode->Text = $this->objUser->CountryCode;
			return $this->lblCountryCode;
		}

		/**
		 * Create and setup QDateTimePicker calLastLoggedIn
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calLastLoggedIn_Create($strControlId = null) {
			$this->calLastLoggedIn = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calLastLoggedIn->Name = QApplication::Translate('Last Logged In');
			$this->calLastLoggedIn->DateTime = $this->objUser->LastLoggedIn;
			$this->calLastLoggedIn->DateTimePickerType = QDateTimePickerType::DateTime;
			return $this->calLastLoggedIn;
		}

		/**
		 * Create and setup QLabel lblLastLoggedIn
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblLastLoggedIn_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblLastLoggedIn = new QLabel($this->objParentObject, $strControlId);
			$this->lblLastLoggedIn->Name = QApplication::Translate('Last Logged In');
			$this->strLastLoggedInDateTimeFormat = $strDateTimeFormat;
			$this->lblLastLoggedIn->Text = sprintf($this->objUser->LastLoggedIn) ? $this->objUser->__toString($this->strLastLoggedInDateTimeFormat) : null;
			return $this->lblLastLoggedIn;
		}

		protected $strLastLoggedInDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objUser->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objUser->Added) ? $this->objUser->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calChanged
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calChanged_Create($strControlId = null) {
			$this->calChanged = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calChanged->Name = QApplication::Translate('Changed');
			$this->calChanged->DateTime = $this->objUser->Changed;
			$this->calChanged->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calChanged->Required = true;
			return $this->calChanged;
		}

		/**
		 * Create and setup QLabel lblChanged
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblChanged_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblChanged = new QLabel($this->objParentObject, $strControlId);
			$this->lblChanged->Name = QApplication::Translate('Changed');
			$this->strChangedDateTimeFormat = $strDateTimeFormat;
			$this->lblChanged->Text = sprintf($this->objUser->Changed) ? $this->objUser->__toString($this->strChangedDateTimeFormat) : null;
			$this->lblChanged->Required = true;
			return $this->lblChanged;
		}

		protected $strChangedDateTimeFormat;

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objUser->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objUser->Owner;
			$this->txtOwner->MaxLength = User::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objUser->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objUser->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objUser->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objUser->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objUser->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objUser->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objUser->Status) ? AuthStatusEnum::$NameArray[$this->objUser->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local User object.
		 * @param boolean $blnReload reload User from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objUser->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objUser->Id;
			if ($this->lblId) $this->lblId->Text = $this->objUser->Id;

			if ($this->lstPerson) {
					$this->lstPerson->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstPerson->AddItem(QApplication::Translate('- Select One -'), null);
				$objPersonArray = Person::LoadAll();
				if ($objPersonArray) foreach ($objPersonArray as $objPerson) {
					$objListItem = new QListItem($objPerson->__toString(), $objPerson->Id);
					if (($this->objUser->Person) && ($this->objUser->Person->Id == $objPerson->Id))
						$objListItem->Selected = true;
					$this->lstPerson->AddItem($objListItem);
				}
			}
			if ($this->lblPersonId) $this->lblPersonId->Text = ($this->objUser->Person) ? $this->objUser->Person->__toString() : null;

			if ($this->txtName) $this->txtName->Text = $this->objUser->Name;
			if ($this->lblName) $this->lblName->Text = $this->objUser->Name;

			if ($this->txtPassword) $this->txtPassword->Text = $this->objUser->Password;
			if ($this->lblPassword) $this->lblPassword->Text = $this->objUser->Password;

			if ($this->chkFounder) $this->chkFounder->Checked = $this->objUser->Founder;
			if ($this->lblFounder) $this->lblFounder->Text = ($this->objUser->Founder) ? QApplication::Translate('Yes') : QApplication::Translate('No');

			if ($this->txtGroupMemberships) $this->txtGroupMemberships->Text = $this->objUser->GroupMemberships;
			if ($this->lblGroupMemberships) $this->lblGroupMemberships->Text = $this->objUser->GroupMemberships;

			if ($this->txtLanguageCode) $this->txtLanguageCode->Text = $this->objUser->LanguageCode;
			if ($this->lblLanguageCode) $this->lblLanguageCode->Text = $this->objUser->LanguageCode;

			if ($this->txtCountryCode) $this->txtCountryCode->Text = $this->objUser->CountryCode;
			if ($this->lblCountryCode) $this->lblCountryCode->Text = $this->objUser->CountryCode;

			if ($this->calLastLoggedIn) $this->calLastLoggedIn->DateTime = $this->objUser->LastLoggedIn;
			if ($this->lblLastLoggedIn) $this->lblLastLoggedIn->Text = sprintf($this->objUser->LastLoggedIn) ? $this->objUser->__toString($this->strLastLoggedInDateTimeFormat) : null;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objUser->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objUser->Added) ? $this->objUser->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->calChanged) $this->calChanged->DateTime = $this->objUser->Changed;
			if ($this->lblChanged) $this->lblChanged->Text = sprintf($this->objUser->Changed) ? $this->objUser->__toString($this->strChangedDateTimeFormat) : null;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objUser->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objUser->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objUser->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objUser->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objUser->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objUser->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objUser->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objUser->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objUser->Status) ? AuthStatusEnum::$NameArray[$this->objUser->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC USER OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's User instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveUser() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objUser->Id = $this->txtId->Text;
				if ($this->lstPerson) $this->objUser->PersonId = $this->lstPerson->SelectedValue;
				if ($this->txtName) $this->objUser->Name = $this->txtName->Text;
				if ($this->txtPassword) $this->objUser->Password = $this->txtPassword->Text;
				if ($this->chkFounder) $this->objUser->Founder = $this->chkFounder->Checked;
				if ($this->txtGroupMemberships) $this->objUser->GroupMemberships = $this->txtGroupMemberships->Text;
				if ($this->txtLanguageCode) $this->objUser->LanguageCode = $this->txtLanguageCode->Text;
				if ($this->txtCountryCode) $this->objUser->CountryCode = $this->txtCountryCode->Text;
				if ($this->calLastLoggedIn) $this->objUser->LastLoggedIn = $this->calLastLoggedIn->DateTime;
				if ($this->calAdded) $this->objUser->Added = $this->calAdded->DateTime;
				if ($this->calChanged) $this->objUser->Changed = $this->calChanged->DateTime;
				if ($this->txtOwner) $this->objUser->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objUser->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objUser->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objUser->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the User object
				$this->objUser->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's User instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteUser() {
			$this->objUser->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'User': return $this->objUser;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to User fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'PersonIdControl':
					if (!$this->lstPerson) return $this->lstPerson_Create();
					return $this->lstPerson;
				case 'PersonIdLabel':
					if (!$this->lblPersonId) return $this->lblPersonId_Create();
					return $this->lblPersonId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'PasswordControl':
					if (!$this->txtPassword) return $this->txtPassword_Create();
					return $this->txtPassword;
				case 'PasswordLabel':
					if (!$this->lblPassword) return $this->lblPassword_Create();
					return $this->lblPassword;
				case 'FounderControl':
					if (!$this->chkFounder) return $this->chkFounder_Create();
					return $this->chkFounder;
				case 'FounderLabel':
					if (!$this->lblFounder) return $this->lblFounder_Create();
					return $this->lblFounder;
				case 'GroupMembershipsControl':
					if (!$this->txtGroupMemberships) return $this->txtGroupMemberships_Create();
					return $this->txtGroupMemberships;
				case 'GroupMembershipsLabel':
					if (!$this->lblGroupMemberships) return $this->lblGroupMemberships_Create();
					return $this->lblGroupMemberships;
				case 'LanguageCodeControl':
					if (!$this->txtLanguageCode) return $this->txtLanguageCode_Create();
					return $this->txtLanguageCode;
				case 'LanguageCodeLabel':
					if (!$this->lblLanguageCode) return $this->lblLanguageCode_Create();
					return $this->lblLanguageCode;
				case 'CountryCodeControl':
					if (!$this->txtCountryCode) return $this->txtCountryCode_Create();
					return $this->txtCountryCode;
				case 'CountryCodeLabel':
					if (!$this->lblCountryCode) return $this->lblCountryCode_Create();
					return $this->lblCountryCode;
				case 'LastLoggedInControl':
					if (!$this->calLastLoggedIn) return $this->calLastLoggedIn_Create();
					return $this->calLastLoggedIn;
				case 'LastLoggedInLabel':
					if (!$this->lblLastLoggedIn) return $this->lblLastLoggedIn_Create();
					return $this->lblLastLoggedIn;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'ChangedControl':
					if (!$this->calChanged) return $this->calChanged_Create();
					return $this->calChanged;
				case 'ChangedLabel':
					if (!$this->lblChanged) return $this->lblChanged_Create();
					return $this->lblChanged;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to User fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'PersonIdControl':
						return ($this->lstPerson = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'PasswordControl':
						return ($this->txtPassword = QType::Cast($mixValue, 'QControl'));
					case 'FounderControl':
						return ($this->chkFounder = QType::Cast($mixValue, 'QControl'));
					case 'GroupMembershipsControl':
						return ($this->txtGroupMemberships = QType::Cast($mixValue, 'QControl'));
					case 'LanguageCodeControl':
						return ($this->txtLanguageCode = QType::Cast($mixValue, 'QControl'));
					case 'CountryCodeControl':
						return ($this->txtCountryCode = QType::Cast($mixValue, 'QControl'));
					case 'LastLoggedInControl':
						return ($this->calLastLoggedIn = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'ChangedControl':
						return ($this->calChanged = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
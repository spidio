<?php
/**
 * No license information found, assuming that the code is released to the Public Domain.
 */

/**
 * dwlnetnl 2007/03/01 13:59 $
 * Table structure updated:
	CREATE TABLE IF NOT EXISTS `spidio_session` (
	  `id` varchar(40) collate utf8_bin NOT NULL,
	  `session_data` longtext collate utf8_bin NOT NULL,
	  `last_access` datetime NOT NULL,
	  `created` datetime NOT NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
 * 
 */

/***************************************************************************
** FILE :  QSession.inc    AUTHOR:  Eric Brigham     Date:  9/3/2006
*
*						   Modified:  Chad Koski   10/20/06  -- minor mods for beta 3
*
*  Description:  The following classes can be used in conjunction with QCodo data objects
*                to store and retrieve session data from the database.  This is good if your site
*                runs on multiple servers.  Everything is transparent to you, menaing that you only have to
*                initialize the QSession class and code normally (ie: using $_SESSION as you normally would
*
*  Instalation:  1.  This class assumes you have a table called sessions of this form
						CREATE TABLE `session` (
						  `id` varchar(40) NOT NULL PRIMARY KEY,
						  `session_data` longtext NOT NULL,
						  `last_access` datetime NOT NULL
						) TYPE=InnoDB;
                     Add the above table to your databasse and code generate to create the Sessions data object.
                 2.  Have prepend.inc include this file (QSession.inc)
*                3.  Find the line in prepend.inc that "has session_start();" and replace it with
                          QSession::SetMaxHours(4); //overrides the default max hours setting of 8 hours
                          QSession::Initialize();
*                4.  Optionally, you can change the max hours for sessions (default is 8) by adding an additional line of
*                                QSession::SetMaxHours(12);
* 
*  Example Usage:   QSession::set("foo","bar");
*                   echo QSession::get("foo"); //echos "bar"
*                   $_SESSION["foo"] = "bar";  //yep, this still works fine
*
*                 
*****************************************************************************/

interface SessionSaveHandler{ 
/*these methods need to be declared in a class and assigned with the
   php function session_set_save_handler()*/
        public static function _open(); 
        public static function _close(); 
        public static function _read($id); 
        public static function _write($id, $data); 
        public static function _destroy($id); 
        public static function _clean($max); 
} 

/* register class for storing stuff in the $_SESSION global vaiable*/
class SessionRegister {
      public static function set ( $strName, $varValue ) {
             $_SESSION[$strName] = $varValue;
             return true;
      }
      
      public static function get ( $strName ) {
             return isset($_SESSION[$strName]) ? $_SESSION[$strName] : null;
      }
      
      public static function DeleteAll() {
             unset($_SESSION);
             return true;
      }
      public static function Delete( $strName ) {
             if( isset($_SESSION[$strName]) )
                 unset($_SESSION[$strName]);
             return true;
      }
      
      public static function Dump () {
             var_dump($_SESSION);
      }      
}

/* SESSION Class for storing sessions in the database */

class QSession extends SessionRegister implements SessionSaveHandler {
      
      /*instance of Sessions object that will hold the current session*/
      public static $objSession_data;
      
      /*number of hours that the session is valid for*/
      private static $max_hours = 8;
      
      //run this to start the session
      public static function Initialize() {
             session_set_save_handler(array("QSession","_open"), 
                                      array("QSession","_close"), 
                                      array("QSession","_read"), 
                                      array("QSession","_write"), 
                                      array("QSession","_destroy"), 
                                      array("QSession","_clean"));
             register_shutdown_function("session_write_close");
             session_start();
             return true;
      }
      
      public static function SetMaxHours( $hours = 8 ) {
             self::$max_hours = $hours;
             return true;
      }
      
      /*db session functions*/
      public static function _open () {
             return true;
      }
       
      public static function _close() {
             return true;      
      } 
        /** 
        * Reads data from the data source 
        * @access public 
        * @static 
        * @returns string 
        */ 
        public static function _read($id){ 
                self::$objSession_data = Session::Load($id);
                if( self::$objSession_data == null ) {
                    /*must be a new session*/
                    self::$objSession_data = new Session();
                    self::$objSession_data->Id = $id;
                    self::$objSession_data->Created = new QDateTime(QDateTime::Now);
                    return ''; /*return empty string if there is no session*/
                 } else {
                    /*must be an existing session*/
                    /*if session is too old, then delete session data, otherwise return it*/
                    //$sessTime = new QDateTime(self::$objSession_data->LastAccess);
                    $sessTime = self::$objSession_data->LastAccess;
                    if( $sessTime->Timestamp < time() - self::$max_hours*3600 ) {
                        /*session is too old*/
                        self::$objSession_data->SessionData = '';
	                    self::$objSession_data->LastAccess = new QDateTime(QDateTime::Now);
                        self::$objSession_data->Save();
                        return ''; /*essentially restarts the session, but keeps the session id*/
                    } else {
                        return self::$objSession_data->SessionData;
                    }
                 }
        } 
        
        /** 
        * Writes serialized data to data source 
        * @access public 
        * @static 
        * @returns bool 
        */ 
        public static function _write($id, $data){ 
               if( $id != self::$objSession_data->Id ) {
                   /*maybe we hijacked a session somehow?*/
                   return false;
               } else {
                   self::$objSession_data->SessionData = $data;
                   self::$objSession_data->LastAccess = new QDateTime(QDateTime::Now);
                   self::$objSession_data->Save();
                   self::DeleteAll(); /*we do this in case we are on multiple servers and we dont want to confuse anyone*/
                   return true;
               }
        } 
        
        /** 
        * Delete session data associated with this id 
        * @access public 
        * @static 
        * @returns bool 
        */ 
        public static function _destroy($id){ 
               if( $id != self::$objSession_data->Id ) {
                   /*maybe we hijacked a session somehow?*/
                   return false;
               } else {
                   self::$objSession_data->Delete();
                   return true;
               }
        } 
        
        /** 
        * Delete old session data 
        * @access public 
        * @static 
        * @returns bool 
        */ 
        public static function _clean($max){
               /*put a function in your Sessions class and call it here, the function should clean out the sessions
               ** table through the $max date (which is a unix/php timestamp*/
               /*EXMPLE:*/
               Session::CleanExpiredSessions($max);
               return true;
        }      
}
?>
<?php
	// This is the HTML template include file (.tpl.php) for tapeEditPanel.
	// Remember that this is a DRAFT.  It is MEANT to be altered/modified.
	// Be sure to move this out of the drafts/dashboard subdirectory before modifying to ensure that subsequent 
	// code re-generations do not overwrite your changes.
?>
	<div id="formControls">
		<?php $_CONTROL->txtId->RenderWithName(); ?>

		<?php $_CONTROL->lblNumber->RenderWithName(); ?>

		<?php $_CONTROL->lstLibrary->RenderWithName(); ?>

		<?php $_CONTROL->lstOrganisation->RenderWithName(); ?>

		<?php $_CONTROL->lstTapeModeEnum->RenderWithName(); ?>

		<?php $_CONTROL->calAdded->RenderWithName(); ?>

		<?php $_CONTROL->txtComment->RenderWithName(); ?>

		<?php $_CONTROL->lblOptlock->RenderWithName(); ?>

		<?php $_CONTROL->txtOwner->RenderWithName(); ?>

		<?php $_CONTROL->txtGroup->RenderWithName(); ?>

		<?php $_CONTROL->txtPerms->RenderWithName(); ?>

		<?php $_CONTROL->lstStatusObject->RenderWithName(); ?>

	</div>

	<div id="formActions">
		<div id="save"><?php $_CONTROL->btnSave->Render(); ?></div>
		<div id="cancel"><?php $_CONTROL->btnCancel->Render(); ?></div>
		<div id="delete"><?php $_CONTROL->btnDelete->Render(); ?></div>
	</div>

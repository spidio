<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Codec class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Codec object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a CodecMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Codec $Codec the actual Codec data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QListBox $CodecEnumIdControl
	 * property-read QLabel $CodecEnumIdLabel
	 * property QTextBox $FourccControl
	 * property-read QLabel $FourccLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class CodecMetaControlGen extends QBaseClass {
		// General Variables
		protected $objCodec;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Codec's individual data fields
		protected $txtId;
		protected $txtName;
		protected $lstCodecEnum;
		protected $txtFourcc;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Codec's individual data fields
		protected $lblId;
		protected $lblName;
		protected $lblCodecEnumId;
		protected $lblFourcc;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * CodecMetaControl to edit a single Codec object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Codec object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this CodecMetaControl
		 * @param Codec $objCodec new or existing Codec object
		 */
		 public function __construct($objParentObject, Codec $objCodec) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this CodecMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Codec object
			$this->objCodec = $objCodec;

			// Figure out if we're Editing or Creating New
			if ($this->objCodec->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this CodecMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Codec object creation - defaults to CreateOrEdit
 		 * @return CodecMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objCodec = Codec::Load($strId);

				// Codec was found -- return it!
				if ($objCodec)
					return new CodecMetaControl($objParentObject, $objCodec);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Codec object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new CodecMetaControl($objParentObject, new Codec());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this CodecMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Codec object creation - defaults to CreateOrEdit
		 * @return CodecMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return CodecMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this CodecMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Codec object creation - defaults to CreateOrEdit
		 * @return CodecMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return CodecMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objCodec->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Codec::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objCodec->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objCodec->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = Codec::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objCodec->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QListBox lstCodecEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstCodecEnum_Create($strControlId = null) {
			$this->lstCodecEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstCodecEnum->Name = QApplication::Translate('Codec Enum');
			$this->lstCodecEnum->Required = true;
			foreach (CodecEnum::$NameArray as $intId => $strValue)
				$this->lstCodecEnum->AddItem(new QListItem($strValue, $intId, $this->objCodec->CodecEnumId == $intId));
			return $this->lstCodecEnum;
		}

		/**
		 * Create and setup QLabel lblCodecEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCodecEnumId_Create($strControlId = null) {
			$this->lblCodecEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblCodecEnumId->Name = QApplication::Translate('Codec Enum');
			$this->lblCodecEnumId->Text = ($this->objCodec->CodecEnumId) ? CodecEnum::$NameArray[$this->objCodec->CodecEnumId] : null;
			$this->lblCodecEnumId->Required = true;
			return $this->lblCodecEnumId;
		}

		/**
		 * Create and setup QTextBox txtFourcc
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtFourcc_Create($strControlId = null) {
			$this->txtFourcc = new QTextBox($this->objParentObject, $strControlId);
			$this->txtFourcc->Name = QApplication::Translate('Fourcc');
			$this->txtFourcc->Text = $this->objCodec->Fourcc;
			$this->txtFourcc->MaxLength = Codec::FourccMaxLength;
			return $this->txtFourcc;
		}

		/**
		 * Create and setup QLabel lblFourcc
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblFourcc_Create($strControlId = null) {
			$this->lblFourcc = new QLabel($this->objParentObject, $strControlId);
			$this->lblFourcc->Name = QApplication::Translate('Fourcc');
			$this->lblFourcc->Text = $this->objCodec->Fourcc;
			return $this->lblFourcc;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objCodec->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objCodec->Owner;
			$this->txtOwner->MaxLength = Codec::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objCodec->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objCodec->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objCodec->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objCodec->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objCodec->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objCodec->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objCodec->Status) ? AuthStatusEnum::$NameArray[$this->objCodec->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Codec object.
		 * @param boolean $blnReload reload Codec from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objCodec->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objCodec->Id;
			if ($this->lblId) $this->lblId->Text = $this->objCodec->Id;

			if ($this->txtName) $this->txtName->Text = $this->objCodec->Name;
			if ($this->lblName) $this->lblName->Text = $this->objCodec->Name;

			if ($this->lstCodecEnum) $this->lstCodecEnum->SelectedValue = $this->objCodec->CodecEnumId;
			if ($this->lblCodecEnumId) $this->lblCodecEnumId->Text = ($this->objCodec->CodecEnumId) ? CodecEnum::$NameArray[$this->objCodec->CodecEnumId] : null;

			if ($this->txtFourcc) $this->txtFourcc->Text = $this->objCodec->Fourcc;
			if ($this->lblFourcc) $this->lblFourcc->Text = $this->objCodec->Fourcc;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objCodec->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objCodec->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objCodec->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objCodec->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objCodec->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objCodec->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objCodec->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objCodec->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objCodec->Status) ? AuthStatusEnum::$NameArray[$this->objCodec->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC CODEC OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Codec instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveCodec() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objCodec->Id = $this->txtId->Text;
				if ($this->txtName) $this->objCodec->Name = $this->txtName->Text;
				if ($this->lstCodecEnum) $this->objCodec->CodecEnumId = $this->lstCodecEnum->SelectedValue;
				if ($this->txtFourcc) $this->objCodec->Fourcc = $this->txtFourcc->Text;
				if ($this->txtOwner) $this->objCodec->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objCodec->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objCodec->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objCodec->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Codec object
				$this->objCodec->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Codec instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteCodec() {
			$this->objCodec->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Codec': return $this->objCodec;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Codec fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'CodecEnumIdControl':
					if (!$this->lstCodecEnum) return $this->lstCodecEnum_Create();
					return $this->lstCodecEnum;
				case 'CodecEnumIdLabel':
					if (!$this->lblCodecEnumId) return $this->lblCodecEnumId_Create();
					return $this->lblCodecEnumId;
				case 'FourccControl':
					if (!$this->txtFourcc) return $this->txtFourcc_Create();
					return $this->txtFourcc;
				case 'FourccLabel':
					if (!$this->lblFourcc) return $this->lblFourcc_Create();
					return $this->lblFourcc;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Codec fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'CodecEnumIdControl':
						return ($this->lstCodecEnum = QType::Cast($mixValue, 'QControl'));
					case 'FourccControl':
						return ($this->txtFourcc = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
<?php
	/**
	 * The abstract AuthPrivilegeGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the AuthPrivilege subclass which
	 * extends this AuthPrivilegeGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthPrivilege class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property integer $AuthRoleEnumId the value for intAuthRoleEnumId (Not Null)
	 * @property string $Who the value for strWho 
	 * @property integer $AuthActionId the value for intAuthActionId (Not Null)
	 * @property integer $AuthPrivilegeTypeEnumId the value for intAuthPrivilegeTypeEnumId (Not Null)
	 * @property integer $AuthClassEnumId the value for intAuthClassEnumId (Not Null)
	 * @property string $RelatedId the value for strRelatedId 
	 * @property AuthAction $AuthAction the value for the AuthAction object referenced by intAuthActionId (Not Null)
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class AuthPrivilegeGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_auth_privilege.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_auth_privilege.auth_role_enum_id
		 * @var integer intAuthRoleEnumId
		 */
		protected $intAuthRoleEnumId;
		const AuthRoleEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_privilege.who
		 * @var string strWho
		 */
		protected $strWho;
		const WhoMaxLength = 36;
		const WhoDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_privilege.auth_action_id
		 * @var integer intAuthActionId
		 */
		protected $intAuthActionId;
		const AuthActionIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_privilege.auth_privilege_type_enum_id
		 * @var integer intAuthPrivilegeTypeEnumId
		 */
		protected $intAuthPrivilegeTypeEnumId;
		const AuthPrivilegeTypeEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_privilege.auth_class_enum_id
		 * @var integer intAuthClassEnumId
		 */
		protected $intAuthClassEnumId;
		const AuthClassEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_privilege.related_id
		 * @var string strRelatedId
		 */
		protected $strRelatedId;
		const RelatedIdMaxLength = 36;
		const RelatedIdDefault = null;


		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_auth_privilege.auth_action_id.
		 *
		 * NOTE: Always use the AuthAction property getter to correctly retrieve this AuthAction object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var AuthAction objAuthAction
		 */
		protected $objAuthAction;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a AuthPrivilege from PK Info
		 * @param string $strId
		 * @return AuthPrivilege
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return AuthPrivilege::QuerySingle(
				QQ::Equal(QQN::AuthPrivilege()->Id, $strId)
			);
		}

		/**
		 * Load all AuthPrivileges
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthPrivilege[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call AuthPrivilege::QueryArray to perform the LoadAll query
			try {
				return AuthPrivilege::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all AuthPrivileges
		 * @return int
		 */
		public static function CountAll() {
			// Call AuthPrivilege::QueryCount to perform the CountAll query
			return AuthPrivilege::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = AuthPrivilege::GetDatabase();

			// Create/Build out the QueryBuilder object with AuthPrivilege-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'auth_privilege');
			AuthPrivilege::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'auth_privilege');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single AuthPrivilege object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return AuthPrivilege the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthPrivilege::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new AuthPrivilege object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return AuthPrivilege::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of AuthPrivilege objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return AuthPrivilege[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthPrivilege::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return AuthPrivilege::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of AuthPrivilege objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthPrivilege::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = AuthPrivilege::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'auth_privilege_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with AuthPrivilege-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				AuthPrivilege::GetSelectFields($objQueryBuilder);
				AuthPrivilege::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return AuthPrivilege::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this AuthPrivilege
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_privilege';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'auth_role_enum_id', $strAliasPrefix . 'auth_role_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'who', $strAliasPrefix . 'who');
			$objBuilder->AddSelectItem($strTableName, 'auth_action_id', $strAliasPrefix . 'auth_action_id');
			$objBuilder->AddSelectItem($strTableName, 'auth_privilege_type_enum_id', $strAliasPrefix . 'auth_privilege_type_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'auth_class_enum_id', $strAliasPrefix . 'auth_class_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'related_id', $strAliasPrefix . 'related_id');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a AuthPrivilege from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this AuthPrivilege::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return AuthPrivilege
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;


			// Create a new instance of the AuthPrivilege object
			$objToReturn = new AuthPrivilege();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'auth_role_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_role_enum_id'] : $strAliasPrefix . 'auth_role_enum_id';
			$objToReturn->intAuthRoleEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'who', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'who'] : $strAliasPrefix . 'who';
			$objToReturn->strWho = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'auth_action_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_action_id'] : $strAliasPrefix . 'auth_action_id';
			$objToReturn->intAuthActionId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'auth_privilege_type_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_privilege_type_enum_id'] : $strAliasPrefix . 'auth_privilege_type_enum_id';
			$objToReturn->intAuthPrivilegeTypeEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'auth_class_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'auth_class_enum_id'] : $strAliasPrefix . 'auth_class_enum_id';
			$objToReturn->intAuthClassEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'related_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'related_id'] : $strAliasPrefix . 'related_id';
			$objToReturn->strRelatedId = $objDbRow->GetColumn($strAliasName, 'VarChar');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'auth_privilege__';

			// Check for AuthAction Early Binding
			$strAlias = $strAliasPrefix . 'auth_action_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objAuthAction = AuthAction::InstantiateDbRow($objDbRow, $strAliasPrefix . 'auth_action_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			return $objToReturn;
		}

		/**
		 * Instantiate an array of AuthPrivileges from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return AuthPrivilege[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = AuthPrivilege::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = AuthPrivilege::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single AuthPrivilege object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return AuthPrivilege
		*/
		public static function LoadById($strId) {
			return AuthPrivilege::QuerySingle(
				QQ::Equal(QQN::AuthPrivilege()->Id, $strId)
			);
		}
			
		/**
		 * Load an array of AuthPrivilege objects,
		 * by AuthActionId Index(es)
		 * @param integer $intAuthActionId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthPrivilege[]
		*/
		public static function LoadArrayByAuthActionId($intAuthActionId, $objOptionalClauses = null) {
			// Call AuthPrivilege::QueryArray to perform the LoadArrayByAuthActionId query
			try {
				return AuthPrivilege::QueryArray(
					QQ::Equal(QQN::AuthPrivilege()->AuthActionId, $intAuthActionId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthPrivileges
		 * by AuthActionId Index(es)
		 * @param integer $intAuthActionId
		 * @return int
		*/
		public static function CountByAuthActionId($intAuthActionId) {
			// Call AuthPrivilege::QueryCount to perform the CountByAuthActionId query
			return AuthPrivilege::QueryCount(
				QQ::Equal(QQN::AuthPrivilege()->AuthActionId, $intAuthActionId)
			);
		}
			
		/**
		 * Load an array of AuthPrivilege objects,
		 * by AuthPrivilegeTypeEnumId Index(es)
		 * @param integer $intAuthPrivilegeTypeEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthPrivilege[]
		*/
		public static function LoadArrayByAuthPrivilegeTypeEnumId($intAuthPrivilegeTypeEnumId, $objOptionalClauses = null) {
			// Call AuthPrivilege::QueryArray to perform the LoadArrayByAuthPrivilegeTypeEnumId query
			try {
				return AuthPrivilege::QueryArray(
					QQ::Equal(QQN::AuthPrivilege()->AuthPrivilegeTypeEnumId, $intAuthPrivilegeTypeEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthPrivileges
		 * by AuthPrivilegeTypeEnumId Index(es)
		 * @param integer $intAuthPrivilegeTypeEnumId
		 * @return int
		*/
		public static function CountByAuthPrivilegeTypeEnumId($intAuthPrivilegeTypeEnumId) {
			// Call AuthPrivilege::QueryCount to perform the CountByAuthPrivilegeTypeEnumId query
			return AuthPrivilege::QueryCount(
				QQ::Equal(QQN::AuthPrivilege()->AuthPrivilegeTypeEnumId, $intAuthPrivilegeTypeEnumId)
			);
		}
			
		/**
		 * Load an array of AuthPrivilege objects,
		 * by AuthClassEnumId Index(es)
		 * @param integer $intAuthClassEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthPrivilege[]
		*/
		public static function LoadArrayByAuthClassEnumId($intAuthClassEnumId, $objOptionalClauses = null) {
			// Call AuthPrivilege::QueryArray to perform the LoadArrayByAuthClassEnumId query
			try {
				return AuthPrivilege::QueryArray(
					QQ::Equal(QQN::AuthPrivilege()->AuthClassEnumId, $intAuthClassEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthPrivileges
		 * by AuthClassEnumId Index(es)
		 * @param integer $intAuthClassEnumId
		 * @return int
		*/
		public static function CountByAuthClassEnumId($intAuthClassEnumId) {
			// Call AuthPrivilege::QueryCount to perform the CountByAuthClassEnumId query
			return AuthPrivilege::QueryCount(
				QQ::Equal(QQN::AuthPrivilege()->AuthClassEnumId, $intAuthClassEnumId)
			);
		}
			
		/**
		 * Load an array of AuthPrivilege objects,
		 * by AuthRoleEnumId Index(es)
		 * @param integer $intAuthRoleEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthPrivilege[]
		*/
		public static function LoadArrayByAuthRoleEnumId($intAuthRoleEnumId, $objOptionalClauses = null) {
			// Call AuthPrivilege::QueryArray to perform the LoadArrayByAuthRoleEnumId query
			try {
				return AuthPrivilege::QueryArray(
					QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, $intAuthRoleEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count AuthPrivileges
		 * by AuthRoleEnumId Index(es)
		 * @param integer $intAuthRoleEnumId
		 * @return int
		*/
		public static function CountByAuthRoleEnumId($intAuthRoleEnumId) {
			// Call AuthPrivilege::QueryCount to perform the CountByAuthRoleEnumId query
			return AuthPrivilege::QueryCount(
				QQ::Equal(QQN::AuthPrivilege()->AuthRoleEnumId, $intAuthRoleEnumId)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this AuthPrivilege
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = AuthPrivilege::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'auth_privilege` (
							`id`,
							`auth_role_enum_id`,
							`who`,
							`auth_action_id`,
							`auth_privilege_type_enum_id`,
							`auth_class_enum_id`,
							`related_id`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->intAuthRoleEnumId) . ',
							' . $objDatabase->SqlVariable($this->strWho) . ',
							' . $objDatabase->SqlVariable($this->intAuthActionId) . ',
							' . $objDatabase->SqlVariable($this->intAuthPrivilegeTypeEnumId) . ',
							' . $objDatabase->SqlVariable($this->intAuthClassEnumId) . ',
							' . $objDatabase->SqlVariable($this->strRelatedId) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'auth_privilege`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`auth_role_enum_id` = ' . $objDatabase->SqlVariable($this->intAuthRoleEnumId) . ',
							`who` = ' . $objDatabase->SqlVariable($this->strWho) . ',
							`auth_action_id` = ' . $objDatabase->SqlVariable($this->intAuthActionId) . ',
							`auth_privilege_type_enum_id` = ' . $objDatabase->SqlVariable($this->intAuthPrivilegeTypeEnumId) . ',
							`auth_class_enum_id` = ' . $objDatabase->SqlVariable($this->intAuthClassEnumId) . ',
							`related_id` = ' . $objDatabase->SqlVariable($this->strRelatedId) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;


			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this AuthPrivilege
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this AuthPrivilege with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = AuthPrivilege::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'auth_privilege`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all AuthPrivileges
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = AuthPrivilege::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'auth_privilege`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'auth_privilege table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = AuthPrivilege::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'auth_privilege`');
		}

		/**
		 * Reload this AuthPrivilege from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved AuthPrivilege object.');

			// Reload the Object
			$objReloaded = AuthPrivilege::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->AuthRoleEnumId = $objReloaded->AuthRoleEnumId;
			$this->strWho = $objReloaded->strWho;
			$this->AuthActionId = $objReloaded->AuthActionId;
			$this->AuthPrivilegeTypeEnumId = $objReloaded->AuthPrivilegeTypeEnumId;
			$this->AuthClassEnumId = $objReloaded->AuthClassEnumId;
			$this->strRelatedId = $objReloaded->strRelatedId;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'AuthRoleEnumId':
					/**
					 * Gets the value for intAuthRoleEnumId (Not Null)
					 * @return integer
					 */
					return $this->intAuthRoleEnumId;

				case 'Who':
					/**
					 * Gets the value for strWho 
					 * @return string
					 */
					return $this->strWho;

				case 'AuthActionId':
					/**
					 * Gets the value for intAuthActionId (Not Null)
					 * @return integer
					 */
					return $this->intAuthActionId;

				case 'AuthPrivilegeTypeEnumId':
					/**
					 * Gets the value for intAuthPrivilegeTypeEnumId (Not Null)
					 * @return integer
					 */
					return $this->intAuthPrivilegeTypeEnumId;

				case 'AuthClassEnumId':
					/**
					 * Gets the value for intAuthClassEnumId (Not Null)
					 * @return integer
					 */
					return $this->intAuthClassEnumId;

				case 'RelatedId':
					/**
					 * Gets the value for strRelatedId 
					 * @return string
					 */
					return $this->strRelatedId;


				///////////////////
				// Member Objects
				///////////////////
				case 'AuthAction':
					/**
					 * Gets the value for the AuthAction object referenced by intAuthActionId (Not Null)
					 * @return AuthAction
					 */
					try {
						if ((!$this->objAuthAction) && (!is_null($this->intAuthActionId)))
							$this->objAuthAction = AuthAction::Load($this->intAuthActionId);
						return $this->objAuthAction;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AuthRoleEnumId':
					/**
					 * Sets the value for intAuthRoleEnumId (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intAuthRoleEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Who':
					/**
					 * Sets the value for strWho 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strWho = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AuthActionId':
					/**
					 * Sets the value for intAuthActionId (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						$this->objAuthAction = null;
						return ($this->intAuthActionId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AuthPrivilegeTypeEnumId':
					/**
					 * Sets the value for intAuthPrivilegeTypeEnumId (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intAuthPrivilegeTypeEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'AuthClassEnumId':
					/**
					 * Sets the value for intAuthClassEnumId (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intAuthClassEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'RelatedId':
					/**
					 * Sets the value for strRelatedId 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strRelatedId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'AuthAction':
					/**
					 * Sets the value for the AuthAction object referenced by intAuthActionId (Not Null)
					 * @param AuthAction $mixValue
					 * @return AuthAction
					 */
					if (is_null($mixValue)) {
						$this->intAuthActionId = null;
						$this->objAuthAction = null;
						return null;
					} else {
						// Make sure $mixValue actually is a AuthAction object
						try {
							$mixValue = QType::Cast($mixValue, 'AuthAction');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED AuthAction object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved AuthAction for this AuthPrivilege');

						// Update Local Member Variables
						$this->objAuthAction = $mixValue;
						$this->intAuthActionId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="AuthPrivilege"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="AuthRoleEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="Who" type="xsd:string"/>';
			$strToReturn .= '<element name="AuthAction" type="xsd1:AuthAction"/>';
			$strToReturn .= '<element name="AuthPrivilegeTypeEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="AuthClassEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="RelatedId" type="xsd:string"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('AuthPrivilege', $strComplexTypeArray)) {
				$strComplexTypeArray['AuthPrivilege'] = AuthPrivilege::GetSoapComplexTypeXml();
				AuthAction::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, AuthPrivilege::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new AuthPrivilege();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if (property_exists($objSoapObject, 'AuthRoleEnumId'))
				$objToReturn->intAuthRoleEnumId = $objSoapObject->AuthRoleEnumId;
			if (property_exists($objSoapObject, 'Who'))
				$objToReturn->strWho = $objSoapObject->Who;
			if ((property_exists($objSoapObject, 'AuthAction')) &&
				($objSoapObject->AuthAction))
				$objToReturn->AuthAction = AuthAction::GetObjectFromSoapObject($objSoapObject->AuthAction);
			if (property_exists($objSoapObject, 'AuthPrivilegeTypeEnumId'))
				$objToReturn->intAuthPrivilegeTypeEnumId = $objSoapObject->AuthPrivilegeTypeEnumId;
			if (property_exists($objSoapObject, 'AuthClassEnumId'))
				$objToReturn->intAuthClassEnumId = $objSoapObject->AuthClassEnumId;
			if (property_exists($objSoapObject, 'RelatedId'))
				$objToReturn->strRelatedId = $objSoapObject->RelatedId;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, AuthPrivilege::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objAuthAction)
				$objObject->objAuthAction = AuthAction::GetSoapObjectFromObject($objObject->objAuthAction, false);
			else if (!$blnBindRelatedObjects)
				$objObject->intAuthActionId = null;
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeAuthPrivilege extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'AuthPrivilege';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_privilege';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'AuthRoleEnumId':
					return new QQNode('auth_role_enum_id', 'AuthRoleEnumId', 'integer', $this);
				case 'Who':
					return new QQNode('who', 'Who', 'string', $this);
				case 'AuthActionId':
					return new QQNode('auth_action_id', 'AuthActionId', 'integer', $this);
				case 'AuthAction':
					return new QQNodeAuthAction('auth_action_id', 'AuthAction', 'integer', $this);
				case 'AuthPrivilegeTypeEnumId':
					return new QQNode('auth_privilege_type_enum_id', 'AuthPrivilegeTypeEnumId', 'integer', $this);
				case 'AuthClassEnumId':
					return new QQNode('auth_class_enum_id', 'AuthClassEnumId', 'integer', $this);
				case 'RelatedId':
					return new QQNode('related_id', 'RelatedId', 'string', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeAuthPrivilege extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'AuthPrivilege';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_privilege';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'AuthRoleEnumId':
					return new QQNode('auth_role_enum_id', 'AuthRoleEnumId', 'integer', $this);
				case 'Who':
					return new QQNode('who', 'Who', 'string', $this);
				case 'AuthActionId':
					return new QQNode('auth_action_id', 'AuthActionId', 'integer', $this);
				case 'AuthAction':
					return new QQNodeAuthAction('auth_action_id', 'AuthAction', 'integer', $this);
				case 'AuthPrivilegeTypeEnumId':
					return new QQNode('auth_privilege_type_enum_id', 'AuthPrivilegeTypeEnumId', 'integer', $this);
				case 'AuthClassEnumId':
					return new QQNode('auth_class_enum_id', 'AuthClassEnumId', 'integer', $this);
				case 'RelatedId':
					return new QQNode('related_id', 'RelatedId', 'string', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
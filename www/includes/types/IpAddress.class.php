<?php

class IpAddress extends QBaseClass {
	protected $strOriginal;
	protected $blnIPv4;

	protected $strIPv4;
	protected $hexIPv4;
	protected $intIPv4;

	protected $strIPv6;

	public function __construct($strIp) {
		$this->strOriginal = $strIp;
		$this->blnIPv4 = (preg_match('/(^::(ffff:)?(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})?|([0-9a-f]{1,4}:[0-9a-f]{1,4}))$)|(^0x[0-9a-f]{1,8}$)|(^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$)/', $strIp));
	}

	public function __get($strName) {
		switch ($strName) {
			case 'IsIPv4': return $this->blnIPv4;
			case 'IPv4': return $this->GetIPv4();
			case 'IPv6': return $this->GetIPv6();
				
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}

	public function __toString() {
		return $this->GetIPv6();
	}

	/**
	 * Return IPv4 representation.
	 *
	 * @return string
	 */
	public function GetIPv4() {
		if (is_null($this->strIPv4)) {
			$this->FillIPv4();
		}

		return $this->strIPv4;
	}

	/**
	 * Return IPv4 hexadecimal representation.
	 *
	 * @return string
	 */
	public function GetIPv4AsHex() {
		if (is_null($this->hexIPv4)) {
			$this->FillIPv4AsHex();
		}

		return $this->hexIPv4;
	}

	/**
	 * Return IPv4 integer representation.
	 *
	 * @return int
	 */
	public function GetIPv4AsLong() {
		if (is_null($this->intIPv4)) {
			$this->FillIPv4AsLong();
		}

		return $this->intIPv4;
	}

	/**
	 * Return IPv6 representation.
	 *
	 * @return string
	 */
	public function GetIPv6() {
		if (is_null($this->strIPv6)) {
			$this->FillIPv6();
		}

		return $this->strIPv6;
	}
	
	/**
	 * Return uncompressed IPv6 representation.
	 *
	 * @return string
	 */
	public function GetIPv6Uncompressed() {
		if (is_null($this->strIPv6)) {
			$this->FillIPv6();
		}
		
		return self::AddressUncompress($this->strIPv6);
	}
	
	/**
	 * Get HTML acronym tag of address.
	 *
	 * @return string
	 */
	public function GetAcronymOfAddress() {
		if ($this->blnIPv4) {
			return sprintf('<acronym title="%s">%s</acronym>', $this->GetIPv4(), $this->GetIPv6());
		} else {
			return $this->GetIPv6();
		}
	}

	/**
	 * Fill IPv4 address member.
	 *
	 */
	protected function FillIPv4() {
		if ($this->blnIPv4) {
			if (strpos($this->strOriginal, '0x') !== false) {
				$this->hexIPv4 = str_replace('0x', '', $this->strOriginal);
				$this->intIPv4 = hexdec($this->hexIPv4);
				$this->strIPv4 = long2ip($this->intIPv4);
			} else if (strpos($this->strOriginal, '::ffff:') !== false && strpos($this->strOriginal, '.') !== false) {
				$this->strIPv4 = self::AddressCompress(str_replace('::ffff:', '', $this->strOriginal));
			} else if (strpos($this->strOriginal, '::') !== false && strpos($this->strOriginal, '.') !== false) {
				$this->strIPv4 = self::AddressCompress(str_replace('::', '', $this->strOriginal));
			} else if (strpos($this->strOriginal, '::ffff:') !== false) {
				$strIpArray = explode(':', str_replace('::ffff:', '', $this->strOriginal));
				
				for ($i = 0; $i < count($strIpArray); $i++) {
					$strIpArray[$i] = str_pad($strIpArray[$i], 4, '0', STR_PAD_LEFT);
				}
				
				$this->hexIPv4 = implode('', $strIpArray);
				$this->intIPv4 = hexdec($this->hexIPv4);
				$this->strIPv4 = self::AddressCompress(long2ip($this->intIPv4));
			} else {
				$this->strIPv4 = $this->strOriginal;
			}
		}
	}

	/**
	 * Fill IPv4 address integer member.
	 *
	 */
	protected function FillIPv4AsLong() {
		if (is_null($this->strIPv4)) {
			$this->FillIPv4();
		}
		
		if (is_null($this->intIPv4)) {
			$this->intIPv4 = ip2long($this->strIPv4);
		}
	}

	/**
	 * Fill IPv4 address hexadecimal member.
	 *
	 */
	protected function FillIPv4AsHex() {
		if (is_null($this->intIPv4)) {
			$this->FillIPv4AsLong();
		}
		
		if (is_null($this->hexIPv4)) { 
			$this->hexIPv4 = dechex($this->intIPv4);
		}
	}
	
	/**
	 * Fill IPv6 address member.
	 *
	 */
	protected function FillIPv6() {
		if ($this->blnIPv4) {
			if (is_null($this->hexIPv4)) {
				$this->FillIPv4AsHex();
			}
			
			$strIp = $this->hexIPv4;
			
			$strIpArray = array();
			for ($i = 0; $i < (strlen($strIp) / 4); $i++) {
				$strIpArray[] = substr($strIp, $i * 4, 4);
			}
			
			$strIPv6 = self::AddressCompress(implode(':', $strIpArray));
			
			if (strpos($this->strOriginal, '::ffff:') !== false || preg_match('/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/', $this->strOriginal)) {
				$this->strIPv6 = sprintf('::ffff:%s', $strIPv6);
			} else {
				$this->strIPv6 = sprintf('::%s', $strIPv6);
			}
		} else {
			$this->strIPv6 = self::AddressCompress($this->strOriginal);
		}
	}
	
	/**
	 * Compress IP address.
	 *
	 * @param string $strIp
	 * @return string
	 */
	public static function AddressCompress($strIp) {
		if (preg_match('/^([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$/', $strIp)) {
			$strIpArray = explode('.', $strIp);
			
			for ($i = 0; $i < count($strIpArray); $i++) {
				$strIpArray[$i] = QType::Cast($strIpArray[$i], QType::Integer); 
			}
			
			return implode('.', $strIpArray);
		} else {
			if (!strstr($strIp, '::')) {
				$strIpArray = explode(':', $strIp);
				$strIpArray = array_reverse($strIpArray);
	
				$blnContinue = true;
				for ($i = 0; $i < count($strIpArray); $i++) {
					if ($strIpArray[$i] != '0000') {
						$strIpArray[$i] = ltrim($strIpArray[$i], '0');
					}
	
					if ($blnContinue) {
						if ($strIpArray[$i] == '0000' && $strIpArray[$i+1] == '0000') {
							unset($strIpArray[$i]);
						} else if ($strIpArray[$i] == '0000' && $strIpArray[$i+1] != '0000') {
							$strIpArray[$i] = null;
							$continue = false;
						}
					}
				}
	
				$strIpArray = array_reverse($strIpArray);
				return implode(':', $strIpArray);
			} else {
				return $strIp;
			}
		}
	}
	
	/**
	 * Uncompress IPv6 address.
	 *
	 * @param string $strIp
	 * @return string
	 */
	public static function AddressUncompress($strIp) {
		if (substr_count($strIp, '::')) {
			$strIp = str_replace('::', str_repeat(':0000', 8 - substr_count($strIp, ':')) . ':', $strIp) ;
		}

		$strIp = explode(':', $strIp);

		$strIpArray = array();
		foreach ($strIp as $strValue) {
			$strIpArray[] = str_pad($strValue, 4, 0, STR_PAD_LEFT);
		}

		return implode(':', $strIpArray);
	}
	
	/* SOAP related function */
	public static function GetSoapComplexTypeXml() {
		$strToReturn = '<complexType name="IpAddress"><sequence>';
		$strToReturn .= '<element name="IsIPv4" type="xsd:boolean"/>';
		$strToReturn .= '<element name="IPv4" type="xsd:string"/>';
		$strToReturn .= '<element name="IPv6" type="xsd:string"/>';
		$strToReturn .= '</sequence></complexType>';
		return $strToReturn;
	}

	public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
		if (!array_key_exists('IpAddress', $strComplexTypeArray)) {
			$strComplexTypeArray['IpAddres'] = IpAddress::GetSoapComplexTypeXml();
		}
	}

	public static function GetArrayFromSoapArray($objSoapArray) {
		$objArrayToReturn = array();

		foreach ($objSoapArray as $objSoapObject)
			array_push($objArrayToReturn, IpAddress::GetObjectFromSoapObject($objSoapObject));

		return $objArrayToReturn;
	}

	public static function GetObjectFromSoapObject($objSoapObject) {
		$objToReturn = new IpAddress();
		if (property_exists($objSoapObject, 'IsIPv4'))
			$objToReturn->blnIPv4 = $objSoapObject->IsIPv4;
		if (property_exists($objSoapObject, 'IPv4'))
			$objToReturn->strIPv4 = $objSoapObject->IPv4;
		if (property_exists($objSoapObject, 'IPv6'))
			$objToReturn->strIPv6 = $objSoapObject->IPv6;
		return $objToReturn;
	}

	public static function GetSoapArrayFromArray($objArray) {
		if (!$objArray)
			return null;

		$objArrayToReturn = array();

		foreach ($objArray as $objObject)
			array_push($objArrayToReturn, IpAddress::GetSoapObjectFromObject($objObject, true));

		return unserialize(serialize($objArrayToReturn));
	}

	public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
		return $objObject;
	}
}

?>
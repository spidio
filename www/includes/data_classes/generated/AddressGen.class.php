<?php
	/**
	 * The abstract AddressGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Address subclass which
	 * extends this AddressGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Address class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $Street the value for strStreet (Not Null)
	 * @property string $Code the value for strCode (Not Null)
	 * @property string $Town the value for strTown (Not Null)
	 * @property string $Country the value for strCountry (Not Null)
	 * @property string $Comment the value for strComment 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property-read Location $_LocationAsAddress the value for the private _objLocationAsAddress (Read-Only) if set due to an expansion on the spidio_location.address_id reverse relationship
	 * @property-read Location[] $_LocationAsAddressArray the value for the private _objLocationAsAddressArray (Read-Only) if set due to an ExpandAsArray on the spidio_location.address_id reverse relationship
	 * @property-read Organisation $_OrganisationAsAddress the value for the private _objOrganisationAsAddress (Read-Only) if set due to an expansion on the spidio_organisation.address_id reverse relationship
	 * @property-read Organisation[] $_OrganisationAsAddressArray the value for the private _objOrganisationAsAddressArray (Read-Only) if set due to an ExpandAsArray on the spidio_organisation.address_id reverse relationship
	 * @property-read Person $_PersonAsAddress the value for the private _objPersonAsAddress (Read-Only) if set due to an expansion on the spidio_person.address_id reverse relationship
	 * @property-read Person[] $_PersonAsAddressArray the value for the private _objPersonAsAddressArray (Read-Only) if set due to an ExpandAsArray on the spidio_person.address_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class AddressGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_address.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_address.street
		 * @var string strStreet
		 */
		protected $strStreet;
		const StreetDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.code
		 * @var string strCode
		 */
		protected $strCode;
		const CodeMaxLength = 10;
		const CodeDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.town
		 * @var string strTown
		 */
		protected $strTown;
		const TownMaxLength = 255;
		const TownDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.country
		 * @var string strCountry
		 */
		protected $strCountry;
		const CountryMaxLength = 3;
		const CountryDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_address.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single LocationAsAddress object
		 * (of type Location), if this Address object was restored with
		 * an expansion on the spidio_location association table.
		 * @var Location _objLocationAsAddress;
		 */
		private $_objLocationAsAddress;

		/**
		 * Private member variable that stores a reference to an array of LocationAsAddress objects
		 * (of type Location[]), if this Address object was restored with
		 * an ExpandAsArray on the spidio_location association table.
		 * @var Location[] _objLocationAsAddressArray;
		 */
		private $_objLocationAsAddressArray = array();

		/**
		 * Private member variable that stores a reference to a single OrganisationAsAddress object
		 * (of type Organisation), if this Address object was restored with
		 * an expansion on the spidio_organisation association table.
		 * @var Organisation _objOrganisationAsAddress;
		 */
		private $_objOrganisationAsAddress;

		/**
		 * Private member variable that stores a reference to an array of OrganisationAsAddress objects
		 * (of type Organisation[]), if this Address object was restored with
		 * an ExpandAsArray on the spidio_organisation association table.
		 * @var Organisation[] _objOrganisationAsAddressArray;
		 */
		private $_objOrganisationAsAddressArray = array();

		/**
		 * Private member variable that stores a reference to a single PersonAsAddress object
		 * (of type Person), if this Address object was restored with
		 * an expansion on the spidio_person association table.
		 * @var Person _objPersonAsAddress;
		 */
		private $_objPersonAsAddress;

		/**
		 * Private member variable that stores a reference to an array of PersonAsAddress objects
		 * (of type Person[]), if this Address object was restored with
		 * an ExpandAsArray on the spidio_person association table.
		 * @var Person[] _objPersonAsAddressArray;
		 */
		private $_objPersonAsAddressArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Address from PK Info
		 * @param string $strId
		 * @return Address
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Address::QuerySingle(
				QQ::Equal(QQN::Address()->Id, $strId)
			);
		}

		/**
		 * Load all Addresses
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Address[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Address::QueryArray to perform the LoadAll query
			try {
				return Address::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Addresses
		 * @return int
		 */
		public static function CountAll() {
			// Call Address::QueryCount to perform the CountAll query
			return Address::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Create/Build out the QueryBuilder object with Address-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'address');
			Address::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'address');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Address object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Address the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Address::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Address object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Address::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Address objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Address[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Address::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Address::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Address objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Address::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'address_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Address-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Address::GetSelectFields($objQueryBuilder);
				Address::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Address::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Address
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'address';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'street', $strAliasPrefix . 'street');
			$objBuilder->AddSelectItem($strTableName, 'code', $strAliasPrefix . 'code');
			$objBuilder->AddSelectItem($strTableName, 'town', $strAliasPrefix . 'town');
			$objBuilder->AddSelectItem($strTableName, 'country', $strAliasPrefix . 'country');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Address from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Address::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Address
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'address__';


				$strAlias = $strAliasPrefix . 'locationasaddress__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objLocationAsAddressArray)) {
						$objPreviousChildItem = $objPreviousItem->_objLocationAsAddressArray[$intPreviousChildItemCount - 1];
						$objChildItem = Location::InstantiateDbRow($objDbRow, $strAliasPrefix . 'locationasaddress__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objLocationAsAddressArray[] = $objChildItem;
					} else
						$objPreviousItem->_objLocationAsAddressArray[] = Location::InstantiateDbRow($objDbRow, $strAliasPrefix . 'locationasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'organisationasaddress__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objOrganisationAsAddressArray)) {
						$objPreviousChildItem = $objPreviousItem->_objOrganisationAsAddressArray[$intPreviousChildItemCount - 1];
						$objChildItem = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasaddress__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objOrganisationAsAddressArray[] = $objChildItem;
					} else
						$objPreviousItem->_objOrganisationAsAddressArray[] = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'personasaddress__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objPersonAsAddressArray)) {
						$objPreviousChildItem = $objPreviousItem->_objPersonAsAddressArray[$intPreviousChildItemCount - 1];
						$objChildItem = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'personasaddress__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objPersonAsAddressArray[] = $objChildItem;
					} else
						$objPreviousItem->_objPersonAsAddressArray[] = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'personasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'address__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the Address object
			$objToReturn = new Address();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'street', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'street'] : $strAliasPrefix . 'street';
			$objToReturn->strStreet = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'code', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'code'] : $strAliasPrefix . 'code';
			$objToReturn->strCode = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'town', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'town'] : $strAliasPrefix . 'town';
			$objToReturn->strTown = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'country', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'country'] : $strAliasPrefix . 'country';
			$objToReturn->strCountry = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'address__';




			// Check for LocationAsAddress Virtual Binding
			$strAlias = $strAliasPrefix . 'locationasaddress__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objLocationAsAddressArray[] = Location::InstantiateDbRow($objDbRow, $strAliasPrefix . 'locationasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objLocationAsAddress = Location::InstantiateDbRow($objDbRow, $strAliasPrefix . 'locationasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for OrganisationAsAddress Virtual Binding
			$strAlias = $strAliasPrefix . 'organisationasaddress__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objOrganisationAsAddressArray[] = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objOrganisationAsAddress = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisationasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for PersonAsAddress Virtual Binding
			$strAlias = $strAliasPrefix . 'personasaddress__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objPersonAsAddressArray[] = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'personasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objPersonAsAddress = Person::InstantiateDbRow($objDbRow, $strAliasPrefix . 'personasaddress__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of Addresses from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Address[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Address::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Address::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Address object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Address
		*/
		public static function LoadById($strId) {
			return Address::QuerySingle(
				QQ::Equal(QQN::Address()->Id, $strId)
			);
		}
			
		/**
		 * Load an array of Address objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Address[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call Address::QueryArray to perform the LoadArrayByOwner query
			try {
				return Address::QueryArray(
					QQ::Equal(QQN::Address()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Addresses
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call Address::QueryCount to perform the CountByOwner query
			return Address::QueryCount(
				QQ::Equal(QQN::Address()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of Address objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Address[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call Address::QueryArray to perform the LoadArrayByStatus query
			try {
				return Address::QueryArray(
					QQ::Equal(QQN::Address()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Addresses
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call Address::QueryCount to perform the CountByStatus query
			return Address::QueryCount(
				QQ::Equal(QQN::Address()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Address
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'address` (
							`id`,
							`street`,
							`code`,
							`town`,
							`country`,
							`comment`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strStreet) . ',
							' . $objDatabase->SqlVariable($this->strCode) . ',
							' . $objDatabase->SqlVariable($this->strTown) . ',
							' . $objDatabase->SqlVariable($this->strCountry) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'address`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Address');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'address`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`street` = ' . $objDatabase->SqlVariable($this->strStreet) . ',
							`code` = ' . $objDatabase->SqlVariable($this->strCode) . ',
							`town` = ' . $objDatabase->SqlVariable($this->strTown) . ',
							`country` = ' . $objDatabase->SqlVariable($this->strCountry) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'address`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Address
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Address with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'address`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Addresses
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'address`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'address table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'address`');
		}

		/**
		 * Reload this Address from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Address object.');

			// Reload the Object
			$objReloaded = Address::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->strStreet = $objReloaded->strStreet;
			$this->strCode = $objReloaded->strCode;
			$this->strTown = $objReloaded->strTown;
			$this->strCountry = $objReloaded->strCountry;
			$this->strComment = $objReloaded->strComment;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'Street':
					/**
					 * Gets the value for strStreet (Not Null)
					 * @return string
					 */
					return $this->strStreet;

				case 'Code':
					/**
					 * Gets the value for strCode (Not Null)
					 * @return string
					 */
					return $this->strCode;

				case 'Town':
					/**
					 * Gets the value for strTown (Not Null)
					 * @return string
					 */
					return $this->strTown;

				case 'Country':
					/**
					 * Gets the value for strCountry (Not Null)
					 * @return string
					 */
					return $this->strCountry;

				case 'Comment':
					/**
					 * Gets the value for strComment 
					 * @return string
					 */
					return $this->strComment;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////

				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_LocationAsAddress':
					/**
					 * Gets the value for the private _objLocationAsAddress (Read-Only)
					 * if set due to an expansion on the spidio_location.address_id reverse relationship
					 * @return Location
					 */
					return $this->_objLocationAsAddress;

				case '_LocationAsAddressArray':
					/**
					 * Gets the value for the private _objLocationAsAddressArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_location.address_id reverse relationship
					 * @return Location[]
					 */
					return (array) $this->_objLocationAsAddressArray;

				case '_OrganisationAsAddress':
					/**
					 * Gets the value for the private _objOrganisationAsAddress (Read-Only)
					 * if set due to an expansion on the spidio_organisation.address_id reverse relationship
					 * @return Organisation
					 */
					return $this->_objOrganisationAsAddress;

				case '_OrganisationAsAddressArray':
					/**
					 * Gets the value for the private _objOrganisationAsAddressArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_organisation.address_id reverse relationship
					 * @return Organisation[]
					 */
					return (array) $this->_objOrganisationAsAddressArray;

				case '_PersonAsAddress':
					/**
					 * Gets the value for the private _objPersonAsAddress (Read-Only)
					 * if set due to an expansion on the spidio_person.address_id reverse relationship
					 * @return Person
					 */
					return $this->_objPersonAsAddress;

				case '_PersonAsAddressArray':
					/**
					 * Gets the value for the private _objPersonAsAddressArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_person.address_id reverse relationship
					 * @return Person[]
					 */
					return (array) $this->_objPersonAsAddressArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Street':
					/**
					 * Sets the value for strStreet (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strStreet = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Code':
					/**
					 * Sets the value for strCode (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strCode = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Town':
					/**
					 * Sets the value for strTown (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strTown = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Country':
					/**
					 * Sets the value for strCountry (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strCountry = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for LocationAsAddress
		//-------------------------------------------------------------------

		/**
		 * Gets all associated LocationsAsAddress as an array of Location objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Location[]
		*/ 
		public function GetLocationAsAddressArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Location::LoadArrayByAddressId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated LocationsAsAddress
		 * @return int
		*/ 
		public function CountLocationsAsAddress() {
			if ((is_null($this->strId)))
				return 0;

			return Location::CountByAddressId($this->strId);
		}

		/**
		 * Associates a LocationAsAddress
		 * @param Location $objLocation
		 * @return void
		*/ 
		public function AssociateLocationAsAddress(Location $objLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateLocationAsAddress on this unsaved Address.');
			if ((is_null($objLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateLocationAsAddress on this Address with an unsaved Location.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_location`
				SET
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objLocation->Id) . '
			');
		}

		/**
		 * Unassociates a LocationAsAddress
		 * @param Location $objLocation
		 * @return void
		*/ 
		public function UnassociateLocationAsAddress(Location $objLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLocationAsAddress on this unsaved Address.');
			if ((is_null($objLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLocationAsAddress on this Address with an unsaved Location.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_location`
				SET
					`address_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objLocation->Id) . ' AND
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all LocationsAsAddress
		 * @return void
		*/ 
		public function UnassociateAllLocationsAsAddress() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLocationAsAddress on this unsaved Address.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_location`
				SET
					`address_id` = null
				WHERE
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated LocationAsAddress
		 * @param Location $objLocation
		 * @return void
		*/ 
		public function DeleteAssociatedLocationAsAddress(Location $objLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLocationAsAddress on this unsaved Address.');
			if ((is_null($objLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLocationAsAddress on this Address with an unsaved Location.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_location`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objLocation->Id) . ' AND
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated LocationsAsAddress
		 * @return void
		*/ 
		public function DeleteAllLocationsAsAddress() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateLocationAsAddress on this unsaved Address.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_location`
				WHERE
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for OrganisationAsAddress
		//-------------------------------------------------------------------

		/**
		 * Gets all associated OrganisationsAsAddress as an array of Organisation objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Organisation[]
		*/ 
		public function GetOrganisationAsAddressArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Organisation::LoadArrayByAddressId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated OrganisationsAsAddress
		 * @return int
		*/ 
		public function CountOrganisationsAsAddress() {
			if ((is_null($this->strId)))
				return 0;

			return Organisation::CountByAddressId($this->strId);
		}

		/**
		 * Associates a OrganisationAsAddress
		 * @param Organisation $objOrganisation
		 * @return void
		*/ 
		public function AssociateOrganisationAsAddress(Organisation $objOrganisation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateOrganisationAsAddress on this unsaved Address.');
			if ((is_null($objOrganisation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateOrganisationAsAddress on this Address with an unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_organisation`
				SET
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objOrganisation->Id) . '
			');
		}

		/**
		 * Unassociates a OrganisationAsAddress
		 * @param Organisation $objOrganisation
		 * @return void
		*/ 
		public function UnassociateOrganisationAsAddress(Organisation $objOrganisation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsAddress on this unsaved Address.');
			if ((is_null($objOrganisation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsAddress on this Address with an unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_organisation`
				SET
					`address_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objOrganisation->Id) . ' AND
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all OrganisationsAsAddress
		 * @return void
		*/ 
		public function UnassociateAllOrganisationsAsAddress() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsAddress on this unsaved Address.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_organisation`
				SET
					`address_id` = null
				WHERE
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated OrganisationAsAddress
		 * @param Organisation $objOrganisation
		 * @return void
		*/ 
		public function DeleteAssociatedOrganisationAsAddress(Organisation $objOrganisation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsAddress on this unsaved Address.');
			if ((is_null($objOrganisation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsAddress on this Address with an unsaved Organisation.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_organisation`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objOrganisation->Id) . ' AND
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated OrganisationsAsAddress
		 * @return void
		*/ 
		public function DeleteAllOrganisationsAsAddress() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateOrganisationAsAddress on this unsaved Address.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_organisation`
				WHERE
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for PersonAsAddress
		//-------------------------------------------------------------------

		/**
		 * Gets all associated PeopleAsAddress as an array of Person objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Person[]
		*/ 
		public function GetPersonAsAddressArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Person::LoadArrayByAddressId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated PeopleAsAddress
		 * @return int
		*/ 
		public function CountPeopleAsAddress() {
			if ((is_null($this->strId)))
				return 0;

			return Person::CountByAddressId($this->strId);
		}

		/**
		 * Associates a PersonAsAddress
		 * @param Person $objPerson
		 * @return void
		*/ 
		public function AssociatePersonAsAddress(Person $objPerson) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociatePersonAsAddress on this unsaved Address.');
			if ((is_null($objPerson->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociatePersonAsAddress on this Address with an unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_person`
				SET
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objPerson->Id) . '
			');
		}

		/**
		 * Unassociates a PersonAsAddress
		 * @param Person $objPerson
		 * @return void
		*/ 
		public function UnassociatePersonAsAddress(Person $objPerson) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociatePersonAsAddress on this unsaved Address.');
			if ((is_null($objPerson->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociatePersonAsAddress on this Address with an unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_person`
				SET
					`address_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objPerson->Id) . ' AND
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all PeopleAsAddress
		 * @return void
		*/ 
		public function UnassociateAllPeopleAsAddress() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociatePersonAsAddress on this unsaved Address.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_person`
				SET
					`address_id` = null
				WHERE
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated PersonAsAddress
		 * @param Person $objPerson
		 * @return void
		*/ 
		public function DeleteAssociatedPersonAsAddress(Person $objPerson) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociatePersonAsAddress on this unsaved Address.');
			if ((is_null($objPerson->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociatePersonAsAddress on this Address with an unsaved Person.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_person`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objPerson->Id) . ' AND
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated PeopleAsAddress
		 * @return void
		*/ 
		public function DeleteAllPeopleAsAddress() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociatePersonAsAddress on this unsaved Address.');

			// Get the Database Object for this Class
			$objDatabase = Address::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_person`
				WHERE
					`address_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Address"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Street" type="xsd:string"/>';
			$strToReturn .= '<element name="Code" type="xsd:string"/>';
			$strToReturn .= '<element name="Town" type="xsd:string"/>';
			$strToReturn .= '<element name="Country" type="xsd:string"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Address', $strComplexTypeArray)) {
				$strComplexTypeArray['Address'] = Address::GetSoapComplexTypeXml();
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Address::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Address();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if (property_exists($objSoapObject, 'Street'))
				$objToReturn->strStreet = $objSoapObject->Street;
			if (property_exists($objSoapObject, 'Code'))
				$objToReturn->strCode = $objSoapObject->Code;
			if (property_exists($objSoapObject, 'Town'))
				$objToReturn->strTown = $objSoapObject->Town;
			if (property_exists($objSoapObject, 'Country'))
				$objToReturn->strCountry = $objSoapObject->Country;
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Address::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeAddress extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Address';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'address';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Street':
					return new QQNode('street', 'Street', 'string', $this);
				case 'Code':
					return new QQNode('code', 'Code', 'string', $this);
				case 'Town':
					return new QQNode('town', 'Town', 'string', $this);
				case 'Country':
					return new QQNode('country', 'Country', 'string', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'LocationAsAddress':
					return new QQReverseReferenceNodeLocation($this, 'locationasaddress', 'reverse_reference', 'address_id');
				case 'OrganisationAsAddress':
					return new QQReverseReferenceNodeOrganisation($this, 'organisationasaddress', 'reverse_reference', 'address_id');
				case 'PersonAsAddress':
					return new QQReverseReferenceNodePerson($this, 'personasaddress', 'reverse_reference', 'address_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeAddress extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Address';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'address';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Street':
					return new QQNode('street', 'Street', 'string', $this);
				case 'Code':
					return new QQNode('code', 'Code', 'string', $this);
				case 'Town':
					return new QQNode('town', 'Town', 'string', $this);
				case 'Country':
					return new QQNode('country', 'Country', 'string', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'LocationAsAddress':
					return new QQReverseReferenceNodeLocation($this, 'locationasaddress', 'reverse_reference', 'address_id');
				case 'OrganisationAsAddress':
					return new QQReverseReferenceNodeOrganisation($this, 'organisationasaddress', 'reverse_reference', 'address_id');
				case 'PersonAsAddress':
					return new QQReverseReferenceNodePerson($this, 'personasaddress', 'reverse_reference', 'address_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
<?php
	/**
	 * This is the abstract Panel class for the List All functionality
	 * of the Camera class.  This code-generated class
	 * contains a datagrid to display an HTML page that can
	 * list a collection of Camera objects.  It includes
	 * functionality to perform pagination and sorting on columns.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QPanel which extends this CameraListPanelBase
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent re-
	 * code generation.
	 * 
	 * @package Spidio
	 * @subpackage Drafts
	 * 
	 */
	class CameraListPanel extends QPanel {
		// Local instance of the Meta DataGrid to list Cameras
		public $dtgCameras;

		// Other public QControls in this panel
		public $btnCreateNew;
		public $pxyEdit;

		// Callback Method Names
		protected $strSetEditPanelMethod;
		protected $strCloseEditPanelMethod;
		
		public function __construct($objParentObject, $strSetEditPanelMethod, $strCloseEditPanelMethod, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Record Method Callbacks
			$this->strSetEditPanelMethod = $strSetEditPanelMethod;
			$this->strCloseEditPanelMethod = $strCloseEditPanelMethod;

			// Setup the Template
			$this->Template = 'CameraListPanel.tpl.php';

			// Instantiate the Meta DataGrid
			$this->dtgCameras = new CameraDataGrid($this);

			// Style the DataGrid (if desired)
			$this->dtgCameras->CssClass = 'datagrid';
			$this->dtgCameras->AlternateRowStyle->CssClass = 'alternate';

			// Add Pagination (if desired)
			$this->dtgCameras->Paginator = new QPaginator($this->dtgCameras);
			$this->dtgCameras->ItemsPerPage = 8;

			// Use the MetaDataGrid functionality to add Columns for this datagrid

			// Create an Edit Column
			$this->pxyEdit = new QControlProxy($this);
			$this->pxyEdit->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'pxyEdit_Click'));
			$this->dtgCameras->MetaAddEditProxyColumn($this->pxyEdit, 'Edit', 'Edit');

			// Create the Other Columns (note that you can use strings for ' . DB_TABLE_PREFIX_1 . 'camera's properties, or you
			// can traverse down QQN::' . DB_TABLE_PREFIX_1 . 'camera() to display fields that are down the hierarchy)
			$this->dtgCameras->MetaAddColumn('Id');
			$this->dtgCameras->MetaAddColumn(QQN::Camera()->Organisation);
			$this->dtgCameras->MetaAddColumn('Brand');
			$this->dtgCameras->MetaAddColumn('Model');
			$this->dtgCameras->MetaAddColumn('Serial');
			$this->dtgCameras->MetaAddColumn('Comment');
			$this->dtgCameras->MetaAddColumn('Optlock');
			$this->dtgCameras->MetaAddColumn('Owner');
			$this->dtgCameras->MetaAddColumn('Group');
			$this->dtgCameras->MetaAddColumn('Perms');
			$this->dtgCameras->MetaAddTypeColumn('Status', 'AuthStatusEnum');

			// Setup the Create New button
			$this->btnCreateNew = new QButton($this);
			$this->btnCreateNew->Text = QApplication::Translate('Create a New') . ' ' . QApplication::Translate('Camera');
			$this->btnCreateNew->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCreateNew_Click'));
		}

		public function pxyEdit_Click($strFormId, $strControlId, $strParameter) {
			$strParameterArray = explode(',', $strParameter);
			$objEditPanel = new CameraEditPanel($this, $this->strCloseEditPanelMethod, $strParameterArray[0]);

			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}

		public function btnCreateNew_Click($strFormId, $strControlId, $strParameter) {
			$objEditPanel = new CameraEditPanel($this, $this->strCloseEditPanelMethod, null);
			$strMethodName = $this->strSetEditPanelMethod;
			$this->objForm->$strMethodName($objEditPanel);
		}
	}
?>
<?php

if (!defined('RUNNING_SOAPIDIO')) {
	define('RUNNING_SOAPIDIO', true);
}

// Include prepend.inc to load Qcodo
require('./../includes/prepend.inc.php');

// Update version information if needed
QApplication::UpdateVersionInfo();

// Set error_reporting()
error_reporting(E_ALL);

// Set correct error handling for Soapidio.
restore_error_handler();
restore_exception_handler();
use_soap_error_handler(true);

?>
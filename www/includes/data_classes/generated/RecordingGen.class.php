<?php
	/**
	 * The abstract RecordingGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Recording subclass which
	 * extends this RecordingGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Recording class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $ProjectId the value for strProjectId (Not Null)
	 * @property string $TapeId the value for strTapeId (Not Null)
	 * @property string $CameraId the value for strCameraId (Not Null)
	 * @property string $CameraPurposeId the value for strCameraPurposeId (Not Null)
	 * @property boolean $TapeModeEnumId the value for blnTapeModeEnumId (Not Null)
	 * @property string $RecordingTypeId the value for strRecordingTypeId (Not Null)
	 * @property integer $Start the value for intStart (Not Null)
	 * @property integer $End the value for intEnd (Not Null)
	 * @property boolean $Widescreen the value for blnWidescreen (Not Null)
	 * @property QDateTime $Date the value for dttDate (Not Null)
	 * @property integer $Scene the value for intScene (Not Null)
	 * @property integer $Take the value for intTake (Not Null)
	 * @property integer $Angle the value for intAngle (Not Null)
	 * @property QDateTime $Added the value for dttAdded (Not Null)
	 * @property QDateTime $Changed the value for dttChanged (Not Null)
	 * @property string $Comment the value for strComment 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Project $Project the value for the Project object referenced by strProjectId (Not Null)
	 * @property Tape $Tape the value for the Tape object referenced by strTapeId (Not Null)
	 * @property Camera $Camera the value for the Camera object referenced by strCameraId (Not Null)
	 * @property CameraPurpose $CameraPurpose the value for the CameraPurpose object referenced by strCameraPurposeId (Not Null)
	 * @property RecordingType $RecordingType the value for the RecordingType object referenced by strRecordingTypeId (Not Null)
	 * @property-read File $_FileAsRecording the value for the private _objFileAsRecording (Read-Only) if set due to an expansion on the spidio_file.recording_id reverse relationship
	 * @property-read File[] $_FileAsRecordingArray the value for the private _objFileAsRecordingArray (Read-Only) if set due to an ExpandAsArray on the spidio_file.recording_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class RecordingGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_recording.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_recording.project_id
		 * @var string strProjectId
		 */
		protected $strProjectId;
		const ProjectIdMaxLength = 36;
		const ProjectIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.tape_id
		 * @var string strTapeId
		 */
		protected $strTapeId;
		const TapeIdMaxLength = 36;
		const TapeIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.camera_id
		 * @var string strCameraId
		 */
		protected $strCameraId;
		const CameraIdMaxLength = 36;
		const CameraIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.camera_purpose_id
		 * @var string strCameraPurposeId
		 */
		protected $strCameraPurposeId;
		const CameraPurposeIdMaxLength = 36;
		const CameraPurposeIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.tape_mode_enum_id
		 * @var boolean blnTapeModeEnumId
		 */
		protected $blnTapeModeEnumId;
		const TapeModeEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.recording_type_id
		 * @var string strRecordingTypeId
		 */
		protected $strRecordingTypeId;
		const RecordingTypeIdMaxLength = 36;
		const RecordingTypeIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.start
		 * @var integer intStart
		 */
		protected $intStart;
		const StartDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.end
		 * @var integer intEnd
		 */
		protected $intEnd;
		const EndDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.widescreen
		 * @var boolean blnWidescreen
		 */
		protected $blnWidescreen;
		const WidescreenDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.date
		 * @var QDateTime dttDate
		 */
		protected $dttDate;
		const DateDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.scene
		 * @var integer intScene
		 */
		protected $intScene;
		const SceneDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.take
		 * @var integer intTake
		 */
		protected $intTake;
		const TakeDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.angle
		 * @var integer intAngle
		 */
		protected $intAngle;
		const AngleDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.added
		 * @var QDateTime dttAdded
		 */
		protected $dttAdded;
		const AddedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.changed
		 * @var QDateTime dttChanged
		 */
		protected $dttChanged;
		const ChangedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_recording.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single FileAsRecording object
		 * (of type File), if this Recording object was restored with
		 * an expansion on the spidio_file association table.
		 * @var File _objFileAsRecording;
		 */
		private $_objFileAsRecording;

		/**
		 * Private member variable that stores a reference to an array of FileAsRecording objects
		 * (of type File[]), if this Recording object was restored with
		 * an ExpandAsArray on the spidio_file association table.
		 * @var File[] _objFileAsRecordingArray;
		 */
		private $_objFileAsRecordingArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_recording.project_id.
		 *
		 * NOTE: Always use the Project property getter to correctly retrieve this Project object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Project objProject
		 */
		protected $objProject;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_recording.tape_id.
		 *
		 * NOTE: Always use the Tape property getter to correctly retrieve this Tape object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Tape objTape
		 */
		protected $objTape;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_recording.camera_id.
		 *
		 * NOTE: Always use the Camera property getter to correctly retrieve this Camera object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Camera objCamera
		 */
		protected $objCamera;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_recording.camera_purpose_id.
		 *
		 * NOTE: Always use the CameraPurpose property getter to correctly retrieve this CameraPurpose object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var CameraPurpose objCameraPurpose
		 */
		protected $objCameraPurpose;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_recording.recording_type_id.
		 *
		 * NOTE: Always use the RecordingType property getter to correctly retrieve this RecordingType object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var RecordingType objRecordingType
		 */
		protected $objRecordingType;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Recording from PK Info
		 * @param string $strId
		 * @return Recording
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Recording::QuerySingle(
				QQ::Equal(QQN::Recording()->Id, $strId)
			);
		}

		/**
		 * Load all Recordings
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadAll query
			try {
				return Recording::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Recordings
		 * @return int
		 */
		public static function CountAll() {
			// Call Recording::QueryCount to perform the CountAll query
			return Recording::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Create/Build out the QueryBuilder object with Recording-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'recording');
			Recording::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'recording');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Recording object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Recording the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Recording::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Recording object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Recording::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Recording objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Recording[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Recording::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Recording::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Recording objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Recording::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'recording_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Recording-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Recording::GetSelectFields($objQueryBuilder);
				Recording::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Recording::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Recording
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'recording';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'project_id', $strAliasPrefix . 'project_id');
			$objBuilder->AddSelectItem($strTableName, 'tape_id', $strAliasPrefix . 'tape_id');
			$objBuilder->AddSelectItem($strTableName, 'camera_id', $strAliasPrefix . 'camera_id');
			$objBuilder->AddSelectItem($strTableName, 'camera_purpose_id', $strAliasPrefix . 'camera_purpose_id');
			$objBuilder->AddSelectItem($strTableName, 'tape_mode_enum_id', $strAliasPrefix . 'tape_mode_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'recording_type_id', $strAliasPrefix . 'recording_type_id');
			$objBuilder->AddSelectItem($strTableName, 'start', $strAliasPrefix . 'start');
			$objBuilder->AddSelectItem($strTableName, 'end', $strAliasPrefix . 'end');
			$objBuilder->AddSelectItem($strTableName, 'widescreen', $strAliasPrefix . 'widescreen');
			$objBuilder->AddSelectItem($strTableName, 'date', $strAliasPrefix . 'date');
			$objBuilder->AddSelectItem($strTableName, 'scene', $strAliasPrefix . 'scene');
			$objBuilder->AddSelectItem($strTableName, 'take', $strAliasPrefix . 'take');
			$objBuilder->AddSelectItem($strTableName, 'angle', $strAliasPrefix . 'angle');
			$objBuilder->AddSelectItem($strTableName, 'added', $strAliasPrefix . 'added');
			$objBuilder->AddSelectItem($strTableName, 'changed', $strAliasPrefix . 'changed');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Recording from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Recording::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Recording
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'recording__';


				$strAlias = $strAliasPrefix . 'fileasrecording__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objFileAsRecordingArray)) {
						$objPreviousChildItem = $objPreviousItem->_objFileAsRecordingArray[$intPreviousChildItemCount - 1];
						$objChildItem = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasrecording__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objFileAsRecordingArray[] = $objChildItem;
					} else
						$objPreviousItem->_objFileAsRecordingArray[] = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasrecording__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'recording__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the Recording object
			$objToReturn = new Recording();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'project_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'project_id'] : $strAliasPrefix . 'project_id';
			$objToReturn->strProjectId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'tape_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'tape_id'] : $strAliasPrefix . 'tape_id';
			$objToReturn->strTapeId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'camera_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'camera_id'] : $strAliasPrefix . 'camera_id';
			$objToReturn->strCameraId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'camera_purpose_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'camera_purpose_id'] : $strAliasPrefix . 'camera_purpose_id';
			$objToReturn->strCameraPurposeId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'tape_mode_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'tape_mode_enum_id'] : $strAliasPrefix . 'tape_mode_enum_id';
			$objToReturn->blnTapeModeEnumId = $objDbRow->GetColumn($strAliasName, 'Bit');
			$strAliasName = array_key_exists($strAliasPrefix . 'recording_type_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'recording_type_id'] : $strAliasPrefix . 'recording_type_id';
			$objToReturn->strRecordingTypeId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'start', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'start'] : $strAliasPrefix . 'start';
			$objToReturn->intStart = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'end', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'end'] : $strAliasPrefix . 'end';
			$objToReturn->intEnd = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'widescreen', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'widescreen'] : $strAliasPrefix . 'widescreen';
			$objToReturn->blnWidescreen = $objDbRow->GetColumn($strAliasName, 'Bit');
			$strAliasName = array_key_exists($strAliasPrefix . 'date', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'date'] : $strAliasPrefix . 'date';
			$objToReturn->dttDate = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'scene', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'scene'] : $strAliasPrefix . 'scene';
			$objToReturn->intScene = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'take', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'take'] : $strAliasPrefix . 'take';
			$objToReturn->intTake = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'angle', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'angle'] : $strAliasPrefix . 'angle';
			$objToReturn->intAngle = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'added', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'added'] : $strAliasPrefix . 'added';
			$objToReturn->dttAdded = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'changed', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'changed'] : $strAliasPrefix . 'changed';
			$objToReturn->dttChanged = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'recording__';

			// Check for Project Early Binding
			$strAlias = $strAliasPrefix . 'project_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objProject = Project::InstantiateDbRow($objDbRow, $strAliasPrefix . 'project_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Tape Early Binding
			$strAlias = $strAliasPrefix . 'tape_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objTape = Tape::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tape_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Camera Early Binding
			$strAlias = $strAliasPrefix . 'camera_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objCamera = Camera::InstantiateDbRow($objDbRow, $strAliasPrefix . 'camera_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for CameraPurpose Early Binding
			$strAlias = $strAliasPrefix . 'camera_purpose_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objCameraPurpose = CameraPurpose::InstantiateDbRow($objDbRow, $strAliasPrefix . 'camera_purpose_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for RecordingType Early Binding
			$strAlias = $strAliasPrefix . 'recording_type_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objRecordingType = RecordingType::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recording_type_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			// Check for FileAsRecording Virtual Binding
			$strAlias = $strAliasPrefix . 'fileasrecording__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objFileAsRecordingArray[] = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasrecording__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objFileAsRecording = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasrecording__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of Recordings from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Recording[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Recording::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Recording::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Recording object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Recording
		*/
		public static function LoadById($strId) {
			return Recording::QuerySingle(
				QQ::Equal(QQN::Recording()->Id, $strId)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by TapeId Index(es)
		 * @param string $strTapeId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByTapeId($strTapeId, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByTapeId query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->TapeId, $strTapeId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by TapeId Index(es)
		 * @param string $strTapeId
		 * @return int
		*/
		public static function CountByTapeId($strTapeId) {
			// Call Recording::QueryCount to perform the CountByTapeId query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->TapeId, $strTapeId)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by ProjectId Index(es)
		 * @param string $strProjectId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByProjectId($strProjectId, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByProjectId query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->ProjectId, $strProjectId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by ProjectId Index(es)
		 * @param string $strProjectId
		 * @return int
		*/
		public static function CountByProjectId($strProjectId) {
			// Call Recording::QueryCount to perform the CountByProjectId query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->ProjectId, $strProjectId)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by CameraId Index(es)
		 * @param string $strCameraId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByCameraId($strCameraId, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByCameraId query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->CameraId, $strCameraId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by CameraId Index(es)
		 * @param string $strCameraId
		 * @return int
		*/
		public static function CountByCameraId($strCameraId) {
			// Call Recording::QueryCount to perform the CountByCameraId query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->CameraId, $strCameraId)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by CameraPurposeId Index(es)
		 * @param string $strCameraPurposeId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByCameraPurposeId($strCameraPurposeId, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByCameraPurposeId query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->CameraPurposeId, $strCameraPurposeId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by CameraPurposeId Index(es)
		 * @param string $strCameraPurposeId
		 * @return int
		*/
		public static function CountByCameraPurposeId($strCameraPurposeId) {
			// Call Recording::QueryCount to perform the CountByCameraPurposeId query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->CameraPurposeId, $strCameraPurposeId)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByOwner query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call Recording::QueryCount to perform the CountByOwner query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByStatus query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call Recording::QueryCount to perform the CountByStatus query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->Status, $intStatus)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by TapeModeEnumId Index(es)
		 * @param boolean $blnTapeModeEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByTapeModeEnumId($blnTapeModeEnumId, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByTapeModeEnumId query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->TapeModeEnumId, $blnTapeModeEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by TapeModeEnumId Index(es)
		 * @param boolean $blnTapeModeEnumId
		 * @return int
		*/
		public static function CountByTapeModeEnumId($blnTapeModeEnumId) {
			// Call Recording::QueryCount to perform the CountByTapeModeEnumId query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->TapeModeEnumId, $blnTapeModeEnumId)
			);
		}
			
		/**
		 * Load an array of Recording objects,
		 * by RecordingTypeId Index(es)
		 * @param string $strRecordingTypeId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/
		public static function LoadArrayByRecordingTypeId($strRecordingTypeId, $objOptionalClauses = null) {
			// Call Recording::QueryArray to perform the LoadArrayByRecordingTypeId query
			try {
				return Recording::QueryArray(
					QQ::Equal(QQN::Recording()->RecordingTypeId, $strRecordingTypeId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Recordings
		 * by RecordingTypeId Index(es)
		 * @param string $strRecordingTypeId
		 * @return int
		*/
		public static function CountByRecordingTypeId($strRecordingTypeId) {
			// Call Recording::QueryCount to perform the CountByRecordingTypeId query
			return Recording::QueryCount(
				QQ::Equal(QQN::Recording()->RecordingTypeId, $strRecordingTypeId)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Recording
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'recording` (
							`id`,
							`project_id`,
							`tape_id`,
							`camera_id`,
							`camera_purpose_id`,
							`tape_mode_enum_id`,
							`recording_type_id`,
							`start`,
							`end`,
							`widescreen`,
							`date`,
							`scene`,
							`take`,
							`angle`,
							`added`,
							`changed`,
							`comment`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strProjectId) . ',
							' . $objDatabase->SqlVariable($this->strTapeId) . ',
							' . $objDatabase->SqlVariable($this->strCameraId) . ',
							' . $objDatabase->SqlVariable($this->strCameraPurposeId) . ',
							' . $objDatabase->SqlVariable($this->blnTapeModeEnumId) . ',
							' . $objDatabase->SqlVariable($this->strRecordingTypeId) . ',
							' . $objDatabase->SqlVariable($this->intStart) . ',
							' . $objDatabase->SqlVariable($this->intEnd) . ',
							' . $objDatabase->SqlVariable($this->blnWidescreen) . ',
							' . $objDatabase->SqlVariable($this->dttDate) . ',
							' . $objDatabase->SqlVariable($this->intScene) . ',
							' . $objDatabase->SqlVariable($this->intTake) . ',
							' . $objDatabase->SqlVariable($this->intAngle) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'recording`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Recording');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'recording`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`project_id` = ' . $objDatabase->SqlVariable($this->strProjectId) . ',
							`tape_id` = ' . $objDatabase->SqlVariable($this->strTapeId) . ',
							`camera_id` = ' . $objDatabase->SqlVariable($this->strCameraId) . ',
							`camera_purpose_id` = ' . $objDatabase->SqlVariable($this->strCameraPurposeId) . ',
							`tape_mode_enum_id` = ' . $objDatabase->SqlVariable($this->blnTapeModeEnumId) . ',
							`recording_type_id` = ' . $objDatabase->SqlVariable($this->strRecordingTypeId) . ',
							`start` = ' . $objDatabase->SqlVariable($this->intStart) . ',
							`end` = ' . $objDatabase->SqlVariable($this->intEnd) . ',
							`widescreen` = ' . $objDatabase->SqlVariable($this->blnWidescreen) . ',
							`date` = ' . $objDatabase->SqlVariable($this->dttDate) . ',
							`scene` = ' . $objDatabase->SqlVariable($this->intScene) . ',
							`take` = ' . $objDatabase->SqlVariable($this->intTake) . ',
							`angle` = ' . $objDatabase->SqlVariable($this->intAngle) . ',
							`added` = ' . $objDatabase->SqlVariable($this->dttAdded) . ',
							`changed` = ' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'recording`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Recording
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Recording with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'recording`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Recordings
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'recording`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'recording table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'recording`');
		}

		/**
		 * Reload this Recording from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Recording object.');

			// Reload the Object
			$objReloaded = Recording::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->ProjectId = $objReloaded->ProjectId;
			$this->TapeId = $objReloaded->TapeId;
			$this->CameraId = $objReloaded->CameraId;
			$this->CameraPurposeId = $objReloaded->CameraPurposeId;
			$this->TapeModeEnumId = $objReloaded->TapeModeEnumId;
			$this->RecordingTypeId = $objReloaded->RecordingTypeId;
			$this->intStart = $objReloaded->intStart;
			$this->intEnd = $objReloaded->intEnd;
			$this->blnWidescreen = $objReloaded->blnWidescreen;
			$this->dttDate = $objReloaded->dttDate;
			$this->intScene = $objReloaded->intScene;
			$this->intTake = $objReloaded->intTake;
			$this->intAngle = $objReloaded->intAngle;
			$this->dttAdded = $objReloaded->dttAdded;
			$this->dttChanged = $objReloaded->dttChanged;
			$this->strComment = $objReloaded->strComment;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'ProjectId':
					/**
					 * Gets the value for strProjectId (Not Null)
					 * @return string
					 */
					return $this->strProjectId;

				case 'TapeId':
					/**
					 * Gets the value for strTapeId (Not Null)
					 * @return string
					 */
					return $this->strTapeId;

				case 'CameraId':
					/**
					 * Gets the value for strCameraId (Not Null)
					 * @return string
					 */
					return $this->strCameraId;

				case 'CameraPurposeId':
					/**
					 * Gets the value for strCameraPurposeId (Not Null)
					 * @return string
					 */
					return $this->strCameraPurposeId;

				case 'TapeModeEnumId':
					/**
					 * Gets the value for blnTapeModeEnumId (Not Null)
					 * @return boolean
					 */
					return $this->blnTapeModeEnumId;

				case 'RecordingTypeId':
					/**
					 * Gets the value for strRecordingTypeId (Not Null)
					 * @return string
					 */
					return $this->strRecordingTypeId;

				case 'Start':
					/**
					 * Gets the value for intStart (Not Null)
					 * @return integer
					 */
					return $this->intStart;

				case 'End':
					/**
					 * Gets the value for intEnd (Not Null)
					 * @return integer
					 */
					return $this->intEnd;

				case 'Widescreen':
					/**
					 * Gets the value for blnWidescreen (Not Null)
					 * @return boolean
					 */
					return $this->blnWidescreen;

				case 'Date':
					/**
					 * Gets the value for dttDate (Not Null)
					 * @return QDateTime
					 */
					return $this->dttDate;

				case 'Scene':
					/**
					 * Gets the value for intScene (Not Null)
					 * @return integer
					 */
					return $this->intScene;

				case 'Take':
					/**
					 * Gets the value for intTake (Not Null)
					 * @return integer
					 */
					return $this->intTake;

				case 'Angle':
					/**
					 * Gets the value for intAngle (Not Null)
					 * @return integer
					 */
					return $this->intAngle;

				case 'Added':
					/**
					 * Gets the value for dttAdded (Not Null)
					 * @return QDateTime
					 */
					return $this->dttAdded;

				case 'Changed':
					/**
					 * Gets the value for dttChanged (Not Null)
					 * @return QDateTime
					 */
					return $this->dttChanged;

				case 'Comment':
					/**
					 * Gets the value for strComment 
					 * @return string
					 */
					return $this->strComment;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Project':
					/**
					 * Gets the value for the Project object referenced by strProjectId (Not Null)
					 * @return Project
					 */
					try {
						if ((!$this->objProject) && (!is_null($this->strProjectId)))
							$this->objProject = Project::Load($this->strProjectId);
						return $this->objProject;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Tape':
					/**
					 * Gets the value for the Tape object referenced by strTapeId (Not Null)
					 * @return Tape
					 */
					try {
						if ((!$this->objTape) && (!is_null($this->strTapeId)))
							$this->objTape = Tape::Load($this->strTapeId);
						return $this->objTape;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Camera':
					/**
					 * Gets the value for the Camera object referenced by strCameraId (Not Null)
					 * @return Camera
					 */
					try {
						if ((!$this->objCamera) && (!is_null($this->strCameraId)))
							$this->objCamera = Camera::Load($this->strCameraId);
						return $this->objCamera;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CameraPurpose':
					/**
					 * Gets the value for the CameraPurpose object referenced by strCameraPurposeId (Not Null)
					 * @return CameraPurpose
					 */
					try {
						if ((!$this->objCameraPurpose) && (!is_null($this->strCameraPurposeId)))
							$this->objCameraPurpose = CameraPurpose::Load($this->strCameraPurposeId);
						return $this->objCameraPurpose;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'RecordingType':
					/**
					 * Gets the value for the RecordingType object referenced by strRecordingTypeId (Not Null)
					 * @return RecordingType
					 */
					try {
						if ((!$this->objRecordingType) && (!is_null($this->strRecordingTypeId)))
							$this->objRecordingType = RecordingType::Load($this->strRecordingTypeId);
						return $this->objRecordingType;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_FileAsRecording':
					/**
					 * Gets the value for the private _objFileAsRecording (Read-Only)
					 * if set due to an expansion on the spidio_file.recording_id reverse relationship
					 * @return File
					 */
					return $this->_objFileAsRecording;

				case '_FileAsRecordingArray':
					/**
					 * Gets the value for the private _objFileAsRecordingArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_file.recording_id reverse relationship
					 * @return File[]
					 */
					return (array) $this->_objFileAsRecordingArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'ProjectId':
					/**
					 * Sets the value for strProjectId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objProject = null;
						return ($this->strProjectId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'TapeId':
					/**
					 * Sets the value for strTapeId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objTape = null;
						return ($this->strTapeId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CameraId':
					/**
					 * Sets the value for strCameraId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objCamera = null;
						return ($this->strCameraId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'CameraPurposeId':
					/**
					 * Sets the value for strCameraPurposeId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objCameraPurpose = null;
						return ($this->strCameraPurposeId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'TapeModeEnumId':
					/**
					 * Sets the value for blnTapeModeEnumId (Not Null)
					 * @param boolean $mixValue
					 * @return boolean
					 */
					try {
						return ($this->blnTapeModeEnumId = QType::Cast($mixValue, QType::Boolean));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'RecordingTypeId':
					/**
					 * Sets the value for strRecordingTypeId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objRecordingType = null;
						return ($this->strRecordingTypeId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Start':
					/**
					 * Sets the value for intStart (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStart = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'End':
					/**
					 * Sets the value for intEnd (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intEnd = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Widescreen':
					/**
					 * Sets the value for blnWidescreen (Not Null)
					 * @param boolean $mixValue
					 * @return boolean
					 */
					try {
						return ($this->blnWidescreen = QType::Cast($mixValue, QType::Boolean));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Date':
					/**
					 * Sets the value for dttDate (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttDate = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Scene':
					/**
					 * Sets the value for intScene (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intScene = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Take':
					/**
					 * Sets the value for intTake (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intTake = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Angle':
					/**
					 * Sets the value for intAngle (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intAngle = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Added':
					/**
					 * Sets the value for dttAdded (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttAdded = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Changed':
					/**
					 * Sets the value for dttChanged (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttChanged = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Project':
					/**
					 * Sets the value for the Project object referenced by strProjectId (Not Null)
					 * @param Project $mixValue
					 * @return Project
					 */
					if (is_null($mixValue)) {
						$this->strProjectId = null;
						$this->objProject = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Project object
						try {
							$mixValue = QType::Cast($mixValue, 'Project');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Project object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Project for this Recording');

						// Update Local Member Variables
						$this->objProject = $mixValue;
						$this->strProjectId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Tape':
					/**
					 * Sets the value for the Tape object referenced by strTapeId (Not Null)
					 * @param Tape $mixValue
					 * @return Tape
					 */
					if (is_null($mixValue)) {
						$this->strTapeId = null;
						$this->objTape = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Tape object
						try {
							$mixValue = QType::Cast($mixValue, 'Tape');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Tape object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Tape for this Recording');

						// Update Local Member Variables
						$this->objTape = $mixValue;
						$this->strTapeId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Camera':
					/**
					 * Sets the value for the Camera object referenced by strCameraId (Not Null)
					 * @param Camera $mixValue
					 * @return Camera
					 */
					if (is_null($mixValue)) {
						$this->strCameraId = null;
						$this->objCamera = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Camera object
						try {
							$mixValue = QType::Cast($mixValue, 'Camera');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Camera object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Camera for this Recording');

						// Update Local Member Variables
						$this->objCamera = $mixValue;
						$this->strCameraId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'CameraPurpose':
					/**
					 * Sets the value for the CameraPurpose object referenced by strCameraPurposeId (Not Null)
					 * @param CameraPurpose $mixValue
					 * @return CameraPurpose
					 */
					if (is_null($mixValue)) {
						$this->strCameraPurposeId = null;
						$this->objCameraPurpose = null;
						return null;
					} else {
						// Make sure $mixValue actually is a CameraPurpose object
						try {
							$mixValue = QType::Cast($mixValue, 'CameraPurpose');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED CameraPurpose object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved CameraPurpose for this Recording');

						// Update Local Member Variables
						$this->objCameraPurpose = $mixValue;
						$this->strCameraPurposeId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'RecordingType':
					/**
					 * Sets the value for the RecordingType object referenced by strRecordingTypeId (Not Null)
					 * @param RecordingType $mixValue
					 * @return RecordingType
					 */
					if (is_null($mixValue)) {
						$this->strRecordingTypeId = null;
						$this->objRecordingType = null;
						return null;
					} else {
						// Make sure $mixValue actually is a RecordingType object
						try {
							$mixValue = QType::Cast($mixValue, 'RecordingType');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED RecordingType object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved RecordingType for this Recording');

						// Update Local Member Variables
						$this->objRecordingType = $mixValue;
						$this->strRecordingTypeId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for FileAsRecording
		//-------------------------------------------------------------------

		/**
		 * Gets all associated FilesAsRecording as an array of File objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/ 
		public function GetFileAsRecordingArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return File::LoadArrayByRecordingId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated FilesAsRecording
		 * @return int
		*/ 
		public function CountFilesAsRecording() {
			if ((is_null($this->strId)))
				return 0;

			return File::CountByRecordingId($this->strId);
		}

		/**
		 * Associates a FileAsRecording
		 * @param File $objFile
		 * @return void
		*/ 
		public function AssociateFileAsRecording(File $objFile) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateFileAsRecording on this unsaved Recording.');
			if ((is_null($objFile->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateFileAsRecording on this Recording with an unsaved File.');

			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_file`
				SET
					`recording_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objFile->Id) . '
			');
		}

		/**
		 * Unassociates a FileAsRecording
		 * @param File $objFile
		 * @return void
		*/ 
		public function UnassociateFileAsRecording(File $objFile) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsRecording on this unsaved Recording.');
			if ((is_null($objFile->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsRecording on this Recording with an unsaved File.');

			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_file`
				SET
					`recording_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objFile->Id) . ' AND
					`recording_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all FilesAsRecording
		 * @return void
		*/ 
		public function UnassociateAllFilesAsRecording() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsRecording on this unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_file`
				SET
					`recording_id` = null
				WHERE
					`recording_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated FileAsRecording
		 * @param File $objFile
		 * @return void
		*/ 
		public function DeleteAssociatedFileAsRecording(File $objFile) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsRecording on this unsaved Recording.');
			if ((is_null($objFile->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsRecording on this Recording with an unsaved File.');

			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_file`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objFile->Id) . ' AND
					`recording_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated FilesAsRecording
		 * @return void
		*/ 
		public function DeleteAllFilesAsRecording() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsRecording on this unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Recording::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_file`
				WHERE
					`recording_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Recording"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Project" type="xsd1:Project"/>';
			$strToReturn .= '<element name="Tape" type="xsd1:Tape"/>';
			$strToReturn .= '<element name="Camera" type="xsd1:Camera"/>';
			$strToReturn .= '<element name="CameraPurpose" type="xsd1:CameraPurpose"/>';
			$strToReturn .= '<element name="TapeModeEnumId" type="xsd:boolean"/>';
			$strToReturn .= '<element name="RecordingType" type="xsd1:RecordingType"/>';
			$strToReturn .= '<element name="Start" type="xsd:int"/>';
			$strToReturn .= '<element name="End" type="xsd:int"/>';
			$strToReturn .= '<element name="Widescreen" type="xsd:boolean"/>';
			$strToReturn .= '<element name="Date" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Scene" type="xsd:int"/>';
			$strToReturn .= '<element name="Take" type="xsd:int"/>';
			$strToReturn .= '<element name="Angle" type="xsd:int"/>';
			$strToReturn .= '<element name="Added" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Changed" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Recording', $strComplexTypeArray)) {
				$strComplexTypeArray['Recording'] = Recording::GetSoapComplexTypeXml();
				Project::AlterSoapComplexTypeArray($strComplexTypeArray);
				Tape::AlterSoapComplexTypeArray($strComplexTypeArray);
				Camera::AlterSoapComplexTypeArray($strComplexTypeArray);
				CameraPurpose::AlterSoapComplexTypeArray($strComplexTypeArray);
				RecordingType::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Recording::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Recording();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'Project')) &&
				($objSoapObject->Project))
				$objToReturn->Project = Project::GetObjectFromSoapObject($objSoapObject->Project);
			if ((property_exists($objSoapObject, 'Tape')) &&
				($objSoapObject->Tape))
				$objToReturn->Tape = Tape::GetObjectFromSoapObject($objSoapObject->Tape);
			if ((property_exists($objSoapObject, 'Camera')) &&
				($objSoapObject->Camera))
				$objToReturn->Camera = Camera::GetObjectFromSoapObject($objSoapObject->Camera);
			if ((property_exists($objSoapObject, 'CameraPurpose')) &&
				($objSoapObject->CameraPurpose))
				$objToReturn->CameraPurpose = CameraPurpose::GetObjectFromSoapObject($objSoapObject->CameraPurpose);
			if (property_exists($objSoapObject, 'TapeModeEnumId'))
				$objToReturn->blnTapeModeEnumId = $objSoapObject->TapeModeEnumId;
			if ((property_exists($objSoapObject, 'RecordingType')) &&
				($objSoapObject->RecordingType))
				$objToReturn->RecordingType = RecordingType::GetObjectFromSoapObject($objSoapObject->RecordingType);
			if (property_exists($objSoapObject, 'Start'))
				$objToReturn->intStart = $objSoapObject->Start;
			if (property_exists($objSoapObject, 'End'))
				$objToReturn->intEnd = $objSoapObject->End;
			if (property_exists($objSoapObject, 'Widescreen'))
				$objToReturn->blnWidescreen = $objSoapObject->Widescreen;
			if (property_exists($objSoapObject, 'Date'))
				$objToReturn->dttDate = new QDateTime($objSoapObject->Date);
			if (property_exists($objSoapObject, 'Scene'))
				$objToReturn->intScene = $objSoapObject->Scene;
			if (property_exists($objSoapObject, 'Take'))
				$objToReturn->intTake = $objSoapObject->Take;
			if (property_exists($objSoapObject, 'Angle'))
				$objToReturn->intAngle = $objSoapObject->Angle;
			if (property_exists($objSoapObject, 'Added'))
				$objToReturn->dttAdded = new QDateTime($objSoapObject->Added);
			if (property_exists($objSoapObject, 'Changed'))
				$objToReturn->dttChanged = new QDateTime($objSoapObject->Changed);
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Recording::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objProject)
				$objObject->objProject = Project::GetSoapObjectFromObject($objObject->objProject, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strProjectId = null;
			if ($objObject->objTape)
				$objObject->objTape = Tape::GetSoapObjectFromObject($objObject->objTape, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strTapeId = null;
			if ($objObject->objCamera)
				$objObject->objCamera = Camera::GetSoapObjectFromObject($objObject->objCamera, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strCameraId = null;
			if ($objObject->objCameraPurpose)
				$objObject->objCameraPurpose = CameraPurpose::GetSoapObjectFromObject($objObject->objCameraPurpose, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strCameraPurposeId = null;
			if ($objObject->objRecordingType)
				$objObject->objRecordingType = RecordingType::GetSoapObjectFromObject($objObject->objRecordingType, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strRecordingTypeId = null;
			if ($objObject->dttDate)
				$objObject->dttDate = $objObject->dttDate->__toString(QDateTime::FormatSoap);
			if ($objObject->dttAdded)
				$objObject->dttAdded = $objObject->dttAdded->__toString(QDateTime::FormatSoap);
			if ($objObject->dttChanged)
				$objObject->dttChanged = $objObject->dttChanged->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeRecording extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Recording';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'recording';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'ProjectId':
					return new QQNode('project_id', 'ProjectId', 'string', $this);
				case 'Project':
					return new QQNodeProject('project_id', 'Project', 'string', $this);
				case 'TapeId':
					return new QQNode('tape_id', 'TapeId', 'string', $this);
				case 'Tape':
					return new QQNodeTape('tape_id', 'Tape', 'string', $this);
				case 'CameraId':
					return new QQNode('camera_id', 'CameraId', 'string', $this);
				case 'Camera':
					return new QQNodeCamera('camera_id', 'Camera', 'string', $this);
				case 'CameraPurposeId':
					return new QQNode('camera_purpose_id', 'CameraPurposeId', 'string', $this);
				case 'CameraPurpose':
					return new QQNodeCameraPurpose('camera_purpose_id', 'CameraPurpose', 'string', $this);
				case 'TapeModeEnumId':
					return new QQNode('tape_mode_enum_id', 'TapeModeEnumId', 'boolean', $this);
				case 'RecordingTypeId':
					return new QQNode('recording_type_id', 'RecordingTypeId', 'string', $this);
				case 'RecordingType':
					return new QQNodeRecordingType('recording_type_id', 'RecordingType', 'string', $this);
				case 'Start':
					return new QQNode('start', 'Start', 'integer', $this);
				case 'End':
					return new QQNode('end', 'End', 'integer', $this);
				case 'Widescreen':
					return new QQNode('widescreen', 'Widescreen', 'boolean', $this);
				case 'Date':
					return new QQNode('date', 'Date', 'QDateTime', $this);
				case 'Scene':
					return new QQNode('scene', 'Scene', 'integer', $this);
				case 'Take':
					return new QQNode('take', 'Take', 'integer', $this);
				case 'Angle':
					return new QQNode('angle', 'Angle', 'integer', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'FileAsRecording':
					return new QQReverseReferenceNodeFile($this, 'fileasrecording', 'reverse_reference', 'recording_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeRecording extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Recording';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'recording';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'ProjectId':
					return new QQNode('project_id', 'ProjectId', 'string', $this);
				case 'Project':
					return new QQNodeProject('project_id', 'Project', 'string', $this);
				case 'TapeId':
					return new QQNode('tape_id', 'TapeId', 'string', $this);
				case 'Tape':
					return new QQNodeTape('tape_id', 'Tape', 'string', $this);
				case 'CameraId':
					return new QQNode('camera_id', 'CameraId', 'string', $this);
				case 'Camera':
					return new QQNodeCamera('camera_id', 'Camera', 'string', $this);
				case 'CameraPurposeId':
					return new QQNode('camera_purpose_id', 'CameraPurposeId', 'string', $this);
				case 'CameraPurpose':
					return new QQNodeCameraPurpose('camera_purpose_id', 'CameraPurpose', 'string', $this);
				case 'TapeModeEnumId':
					return new QQNode('tape_mode_enum_id', 'TapeModeEnumId', 'boolean', $this);
				case 'RecordingTypeId':
					return new QQNode('recording_type_id', 'RecordingTypeId', 'string', $this);
				case 'RecordingType':
					return new QQNodeRecordingType('recording_type_id', 'RecordingType', 'string', $this);
				case 'Start':
					return new QQNode('start', 'Start', 'integer', $this);
				case 'End':
					return new QQNode('end', 'End', 'integer', $this);
				case 'Widescreen':
					return new QQNode('widescreen', 'Widescreen', 'boolean', $this);
				case 'Date':
					return new QQNode('date', 'Date', 'QDateTime', $this);
				case 'Scene':
					return new QQNode('scene', 'Scene', 'integer', $this);
				case 'Take':
					return new QQNode('take', 'Take', 'integer', $this);
				case 'Angle':
					return new QQNode('angle', 'Angle', 'integer', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Changed':
					return new QQNode('changed', 'Changed', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'FileAsRecording':
					return new QQReverseReferenceNodeFile($this, 'fileasrecording', 'reverse_reference', 'recording_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
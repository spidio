<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?php _p($this->SiteTitle); ?> &rsaquo; <?php _p($this->PageTitle); ?></title>
	<link href="<?php echo _p($this->GetStylesFilePath('spafaribuild.css')); ?>" rel="stylesheet" type="text/css" />
	<!--
		Original style by DarwinBuild.
		Copyright by DarwinBuild Project.
		http://www.opendarwin.org/projects/darwinbuild/
	-->
	<meta name="author" content="<?php _p($this->SiteCopyrightAuthor); ?>" />
	<meta name="copyright" content="Copyright (c) <?php _p($this->SiteCopyrightYears); ?> <?php _p($this->SiteCopyrightAuthor); ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=<?php _p(QApplication::$EncodingType); ?>" />
</head>

<body>
<div>

<?php $this->RenderBegin(); ?>

<div id="sidebar">
	<a href="<?php _p($this->UrlIndex); ?>"><img src="<?php _p($this->GetLogoFilePath()); ?>" alt="<?php _p($this->SiteTitle); ?> Logo" /></a>
	
	<div class="group">
		<div class="grouptitle"><?php _t('User information'); ?></div>
		<ul class="grouplinks">
		<?php if ($this->UserLoggedIn) { ?>
			<li><?php _t('Name: '); _p($this->UserName, false); ?> [<a href="<?php _p($this->UrlProfile); ?>"><?php _t('profile'); ?></a>]</li>
			<li><a href="<?php _p($this->UrlLogout); ?>"><?php _t('Logout user'); ?></a></li>
		<?php } else { ?>
			<li><?php _t('Name:'); _t('not logged in'); ?></li>
			<li><?php _t('Logout user'); ?></li>
		<?php } ?>
		</ul>
	</div>

	<div class="group">
		<div class="grouptitle"><a href="<?php _p($this->UrlRecent); ?>"><?php _t('Recent changes'); ?></a></div>
		<ul class="grouplinks">
		<?php if ($this->UserLoggedIn || $this->ShowRecentChanges) { ?>
		<?php if (!count($this->Recent)) { ?>
			<li class="recent_error"><?php _t('Not available because there are no changes made yet.'); ?></li>
		<?php } else if ($this->UserLoggedIn || $this->ShowRecentChanges) { ?>
			<?php foreach ($this->Recent as $arrValue) { ?>
			<li class="recent"><a href="<?php _p($arrValue['Url']); ?>"><?php _p($arrValue['Text']) ?></a></li>
			<li class="recent_details"><?php _p($arrValue['Details'], false); ?></li>
			<?php } ?>
		<?php } ?>
		<?php } else { ?>
			<li class="recent_error"><?php _t('Not available because you are currently not logged in.'); ?></li>
		<?php } ?>
		</ul>
	</div>
	
	<div class="group">
		<div class="grouptitle"><a href="<?php _p($this->UrlSearch); ?>"><?php _t('Spotsight search'); ?></a></div>
		<div><?php $this->txtSearch->Render(); ?></div>
		<div>
			<?php $this->btnSearchCommit->Render(); ?>
			<?php $this->btnSearchReset->Render(); ?>
			<?php $this->objSearchWaitIcon->Render(); ?>
		</div>
	</div>

<!-- 
	<div class="group">
		<div class="grouptitle">Downloads</div>
		<ul class="grouplinks">
			<li><a href="releases/index.html">DarwinBuild Releases</a></li>
			<li><a href="plists/index.html">Property Lists</a></li>
			<li>&nbsp;</li>
			<li><a href="sources.html">Getting the Source Code</a></li>
		</ul>
	</div>

	<div class="group">
		<div class="grouptitle">Documentation</div>
		<ul class="grouplinks">
			<li><a href="doc/build/index.html">Building Darwin Projects</a></li>
			<li><ul class="groupsublinks">
				<li><a href="doc/build/index.html#command">Build Command</a></li>
			</ul></li>
		</ul>
	</div>
 -->
</div>

<div id="banner"><?php _p($this->SiteTitle); ?> &rsaquo; <?php _p($this->PageTitle); ?></div>

<?php if ($this->ConfigValidatoricons) { ?>
<div id="validicons">
	<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1!" /></a>
	<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!" /></a>
</div>
<?php } ?>

<div id="menu">
<?php _p($this->Menu); ?>
</div>

<div id="content">

<?php $this->pnlSearch->Render(); ?>
<?php

abstract class AuthenticationBase extends QBaseClass {
	/**
	 * Authentication scheme.
	 *
	 * @var int
	 */
	protected $intAuthenticationType = AuthenticationType::Plain;
	
	/**
	 * Supporting authentication JavaScript for scheme.
	 *
	 * @var unknown_type
	 */
	protected $strJavaScriptArray = array(
		AuthenticationType::Base64	=>	'Base64.encode',
		AuthenticationType::Md5		=>	'MD5',
		AuthenticationType::Sha1	=>	'SHA1'
	);
		
	/**
	 * Get Javascript function string.
	 *
	 * @return string
	 */
	public function GetAuthenticationJavaScript() {
		if ($this->intAuthenticationType == AuthenticationType::Plain) {
			return "function setPasswordSalt() {
						time = parseInt(new Date().getTime().toString().substring(0, 10));
						qc.getControl('txtPasswordSalt').value = qc.getControl('txtPassword').value);
						qc.getControl('txtSalt').value = '';
						qc.getControl('txtPassword').value = null;
					}";
		} else {
			return "function setPasswordSalt() {
						time = parseInt(new Date().getTime().toString().substring(0, 10));
						qc.getControl('txtPasswordSalt').value = {$this->strJavaScriptArray[$this->intAuthenticationType]}({$this->strJavaScriptArray[$this->intAuthenticationType]}(qc.getControl('txtPassword').value) + time);
						qc.getControl('txtSalt').value = time;
						qc.getControl('txtPassword').value = null;
					}";
		}
		
	}
	
	abstract protected function GetUser();
	abstract public function IsLoggedIn();
	abstract public function ResetSession();
	abstract public function UserExists($strName);
	abstract protected function AddLogEntry($intLogItemEnum, UUID $uuidItemId = null);
	
	/**
	 * Check password against password salt.
	 *
	 * @param string $strPassword
	 * @param string $strPasswordSalt
	 * @param string $strSalt
	 * @return bool
	 */
	protected function CheckPassword($strPassword, $strPasswordSalt, $strSalt) {
		switch ($this->AuthenticationType) {
			case AuthenticationType::Plain:
				return (sprintf('%s%s', $strPassword, $strSalt) == $strPasswordSalt);
			case AuthenticationType::Base64:
				return (base64_encode(sprintf('%s%s', base64_encode($strPassword), $strSalt)) == $strPasswordSalt);
			case AuthenticationType::Md5:
				return (md5(sprintf('%s%s', $strPassword, $strSalt)) == $strPasswordSalt);
			case AuthenticationType::Sha1:
				return (sha1(sprintf('%s%s', $strPassword, $strSalt)) == $strPasswordSalt);
		}
	}
	
	/**
	 * Get data from config table.
	 *
	 * @param string $strName
	 * @param string $strType
	 * @return mixed
	 */
	protected function GetConfig($strName, $strType = QType::String) {
		return QApplication::GetConfig($strName, $strType);
	}
	
	/**
	 * Generate salted password based on the defined encoding.
	 *
	 * @param string $strPassword
	 * @param string $strSalt
	 * @return string
	 */
	public function GenerateSaltedPassword($strPassword, $strSalt) {
		switch ($this->intAuthenticationType) {
			case AuthenticationType::Plain:
				return $strPassword;
			case AuthenticationType::Base64:
				return base64_encode(base64_encode($strPassword).$strSalt);
			case AuthenticationType::Md5:
				return md5(md5($strPassword).$strSalt);
			case AuthenticationType::Sha1:
				return sha1(sha1($strPassword).$strSalt);
		}
	}
	
	/**
	 * Enter description here...
	 *
	 * @param string $strLanguageCode
	 * @param string $strCountryCode
	 */
	protected function UpdateLanguage($strLanguageCode, $strCountryCode = null) {
		QApplication::$LanguageCode = $strLanguageCode;
		
		if (!is_null($strCountryCode)) {
			QApplication::$CountryCode = $strCountryCode;
		}
		
		QI18n::Initialize();
	}
	
	/**
	 * Return IpAddress of the requesting client.
	 *
	 * @return IpAddress
	 */
	protected function UserIpAddress() {
		return new IpAddress(QApplication::GetClientAddress());
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'AuthenticationType': return $this->intAuthenticationType;
			case 'User': return $this->GetUser();
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		switch ($strName) {
			case 'AuthenticationType':
				try {
					return ($this->intAuthenticationType = QType::Cast($mixValue, QType::Integer));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
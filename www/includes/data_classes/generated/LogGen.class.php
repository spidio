<?php
	/**
	 * The abstract LogGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Log subclass which
	 * extends this LogGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Log class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property-read integer $Id the value for intId (Read-Only PK)
	 * @property string $UserId the value for strUserId 
	 * @property integer $LogItemEnumId the value for intLogItemEnumId (Not Null)
	 * @property integer $LogActionEnumId the value for intLogActionEnumId (Not Null)
	 * @property string $ItemId the value for strItemId 
	 * @property QDateTime $Date the value for dttDate (Not Null)
	 * @property string $SessionName the value for strSessionName (Not Null)
	 * @property string $IpAddress the value for strIpAddress (Not Null)
	 * @property User $User the value for the User object referenced by strUserId 
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class LogGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK Identity column spidio_log.id
		 * @var integer intId
		 */
		protected $intId;
		const IdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.user_id
		 * @var string strUserId
		 */
		protected $strUserId;
		const UserIdMaxLength = 36;
		const UserIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.log_item_enum_id
		 * @var integer intLogItemEnumId
		 */
		protected $intLogItemEnumId;
		const LogItemEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.log_action_enum_id
		 * @var integer intLogActionEnumId
		 */
		protected $intLogActionEnumId;
		const LogActionEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.item_id
		 * @var string strItemId
		 */
		protected $strItemId;
		const ItemIdMaxLength = 36;
		const ItemIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.date
		 * @var QDateTime dttDate
		 */
		protected $dttDate;
		const DateDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.session_name
		 * @var string strSessionName
		 */
		protected $strSessionName;
		const SessionNameMaxLength = 38;
		const SessionNameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_log.ip_address
		 * @var string strIpAddress
		 */
		protected $strIpAddress;
		const IpAddressMaxLength = 49;
		const IpAddressDefault = null;


		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_log.user_id.
		 *
		 * NOTE: Always use the User property getter to correctly retrieve this User object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var User objUser
		 */
		protected $objUser;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Log from PK Info
		 * @param integer $intId
		 * @return Log
		 */
		public static function Load($intId) {
			// Use QuerySingle to Perform the Query
			return Log::QuerySingle(
				QQ::Equal(QQN::Log()->Id, $intId)
			);
		}

		/**
		 * Load all Logs
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Log[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Log::QueryArray to perform the LoadAll query
			try {
				return Log::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Logs
		 * @return int
		 */
		public static function CountAll() {
			// Call Log::QueryCount to perform the CountAll query
			return Log::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Log::GetDatabase();

			// Create/Build out the QueryBuilder object with Log-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'log');
			Log::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'log');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Log object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Log the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Log::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Log object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Log::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Log objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Log[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Log::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Log::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Log objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Log::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Log::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'log_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Log-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Log::GetSelectFields($objQueryBuilder);
				Log::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Log::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Log
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'log';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'user_id', $strAliasPrefix . 'user_id');
			$objBuilder->AddSelectItem($strTableName, 'log_item_enum_id', $strAliasPrefix . 'log_item_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'log_action_enum_id', $strAliasPrefix . 'log_action_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'item_id', $strAliasPrefix . 'item_id');
			$objBuilder->AddSelectItem($strTableName, 'date', $strAliasPrefix . 'date');
			$objBuilder->AddSelectItem($strTableName, 'session_name', $strAliasPrefix . 'session_name');
			$objBuilder->AddSelectItem($strTableName, 'ip_address', $strAliasPrefix . 'ip_address');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Log from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Log::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Log
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;


			// Create a new instance of the Log object
			$objToReturn = new Log();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->intId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'user_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'user_id'] : $strAliasPrefix . 'user_id';
			$objToReturn->strUserId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'log_item_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'log_item_enum_id'] : $strAliasPrefix . 'log_item_enum_id';
			$objToReturn->intLogItemEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'log_action_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'log_action_enum_id'] : $strAliasPrefix . 'log_action_enum_id';
			$objToReturn->intLogActionEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'item_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'item_id'] : $strAliasPrefix . 'item_id';
			$objToReturn->strItemId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'date', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'date'] : $strAliasPrefix . 'date';
			$objToReturn->dttDate = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'session_name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'session_name'] : $strAliasPrefix . 'session_name';
			$objToReturn->strSessionName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'ip_address', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'ip_address'] : $strAliasPrefix . 'ip_address';
			$objToReturn->strIpAddress = $objDbRow->GetColumn($strAliasName, 'VarChar');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'log__';

			// Check for User Early Binding
			$strAlias = $strAliasPrefix . 'user_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objUser = User::InstantiateDbRow($objDbRow, $strAliasPrefix . 'user_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			return $objToReturn;
		}

		/**
		 * Instantiate an array of Logs from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Log[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Log::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Log::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Log object,
		 * by Id Index(es)
		 * @param integer $intId
		 * @return Log
		*/
		public static function LoadById($intId) {
			return Log::QuerySingle(
				QQ::Equal(QQN::Log()->Id, $intId)
			);
		}
			
		/**
		 * Load an array of Log objects,
		 * by UserId Index(es)
		 * @param string $strUserId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Log[]
		*/
		public static function LoadArrayByUserId($strUserId, $objOptionalClauses = null) {
			// Call Log::QueryArray to perform the LoadArrayByUserId query
			try {
				return Log::QueryArray(
					QQ::Equal(QQN::Log()->UserId, $strUserId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Logs
		 * by UserId Index(es)
		 * @param string $strUserId
		 * @return int
		*/
		public static function CountByUserId($strUserId) {
			// Call Log::QueryCount to perform the CountByUserId query
			return Log::QueryCount(
				QQ::Equal(QQN::Log()->UserId, $strUserId)
			);
		}
			
		/**
		 * Load an array of Log objects,
		 * by LogActionEnumId Index(es)
		 * @param integer $intLogActionEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Log[]
		*/
		public static function LoadArrayByLogActionEnumId($intLogActionEnumId, $objOptionalClauses = null) {
			// Call Log::QueryArray to perform the LoadArrayByLogActionEnumId query
			try {
				return Log::QueryArray(
					QQ::Equal(QQN::Log()->LogActionEnumId, $intLogActionEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Logs
		 * by LogActionEnumId Index(es)
		 * @param integer $intLogActionEnumId
		 * @return int
		*/
		public static function CountByLogActionEnumId($intLogActionEnumId) {
			// Call Log::QueryCount to perform the CountByLogActionEnumId query
			return Log::QueryCount(
				QQ::Equal(QQN::Log()->LogActionEnumId, $intLogActionEnumId)
			);
		}
			
		/**
		 * Load an array of Log objects,
		 * by LogItemEnumId Index(es)
		 * @param integer $intLogItemEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Log[]
		*/
		public static function LoadArrayByLogItemEnumId($intLogItemEnumId, $objOptionalClauses = null) {
			// Call Log::QueryArray to perform the LoadArrayByLogItemEnumId query
			try {
				return Log::QueryArray(
					QQ::Equal(QQN::Log()->LogItemEnumId, $intLogItemEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Logs
		 * by LogItemEnumId Index(es)
		 * @param integer $intLogItemEnumId
		 * @return int
		*/
		public static function CountByLogItemEnumId($intLogItemEnumId) {
			// Call Log::QueryCount to perform the CountByLogItemEnumId query
			return Log::QueryCount(
				QQ::Equal(QQN::Log()->LogItemEnumId, $intLogItemEnumId)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Log
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return int
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Log::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'log` (
							`user_id`,
							`log_item_enum_id`,
							`log_action_enum_id`,
							`item_id`,
							`date`,
							`session_name`,
							`ip_address`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strUserId) . ',
							' . $objDatabase->SqlVariable($this->intLogItemEnumId) . ',
							' . $objDatabase->SqlVariable($this->intLogActionEnumId) . ',
							' . $objDatabase->SqlVariable($this->strItemId) . ',
							' . $objDatabase->SqlVariable($this->dttDate) . ',
							' . $objDatabase->SqlVariable($this->strSessionName) . ',
							' . $objDatabase->SqlVariable($this->strIpAddress) . '
						)
					');

					// Update Identity column and return its value
					$mixToReturn = $this->intId = $objDatabase->InsertId('' . DB_TABLE_PREFIX_1 . 'log', 'id');
				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'log`
						SET
							`user_id` = ' . $objDatabase->SqlVariable($this->strUserId) . ',
							`log_item_enum_id` = ' . $objDatabase->SqlVariable($this->intLogItemEnumId) . ',
							`log_action_enum_id` = ' . $objDatabase->SqlVariable($this->intLogActionEnumId) . ',
							`item_id` = ' . $objDatabase->SqlVariable($this->strItemId) . ',
							`date` = ' . $objDatabase->SqlVariable($this->dttDate) . ',
							`session_name` = ' . $objDatabase->SqlVariable($this->strSessionName) . ',
							`ip_address` = ' . $objDatabase->SqlVariable($this->strIpAddress) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->intId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;


			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Log
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Log with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Log::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'log`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->intId) . '');
		}

		/**
		 * Delete all Logs
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Log::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'log`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'log table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Log::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'log`');
		}

		/**
		 * Reload this Log from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Log object.');

			// Reload the Object
			$objReloaded = Log::Load($this->intId);

			// Update $this's local variables to match
			$this->UserId = $objReloaded->UserId;
			$this->LogItemEnumId = $objReloaded->LogItemEnumId;
			$this->LogActionEnumId = $objReloaded->LogActionEnumId;
			$this->strItemId = $objReloaded->strItemId;
			$this->dttDate = $objReloaded->dttDate;
			$this->strSessionName = $objReloaded->strSessionName;
			$this->strIpAddress = $objReloaded->strIpAddress;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for intId (Read-Only PK)
					 * @return integer
					 */
					return $this->intId;

				case 'UserId':
					/**
					 * Gets the value for strUserId 
					 * @return string
					 */
					return $this->strUserId;

				case 'LogItemEnumId':
					/**
					 * Gets the value for intLogItemEnumId (Not Null)
					 * @return integer
					 */
					return $this->intLogItemEnumId;

				case 'LogActionEnumId':
					/**
					 * Gets the value for intLogActionEnumId (Not Null)
					 * @return integer
					 */
					return $this->intLogActionEnumId;

				case 'ItemId':
					/**
					 * Gets the value for strItemId 
					 * @return string
					 */
					return $this->strItemId;

				case 'Date':
					/**
					 * Gets the value for dttDate (Not Null)
					 * @return QDateTime
					 */
					return $this->dttDate;

				case 'SessionName':
					/**
					 * Gets the value for strSessionName (Not Null)
					 * @return string
					 */
					return $this->strSessionName;

				case 'IpAddress':
					/**
					 * Gets the value for strIpAddress (Not Null)
					 * @return string
					 */
					return $this->strIpAddress;


				///////////////////
				// Member Objects
				///////////////////
				case 'User':
					/**
					 * Gets the value for the User object referenced by strUserId 
					 * @return User
					 */
					try {
						if ((!$this->objUser) && (!is_null($this->strUserId)))
							$this->objUser = User::Load($this->strUserId);
						return $this->objUser;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'UserId':
					/**
					 * Sets the value for strUserId 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objUser = null;
						return ($this->strUserId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'LogItemEnumId':
					/**
					 * Sets the value for intLogItemEnumId (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intLogItemEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'LogActionEnumId':
					/**
					 * Sets the value for intLogActionEnumId (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intLogActionEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'ItemId':
					/**
					 * Sets the value for strItemId 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strItemId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Date':
					/**
					 * Sets the value for dttDate (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttDate = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'SessionName':
					/**
					 * Sets the value for strSessionName (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strSessionName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'IpAddress':
					/**
					 * Sets the value for strIpAddress (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strIpAddress = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'User':
					/**
					 * Sets the value for the User object referenced by strUserId 
					 * @param User $mixValue
					 * @return User
					 */
					if (is_null($mixValue)) {
						$this->strUserId = null;
						$this->objUser = null;
						return null;
					} else {
						// Make sure $mixValue actually is a User object
						try {
							$mixValue = QType::Cast($mixValue, 'User');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED User object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved User for this Log');

						// Update Local Member Variables
						$this->objUser = $mixValue;
						$this->strUserId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Log"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:int"/>';
			$strToReturn .= '<element name="User" type="xsd1:User"/>';
			$strToReturn .= '<element name="LogItemEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="LogActionEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="ItemId" type="xsd:string"/>';
			$strToReturn .= '<element name="Date" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="SessionName" type="xsd:string"/>';
			$strToReturn .= '<element name="IpAddress" type="xsd:string"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Log', $strComplexTypeArray)) {
				$strComplexTypeArray['Log'] = Log::GetSoapComplexTypeXml();
				User::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Log::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Log();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->intId = $objSoapObject->Id;
			if ((property_exists($objSoapObject, 'User')) &&
				($objSoapObject->User))
				$objToReturn->User = User::GetObjectFromSoapObject($objSoapObject->User);
			if (property_exists($objSoapObject, 'LogItemEnumId'))
				$objToReturn->intLogItemEnumId = $objSoapObject->LogItemEnumId;
			if (property_exists($objSoapObject, 'LogActionEnumId'))
				$objToReturn->intLogActionEnumId = $objSoapObject->LogActionEnumId;
			if (property_exists($objSoapObject, 'ItemId'))
				$objToReturn->strItemId = $objSoapObject->ItemId;
			if (property_exists($objSoapObject, 'Date'))
				$objToReturn->dttDate = new QDateTime($objSoapObject->Date);
			if (property_exists($objSoapObject, 'SessionName'))
				$objToReturn->strSessionName = $objSoapObject->SessionName;
			if (property_exists($objSoapObject, 'IpAddress'))
				$objToReturn->strIpAddress = $objSoapObject->IpAddress;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Log::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objUser)
				$objObject->objUser = User::GetSoapObjectFromObject($objObject->objUser, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strUserId = null;
			if ($objObject->dttDate)
				$objObject->dttDate = $objObject->dttDate->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeLog extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Log';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'log';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'integer', $this);
				case 'UserId':
					return new QQNode('user_id', 'UserId', 'string', $this);
				case 'User':
					return new QQNodeUser('user_id', 'User', 'string', $this);
				case 'LogItemEnumId':
					return new QQNode('log_item_enum_id', 'LogItemEnumId', 'integer', $this);
				case 'LogActionEnumId':
					return new QQNode('log_action_enum_id', 'LogActionEnumId', 'integer', $this);
				case 'ItemId':
					return new QQNode('item_id', 'ItemId', 'string', $this);
				case 'Date':
					return new QQNode('date', 'Date', 'QDateTime', $this);
				case 'SessionName':
					return new QQNode('session_name', 'SessionName', 'string', $this);
				case 'IpAddress':
					return new QQNode('ip_address', 'IpAddress', 'string', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'integer', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeLog extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Log';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'log';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'integer', $this);
				case 'UserId':
					return new QQNode('user_id', 'UserId', 'string', $this);
				case 'User':
					return new QQNodeUser('user_id', 'User', 'string', $this);
				case 'LogItemEnumId':
					return new QQNode('log_item_enum_id', 'LogItemEnumId', 'integer', $this);
				case 'LogActionEnumId':
					return new QQNode('log_action_enum_id', 'LogActionEnumId', 'integer', $this);
				case 'ItemId':
					return new QQNode('item_id', 'ItemId', 'string', $this);
				case 'Date':
					return new QQNode('date', 'Date', 'QDateTime', $this);
				case 'SessionName':
					return new QQNode('session_name', 'SessionName', 'string', $this);
				case 'IpAddress':
					return new QQNode('ip_address', 'IpAddress', 'string', $this);

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'integer', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
<?php
	/**
	 * The abstract TapeGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the Tape subclass which
	 * extends this TapeGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the Tape class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property-read integer $Number the value for intNumber (Read-Only PK)
	 * @property string $LibraryId the value for strLibraryId (Not Null)
	 * @property string $OrganisationId the value for strOrganisationId (Not Null)
	 * @property integer $TapeModeEnumId the value for intTapeModeEnumId 
	 * @property QDateTime $Added the value for dttAdded (Not Null)
	 * @property string $Comment the value for strComment 
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property Library $Library the value for the Library object referenced by strLibraryId (Not Null)
	 * @property Organisation $Organisation the value for the Organisation object referenced by strOrganisationId (Not Null)
	 * @property-read Recording $_RecordingAsTape the value for the private _objRecordingAsTape (Read-Only) if set due to an expansion on the spidio_recording.tape_id reverse relationship
	 * @property-read Recording[] $_RecordingAsTapeArray the value for the private _objRecordingAsTapeArray (Read-Only) if set due to an ExpandAsArray on the spidio_recording.tape_id reverse relationship
	 * @property-read TapeLocation $_TapeLocationAsTape the value for the private _objTapeLocationAsTape (Read-Only) if set due to an expansion on the spidio_tape_location.tape_id reverse relationship
	 * @property-read TapeLocation[] $_TapeLocationAsTapeArray the value for the private _objTapeLocationAsTapeArray (Read-Only) if set due to an ExpandAsArray on the spidio_tape_location.tape_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class TapeGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_tape.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database Identity column spidio_tape.number
		 * @var integer intNumber
		 */
		protected $intNumber;
		const NumberDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.library_id
		 * @var string strLibraryId
		 */
		protected $strLibraryId;
		const LibraryIdMaxLength = 36;
		const LibraryIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.organisation_id
		 * @var string strOrganisationId
		 */
		protected $strOrganisationId;
		const OrganisationIdMaxLength = 36;
		const OrganisationIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.tape_mode_enum_id
		 * @var integer intTapeModeEnumId
		 */
		protected $intTapeModeEnumId;
		const TapeModeEnumIdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.added
		 * @var QDateTime dttAdded
		 */
		protected $dttAdded;
		const AddedDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.comment
		 * @var string strComment
		 */
		protected $strComment;
		const CommentDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_tape.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single RecordingAsTape object
		 * (of type Recording), if this Tape object was restored with
		 * an expansion on the spidio_recording association table.
		 * @var Recording _objRecordingAsTape;
		 */
		private $_objRecordingAsTape;

		/**
		 * Private member variable that stores a reference to an array of RecordingAsTape objects
		 * (of type Recording[]), if this Tape object was restored with
		 * an ExpandAsArray on the spidio_recording association table.
		 * @var Recording[] _objRecordingAsTapeArray;
		 */
		private $_objRecordingAsTapeArray = array();

		/**
		 * Private member variable that stores a reference to a single TapeLocationAsTape object
		 * (of type TapeLocation), if this Tape object was restored with
		 * an expansion on the spidio_tape_location association table.
		 * @var TapeLocation _objTapeLocationAsTape;
		 */
		private $_objTapeLocationAsTape;

		/**
		 * Private member variable that stores a reference to an array of TapeLocationAsTape objects
		 * (of type TapeLocation[]), if this Tape object was restored with
		 * an ExpandAsArray on the spidio_tape_location association table.
		 * @var TapeLocation[] _objTapeLocationAsTapeArray;
		 */
		private $_objTapeLocationAsTapeArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_tape.library_id.
		 *
		 * NOTE: Always use the Library property getter to correctly retrieve this Library object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Library objLibrary
		 */
		protected $objLibrary;

		/**
		 * Protected member variable that contains the object pointed by the reference
		 * in the database column spidio_tape.organisation_id.
		 *
		 * NOTE: Always use the Organisation property getter to correctly retrieve this Organisation object.
		 * (Because this class implements late binding, this variable reference MAY be null.)
		 * @var Organisation objOrganisation
		 */
		protected $objOrganisation;





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a Tape from PK Info
		 * @param string $strId
		 * @return Tape
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return Tape::QuerySingle(
				QQ::Equal(QQN::Tape()->Id, $strId)
			);
		}

		/**
		 * Load all Tapes
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call Tape::QueryArray to perform the LoadAll query
			try {
				return Tape::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all Tapes
		 * @return int
		 */
		public static function CountAll() {
			// Call Tape::QueryCount to perform the CountAll query
			return Tape::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Create/Build out the QueryBuilder object with Tape-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'tape');
			Tape::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'tape');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single Tape object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Tape the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Tape::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new Tape object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Tape::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of Tape objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return Tape[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Tape::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return Tape::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of Tape objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = Tape::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'tape_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with Tape-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				Tape::GetSelectFields($objQueryBuilder);
				Tape::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return Tape::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this Tape
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'tape';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'number', $strAliasPrefix . 'number');
			$objBuilder->AddSelectItem($strTableName, 'library_id', $strAliasPrefix . 'library_id');
			$objBuilder->AddSelectItem($strTableName, 'organisation_id', $strAliasPrefix . 'organisation_id');
			$objBuilder->AddSelectItem($strTableName, 'tape_mode_enum_id', $strAliasPrefix . 'tape_mode_enum_id');
			$objBuilder->AddSelectItem($strTableName, 'added', $strAliasPrefix . 'added');
			$objBuilder->AddSelectItem($strTableName, 'comment', $strAliasPrefix . 'comment');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a Tape from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this Tape::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return Tape
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'tape__';


				$strAlias = $strAliasPrefix . 'recordingastape__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objRecordingAsTapeArray)) {
						$objPreviousChildItem = $objPreviousItem->_objRecordingAsTapeArray[$intPreviousChildItemCount - 1];
						$objChildItem = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingastape__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objRecordingAsTapeArray[] = $objChildItem;
					} else
						$objPreviousItem->_objRecordingAsTapeArray[] = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingastape__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'tapelocationastape__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objTapeLocationAsTapeArray)) {
						$objPreviousChildItem = $objPreviousItem->_objTapeLocationAsTapeArray[$intPreviousChildItemCount - 1];
						$objChildItem = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationastape__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objTapeLocationAsTapeArray[] = $objChildItem;
					} else
						$objPreviousItem->_objTapeLocationAsTapeArray[] = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationastape__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'tape__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the Tape object
			$objToReturn = new Tape();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'number', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'number'] : $strAliasPrefix . 'number';
			$objToReturn->intNumber = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'library_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'library_id'] : $strAliasPrefix . 'library_id';
			$objToReturn->strLibraryId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'organisation_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'organisation_id'] : $strAliasPrefix . 'organisation_id';
			$objToReturn->strOrganisationId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'tape_mode_enum_id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'tape_mode_enum_id'] : $strAliasPrefix . 'tape_mode_enum_id';
			$objToReturn->intTapeModeEnumId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'added', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'added'] : $strAliasPrefix . 'added';
			$objToReturn->dttAdded = $objDbRow->GetColumn($strAliasName, 'DateTime');
			$strAliasName = array_key_exists($strAliasPrefix . 'comment', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'comment'] : $strAliasPrefix . 'comment';
			$objToReturn->strComment = $objDbRow->GetColumn($strAliasName, 'Blob');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'tape__';

			// Check for Library Early Binding
			$strAlias = $strAliasPrefix . 'library_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objLibrary = Library::InstantiateDbRow($objDbRow, $strAliasPrefix . 'library_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);

			// Check for Organisation Early Binding
			$strAlias = $strAliasPrefix . 'organisation_id__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName)))
				$objToReturn->objOrganisation = Organisation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'organisation_id__', $strExpandAsArrayNodes, null, $strColumnAliasArray);




			// Check for RecordingAsTape Virtual Binding
			$strAlias = $strAliasPrefix . 'recordingastape__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objRecordingAsTapeArray[] = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingastape__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objRecordingAsTape = Recording::InstantiateDbRow($objDbRow, $strAliasPrefix . 'recordingastape__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for TapeLocationAsTape Virtual Binding
			$strAlias = $strAliasPrefix . 'tapelocationastape__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objTapeLocationAsTapeArray[] = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationastape__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objTapeLocationAsTape = TapeLocation::InstantiateDbRow($objDbRow, $strAliasPrefix . 'tapelocationastape__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of Tapes from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return Tape[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = Tape::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = Tape::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single Tape object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return Tape
		*/
		public static function LoadById($strId) {
			return Tape::QuerySingle(
				QQ::Equal(QQN::Tape()->Id, $strId)
			);
		}
			
		/**
		 * Load a single Tape object,
		 * by Number, LibraryId Index(es)
		 * @param integer $intNumber
		 * @param string $strLibraryId
		 * @return Tape
		*/
		public static function LoadByNumberLibraryId($intNumber, $strLibraryId) {
			return Tape::QuerySingle(
				QQ::AndCondition(
				QQ::Equal(QQN::Tape()->Number, $intNumber),
				QQ::Equal(QQN::Tape()->LibraryId, $strLibraryId)
				)
			);
		}
			
		/**
		 * Load an array of Tape objects,
		 * by OrganisationId Index(es)
		 * @param string $strOrganisationId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		*/
		public static function LoadArrayByOrganisationId($strOrganisationId, $objOptionalClauses = null) {
			// Call Tape::QueryArray to perform the LoadArrayByOrganisationId query
			try {
				return Tape::QueryArray(
					QQ::Equal(QQN::Tape()->OrganisationId, $strOrganisationId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Tapes
		 * by OrganisationId Index(es)
		 * @param string $strOrganisationId
		 * @return int
		*/
		public static function CountByOrganisationId($strOrganisationId) {
			// Call Tape::QueryCount to perform the CountByOrganisationId query
			return Tape::QueryCount(
				QQ::Equal(QQN::Tape()->OrganisationId, $strOrganisationId)
			);
		}
			
		/**
		 * Load an array of Tape objects,
		 * by LibraryId Index(es)
		 * @param string $strLibraryId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		*/
		public static function LoadArrayByLibraryId($strLibraryId, $objOptionalClauses = null) {
			// Call Tape::QueryArray to perform the LoadArrayByLibraryId query
			try {
				return Tape::QueryArray(
					QQ::Equal(QQN::Tape()->LibraryId, $strLibraryId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Tapes
		 * by LibraryId Index(es)
		 * @param string $strLibraryId
		 * @return int
		*/
		public static function CountByLibraryId($strLibraryId) {
			// Call Tape::QueryCount to perform the CountByLibraryId query
			return Tape::QueryCount(
				QQ::Equal(QQN::Tape()->LibraryId, $strLibraryId)
			);
		}
			
		/**
		 * Load an array of Tape objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call Tape::QueryArray to perform the LoadArrayByOwner query
			try {
				return Tape::QueryArray(
					QQ::Equal(QQN::Tape()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Tapes
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call Tape::QueryCount to perform the CountByOwner query
			return Tape::QueryCount(
				QQ::Equal(QQN::Tape()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of Tape objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call Tape::QueryArray to perform the LoadArrayByStatus query
			try {
				return Tape::QueryArray(
					QQ::Equal(QQN::Tape()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Tapes
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call Tape::QueryCount to perform the CountByStatus query
			return Tape::QueryCount(
				QQ::Equal(QQN::Tape()->Status, $intStatus)
			);
		}
			
		/**
		 * Load an array of Tape objects,
		 * by TapeModeEnumId Index(es)
		 * @param integer $intTapeModeEnumId
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Tape[]
		*/
		public static function LoadArrayByTapeModeEnumId($intTapeModeEnumId, $objOptionalClauses = null) {
			// Call Tape::QueryArray to perform the LoadArrayByTapeModeEnumId query
			try {
				return Tape::QueryArray(
					QQ::Equal(QQN::Tape()->TapeModeEnumId, $intTapeModeEnumId),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count Tapes
		 * by TapeModeEnumId Index(es)
		 * @param integer $intTapeModeEnumId
		 * @return int
		*/
		public static function CountByTapeModeEnumId($intTapeModeEnumId) {
			// Call Tape::QueryCount to perform the CountByTapeModeEnumId query
			return Tape::QueryCount(
				QQ::Equal(QQN::Tape()->TapeModeEnumId, $intTapeModeEnumId)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this Tape
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return int
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'tape` (
							`id`,
							`library_id`,
							`organisation_id`,
							`tape_mode_enum_id`,
							`added`,
							`comment`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strLibraryId) . ',
							' . $objDatabase->SqlVariable($this->strOrganisationId) . ',
							' . $objDatabase->SqlVariable($this->intTapeModeEnumId) . ',
							' . $objDatabase->SqlVariable(new QDateTime(QDateTime::Now)) . ',
							' . $objDatabase->SqlVariable($this->strComment) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'tape`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('Tape');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'tape`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`library_id` = ' . $objDatabase->SqlVariable($this->strLibraryId) . ',
							`organisation_id` = ' . $objDatabase->SqlVariable($this->strOrganisationId) . ',
							`tape_mode_enum_id` = ' . $objDatabase->SqlVariable($this->intTapeModeEnumId) . ',
							`added` = ' . $objDatabase->SqlVariable($this->dttAdded) . ',
							`comment` = ' . $objDatabase->SqlVariable($this->strComment) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'tape`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this Tape
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this Tape with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'tape`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all Tapes
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'tape`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'tape table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'tape`');
		}

		/**
		 * Reload this Tape from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved Tape object.');

			// Reload the Object
			$objReloaded = Tape::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->LibraryId = $objReloaded->LibraryId;
			$this->OrganisationId = $objReloaded->OrganisationId;
			$this->TapeModeEnumId = $objReloaded->TapeModeEnumId;
			$this->dttAdded = $objReloaded->dttAdded;
			$this->strComment = $objReloaded->strComment;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'Number':
					/**
					 * Gets the value for intNumber (Read-Only PK)
					 * @return integer
					 */
					return $this->intNumber;

				case 'LibraryId':
					/**
					 * Gets the value for strLibraryId (Not Null)
					 * @return string
					 */
					return $this->strLibraryId;

				case 'OrganisationId':
					/**
					 * Gets the value for strOrganisationId (Not Null)
					 * @return string
					 */
					return $this->strOrganisationId;

				case 'TapeModeEnumId':
					/**
					 * Gets the value for intTapeModeEnumId 
					 * @return integer
					 */
					return $this->intTapeModeEnumId;

				case 'Added':
					/**
					 * Gets the value for dttAdded (Not Null)
					 * @return QDateTime
					 */
					return $this->dttAdded;

				case 'Comment':
					/**
					 * Gets the value for strComment 
					 * @return string
					 */
					return $this->strComment;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////
				case 'Library':
					/**
					 * Gets the value for the Library object referenced by strLibraryId (Not Null)
					 * @return Library
					 */
					try {
						if ((!$this->objLibrary) && (!is_null($this->strLibraryId)))
							$this->objLibrary = Library::Load($this->strLibraryId);
						return $this->objLibrary;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Organisation':
					/**
					 * Gets the value for the Organisation object referenced by strOrganisationId (Not Null)
					 * @return Organisation
					 */
					try {
						if ((!$this->objOrganisation) && (!is_null($this->strOrganisationId)))
							$this->objOrganisation = Organisation::Load($this->strOrganisationId);
						return $this->objOrganisation;
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_RecordingAsTape':
					/**
					 * Gets the value for the private _objRecordingAsTape (Read-Only)
					 * if set due to an expansion on the spidio_recording.tape_id reverse relationship
					 * @return Recording
					 */
					return $this->_objRecordingAsTape;

				case '_RecordingAsTapeArray':
					/**
					 * Gets the value for the private _objRecordingAsTapeArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_recording.tape_id reverse relationship
					 * @return Recording[]
					 */
					return (array) $this->_objRecordingAsTapeArray;

				case '_TapeLocationAsTape':
					/**
					 * Gets the value for the private _objTapeLocationAsTape (Read-Only)
					 * if set due to an expansion on the spidio_tape_location.tape_id reverse relationship
					 * @return TapeLocation
					 */
					return $this->_objTapeLocationAsTape;

				case '_TapeLocationAsTapeArray':
					/**
					 * Gets the value for the private _objTapeLocationAsTapeArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_tape_location.tape_id reverse relationship
					 * @return TapeLocation[]
					 */
					return (array) $this->_objTapeLocationAsTapeArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'LibraryId':
					/**
					 * Sets the value for strLibraryId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objLibrary = null;
						return ($this->strLibraryId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'OrganisationId':
					/**
					 * Sets the value for strOrganisationId (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						$this->objOrganisation = null;
						return ($this->strOrganisationId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'TapeModeEnumId':
					/**
					 * Sets the value for intTapeModeEnumId 
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intTapeModeEnumId = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Added':
					/**
					 * Sets the value for dttAdded (Not Null)
					 * @param QDateTime $mixValue
					 * @return QDateTime
					 */
					try {
						return ($this->dttAdded = QType::Cast($mixValue, QType::DateTime));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Comment':
					/**
					 * Sets the value for strComment 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strComment = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				case 'Library':
					/**
					 * Sets the value for the Library object referenced by strLibraryId (Not Null)
					 * @param Library $mixValue
					 * @return Library
					 */
					if (is_null($mixValue)) {
						$this->strLibraryId = null;
						$this->objLibrary = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Library object
						try {
							$mixValue = QType::Cast($mixValue, 'Library');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Library object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Library for this Tape');

						// Update Local Member Variables
						$this->objLibrary = $mixValue;
						$this->strLibraryId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				case 'Organisation':
					/**
					 * Sets the value for the Organisation object referenced by strOrganisationId (Not Null)
					 * @param Organisation $mixValue
					 * @return Organisation
					 */
					if (is_null($mixValue)) {
						$this->strOrganisationId = null;
						$this->objOrganisation = null;
						return null;
					} else {
						// Make sure $mixValue actually is a Organisation object
						try {
							$mixValue = QType::Cast($mixValue, 'Organisation');
						} catch (QInvalidCastException $objExc) {
							$objExc->IncrementOffset();
							throw $objExc;
						} 

						// Make sure $mixValue is a SAVED Organisation object
						if (is_null($mixValue->Id))
							throw new QCallerException('Unable to set an unsaved Organisation for this Tape');

						// Update Local Member Variables
						$this->objOrganisation = $mixValue;
						$this->strOrganisationId = $mixValue->Id;

						// Return $mixValue
						return $mixValue;
					}
					break;

				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for RecordingAsTape
		//-------------------------------------------------------------------

		/**
		 * Gets all associated RecordingsAsTape as an array of Recording objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return Recording[]
		*/ 
		public function GetRecordingAsTapeArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return Recording::LoadArrayByTapeId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated RecordingsAsTape
		 * @return int
		*/ 
		public function CountRecordingsAsTape() {
			if ((is_null($this->strId)))
				return 0;

			return Recording::CountByTapeId($this->strId);
		}

		/**
		 * Associates a RecordingAsTape
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function AssociateRecordingAsTape(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateRecordingAsTape on this unsaved Tape.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateRecordingAsTape on this Tape with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . '
			');
		}

		/**
		 * Unassociates a RecordingAsTape
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function UnassociateRecordingAsTape(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsTape on this unsaved Tape.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsTape on this Tape with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`tape_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . ' AND
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all RecordingsAsTape
		 * @return void
		*/ 
		public function UnassociateAllRecordingsAsTape() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsTape on this unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_recording`
				SET
					`tape_id` = null
				WHERE
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated RecordingAsTape
		 * @param Recording $objRecording
		 * @return void
		*/ 
		public function DeleteAssociatedRecordingAsTape(Recording $objRecording) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsTape on this unsaved Tape.');
			if ((is_null($objRecording->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsTape on this Tape with an unsaved Recording.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_recording`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objRecording->Id) . ' AND
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated RecordingsAsTape
		 * @return void
		*/ 
		public function DeleteAllRecordingsAsTape() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateRecordingAsTape on this unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_recording`
				WHERE
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

			
		
		// Related Objects' Methods for TapeLocationAsTape
		//-------------------------------------------------------------------

		/**
		 * Gets all associated TapeLocationsAsTape as an array of TapeLocation objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return TapeLocation[]
		*/ 
		public function GetTapeLocationAsTapeArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return TapeLocation::LoadArrayByTapeId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated TapeLocationsAsTape
		 * @return int
		*/ 
		public function CountTapeLocationsAsTape() {
			if ((is_null($this->strId)))
				return 0;

			return TapeLocation::CountByTapeId($this->strId);
		}

		/**
		 * Associates a TapeLocationAsTape
		 * @param TapeLocation $objTapeLocation
		 * @return void
		*/ 
		public function AssociateTapeLocationAsTape(TapeLocation $objTapeLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateTapeLocationAsTape on this unsaved Tape.');
			if ((is_null($objTapeLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateTapeLocationAsTape on this Tape with an unsaved TapeLocation.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape_location`
				SET
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTapeLocation->Id) . '
			');
		}

		/**
		 * Unassociates a TapeLocationAsTape
		 * @param TapeLocation $objTapeLocation
		 * @return void
		*/ 
		public function UnassociateTapeLocationAsTape(TapeLocation $objTapeLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsTape on this unsaved Tape.');
			if ((is_null($objTapeLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsTape on this Tape with an unsaved TapeLocation.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape_location`
				SET
					`tape_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTapeLocation->Id) . ' AND
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all TapeLocationsAsTape
		 * @return void
		*/ 
		public function UnassociateAllTapeLocationsAsTape() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsTape on this unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_tape_location`
				SET
					`tape_id` = null
				WHERE
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated TapeLocationAsTape
		 * @param TapeLocation $objTapeLocation
		 * @return void
		*/ 
		public function DeleteAssociatedTapeLocationAsTape(TapeLocation $objTapeLocation) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsTape on this unsaved Tape.');
			if ((is_null($objTapeLocation->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsTape on this Tape with an unsaved TapeLocation.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_tape_location`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objTapeLocation->Id) . ' AND
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated TapeLocationsAsTape
		 * @return void
		*/ 
		public function DeleteAllTapeLocationsAsTape() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateTapeLocationAsTape on this unsaved Tape.');

			// Get the Database Object for this Class
			$objDatabase = Tape::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_tape_location`
				WHERE
					`tape_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="Tape"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Number" type="xsd:int"/>';
			$strToReturn .= '<element name="Library" type="xsd1:Library"/>';
			$strToReturn .= '<element name="Organisation" type="xsd1:Organisation"/>';
			$strToReturn .= '<element name="TapeModeEnumId" type="xsd:int"/>';
			$strToReturn .= '<element name="Added" type="xsd:dateTime"/>';
			$strToReturn .= '<element name="Comment" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('Tape', $strComplexTypeArray)) {
				$strComplexTypeArray['Tape'] = Tape::GetSoapComplexTypeXml();
				Library::AlterSoapComplexTypeArray($strComplexTypeArray);
				Organisation::AlterSoapComplexTypeArray($strComplexTypeArray);
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, Tape::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new Tape();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if (property_exists($objSoapObject, 'Number'))
				$objToReturn->intNumber = $objSoapObject->Number;
			if ((property_exists($objSoapObject, 'Library')) &&
				($objSoapObject->Library))
				$objToReturn->Library = Library::GetObjectFromSoapObject($objSoapObject->Library);
			if ((property_exists($objSoapObject, 'Organisation')) &&
				($objSoapObject->Organisation))
				$objToReturn->Organisation = Organisation::GetObjectFromSoapObject($objSoapObject->Organisation);
			if (property_exists($objSoapObject, 'TapeModeEnumId'))
				$objToReturn->intTapeModeEnumId = $objSoapObject->TapeModeEnumId;
			if (property_exists($objSoapObject, 'Added'))
				$objToReturn->dttAdded = new QDateTime($objSoapObject->Added);
			if (property_exists($objSoapObject, 'Comment'))
				$objToReturn->strComment = $objSoapObject->Comment;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, Tape::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			if ($objObject->objLibrary)
				$objObject->objLibrary = Library::GetSoapObjectFromObject($objObject->objLibrary, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strLibraryId = null;
			if ($objObject->objOrganisation)
				$objObject->objOrganisation = Organisation::GetSoapObjectFromObject($objObject->objOrganisation, false);
			else if (!$blnBindRelatedObjects)
				$objObject->strOrganisationId = null;
			if ($objObject->dttAdded)
				$objObject->dttAdded = $objObject->dttAdded->__toString(QDateTime::FormatSoap);
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeTape extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Tape';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'tape';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Number':
					return new QQNode('number', 'Number', 'integer', $this);
				case 'LibraryId':
					return new QQNode('library_id', 'LibraryId', 'string', $this);
				case 'Library':
					return new QQNodeLibrary('library_id', 'Library', 'string', $this);
				case 'OrganisationId':
					return new QQNode('organisation_id', 'OrganisationId', 'string', $this);
				case 'Organisation':
					return new QQNodeOrganisation('organisation_id', 'Organisation', 'string', $this);
				case 'TapeModeEnumId':
					return new QQNode('tape_mode_enum_id', 'TapeModeEnumId', 'integer', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'RecordingAsTape':
					return new QQReverseReferenceNodeRecording($this, 'recordingastape', 'reverse_reference', 'tape_id');
				case 'TapeLocationAsTape':
					return new QQReverseReferenceNodeTapeLocation($this, 'tapelocationastape', 'reverse_reference', 'tape_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeTape extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'Tape';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'tape';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Number':
					return new QQNode('number', 'Number', 'integer', $this);
				case 'LibraryId':
					return new QQNode('library_id', 'LibraryId', 'string', $this);
				case 'Library':
					return new QQNodeLibrary('library_id', 'Library', 'string', $this);
				case 'OrganisationId':
					return new QQNode('organisation_id', 'OrganisationId', 'string', $this);
				case 'Organisation':
					return new QQNodeOrganisation('organisation_id', 'Organisation', 'string', $this);
				case 'TapeModeEnumId':
					return new QQNode('tape_mode_enum_id', 'TapeModeEnumId', 'integer', $this);
				case 'Added':
					return new QQNode('added', 'Added', 'QDateTime', $this);
				case 'Comment':
					return new QQNode('comment', 'Comment', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'RecordingAsTape':
					return new QQReverseReferenceNodeRecording($this, 'recordingastape', 'reverse_reference', 'tape_id');
				case 'TapeLocationAsTape':
					return new QQReverseReferenceNodeTapeLocation($this, 'tapelocationastape', 'reverse_reference', 'tape_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
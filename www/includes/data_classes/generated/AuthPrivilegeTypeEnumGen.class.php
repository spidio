<?php
	/**
	 * The AuthPrivilegeTypeEnum class defined here contains
	 * code for the AuthPrivilegeTypeEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_auth_privilege_type_enum" table
	 * in the database.
	 * 
	 * To use, you should use the AuthPrivilegeTypeEnum subclass which
	 * extends this AuthPrivilegeTypeEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthPrivilegeTypeEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class AuthPrivilegeTypeEnumGen extends QBaseClass {
		const object = 1;
		const table = 2;
		const all = 3;

		const MaxId = 3;

		public static $NameArray = array(
			1 => 'object',
			2 => 'table',
			3 => 'all');

		public static $TokenArray = array(
			1 => 'object',
			2 => 'table',
			3 => 'all');

		public static $ReverseNameArray = array(
			'object' => 1,
			'table' => 2,
			'all' => 3);

		public static $ReverseTokenArray = array(
			'object' => 1,
			'table' => 2,
			'all' => 3);

		public static function ToString($intAuthPrivilegeTypeEnumId) {
			switch ($intAuthPrivilegeTypeEnumId) {
				case 1: return 'object';
				case 2: return 'table';
				case 3: return 'all';
				default:
					throw new QCallerException(sprintf('Invalid intAuthPrivilegeTypeEnumId: %s', $intAuthPrivilegeTypeEnumId));
			}
		}

		public static function ToToken($intAuthPrivilegeTypeEnumId) {
			switch ($intAuthPrivilegeTypeEnumId) {
				case 1: return 'object';
				case 2: return 'table';
				case 3: return 'all';
				default:
					throw new QCallerException(sprintf('Invalid intAuthPrivilegeTypeEnumId: %s', $intAuthPrivilegeTypeEnumId));
			}
		}

	}
?>
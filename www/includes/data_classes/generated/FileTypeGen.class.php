<?php
	/**
	 * The abstract FileTypeGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the FileType subclass which
	 * extends this FileTypeGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the FileType class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property string $Id the value for strId (PK)
	 * @property string $Name the value for strName (Unique)
	 * @property string $Extension the value for strExtension (Not Null)
	 * @property-read string $Optlock the value for strOptlock (Read-Only Timestamp)
	 * @property string $Owner the value for strOwner 
	 * @property integer $Group the value for intGroup (Not Null)
	 * @property integer $Perms the value for intPerms (Not Null)
	 * @property integer $Status the value for intStatus (Not Null)
	 * @property-read File $_FileAsFileType the value for the private _objFileAsFileType (Read-Only) if set due to an expansion on the spidio_file.file_type_id reverse relationship
	 * @property-read File[] $_FileAsFileTypeArray the value for the private _objFileAsFileTypeArray (Read-Only) if set due to an ExpandAsArray on the spidio_file.file_type_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class FileTypeGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK column spidio_file_type.id
		 * @var string strId
		 */
		protected $strId;
		const IdMaxLength = 36;
		const IdDefault = null;


		/**
		 * Protected internal member variable that stores the original version of the PK column value (if restored)
		 * Used by Save() to update a PK column during UPDATE
		 * @var string __strId;
		 */
		protected $__strId;

		/**
		 * Protected member variable that maps to the database column spidio_file_type.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 255;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file_type.extension
		 * @var string strExtension
		 */
		protected $strExtension;
		const ExtensionMaxLength = 25;
		const ExtensionDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file_type.optlock
		 * @var string strOptlock
		 */
		protected $strOptlock;
		const OptlockDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file_type.owner
		 * @var string strOwner
		 */
		protected $strOwner;
		const OwnerMaxLength = 36;
		const OwnerDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file_type.group
		 * @var integer intGroup
		 */
		protected $intGroup;
		const GroupDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file_type.perms
		 * @var integer intPerms
		 */
		protected $intPerms;
		const PermsDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_file_type.status
		 * @var integer intStatus
		 */
		protected $intStatus;
		const StatusDefault = null;


		/**
		 * Private member variable that stores a reference to a single FileAsFileType object
		 * (of type File), if this FileType object was restored with
		 * an expansion on the spidio_file association table.
		 * @var File _objFileAsFileType;
		 */
		private $_objFileAsFileType;

		/**
		 * Private member variable that stores a reference to an array of FileAsFileType objects
		 * (of type File[]), if this FileType object was restored with
		 * an ExpandAsArray on the spidio_file association table.
		 * @var File[] _objFileAsFileTypeArray;
		 */
		private $_objFileAsFileTypeArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a FileType from PK Info
		 * @param string $strId
		 * @return FileType
		 */
		public static function Load($strId) {
			// Use QuerySingle to Perform the Query
			return FileType::QuerySingle(
				QQ::Equal(QQN::FileType()->Id, $strId)
			);
		}

		/**
		 * Load all FileTypes
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return FileType[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call FileType::QueryArray to perform the LoadAll query
			try {
				return FileType::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all FileTypes
		 * @return int
		 */
		public static function CountAll() {
			// Call FileType::QueryCount to perform the CountAll query
			return FileType::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Create/Build out the QueryBuilder object with FileType-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'file_type');
			FileType::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'file_type');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single FileType object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return FileType the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = FileType::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new FileType object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return FileType::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of FileType objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return FileType[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = FileType::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return FileType::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of FileType objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = FileType::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'file_type_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with FileType-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				FileType::GetSelectFields($objQueryBuilder);
				FileType::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return FileType::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this FileType
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'file_type';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'extension', $strAliasPrefix . 'extension');
			$objBuilder->AddSelectItem($strTableName, 'optlock', $strAliasPrefix . 'optlock');
			$objBuilder->AddSelectItem($strTableName, 'owner', $strAliasPrefix . 'owner');
			$objBuilder->AddSelectItem($strTableName, 'group', $strAliasPrefix . 'group');
			$objBuilder->AddSelectItem($strTableName, 'perms', $strAliasPrefix . 'perms');
			$objBuilder->AddSelectItem($strTableName, 'status', $strAliasPrefix . 'status');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a FileType from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this FileType::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return FileType
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->strId == $objDbRow->GetColumn($strAliasName, 'VarChar'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'file_type__';


				$strAlias = $strAliasPrefix . 'fileasfiletype__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objFileAsFileTypeArray)) {
						$objPreviousChildItem = $objPreviousItem->_objFileAsFileTypeArray[$intPreviousChildItemCount - 1];
						$objChildItem = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasfiletype__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objFileAsFileTypeArray[] = $objChildItem;
					} else
						$objPreviousItem->_objFileAsFileTypeArray[] = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasfiletype__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'file_type__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the FileType object
			$objToReturn = new FileType();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$objToReturn->__strId = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'extension', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'extension'] : $strAliasPrefix . 'extension';
			$objToReturn->strExtension = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'optlock', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'optlock'] : $strAliasPrefix . 'optlock';
			$objToReturn->strOptlock = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'owner', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'owner'] : $strAliasPrefix . 'owner';
			$objToReturn->strOwner = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'group', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'group'] : $strAliasPrefix . 'group';
			$objToReturn->intGroup = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'perms', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'perms'] : $strAliasPrefix . 'perms';
			$objToReturn->intPerms = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'status', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'status'] : $strAliasPrefix . 'status';
			$objToReturn->intStatus = $objDbRow->GetColumn($strAliasName, 'Integer');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'file_type__';




			// Check for FileAsFileType Virtual Binding
			$strAlias = $strAliasPrefix . 'fileasfiletype__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objFileAsFileTypeArray[] = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasfiletype__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objFileAsFileType = File::InstantiateDbRow($objDbRow, $strAliasPrefix . 'fileasfiletype__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of FileTypes from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return FileType[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = FileType::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = FileType::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single FileType object,
		 * by Id Index(es)
		 * @param string $strId
		 * @return FileType
		*/
		public static function LoadById($strId) {
			return FileType::QuerySingle(
				QQ::Equal(QQN::FileType()->Id, $strId)
			);
		}
			
		/**
		 * Load a single FileType object,
		 * by Name Index(es)
		 * @param string $strName
		 * @return FileType
		*/
		public static function LoadByName($strName) {
			return FileType::QuerySingle(
				QQ::Equal(QQN::FileType()->Name, $strName)
			);
		}
			
		/**
		 * Load an array of FileType objects,
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return FileType[]
		*/
		public static function LoadArrayByOwner($strOwner, $objOptionalClauses = null) {
			// Call FileType::QueryArray to perform the LoadArrayByOwner query
			try {
				return FileType::QueryArray(
					QQ::Equal(QQN::FileType()->Owner, $strOwner),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count FileTypes
		 * by Owner Index(es)
		 * @param string $strOwner
		 * @return int
		*/
		public static function CountByOwner($strOwner) {
			// Call FileType::QueryCount to perform the CountByOwner query
			return FileType::QueryCount(
				QQ::Equal(QQN::FileType()->Owner, $strOwner)
			);
		}
			
		/**
		 * Load an array of FileType objects,
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return FileType[]
		*/
		public static function LoadArrayByStatus($intStatus, $objOptionalClauses = null) {
			// Call FileType::QueryArray to perform the LoadArrayByStatus query
			try {
				return FileType::QueryArray(
					QQ::Equal(QQN::FileType()->Status, $intStatus),
					$objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count FileTypes
		 * by Status Index(es)
		 * @param integer $intStatus
		 * @return int
		*/
		public static function CountByStatus($intStatus) {
			// Call FileType::QueryCount to perform the CountByStatus query
			return FileType::QueryCount(
				QQ::Equal(QQN::FileType()->Status, $intStatus)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this FileType
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return void
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'file_type` (
							`id`,
							`name`,
							`extension`,
							`owner`,
							`group`,
							`perms`,
							`status`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strId) . ',
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->strExtension) . ',
							' . $objDatabase->SqlVariable($this->strOwner) . ',
							' . $objDatabase->SqlVariable($this->intGroup) . ',
							' . $objDatabase->SqlVariable($this->intPerms) . ',
							' . $objDatabase->SqlVariable($this->intStatus) . '
						)
					');


				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)
					if (!$blnForceUpdate) {
						// Perform the Optimistic Locking check
						$objResult = $objDatabase->Query('
							SELECT
								`optlock`
							FROM
								`' . DB_TABLE_PREFIX_1 . 'file_type`
							WHERE
								`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
						');
						
						$objRow = $objResult->FetchArray();
						if ($objRow[0] != $this->strOptlock)
							throw new QOptimisticLockingException('FileType');
					}

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'file_type`
						SET
							`id` = ' . $objDatabase->SqlVariable($this->strId) . ',
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`extension` = ' . $objDatabase->SqlVariable($this->strExtension) . ',
							`owner` = ' . $objDatabase->SqlVariable($this->strOwner) . ',
							`group` = ' . $objDatabase->SqlVariable($this->intGroup) . ',
							`perms` = ' . $objDatabase->SqlVariable($this->intPerms) . ',
							`status` = ' . $objDatabase->SqlVariable($this->intStatus) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;
			$this->__strId = $this->strId;

			// Update Local Timestamp
			$objResult = $objDatabase->Query('
				SELECT
					`optlock`
				FROM
					`' . DB_TABLE_PREFIX_1 . 'file_type`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->__strId) . '
			');
						
			$objRow = $objResult->FetchArray();
			$this->strOptlock = $objRow[0];

			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this FileType
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this FileType with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'file_type`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->strId) . '');
		}

		/**
		 * Delete all FileTypes
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'file_type`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'file_type table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'file_type`');
		}

		/**
		 * Reload this FileType from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved FileType object.');

			// Reload the Object
			$objReloaded = FileType::Load($this->strId);

			// Update $this's local variables to match
			$this->strId = $objReloaded->strId;
			$this->__strId = $this->strId;
			$this->strName = $objReloaded->strName;
			$this->strExtension = $objReloaded->strExtension;
			$this->strOptlock = $objReloaded->strOptlock;
			$this->strOwner = $objReloaded->strOwner;
			$this->intGroup = $objReloaded->intGroup;
			$this->intPerms = $objReloaded->intPerms;
			$this->Status = $objReloaded->Status;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for strId (PK)
					 * @return string
					 */
					return $this->strId;

				case 'Name':
					/**
					 * Gets the value for strName (Unique)
					 * @return string
					 */
					return $this->strName;

				case 'Extension':
					/**
					 * Gets the value for strExtension (Not Null)
					 * @return string
					 */
					return $this->strExtension;

				case 'Optlock':
					/**
					 * Gets the value for strOptlock (Read-Only Timestamp)
					 * @return string
					 */
					return $this->strOptlock;

				case 'Owner':
					/**
					 * Gets the value for strOwner 
					 * @return string
					 */
					return $this->strOwner;

				case 'Group':
					/**
					 * Gets the value for intGroup (Not Null)
					 * @return integer
					 */
					return $this->intGroup;

				case 'Perms':
					/**
					 * Gets the value for intPerms (Not Null)
					 * @return integer
					 */
					return $this->intPerms;

				case 'Status':
					/**
					 * Gets the value for intStatus (Not Null)
					 * @return integer
					 */
					return $this->intStatus;


				///////////////////
				// Member Objects
				///////////////////

				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_FileAsFileType':
					/**
					 * Gets the value for the private _objFileAsFileType (Read-Only)
					 * if set due to an expansion on the spidio_file.file_type_id reverse relationship
					 * @return File
					 */
					return $this->_objFileAsFileType;

				case '_FileAsFileTypeArray':
					/**
					 * Gets the value for the private _objFileAsFileTypeArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_file.file_type_id reverse relationship
					 * @return File[]
					 */
					return (array) $this->_objFileAsFileTypeArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Sets the value for strId (PK)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strId = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Name':
					/**
					 * Sets the value for strName (Unique)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Extension':
					/**
					 * Sets the value for strExtension (Not Null)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strExtension = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Owner':
					/**
					 * Sets the value for strOwner 
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strOwner = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Group':
					/**
					 * Sets the value for intGroup (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intGroup = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Perms':
					/**
					 * Sets the value for intPerms (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intPerms = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'Status':
					/**
					 * Sets the value for intStatus (Not Null)
					 * @param integer $mixValue
					 * @return integer
					 */
					try {
						return ($this->intStatus = QType::Cast($mixValue, QType::Integer));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for FileAsFileType
		//-------------------------------------------------------------------

		/**
		 * Gets all associated FilesAsFileType as an array of File objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return File[]
		*/ 
		public function GetFileAsFileTypeArray($objOptionalClauses = null) {
			if ((is_null($this->strId)))
				return array();

			try {
				return File::LoadArrayByFileTypeId($this->strId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated FilesAsFileType
		 * @return int
		*/ 
		public function CountFilesAsFileType() {
			if ((is_null($this->strId)))
				return 0;

			return File::CountByFileTypeId($this->strId);
		}

		/**
		 * Associates a FileAsFileType
		 * @param File $objFile
		 * @return void
		*/ 
		public function AssociateFileAsFileType(File $objFile) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateFileAsFileType on this unsaved FileType.');
			if ((is_null($objFile->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateFileAsFileType on this FileType with an unsaved File.');

			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_file`
				SET
					`file_type_id` = ' . $objDatabase->SqlVariable($this->strId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objFile->Id) . '
			');
		}

		/**
		 * Unassociates a FileAsFileType
		 * @param File $objFile
		 * @return void
		*/ 
		public function UnassociateFileAsFileType(File $objFile) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsFileType on this unsaved FileType.');
			if ((is_null($objFile->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsFileType on this FileType with an unsaved File.');

			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_file`
				SET
					`file_type_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objFile->Id) . ' AND
					`file_type_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Unassociates all FilesAsFileType
		 * @return void
		*/ 
		public function UnassociateAllFilesAsFileType() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsFileType on this unsaved FileType.');

			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_file`
				SET
					`file_type_id` = null
				WHERE
					`file_type_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes an associated FileAsFileType
		 * @param File $objFile
		 * @return void
		*/ 
		public function DeleteAssociatedFileAsFileType(File $objFile) {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsFileType on this unsaved FileType.');
			if ((is_null($objFile->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsFileType on this FileType with an unsaved File.');

			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_file`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objFile->Id) . ' AND
					`file_type_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}

		/**
		 * Deletes all associated FilesAsFileType
		 * @return void
		*/ 
		public function DeleteAllFilesAsFileType() {
			if ((is_null($this->strId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateFileAsFileType on this unsaved FileType.');

			// Get the Database Object for this Class
			$objDatabase = FileType::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_file`
				WHERE
					`file_type_id` = ' . $objDatabase->SqlVariable($this->strId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="FileType"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:string"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="Extension" type="xsd:string"/>';
			$strToReturn .= '<element name="Optlock" type="xsd:string"/>';
			$strToReturn .= '<element name="Owner" type="xsd:string"/>';
			$strToReturn .= '<element name="Group" type="xsd:int"/>';
			$strToReturn .= '<element name="Perms" type="xsd:int"/>';
			$strToReturn .= '<element name="Status" type="xsd:int"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('FileType', $strComplexTypeArray)) {
				$strComplexTypeArray['FileType'] = FileType::GetSoapComplexTypeXml();
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, FileType::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new FileType();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->strId = $objSoapObject->Id;
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'Extension'))
				$objToReturn->strExtension = $objSoapObject->Extension;
			if (property_exists($objSoapObject, 'Optlock'))
				$objToReturn->strOptlock = $objSoapObject->Optlock;
			if (property_exists($objSoapObject, 'Owner'))
				$objToReturn->strOwner = $objSoapObject->Owner;
			if (property_exists($objSoapObject, 'Group'))
				$objToReturn->intGroup = $objSoapObject->Group;
			if (property_exists($objSoapObject, 'Perms'))
				$objToReturn->intPerms = $objSoapObject->Perms;
			if (property_exists($objSoapObject, 'Status'))
				$objToReturn->intStatus = $objSoapObject->Status;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, FileType::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeFileType extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'FileType';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'file_type';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Extension':
					return new QQNode('extension', 'Extension', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'FileAsFileType':
					return new QQReverseReferenceNodeFile($this, 'fileasfiletype', 'reverse_reference', 'file_type_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeFileType extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'FileType';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'file_type';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'string', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'Extension':
					return new QQNode('extension', 'Extension', 'string', $this);
				case 'Optlock':
					return new QQNode('optlock', 'Optlock', 'string', $this);
				case 'Owner':
					return new QQNode('owner', 'Owner', 'string', $this);
				case 'Group':
					return new QQNode('group', 'Group', 'integer', $this);
				case 'Perms':
					return new QQNode('perms', 'Perms', 'integer', $this);
				case 'Status':
					return new QQNode('status', 'Status', 'integer', $this);
				case 'FileAsFileType':
					return new QQReverseReferenceNodeFile($this, 'fileasfiletype', 'reverse_reference', 'file_type_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'string', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>
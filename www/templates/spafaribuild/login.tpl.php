<?php require('header.inc.php'); ?>

<script type="text/javascript" src="<?php _p($this->JavaScriptBase64); ?>"></script>
<script type="text/javascript" src="<?php _p($this->JavaScriptMd5); ?>"></script>
<script type="text/javascript" src="<?php _p($this->JavaScriptSha1); ?>"></script>
<script type="text/javascript"><?php _p($this->JavaScriptLogic); ?></script>

<div>
	<table id="logindata">
		<tr>
			<td><?php $this->clbUserName->Render(); ?></td>
			<td><?php $this->txtUserName->RenderWithError(); ?></td>
		</tr>
		<tr>
			<td><?php $this->clbPassword->Render(); ?></td>
			<td><?php $this->txtPassword->RenderWithError(); ?></td>
		</tr>
		<tr>
			<td colspan="2"><?php $this->btnLogin->Render(); ?>&nbsp;<?php $this->btnLoginRemember->Render(); ?></td>
		</tr>
		<tr>
			<td colspan="2"><?php $this->lblErrorMessage->Render(); ?></td>
		</tr>
	</table>
</div>

<?php $this->txtSalt->Render(); ?>
<?php $this->txtPasswordSalt->Render(); ?>

<?php require('footer.inc.php'); ?>
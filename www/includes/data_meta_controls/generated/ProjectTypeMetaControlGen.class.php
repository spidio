<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the ProjectType class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single ProjectType object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a ProjectTypeMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read ProjectType $ProjectType the actual ProjectType data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class ProjectTypeMetaControlGen extends QBaseClass {
		// General Variables
		protected $objProjectType;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of ProjectType's individual data fields
		protected $txtId;
		protected $txtName;
		protected $txtComment;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of ProjectType's individual data fields
		protected $lblId;
		protected $lblName;
		protected $lblComment;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * ProjectTypeMetaControl to edit a single ProjectType object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single ProjectType object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectTypeMetaControl
		 * @param ProjectType $objProjectType new or existing ProjectType object
		 */
		 public function __construct($objParentObject, ProjectType $objProjectType) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this ProjectTypeMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked ProjectType object
			$this->objProjectType = $objProjectType;

			// Figure out if we're Editing or Creating New
			if ($this->objProjectType->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectTypeMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing ProjectType object creation - defaults to CreateOrEdit
 		 * @return ProjectTypeMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objProjectType = ProjectType::Load($strId);

				// ProjectType was found -- return it!
				if ($objProjectType)
					return new ProjectTypeMetaControl($objParentObject, $objProjectType);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a ProjectType object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new ProjectTypeMetaControl($objParentObject, new ProjectType());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectTypeMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing ProjectType object creation - defaults to CreateOrEdit
		 * @return ProjectTypeMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return ProjectTypeMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this ProjectTypeMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing ProjectType object creation - defaults to CreateOrEdit
		 * @return ProjectTypeMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return ProjectTypeMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objProjectType->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = ProjectType::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objProjectType->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objProjectType->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = ProjectType::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objProjectType->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objProjectType->Comment;
			$this->txtComment->Required = true;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objProjectType->Comment;
			$this->lblComment->Required = true;
			return $this->lblComment;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objProjectType->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objProjectType->Owner;
			$this->txtOwner->MaxLength = ProjectType::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objProjectType->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objProjectType->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objProjectType->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objProjectType->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objProjectType->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objProjectType->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objProjectType->Status) ? AuthStatusEnum::$NameArray[$this->objProjectType->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local ProjectType object.
		 * @param boolean $blnReload reload ProjectType from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objProjectType->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objProjectType->Id;
			if ($this->lblId) $this->lblId->Text = $this->objProjectType->Id;

			if ($this->txtName) $this->txtName->Text = $this->objProjectType->Name;
			if ($this->lblName) $this->lblName->Text = $this->objProjectType->Name;

			if ($this->txtComment) $this->txtComment->Text = $this->objProjectType->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objProjectType->Comment;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objProjectType->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objProjectType->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objProjectType->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objProjectType->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objProjectType->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objProjectType->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objProjectType->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objProjectType->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objProjectType->Status) ? AuthStatusEnum::$NameArray[$this->objProjectType->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC PROJECTTYPE OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's ProjectType instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveProjectType() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objProjectType->Id = $this->txtId->Text;
				if ($this->txtName) $this->objProjectType->Name = $this->txtName->Text;
				if ($this->txtComment) $this->objProjectType->Comment = $this->txtComment->Text;
				if ($this->txtOwner) $this->objProjectType->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objProjectType->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objProjectType->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objProjectType->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the ProjectType object
				$this->objProjectType->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's ProjectType instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteProjectType() {
			$this->objProjectType->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'ProjectType': return $this->objProjectType;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to ProjectType fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to ProjectType fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
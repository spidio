<?php

// Load Framework
require_once('Framework.php');

class SpidioAuthUser extends SpidioAuth implements AuthenticationUser {
	/**
	 * Is session timed out?
	 *
	 * @var bool
	 */
	protected static $blnSessionTimedOut;
	
	/**
	 * Log in user based on given credentals.
	 *
	 * @param string $strUserName
	 * @param string $strPasswordSalt
	 * @param string $strSalt
	 * @param bool $blnKeepLogin
	 * @return bool
	 */
	public function Login($strUserName, $strPasswordSalt, $strSalt, $blnKeepLogin = false) {
		if ($objUser = User::LoadByName(ucfirst(strtolower($strUserName)))) {
			if ($this->CheckPassword($objUser->Password, $strPasswordSalt, $strSalt)) {
				$this->SessionRegisterUser($objUser);
				
				if ($blnKeepLogin) {
					$this->KeyRegister();
					$this->AddLogEntry(LogItemEnum::USERLOGIN, new UUID(self::$objSessionKey->Id));
				} else {
					$this->AddLogEntry(LogItemEnum::USERLOGIN);
				}
				
				QApplication::RedirectToReturnPage();
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Logout user and redirect to login page.
	 *
	 * @param bool $blnForce
	 * @return bool
	 */
	public function Logout($blnForce = false) {
		if ($this->IsLoggedIn()) {
			if ($blnForce) {
				$this->AddLogEntry(LogItemEnum::USERLOGOUTFORCE);
			} else {
				$this->AddLogEntry(LogItemEnum::USERLOGOUT);
			}
			
			$this->SessionUnRegisterUser();
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Load session id from cookie if it exists before for session updated.
	 *
	 */
	public function SessionLoad() {
		if ($this->ExistCookie('sid')) {
			// Load Session from database.
			self::$objSession = Session::Load($this->GetCookie('sid'));
		}
		
		if (!is_null(self::$objSession)) {
			// Check if Session is expired.
			$dttExpire = new QDateTime(QDateTime::Now);
			$dttExpire->AddSeconds(-$this->SessionGetTimeout());
			
			if ($dttExpire->IsLaterThan(self::$objSession->Changed) && !self::$blnSessionTimedOut && !$this->KeyExists()) {
				// We're now in Session timeout mode.
				self::$blnSessionTimedOut = true;
				
				// Unregister User from session
				$this->SessionUnRegisterUser();
				
				// Add Log entry.
				$this->AddLogEntry(LogItemEnum::SESSIONTIMEOUT);
				if ($this->IsLoggedIn()) {
					$this->AddLogEntry(LogItemEnum::USERLOGOUTFORCE);
				}
			}
		}
	}
}

?>
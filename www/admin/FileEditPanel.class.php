<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the File class.  It uses the code-generated
	 * FileMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a File columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both file_edit.php AND
	 * file_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class FileEditPanel extends QPanel {
		// Local instance of the FileMetaControl
		protected $mctFile;

		// Controls for File's Data Fields
		public $txtId;
		public $lstRecording;
		public $lstFileType;
		public $lstCodecIdVideoObject;
		public $lstCodecIdAudioObject;
		public $txtName;
		public $txtCustomExtension;
		public $txtComment;
		public $calAdded;
		public $calChanged;
		public $lblOptlock;
		public $txtOwner;
		public $txtGroup;
		public $txtPerms;
		public $lstStatusObject;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'FileEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the FileMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctFile = FileMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on File's data fields
			$this->txtId = $this->mctFile->txtId_Create();
			$this->lstRecording = $this->mctFile->lstRecording_Create();
			$this->lstFileType = $this->mctFile->lstFileType_Create();
			$this->lstCodecIdVideoObject = $this->mctFile->lstCodecIdVideoObject_Create();
			$this->lstCodecIdAudioObject = $this->mctFile->lstCodecIdAudioObject_Create();
			$this->txtName = $this->mctFile->txtName_Create();
			$this->txtCustomExtension = $this->mctFile->txtCustomExtension_Create();
			$this->txtComment = $this->mctFile->txtComment_Create();
			$this->calAdded = $this->mctFile->calAdded_Create();
			$this->calChanged = $this->mctFile->calChanged_Create();
			$this->lblOptlock = $this->mctFile->lblOptlock_Create();
			$this->txtOwner = $this->mctFile->txtOwner_Create();
			$this->txtGroup = $this->mctFile->txtGroup_Create();
			$this->txtPerms = $this->mctFile->txtPerms_Create();
			$this->lstStatusObject = $this->mctFile->lstStatusObject_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('File') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctFile->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the FileMetaControl
			$this->mctFile->SaveFile();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the FileMetaControl
			$this->mctFile->DeleteFile();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
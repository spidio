<?php

class BBCode {
	protected $objBBCode;

	public function __construct() {
		$this->objBBCode = new StringParser_BBCode();

		// Set Config flags
//		$this->objBBCode->setRootParagraphHandling(true);

		// Add Filters
		$this->objBBCode->addFilter(STRINGPARSER_FILTER_PRE, array($this, 'ConvertLineBreaks'));

		// Add Parsers
		$this->objBBCode->addParser(array('block', 'inline', 'link', 'listitem'), 'htmlspecialchars');
		$this->objBBCode->addParser(array('block', 'inline', 'link', 'listitem'), 'nl2br');
		$this->objBBCode->addParser('list', array($this, 'StripContents'));

		// Add Code
		$this->objBBCode->addCode('b', 'simple_replace', null, array('start_tag' => '<b>', 'end_tag' => '</b>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->objBBCode->addCode('i', 'simple_replace', null, array('start_tag' => '<i>', 'end_tag' => '</i>'), 'inline', array('listitem', 'block', 'inline', 'link'), array());
		$this->objBBCode->addCode('list', 'simple_replace', null, array('start_tag' => '<ul>', 'end_tag' => '</ul>'), 'list', array('block', 'listitem'), array());
		$this->objBBCode->setCodeFlag('list', 'paragraph_type', BBCODE_PARAGRAPH_BLOCK_ELEMENT);
		$this->objBBCode->setCodeFlag('list', 'opentag.before.newline', BBCODE_NEWLINE_DROP);
		$this->objBBCode->setCodeFlag('list', 'closetag.before.newline', BBCODE_NEWLINE_DROP);
		$this->objBBCode->addCode('*', 'simple_replace', null, array('start_tag' => '<li>', 'end_tag' => '</li>'), 'listitem', array('list'), array());
		$this->objBBCode->setCodeFlag('*', 'closetag', BBCODE_CLOSETAG_OPTIONAL);
		$this->objBBCode->setCodeFlag('*', 'paragraphs', true);
		$this->objBBCode->addCode('url', 'usecontent?', array($this, 'BBCode_url'), array('usecontent_param' => 'default'), 'link', array ('listitem', 'block', 'inline'), array ('link'));
		$this->objBBCode->addCode('link', 'callback_replace_single', array($this, 'BBCode_url'), array (), 'link', array('listitem', 'block', 'inline'), array('link'));
		$this->objBBCode->addCode('img', 'usecontent', array($this, 'BBCode_img'), array(), 'image', array('listitem', 'block', 'inline', 'link'), array());
		$this->objBBCode->addCode('bild', 'usecontent', array($this, 'BBCode_img'), array(), 'image', array('listitem', 'block', 'inline', 'link'), array());
	}
	
	
	public function Parse($strText) {
		return $this->objBBCode->parse($strText);
	}

	/**
	 * Unify line breaks of different operating systems
	 *
	 * @param string $strText
	 * @return string
	 */
	public function ConvertLineBreaks($strText) {
		return preg_replace("/\015\012|\015|\012/", "\n", $strText);
	}
	
	
	protected function ConvertInternalUrlSchemeLinks($strUrl) {
		$intSchemeLength = strlen(QApplication::GetConfig('bbcode_urlscheme'));
		if (substr($strUrl, 0, $intSchemeLength + 2) == sprintf('%s//', QApplication::GetConfig('bbcode_urlscheme'))) {
			return $strUrl;
		} else if (substr($strUrl, 0, $intSchemeLength) == QApplication::GetConfig('bbcode_urlscheme')) {
			$strComponentArray = parse_url($strUrl);
			
			if (substr($strComponentArray['path'], 0, 1) == '/') {
				$strUrl = sprintf('%s%s', str_replace(basename(QApplication::$ScriptFilename), '', QApplication::$ScriptName), substr($strComponentArray['path'], 1));
			} else {
				$strUrl = sprintf('%s%s', str_replace(basename(QApplication::$ScriptFilename), '', QApplication::$ScriptName), $strComponentArray['path']);
			}
			
			return $strUrl;
		}
	}

	/**
	 * Remove everything but the newline charachter
	 *
	 * @param string $strText
	 * @return string
	 */
	public function StripContents($strText) {
		return preg_replace("/[^\n]/", '', $strText);
	}

	/**
	 * PHP function for [img] BBCode.
	 *
	 * @param string $strAction
	 * @param string[] $strAttributeArray
	 * @param string $strContent
	 * @param string[] $strParamArray
	 * @param StringParser_BBCode_Node_Element $objNode
	 * @return bool|string
	 */
	public function BBCode_img($strAction, $strAttributeArray, $strContent, $strParamArray, StringParser_BBCode_Node_Element &$objNode) {
		if (!isset($strAttributeArray['default'])) {
			$strUrl = $this->ConvertInternalUrlSchemeLinks($strContent);
			$strAlt = htmlspecialchars($strAttributeArray['alt']);
		} else {
			$strUrl = $this->ConvertInternalUrlSchemeLinks($strAttributeArray['default']);
			$strAlt = htmlspecialchars($strContent);
		}
				
		if ($strAction == 'validate') {
			if (empty($strUrl) && empty($strAlt)) {
				return false;
			}
			
			if (substr($strUrl, 0, 5) == 'data:' || substr($strUrl, 0, 5) == 'file:' || substr($strUrl, 0, 11) == 'javascript:' || substr($strUrl, 0, 4) == 'jar:') {
				return false;
			}
			
			return true;
		}

		return sprintf('<img src="%s" alt="%s" />', $strUrl, $strAlt);
	}
	
	/**
	 * PHP function for [url] BBCode.
	 *
	 * @param string $strAction
	 * @param string[] $strAttributeArray
	 * @param string $strContent
	 * @param string[] $strParamArray
	 * @param StringParser_BBCode_Node_Element $objNode
	 * @return bool|string
	 */
	public function BBCode_url($strAction, $strAttributeArray, $strContent, $strParamArray, StringParser_BBCode_Node_Element &$objNode) {
		if (!isset($strAttributeArray['default'])) {
			$strUrl = $this->ConvertInternalUrlSchemeLinks($strContent);
			$strText = htmlspecialchars($this->ConvertInternalUrlSchemeLinks($strContent));
		} else {
			$strUrl = $this->ConvertInternalUrlSchemeLinks($strAttributeArray['default']);
			$strText = $strContent;
		}
		
		if ($strAction == 'validate') {
			if (substr($strUrl, 0, 5) == 'data:' || substr($strUrl, 0, 5) == 'file:' || substr($strUrl, 0, 11) == 'javascript:' || substr($strUrl, 0, 4) == 'jar:') {
				return false;
			}
			
			return true;
		}
		
		return sprintf('<a href="%s">%s</a>', htmlspecialchars($strUrl), $strText);
	}
}

?>
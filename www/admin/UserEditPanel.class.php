<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the User class.  It uses the code-generated
	 * UserMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a User columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both user_edit.php AND
	 * user_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class UserEditPanel extends QPanel {
		// Local instance of the UserMetaControl
		protected $mctUser;

		// Controls for User's Data Fields
		public $txtId;
		public $lstPerson;
		public $txtName;
		public $txtPassword;
		public $chkFounder;
		public $txtGroupMemberships;
		public $txtLanguageCode;
		public $txtCountryCode;
		public $calLastLoggedIn;
		public $calAdded;
		public $calChanged;
		public $lblOptlock;
		public $txtOwner;
		public $txtGroup;
		public $txtPerms;
		public $lstStatusObject;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'UserEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the UserMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctUser = UserMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on User's data fields
			$this->txtId = $this->mctUser->txtId_Create();
			$this->lstPerson = $this->mctUser->lstPerson_Create();
			$this->txtName = $this->mctUser->txtName_Create();
			$this->txtPassword = $this->mctUser->txtPassword_Create();
			$this->chkFounder = $this->mctUser->chkFounder_Create();
			$this->txtGroupMemberships = $this->mctUser->txtGroupMemberships_Create();
			$this->txtLanguageCode = $this->mctUser->txtLanguageCode_Create();
			$this->txtCountryCode = $this->mctUser->txtCountryCode_Create();
			$this->calLastLoggedIn = $this->mctUser->calLastLoggedIn_Create();
			$this->calAdded = $this->mctUser->calAdded_Create();
			$this->calChanged = $this->mctUser->calChanged_Create();
			$this->lblOptlock = $this->mctUser->lblOptlock_Create();
			$this->txtOwner = $this->mctUser->txtOwner_Create();
			$this->txtGroup = $this->mctUser->txtGroup_Create();
			$this->txtPerms = $this->mctUser->txtPerms_Create();
			$this->lstStatusObject = $this->mctUser->lstStatusObject_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('User') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctUser->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the UserMetaControl
			$this->mctUser->SaveUser();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the UserMetaControl
			$this->mctUser->DeleteUser();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
<?php
	/**
	 * This is a quick-and-dirty draft QPanel object to do Create, Edit, and Delete functionality
	 * of the Organisation class.  It uses the code-generated
	 * OrganisationMetaControl class, which has meta-methods to help with
	 * easily creating/defining controls to modify the fields of a Organisation columns.
	 *
	 * Any display customizations and presentation-tier logic can be implemented
	 * here by overriding existing or implementing new methods, properties and variables.
	 * 
	 * NOTE: This file is overwritten on any code regenerations.  If you want to make
	 * permanent changes, it is STRONGLY RECOMMENDED to move both organisation_edit.php AND
	 * organisation_edit.tpl.php out of this Form Drafts directory.
	 *
	 * @package Spidio
	 * @subpackage Drafts
	 */
	class OrganisationEditPanel extends QPanel {
		// Local instance of the OrganisationMetaControl
		protected $mctOrganisation;

		// Controls for Organisation's Data Fields
		public $txtId;
		public $lstAddress;
		public $lstPerson;
		public $txtName;
		public $txtPhone;
		public $txtPhoneA;
		public $txtPhoneC;
		public $txtEmail;
		public $lblOptlock;
		public $txtOwner;
		public $txtGroup;
		public $txtPerms;
		public $lstStatusObject;

		// Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

		// Other Controls
		public $btnSave;
		public $btnDelete;
		public $btnCancel;

		// Callback
		protected $strClosePanelMethod;

		public function __construct($objParentObject, $strClosePanelMethod, $strId = null, $strControlId = null) {
			// Call the Parent
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Setup Callback and Template
			$this->strTemplate = 'OrganisationEditPanel.tpl.php';
			$this->strClosePanelMethod = $strClosePanelMethod;

			// Construct the OrganisationMetaControl
			// MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
			$this->mctOrganisation = OrganisationMetaControl::Create($this, $strId);

			// Call MetaControl's methods to create qcontrols based on Organisation's data fields
			$this->txtId = $this->mctOrganisation->txtId_Create();
			$this->lstAddress = $this->mctOrganisation->lstAddress_Create();
			$this->lstPerson = $this->mctOrganisation->lstPerson_Create();
			$this->txtName = $this->mctOrganisation->txtName_Create();
			$this->txtPhone = $this->mctOrganisation->txtPhone_Create();
			$this->txtPhoneA = $this->mctOrganisation->txtPhoneA_Create();
			$this->txtPhoneC = $this->mctOrganisation->txtPhoneC_Create();
			$this->txtEmail = $this->mctOrganisation->txtEmail_Create();
			$this->lblOptlock = $this->mctOrganisation->lblOptlock_Create();
			$this->txtOwner = $this->mctOrganisation->txtOwner_Create();
			$this->txtGroup = $this->mctOrganisation->txtGroup_Create();
			$this->txtPerms = $this->mctOrganisation->txtPerms_Create();
			$this->lstStatusObject = $this->mctOrganisation->lstStatusObject_Create();

			// Create Buttons and Actions on this Form
			$this->btnSave = new QButton($this);
			$this->btnSave->Text = QApplication::Translate('Save');
			$this->btnSave->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnSave_Click'));
			$this->btnSave->CausesValidation = $this;

			$this->btnCancel = new QButton($this);
			$this->btnCancel->Text = QApplication::Translate('Cancel');
			$this->btnCancel->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnCancel_Click'));

			$this->btnDelete = new QButton($this);
			$this->btnDelete->Text = QApplication::Translate('Delete');
			$this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(QApplication::Translate('Are you SURE you want to DELETE this') . ' ' . QApplication::Translate('Organisation') . '?'));
			$this->btnDelete->AddAction(new QClickEvent(), new QAjaxControlAction($this, 'btnDelete_Click'));
			$this->btnDelete->Visible = $this->mctOrganisation->EditMode;
		}

		// Control AjaxAction Event Handlers
		public function btnSave_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Save" processing to the OrganisationMetaControl
			$this->mctOrganisation->SaveOrganisation();
			$this->CloseSelf(true);
		}

		public function btnDelete_Click($strFormId, $strControlId, $strParameter) {
			// Delegate "Delete" processing to the OrganisationMetaControl
			$this->mctOrganisation->DeleteOrganisation();
			$this->CloseSelf(true);
		}

		public function btnCancel_Click($strFormId, $strControlId, $strParameter) {
			$this->CloseSelf(false);
		}

		// Close Myself and Call ClosePanelMethod Callback
		protected function CloseSelf($blnChangesMade) {
			$strMethod = $this->strClosePanelMethod;
			$this->objForm->$strMethod($blnChangesMade);
		}
	}
?>
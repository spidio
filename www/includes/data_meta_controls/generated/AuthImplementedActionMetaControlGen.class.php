<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the AuthImplementedAction class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single AuthImplementedAction object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a AuthImplementedActionMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read AuthImplementedAction $AuthImplementedAction the actual AuthImplementedAction data class being edited
	 * property QListBox $AuthClassEnumIdControl
	 * property-read QLabel $AuthClassEnumIdLabel
	 * property QListBox $AuthActionIdControl
	 * property-read QLabel $AuthActionIdLabel
	 * property QListBox $AuthStatusEnumIdControl
	 * property-read QLabel $AuthStatusEnumIdLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class AuthImplementedActionMetaControlGen extends QBaseClass {
		// General Variables
		protected $objAuthImplementedAction;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of AuthImplementedAction's individual data fields
		protected $lstAuthClassEnum;
		protected $lstAuthAction;
		protected $lstAuthStatusEnum;

		// Controls that allow the viewing of AuthImplementedAction's individual data fields
		protected $lblAuthClassEnumId;
		protected $lblAuthActionId;
		protected $lblAuthStatusEnumId;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * AuthImplementedActionMetaControl to edit a single AuthImplementedAction object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single AuthImplementedAction object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthImplementedActionMetaControl
		 * @param AuthImplementedAction $objAuthImplementedAction new or existing AuthImplementedAction object
		 */
		 public function __construct($objParentObject, AuthImplementedAction $objAuthImplementedAction) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this AuthImplementedActionMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked AuthImplementedAction object
			$this->objAuthImplementedAction = $objAuthImplementedAction;

			// Figure out if we're Editing or Creating New
			if ($this->objAuthImplementedAction->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthImplementedActionMetaControl
		 * @param integer $intAuthClassEnumId primary key value
		 * @param integer $intAuthActionId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing AuthImplementedAction object creation - defaults to CreateOrEdit
 		 * @return AuthImplementedActionMetaControl
		 */
		public static function Create($objParentObject, $intAuthClassEnumId = null, $intAuthActionId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($intAuthClassEnumId) && strlen($intAuthActionId)) {
				$objAuthImplementedAction = AuthImplementedAction::Load($intAuthClassEnumId, $intAuthActionId);

				// AuthImplementedAction was found -- return it!
				if ($objAuthImplementedAction)
					return new AuthImplementedActionMetaControl($objParentObject, $objAuthImplementedAction);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a AuthImplementedAction object with PK arguments: ' . $intAuthClassEnumId . ', ' . $intAuthActionId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new AuthImplementedActionMetaControl($objParentObject, new AuthImplementedAction());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthImplementedActionMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing AuthImplementedAction object creation - defaults to CreateOrEdit
		 * @return AuthImplementedActionMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intAuthClassEnumId = QApplication::PathInfo(0);
			$intAuthActionId = QApplication::PathInfo(1);
			return AuthImplementedActionMetaControl::Create($objParentObject, $intAuthClassEnumId, $intAuthActionId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this AuthImplementedActionMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing AuthImplementedAction object creation - defaults to CreateOrEdit
		 * @return AuthImplementedActionMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$intAuthClassEnumId = QApplication::QueryString('intAuthClassEnumId');
			$intAuthActionId = QApplication::QueryString('intAuthActionId');
			return AuthImplementedActionMetaControl::Create($objParentObject, $intAuthClassEnumId, $intAuthActionId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QListBox lstAuthClassEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthClassEnum_Create($strControlId = null) {
			$this->lstAuthClassEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthClassEnum->Name = QApplication::Translate('Auth Class Enum');
			$this->lstAuthClassEnum->Required = true;
			foreach (AuthClassEnum::$NameArray as $intId => $strValue)
				$this->lstAuthClassEnum->AddItem(new QListItem($strValue, $intId, $this->objAuthImplementedAction->AuthClassEnumId == $intId));
			return $this->lstAuthClassEnum;
		}

		/**
		 * Create and setup QLabel lblAuthClassEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthClassEnumId_Create($strControlId = null) {
			$this->lblAuthClassEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthClassEnumId->Name = QApplication::Translate('Auth Class Enum');
			$this->lblAuthClassEnumId->Text = ($this->objAuthImplementedAction->AuthClassEnumId) ? AuthClassEnum::$NameArray[$this->objAuthImplementedAction->AuthClassEnumId] : null;
			$this->lblAuthClassEnumId->Required = true;
			return $this->lblAuthClassEnumId;
		}

		/**
		 * Create and setup QListBox lstAuthAction
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthAction_Create($strControlId = null) {
			$this->lstAuthAction = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthAction->Name = QApplication::Translate('Auth Action');
			$this->lstAuthAction->Required = true;
			if (!$this->blnEditMode)
				$this->lstAuthAction->AddItem(QApplication::Translate('- Select One -'), null);
			$objAuthActionArray = AuthAction::LoadAll();
			if ($objAuthActionArray) foreach ($objAuthActionArray as $objAuthAction) {
				$objListItem = new QListItem($objAuthAction->__toString(), $objAuthAction->Id);
				if (($this->objAuthImplementedAction->AuthAction) && ($this->objAuthImplementedAction->AuthAction->Id == $objAuthAction->Id))
					$objListItem->Selected = true;
				$this->lstAuthAction->AddItem($objListItem);
			}
			return $this->lstAuthAction;
		}

		/**
		 * Create and setup QLabel lblAuthActionId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthActionId_Create($strControlId = null) {
			$this->lblAuthActionId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthActionId->Name = QApplication::Translate('Auth Action');
			$this->lblAuthActionId->Text = ($this->objAuthImplementedAction->AuthAction) ? $this->objAuthImplementedAction->AuthAction->__toString() : null;
			$this->lblAuthActionId->Required = true;
			return $this->lblAuthActionId;
		}

		/**
		 * Create and setup QListBox lstAuthStatusEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAuthStatusEnum_Create($strControlId = null) {
			$this->lstAuthStatusEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstAuthStatusEnum->Name = QApplication::Translate('Auth Status Enum');
			$this->lstAuthStatusEnum->AddItem(QApplication::Translate('- Select One -'), null);
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstAuthStatusEnum->AddItem(new QListItem($strValue, $intId, $this->objAuthImplementedAction->AuthStatusEnumId == $intId));
			return $this->lstAuthStatusEnum;
		}

		/**
		 * Create and setup QLabel lblAuthStatusEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAuthStatusEnumId_Create($strControlId = null) {
			$this->lblAuthStatusEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAuthStatusEnumId->Name = QApplication::Translate('Auth Status Enum');
			$this->lblAuthStatusEnumId->Text = ($this->objAuthImplementedAction->AuthStatusEnumId) ? AuthStatusEnum::$NameArray[$this->objAuthImplementedAction->AuthStatusEnumId] : null;
			return $this->lblAuthStatusEnumId;
		}



		/**
		 * Refresh this MetaControl with Data from the local AuthImplementedAction object.
		 * @param boolean $blnReload reload AuthImplementedAction from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objAuthImplementedAction->Reload();

			if ($this->lstAuthClassEnum) $this->lstAuthClassEnum->SelectedValue = $this->objAuthImplementedAction->AuthClassEnumId;
			if ($this->lblAuthClassEnumId) $this->lblAuthClassEnumId->Text = ($this->objAuthImplementedAction->AuthClassEnumId) ? AuthClassEnum::$NameArray[$this->objAuthImplementedAction->AuthClassEnumId] : null;

			if ($this->lstAuthAction) {
					$this->lstAuthAction->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstAuthAction->AddItem(QApplication::Translate('- Select One -'), null);
				$objAuthActionArray = AuthAction::LoadAll();
				if ($objAuthActionArray) foreach ($objAuthActionArray as $objAuthAction) {
					$objListItem = new QListItem($objAuthAction->__toString(), $objAuthAction->Id);
					if (($this->objAuthImplementedAction->AuthAction) && ($this->objAuthImplementedAction->AuthAction->Id == $objAuthAction->Id))
						$objListItem->Selected = true;
					$this->lstAuthAction->AddItem($objListItem);
				}
			}
			if ($this->lblAuthActionId) $this->lblAuthActionId->Text = ($this->objAuthImplementedAction->AuthAction) ? $this->objAuthImplementedAction->AuthAction->__toString() : null;

			if ($this->lstAuthStatusEnum) $this->lstAuthStatusEnum->SelectedValue = $this->objAuthImplementedAction->AuthStatusEnumId;
			if ($this->lblAuthStatusEnumId) $this->lblAuthStatusEnumId->Text = ($this->objAuthImplementedAction->AuthStatusEnumId) ? AuthStatusEnum::$NameArray[$this->objAuthImplementedAction->AuthStatusEnumId] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC AUTHIMPLEMENTEDACTION OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's AuthImplementedAction instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveAuthImplementedAction() {
			try {
				// Update any fields for controls that have been created
				if ($this->lstAuthClassEnum) $this->objAuthImplementedAction->AuthClassEnumId = $this->lstAuthClassEnum->SelectedValue;
				if ($this->lstAuthAction) $this->objAuthImplementedAction->AuthActionId = $this->lstAuthAction->SelectedValue;
				if ($this->lstAuthStatusEnum) $this->objAuthImplementedAction->AuthStatusEnumId = $this->lstAuthStatusEnum->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the AuthImplementedAction object
				$this->objAuthImplementedAction->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's AuthImplementedAction instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteAuthImplementedAction() {
			$this->objAuthImplementedAction->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'AuthImplementedAction': return $this->objAuthImplementedAction;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to AuthImplementedAction fields -- will be created dynamically if not yet created
				case 'AuthClassEnumIdControl':
					if (!$this->lstAuthClassEnum) return $this->lstAuthClassEnum_Create();
					return $this->lstAuthClassEnum;
				case 'AuthClassEnumIdLabel':
					if (!$this->lblAuthClassEnumId) return $this->lblAuthClassEnumId_Create();
					return $this->lblAuthClassEnumId;
				case 'AuthActionIdControl':
					if (!$this->lstAuthAction) return $this->lstAuthAction_Create();
					return $this->lstAuthAction;
				case 'AuthActionIdLabel':
					if (!$this->lblAuthActionId) return $this->lblAuthActionId_Create();
					return $this->lblAuthActionId;
				case 'AuthStatusEnumIdControl':
					if (!$this->lstAuthStatusEnum) return $this->lstAuthStatusEnum_Create();
					return $this->lstAuthStatusEnum;
				case 'AuthStatusEnumIdLabel':
					if (!$this->lblAuthStatusEnumId) return $this->lblAuthStatusEnumId_Create();
					return $this->lblAuthStatusEnumId;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to AuthImplementedAction fields
					case 'AuthClassEnumIdControl':
						return ($this->lstAuthClassEnum = QType::Cast($mixValue, 'QControl'));
					case 'AuthActionIdControl':
						return ($this->lstAuthAction = QType::Cast($mixValue, 'QControl'));
					case 'AuthStatusEnumIdControl':
						return ($this->lstAuthStatusEnum = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
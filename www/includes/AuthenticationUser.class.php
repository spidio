<?php

interface AuthenticationUser {
	public function Login($strUserName, $strPasswordSalt, $strSalt);
	public function Logout($blnForce = false);
}

?>
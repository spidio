<?php

class Cookie extends QBaseClass {
	protected $strName;
	protected $strValue;
	
	protected $dttExpire;
	
	protected $strPath;
	protected $strDomain;
	
	protected $blnSecure;
	protected $blnHttpOnly = true; // Set for security reasons.
	
	protected $blnSave; // This cookie needs saving.
	protected $blnDelete; // This cookie needs deletion.
	
	/**
	 * New HTTP cookie
	 *
	 * @param string $strName Name of cookie
	 * @param string $strValue Value of cookie
	 * @param QDateTime $dttExpire Expire date of cookie 
	 * @param bool $blnSafe Cookie needs saving?
	 */
	public function __construct($strName, $strValue, $dttExpire = null, $blnSave = true) {
		$this->strName = $strName;
		$this->strValue = $strValue;
		$this->dttExpire = $dttExpire;
		$this->blnSave = $blnSave;
		$this->blnDelete = false;
	}
	
	/**
	 * String representation.
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->strValue;
	}
	
	/**
	 * Save a cookie.
	 *
	 */
	public function Save() {
		if ($this->blnSave) {
			$strHeader = sprintf('Set-Cookie: %s=%s', rawurlencode($this->strName), rawurlencode($this->strValue));
			
			if ($this->dttExpire instanceof QDateTime) {
				$strHeader .= sprintf('; expires=%s', $this->dttExpire->format(QDateTime::COOKIE));
			}
			
			if (!empty($this->strPath)) {
				$strHeader .= sprintf('; path=%s', $this->strPath);
			}
			
			if (!empty($this->strDomain)) {
				$strHeader .= sprintf('; domain=%s', $this->strDomain);
			}
			
			if ($this->blnSecure) {
				$strHeader .= '; secure';
			}
			
			if ($this->blnHttpOnly) {
				$strHeader .= '; HttpOnly';
			}
			
			header($strHeader, false);
			$this->blnSave = false;
		}
	}
	
	/**
	 * Mark cookie to delete..
	 *
	 */
	public function Delete() {
		$this->blnSave = false;
		$this->blnDelete = true;
	}
	
	/**
	 * Clean up stuff for this cookie.
	 *
	 */
	public function GarbageCollect() {
		if ($this->blnDelete) {
			$this->dttExpire = new QDateTime(QDateTime::Now);
			$this->dttExpire->AddYears(-1);
			header(sprintf('Set-Cookie: %s=; expires=%s', rawurlencode($this->strName), $this->dttExpire->format(QDateTime::COOKIE)), false);
		}
	}
	
	public function __get($strName) {
		switch ($strName) {
			case 'Name': return $this->strName;
			case 'Value': return $this->strValue;
			case 'Expire': return $this->dttExpire;
			case 'Domain': return $this->strDomain;
			case 'Path': return $this->strPath;
			case 'Secure': return $this->blnSecure;
			case 'HttpOnly': return $this->blnHttpOnly;
			case 'Save': return $this->blnSave;
			case 'Delete': return $this->blnDelete;
			
			default:
				try {
					return parent::__get($strName);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
	
	public function __set($strName, $mixValue) {
		// Mark cookie so it will be saved.
		$this->blnSave = true;
		
		// Mark cookie so it won't deleted.
		$this->blnDelete = false;
		
		switch ($strName) {
			case 'Value':
				try {
					return ($this->strValue = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Expire':
				try {
					return ($this->dttExpire = QType::Cast($mixValue, QType::DateTime));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Domain':
				try {
					return ($this->strDomain = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Path':
				try {
					return ($this->strPath = QType::Cast($mixValue, QType::String));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'Secure':
				try {
					return ($this->blnSecure = QType::Cast($mixValue, QType::Boolean));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
			case 'HttpOnly':
				try {
					return ($this->blnHttpOnly = QType::Cast($mixValue, QType::Boolean));
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
				
			default:
				try {
					return parent::__set($strName, $mixValue);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}
		}
	}
}

?>
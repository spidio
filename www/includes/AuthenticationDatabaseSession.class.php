<?php

interface AuthenticationDatabaseSession {
	function SessionStart();
	function SessionRegisterUser(User $objUser);
	function SessionUnRegisterUser();
	function SessionDestroy();
	function SessionGarbageCollect();
	function SessionGetName();
	function SessionExists($strSessionId);
	function SessionIsValid($strSessionId);
}

?>
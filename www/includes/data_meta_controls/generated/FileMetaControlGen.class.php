<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the File class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single File object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a FileMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read File $File the actual File data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $RecordingIdControl
	 * property-read QLabel $RecordingIdLabel
	 * property QListBox $FileTypeIdControl
	 * property-read QLabel $FileTypeIdLabel
	 * property QListBox $CodecIdVideoControl
	 * property-read QLabel $CodecIdVideoLabel
	 * property QListBox $CodecIdAudioControl
	 * property-read QLabel $CodecIdAudioLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $CustomExtensionControl
	 * property-read QLabel $CustomExtensionLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QDateTimePicker $ChangedControl
	 * property-read QLabel $ChangedLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class FileMetaControlGen extends QBaseClass {
		// General Variables
		protected $objFile;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of File's individual data fields
		protected $txtId;
		protected $lstRecording;
		protected $lstFileType;
		protected $lstCodecIdVideoObject;
		protected $lstCodecIdAudioObject;
		protected $txtName;
		protected $txtCustomExtension;
		protected $txtComment;
		protected $calAdded;
		protected $calChanged;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of File's individual data fields
		protected $lblId;
		protected $lblRecordingId;
		protected $lblFileTypeId;
		protected $lblCodecIdVideo;
		protected $lblCodecIdAudio;
		protected $lblName;
		protected $lblCustomExtension;
		protected $lblComment;
		protected $lblAdded;
		protected $lblChanged;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * FileMetaControl to edit a single File object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single File object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this FileMetaControl
		 * @param File $objFile new or existing File object
		 */
		 public function __construct($objParentObject, File $objFile) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this FileMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked File object
			$this->objFile = $objFile;

			// Figure out if we're Editing or Creating New
			if ($this->objFile->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this FileMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing File object creation - defaults to CreateOrEdit
 		 * @return FileMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objFile = File::Load($strId);

				// File was found -- return it!
				if ($objFile)
					return new FileMetaControl($objParentObject, $objFile);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a File object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new FileMetaControl($objParentObject, new File());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this FileMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing File object creation - defaults to CreateOrEdit
		 * @return FileMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return FileMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this FileMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing File object creation - defaults to CreateOrEdit
		 * @return FileMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return FileMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objFile->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = File::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objFile->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstRecording
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstRecording_Create($strControlId = null) {
			$this->lstRecording = new QListBox($this->objParentObject, $strControlId);
			$this->lstRecording->Name = QApplication::Translate('Recording');
			$this->lstRecording->Required = true;
			if (!$this->blnEditMode)
				$this->lstRecording->AddItem(QApplication::Translate('- Select One -'), null);
			$objRecordingArray = Recording::LoadAll();
			if ($objRecordingArray) foreach ($objRecordingArray as $objRecording) {
				$objListItem = new QListItem($objRecording->__toString(), $objRecording->Id);
				if (($this->objFile->Recording) && ($this->objFile->Recording->Id == $objRecording->Id))
					$objListItem->Selected = true;
				$this->lstRecording->AddItem($objListItem);
			}
			return $this->lstRecording;
		}

		/**
		 * Create and setup QLabel lblRecordingId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblRecordingId_Create($strControlId = null) {
			$this->lblRecordingId = new QLabel($this->objParentObject, $strControlId);
			$this->lblRecordingId->Name = QApplication::Translate('Recording');
			$this->lblRecordingId->Text = ($this->objFile->Recording) ? $this->objFile->Recording->__toString() : null;
			$this->lblRecordingId->Required = true;
			return $this->lblRecordingId;
		}

		/**
		 * Create and setup QListBox lstFileType
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstFileType_Create($strControlId = null) {
			$this->lstFileType = new QListBox($this->objParentObject, $strControlId);
			$this->lstFileType->Name = QApplication::Translate('File Type');
			$this->lstFileType->Required = true;
			if (!$this->blnEditMode)
				$this->lstFileType->AddItem(QApplication::Translate('- Select One -'), null);
			$objFileTypeArray = FileType::LoadAll();
			if ($objFileTypeArray) foreach ($objFileTypeArray as $objFileType) {
				$objListItem = new QListItem($objFileType->__toString(), $objFileType->Id);
				if (($this->objFile->FileType) && ($this->objFile->FileType->Id == $objFileType->Id))
					$objListItem->Selected = true;
				$this->lstFileType->AddItem($objListItem);
			}
			return $this->lstFileType;
		}

		/**
		 * Create and setup QLabel lblFileTypeId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblFileTypeId_Create($strControlId = null) {
			$this->lblFileTypeId = new QLabel($this->objParentObject, $strControlId);
			$this->lblFileTypeId->Name = QApplication::Translate('File Type');
			$this->lblFileTypeId->Text = ($this->objFile->FileType) ? $this->objFile->FileType->__toString() : null;
			$this->lblFileTypeId->Required = true;
			return $this->lblFileTypeId;
		}

		/**
		 * Create and setup QListBox lstCodecIdVideoObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstCodecIdVideoObject_Create($strControlId = null) {
			$this->lstCodecIdVideoObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstCodecIdVideoObject->Name = QApplication::Translate('Codec Id Video Object');
			$this->lstCodecIdVideoObject->AddItem(QApplication::Translate('- Select One -'), null);
			$objCodecIdVideoObjectArray = Codec::LoadAll();
			if ($objCodecIdVideoObjectArray) foreach ($objCodecIdVideoObjectArray as $objCodecIdVideoObject) {
				$objListItem = new QListItem($objCodecIdVideoObject->__toString(), $objCodecIdVideoObject->Id);
				if (($this->objFile->CodecIdVideoObject) && ($this->objFile->CodecIdVideoObject->Id == $objCodecIdVideoObject->Id))
					$objListItem->Selected = true;
				$this->lstCodecIdVideoObject->AddItem($objListItem);
			}
			return $this->lstCodecIdVideoObject;
		}

		/**
		 * Create and setup QLabel lblCodecIdVideo
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCodecIdVideo_Create($strControlId = null) {
			$this->lblCodecIdVideo = new QLabel($this->objParentObject, $strControlId);
			$this->lblCodecIdVideo->Name = QApplication::Translate('Codec Id Video Object');
			$this->lblCodecIdVideo->Text = ($this->objFile->CodecIdVideoObject) ? $this->objFile->CodecIdVideoObject->__toString() : null;
			return $this->lblCodecIdVideo;
		}

		/**
		 * Create and setup QListBox lstCodecIdAudioObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstCodecIdAudioObject_Create($strControlId = null) {
			$this->lstCodecIdAudioObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstCodecIdAudioObject->Name = QApplication::Translate('Codec Id Audio Object');
			$this->lstCodecIdAudioObject->AddItem(QApplication::Translate('- Select One -'), null);
			$objCodecIdAudioObjectArray = Codec::LoadAll();
			if ($objCodecIdAudioObjectArray) foreach ($objCodecIdAudioObjectArray as $objCodecIdAudioObject) {
				$objListItem = new QListItem($objCodecIdAudioObject->__toString(), $objCodecIdAudioObject->Id);
				if (($this->objFile->CodecIdAudioObject) && ($this->objFile->CodecIdAudioObject->Id == $objCodecIdAudioObject->Id))
					$objListItem->Selected = true;
				$this->lstCodecIdAudioObject->AddItem($objListItem);
			}
			return $this->lstCodecIdAudioObject;
		}

		/**
		 * Create and setup QLabel lblCodecIdAudio
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCodecIdAudio_Create($strControlId = null) {
			$this->lblCodecIdAudio = new QLabel($this->objParentObject, $strControlId);
			$this->lblCodecIdAudio->Name = QApplication::Translate('Codec Id Audio Object');
			$this->lblCodecIdAudio->Text = ($this->objFile->CodecIdAudioObject) ? $this->objFile->CodecIdAudioObject->__toString() : null;
			return $this->lblCodecIdAudio;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objFile->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = File::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objFile->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtCustomExtension
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtCustomExtension_Create($strControlId = null) {
			$this->txtCustomExtension = new QTextBox($this->objParentObject, $strControlId);
			$this->txtCustomExtension->Name = QApplication::Translate('Custom Extension');
			$this->txtCustomExtension->Text = $this->objFile->CustomExtension;
			$this->txtCustomExtension->MaxLength = File::CustomExtensionMaxLength;
			return $this->txtCustomExtension;
		}

		/**
		 * Create and setup QLabel lblCustomExtension
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCustomExtension_Create($strControlId = null) {
			$this->lblCustomExtension = new QLabel($this->objParentObject, $strControlId);
			$this->lblCustomExtension->Name = QApplication::Translate('Custom Extension');
			$this->lblCustomExtension->Text = $this->objFile->CustomExtension;
			return $this->lblCustomExtension;
		}

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objFile->Comment;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objFile->Comment;
			return $this->lblComment;
		}

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objFile->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objFile->Added) ? $this->objFile->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calChanged
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calChanged_Create($strControlId = null) {
			$this->calChanged = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calChanged->Name = QApplication::Translate('Changed');
			$this->calChanged->DateTime = $this->objFile->Changed;
			$this->calChanged->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calChanged->Required = true;
			return $this->calChanged;
		}

		/**
		 * Create and setup QLabel lblChanged
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblChanged_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblChanged = new QLabel($this->objParentObject, $strControlId);
			$this->lblChanged->Name = QApplication::Translate('Changed');
			$this->strChangedDateTimeFormat = $strDateTimeFormat;
			$this->lblChanged->Text = sprintf($this->objFile->Changed) ? $this->objFile->__toString($this->strChangedDateTimeFormat) : null;
			$this->lblChanged->Required = true;
			return $this->lblChanged;
		}

		protected $strChangedDateTimeFormat;

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objFile->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objFile->Owner;
			$this->txtOwner->MaxLength = File::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objFile->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objFile->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objFile->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objFile->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objFile->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objFile->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objFile->Status) ? AuthStatusEnum::$NameArray[$this->objFile->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local File object.
		 * @param boolean $blnReload reload File from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objFile->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objFile->Id;
			if ($this->lblId) $this->lblId->Text = $this->objFile->Id;

			if ($this->lstRecording) {
					$this->lstRecording->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstRecording->AddItem(QApplication::Translate('- Select One -'), null);
				$objRecordingArray = Recording::LoadAll();
				if ($objRecordingArray) foreach ($objRecordingArray as $objRecording) {
					$objListItem = new QListItem($objRecording->__toString(), $objRecording->Id);
					if (($this->objFile->Recording) && ($this->objFile->Recording->Id == $objRecording->Id))
						$objListItem->Selected = true;
					$this->lstRecording->AddItem($objListItem);
				}
			}
			if ($this->lblRecordingId) $this->lblRecordingId->Text = ($this->objFile->Recording) ? $this->objFile->Recording->__toString() : null;

			if ($this->lstFileType) {
					$this->lstFileType->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstFileType->AddItem(QApplication::Translate('- Select One -'), null);
				$objFileTypeArray = FileType::LoadAll();
				if ($objFileTypeArray) foreach ($objFileTypeArray as $objFileType) {
					$objListItem = new QListItem($objFileType->__toString(), $objFileType->Id);
					if (($this->objFile->FileType) && ($this->objFile->FileType->Id == $objFileType->Id))
						$objListItem->Selected = true;
					$this->lstFileType->AddItem($objListItem);
				}
			}
			if ($this->lblFileTypeId) $this->lblFileTypeId->Text = ($this->objFile->FileType) ? $this->objFile->FileType->__toString() : null;

			if ($this->lstCodecIdVideoObject) {
					$this->lstCodecIdVideoObject->RemoveAllItems();
				$this->lstCodecIdVideoObject->AddItem(QApplication::Translate('- Select One -'), null);
				$objCodecIdVideoObjectArray = Codec::LoadAll();
				if ($objCodecIdVideoObjectArray) foreach ($objCodecIdVideoObjectArray as $objCodecIdVideoObject) {
					$objListItem = new QListItem($objCodecIdVideoObject->__toString(), $objCodecIdVideoObject->Id);
					if (($this->objFile->CodecIdVideoObject) && ($this->objFile->CodecIdVideoObject->Id == $objCodecIdVideoObject->Id))
						$objListItem->Selected = true;
					$this->lstCodecIdVideoObject->AddItem($objListItem);
				}
			}
			if ($this->lblCodecIdVideo) $this->lblCodecIdVideo->Text = ($this->objFile->CodecIdVideoObject) ? $this->objFile->CodecIdVideoObject->__toString() : null;

			if ($this->lstCodecIdAudioObject) {
					$this->lstCodecIdAudioObject->RemoveAllItems();
				$this->lstCodecIdAudioObject->AddItem(QApplication::Translate('- Select One -'), null);
				$objCodecIdAudioObjectArray = Codec::LoadAll();
				if ($objCodecIdAudioObjectArray) foreach ($objCodecIdAudioObjectArray as $objCodecIdAudioObject) {
					$objListItem = new QListItem($objCodecIdAudioObject->__toString(), $objCodecIdAudioObject->Id);
					if (($this->objFile->CodecIdAudioObject) && ($this->objFile->CodecIdAudioObject->Id == $objCodecIdAudioObject->Id))
						$objListItem->Selected = true;
					$this->lstCodecIdAudioObject->AddItem($objListItem);
				}
			}
			if ($this->lblCodecIdAudio) $this->lblCodecIdAudio->Text = ($this->objFile->CodecIdAudioObject) ? $this->objFile->CodecIdAudioObject->__toString() : null;

			if ($this->txtName) $this->txtName->Text = $this->objFile->Name;
			if ($this->lblName) $this->lblName->Text = $this->objFile->Name;

			if ($this->txtCustomExtension) $this->txtCustomExtension->Text = $this->objFile->CustomExtension;
			if ($this->lblCustomExtension) $this->lblCustomExtension->Text = $this->objFile->CustomExtension;

			if ($this->txtComment) $this->txtComment->Text = $this->objFile->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objFile->Comment;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objFile->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objFile->Added) ? $this->objFile->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->calChanged) $this->calChanged->DateTime = $this->objFile->Changed;
			if ($this->lblChanged) $this->lblChanged->Text = sprintf($this->objFile->Changed) ? $this->objFile->__toString($this->strChangedDateTimeFormat) : null;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objFile->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objFile->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objFile->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objFile->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objFile->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objFile->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objFile->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objFile->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objFile->Status) ? AuthStatusEnum::$NameArray[$this->objFile->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC FILE OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's File instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveFile() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objFile->Id = $this->txtId->Text;
				if ($this->lstRecording) $this->objFile->RecordingId = $this->lstRecording->SelectedValue;
				if ($this->lstFileType) $this->objFile->FileTypeId = $this->lstFileType->SelectedValue;
				if ($this->lstCodecIdVideoObject) $this->objFile->CodecIdVideo = $this->lstCodecIdVideoObject->SelectedValue;
				if ($this->lstCodecIdAudioObject) $this->objFile->CodecIdAudio = $this->lstCodecIdAudioObject->SelectedValue;
				if ($this->txtName) $this->objFile->Name = $this->txtName->Text;
				if ($this->txtCustomExtension) $this->objFile->CustomExtension = $this->txtCustomExtension->Text;
				if ($this->txtComment) $this->objFile->Comment = $this->txtComment->Text;
				if ($this->calAdded) $this->objFile->Added = $this->calAdded->DateTime;
				if ($this->calChanged) $this->objFile->Changed = $this->calChanged->DateTime;
				if ($this->txtOwner) $this->objFile->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objFile->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objFile->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objFile->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the File object
				$this->objFile->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's File instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteFile() {
			$this->objFile->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'File': return $this->objFile;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to File fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'RecordingIdControl':
					if (!$this->lstRecording) return $this->lstRecording_Create();
					return $this->lstRecording;
				case 'RecordingIdLabel':
					if (!$this->lblRecordingId) return $this->lblRecordingId_Create();
					return $this->lblRecordingId;
				case 'FileTypeIdControl':
					if (!$this->lstFileType) return $this->lstFileType_Create();
					return $this->lstFileType;
				case 'FileTypeIdLabel':
					if (!$this->lblFileTypeId) return $this->lblFileTypeId_Create();
					return $this->lblFileTypeId;
				case 'CodecIdVideoControl':
					if (!$this->lstCodecIdVideoObject) return $this->lstCodecIdVideoObject_Create();
					return $this->lstCodecIdVideoObject;
				case 'CodecIdVideoLabel':
					if (!$this->lblCodecIdVideo) return $this->lblCodecIdVideo_Create();
					return $this->lblCodecIdVideo;
				case 'CodecIdAudioControl':
					if (!$this->lstCodecIdAudioObject) return $this->lstCodecIdAudioObject_Create();
					return $this->lstCodecIdAudioObject;
				case 'CodecIdAudioLabel':
					if (!$this->lblCodecIdAudio) return $this->lblCodecIdAudio_Create();
					return $this->lblCodecIdAudio;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'CustomExtensionControl':
					if (!$this->txtCustomExtension) return $this->txtCustomExtension_Create();
					return $this->txtCustomExtension;
				case 'CustomExtensionLabel':
					if (!$this->lblCustomExtension) return $this->lblCustomExtension_Create();
					return $this->lblCustomExtension;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'ChangedControl':
					if (!$this->calChanged) return $this->calChanged_Create();
					return $this->calChanged;
				case 'ChangedLabel':
					if (!$this->lblChanged) return $this->lblChanged_Create();
					return $this->lblChanged;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to File fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'RecordingIdControl':
						return ($this->lstRecording = QType::Cast($mixValue, 'QControl'));
					case 'FileTypeIdControl':
						return ($this->lstFileType = QType::Cast($mixValue, 'QControl'));
					case 'CodecIdVideoControl':
						return ($this->lstCodecIdVideoObject = QType::Cast($mixValue, 'QControl'));
					case 'CodecIdAudioControl':
						return ($this->lstCodecIdAudioObject = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'CustomExtensionControl':
						return ($this->txtCustomExtension = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'ChangedControl':
						return ($this->calChanged = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
<?php

// Load Spidio SOAP foundation.
require('prepend.inc.php');

class SoapidioAuth extends SoapidioService {
	/**
	 * Get AuthenticationType
	 *
	 * @return int
	 */
	public function GetAuthenticationType() {
		return $this->objAuth->AuthenticationType;
	}
	
	/**
	 * Login a user based on given credentals.
	 *
	 * @param string $strUserName
	 * @param string $strPasswordSalt
	 * @param string $strSalt
	 * @return UUID
	 */
	public function Login($strUserName, $strPasswordSalt, $strSalt) {
		if (!$this->objAuth->IsLoggedIn()) {
			if ($this->objAuth->Login($strUserName, $strPasswordSalt, $strSalt)) {
				return $this->objAuth->SessionGetName();
			}
		} else {
			return false;
		}
	}
	
	/**
	 * Logout the logged user.
	 *
	 * @return bool
	 */
	public function Logout() {
		return $this->objAuth->Logout();
	}
	
	/**
	 * Clear current Session.
	 *
	 * @return string
	 */
	public function ResetSession() {
		$this->objAuth->ResetSession();
		return $this->objAuth->SessionGetName();
	}
	
	/**
	 * Attach given Session to current Session.
	 *
	 * @param Session $objSession
	 * @return string $strSessionId
	 */
	public function AttachSession(Session $objSession) {
		$this->objAuth->Session = $objSession;
		
		if ($objSession->AutoLogin) {
			$this->objAuth->SessionKey = $objSession->SessionKeyAsSession;
		}
		
		return $this->objAuth->SessionGetName();
	}
	
	/**
	 * Detach given Session.
	 *
	 */
	public function DetachSession() {
		$this->objAuth->Session = null;
		$this->objAuth->SessionKey = null;
	}
	
	/**
	 * Check if Session exists.
	 *
	 * @param string $strSessionId
	 * @return bool
	 */
	public function CheckSession($strSessionId) {
		return $this->objAuth->SessionExists($strSessionId);
	}
	
	/**
	 * Check if Session has an assigned User.
	 *
	 * @param string $strSessionId
	 * @return bool
	 */
	public function ValidSession($strSessionId) {
		return $this->objAuth->SessionIsValid($strSessionId);
	}
}

SoapidioAuth::Run('SoapidioAuth', 'http://spidio.sourceforge.net/namespaces/auth');

?>
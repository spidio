<?php
	require(__DATAGEN_CLASSES__ . '/AuthPrivilegeTypeEnumGen.class.php');

	/**
	 * The AuthPrivilegeTypeEnum class defined here contains any
	 * customized code for the AuthPrivilegeTypeEnum enumerated type. 
	 * 
	 * It represents the enumerated values found in the "' . DB_TABLE_PREFIX_1 . 'auth_privilege_type_enum" table in the database,
	 * and extends from the code generated abstract AuthPrivilegeTypeEnumGen
	 * class, which contains all the values extracted from the database.
	 * 
	 * Type classes which are generally used to attach a type to data object.
	 * However, they may be used as simple database indepedant enumerated type.
	 * 
	 * @package Spidio
	 * @subpackage DataObjects
	 */
	abstract class AuthPrivilegeTypeEnum extends AuthPrivilegeTypeEnumGen {
	}
?>
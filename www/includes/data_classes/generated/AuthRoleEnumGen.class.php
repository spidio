<?php
	/**
	 * The AuthRoleEnum class defined here contains
	 * code for the AuthRoleEnum enumerated type.  It represents
	 * the enumerated values found in the "spidio_auth_role_enum" table
	 * in the database.
	 * 
	 * To use, you should use the AuthRoleEnum subclass which
	 * extends this AuthRoleEnumGen class.
	 * 
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthRoleEnum class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 */
	abstract class AuthRoleEnumGen extends QBaseClass {
		const _self = 1;
		const group = 2;
		const user = 3;
		const creator = 4;
		const session = 5;
		const other = 6;

		const MaxId = 6;

		public static $NameArray = array(
			1 => '_self',
			2 => 'group',
			3 => 'user',
			4 => 'creator',
			5 => 'session',
			6 => 'other');

		public static $TokenArray = array(
			1 => '_self',
			2 => 'group',
			3 => 'user',
			4 => 'creator',
			5 => 'session',
			6 => 'other');

		public static $ReverseNameArray = array(
			'_self' => 1,
			'group' => 2,
			'user' => 3,
			'creator' => 4,
			'session' => 5,
			'other' => 6);

		public static $ReverseTokenArray = array(
			'_self' => 1,
			'group' => 2,
			'user' => 3,
			'creator' => 4,
			'session' => 5,
			'other' => 6);

		public static function ToString($intAuthRoleEnumId) {
			switch ($intAuthRoleEnumId) {
				case 1: return '_self';
				case 2: return 'group';
				case 3: return 'user';
				case 4: return 'creator';
				case 5: return 'session';
				case 6: return 'other';
				default:
					throw new QCallerException(sprintf('Invalid intAuthRoleEnumId: %s', $intAuthRoleEnumId));
			}
		}

		public static function ToToken($intAuthRoleEnumId) {
			switch ($intAuthRoleEnumId) {
				case 1: return '_self';
				case 2: return 'group';
				case 3: return 'user';
				case 4: return 'creator';
				case 5: return 'session';
				case 6: return 'other';
				default:
					throw new QCallerException(sprintf('Invalid intAuthRoleEnumId: %s', $intAuthRoleEnumId));
			}
		}

	}
?>
<?php 
	// ClassPaths for the Address class
		QApplicationBase::$ClassFile['address'] = __DATA_CLASSES__ . '/Address.class.php';
		QApplicationBase::$ClassFile['qqnodeaddress'] = __DATA_CLASSES__ . '/Address.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeaddress'] = __DATA_CLASSES__ . '/Address.class.php';
		QApplicationBase::$ClassFile['addressmetacontrol'] = __DATA_META_CONTROLS__ . '/AddressMetaControl.class.php';
		QApplicationBase::$ClassFile['addressdatagrid'] = __DATA_META_CONTROLS__ . '/AddressDataGrid.class.php';

	// ClassPaths for the AuthAction class
		QApplicationBase::$ClassFile['authaction'] = __DATA_CLASSES__ . '/AuthAction.class.php';
		QApplicationBase::$ClassFile['qqnodeauthaction'] = __DATA_CLASSES__ . '/AuthAction.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeauthaction'] = __DATA_CLASSES__ . '/AuthAction.class.php';
		QApplicationBase::$ClassFile['authactionmetacontrol'] = __DATA_META_CONTROLS__ . '/AuthActionMetaControl.class.php';
		QApplicationBase::$ClassFile['authactiondatagrid'] = __DATA_META_CONTROLS__ . '/AuthActionDataGrid.class.php';

	// ClassPaths for the AuthGroup class
		QApplicationBase::$ClassFile['authgroup'] = __DATA_CLASSES__ . '/AuthGroup.class.php';
		QApplicationBase::$ClassFile['qqnodeauthgroup'] = __DATA_CLASSES__ . '/AuthGroup.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeauthgroup'] = __DATA_CLASSES__ . '/AuthGroup.class.php';
		QApplicationBase::$ClassFile['authgroupmetacontrol'] = __DATA_META_CONTROLS__ . '/AuthGroupMetaControl.class.php';
		QApplicationBase::$ClassFile['authgroupdatagrid'] = __DATA_META_CONTROLS__ . '/AuthGroupDataGrid.class.php';

	// ClassPaths for the AuthImplementedAction class
		QApplicationBase::$ClassFile['authimplementedaction'] = __DATA_CLASSES__ . '/AuthImplementedAction.class.php';
		QApplicationBase::$ClassFile['qqnodeauthimplementedaction'] = __DATA_CLASSES__ . '/AuthImplementedAction.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeauthimplementedaction'] = __DATA_CLASSES__ . '/AuthImplementedAction.class.php';
		QApplicationBase::$ClassFile['authimplementedactionmetacontrol'] = __DATA_META_CONTROLS__ . '/AuthImplementedActionMetaControl.class.php';
		QApplicationBase::$ClassFile['authimplementedactiondatagrid'] = __DATA_META_CONTROLS__ . '/AuthImplementedActionDataGrid.class.php';

	// ClassPaths for the AuthPrivilege class
		QApplicationBase::$ClassFile['authprivilege'] = __DATA_CLASSES__ . '/AuthPrivilege.class.php';
		QApplicationBase::$ClassFile['qqnodeauthprivilege'] = __DATA_CLASSES__ . '/AuthPrivilege.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeauthprivilege'] = __DATA_CLASSES__ . '/AuthPrivilege.class.php';
		QApplicationBase::$ClassFile['authprivilegemetacontrol'] = __DATA_META_CONTROLS__ . '/AuthPrivilegeMetaControl.class.php';
		QApplicationBase::$ClassFile['authprivilegedatagrid'] = __DATA_META_CONTROLS__ . '/AuthPrivilegeDataGrid.class.php';

	// ClassPaths for the Camera class
		QApplicationBase::$ClassFile['camera'] = __DATA_CLASSES__ . '/Camera.class.php';
		QApplicationBase::$ClassFile['qqnodecamera'] = __DATA_CLASSES__ . '/Camera.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodecamera'] = __DATA_CLASSES__ . '/Camera.class.php';
		QApplicationBase::$ClassFile['camerametacontrol'] = __DATA_META_CONTROLS__ . '/CameraMetaControl.class.php';
		QApplicationBase::$ClassFile['cameradatagrid'] = __DATA_META_CONTROLS__ . '/CameraDataGrid.class.php';

	// ClassPaths for the CameraPurpose class
		QApplicationBase::$ClassFile['camerapurpose'] = __DATA_CLASSES__ . '/CameraPurpose.class.php';
		QApplicationBase::$ClassFile['qqnodecamerapurpose'] = __DATA_CLASSES__ . '/CameraPurpose.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodecamerapurpose'] = __DATA_CLASSES__ . '/CameraPurpose.class.php';
		QApplicationBase::$ClassFile['camerapurposemetacontrol'] = __DATA_META_CONTROLS__ . '/CameraPurposeMetaControl.class.php';
		QApplicationBase::$ClassFile['camerapurposedatagrid'] = __DATA_META_CONTROLS__ . '/CameraPurposeDataGrid.class.php';

	// ClassPaths for the Codec class
		QApplicationBase::$ClassFile['codec'] = __DATA_CLASSES__ . '/Codec.class.php';
		QApplicationBase::$ClassFile['qqnodecodec'] = __DATA_CLASSES__ . '/Codec.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodecodec'] = __DATA_CLASSES__ . '/Codec.class.php';
		QApplicationBase::$ClassFile['codecmetacontrol'] = __DATA_META_CONTROLS__ . '/CodecMetaControl.class.php';
		QApplicationBase::$ClassFile['codecdatagrid'] = __DATA_META_CONTROLS__ . '/CodecDataGrid.class.php';

	// ClassPaths for the Config class
		QApplicationBase::$ClassFile['config'] = __DATA_CLASSES__ . '/Config.class.php';
		QApplicationBase::$ClassFile['qqnodeconfig'] = __DATA_CLASSES__ . '/Config.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeconfig'] = __DATA_CLASSES__ . '/Config.class.php';
		QApplicationBase::$ClassFile['configmetacontrol'] = __DATA_META_CONTROLS__ . '/ConfigMetaControl.class.php';
		QApplicationBase::$ClassFile['configdatagrid'] = __DATA_META_CONTROLS__ . '/ConfigDataGrid.class.php';

	// ClassPaths for the File class
		QApplicationBase::$ClassFile['file'] = __DATA_CLASSES__ . '/File.class.php';
		QApplicationBase::$ClassFile['qqnodefile'] = __DATA_CLASSES__ . '/File.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodefile'] = __DATA_CLASSES__ . '/File.class.php';
		QApplicationBase::$ClassFile['filemetacontrol'] = __DATA_META_CONTROLS__ . '/FileMetaControl.class.php';
		QApplicationBase::$ClassFile['filedatagrid'] = __DATA_META_CONTROLS__ . '/FileDataGrid.class.php';

	// ClassPaths for the FileType class
		QApplicationBase::$ClassFile['filetype'] = __DATA_CLASSES__ . '/FileType.class.php';
		QApplicationBase::$ClassFile['qqnodefiletype'] = __DATA_CLASSES__ . '/FileType.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodefiletype'] = __DATA_CLASSES__ . '/FileType.class.php';
		QApplicationBase::$ClassFile['filetypemetacontrol'] = __DATA_META_CONTROLS__ . '/FileTypeMetaControl.class.php';
		QApplicationBase::$ClassFile['filetypedatagrid'] = __DATA_META_CONTROLS__ . '/FileTypeDataGrid.class.php';

	// ClassPaths for the Library class
		QApplicationBase::$ClassFile['library'] = __DATA_CLASSES__ . '/Library.class.php';
		QApplicationBase::$ClassFile['qqnodelibrary'] = __DATA_CLASSES__ . '/Library.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodelibrary'] = __DATA_CLASSES__ . '/Library.class.php';
		QApplicationBase::$ClassFile['librarymetacontrol'] = __DATA_META_CONTROLS__ . '/LibraryMetaControl.class.php';
		QApplicationBase::$ClassFile['librarydatagrid'] = __DATA_META_CONTROLS__ . '/LibraryDataGrid.class.php';

	// ClassPaths for the Location class
		QApplicationBase::$ClassFile['location'] = __DATA_CLASSES__ . '/Location.class.php';
		QApplicationBase::$ClassFile['qqnodelocation'] = __DATA_CLASSES__ . '/Location.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodelocation'] = __DATA_CLASSES__ . '/Location.class.php';
		QApplicationBase::$ClassFile['locationmetacontrol'] = __DATA_META_CONTROLS__ . '/LocationMetaControl.class.php';
		QApplicationBase::$ClassFile['locationdatagrid'] = __DATA_META_CONTROLS__ . '/LocationDataGrid.class.php';

	// ClassPaths for the Log class
		QApplicationBase::$ClassFile['log'] = __DATA_CLASSES__ . '/Log.class.php';
		QApplicationBase::$ClassFile['qqnodelog'] = __DATA_CLASSES__ . '/Log.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodelog'] = __DATA_CLASSES__ . '/Log.class.php';
		QApplicationBase::$ClassFile['logmetacontrol'] = __DATA_META_CONTROLS__ . '/LogMetaControl.class.php';
		QApplicationBase::$ClassFile['logdatagrid'] = __DATA_META_CONTROLS__ . '/LogDataGrid.class.php';

	// ClassPaths for the News class
		QApplicationBase::$ClassFile['news'] = __DATA_CLASSES__ . '/News.class.php';
		QApplicationBase::$ClassFile['qqnodenews'] = __DATA_CLASSES__ . '/News.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodenews'] = __DATA_CLASSES__ . '/News.class.php';
		QApplicationBase::$ClassFile['newsmetacontrol'] = __DATA_META_CONTROLS__ . '/NewsMetaControl.class.php';
		QApplicationBase::$ClassFile['newsdatagrid'] = __DATA_META_CONTROLS__ . '/NewsDataGrid.class.php';

	// ClassPaths for the Organisation class
		QApplicationBase::$ClassFile['organisation'] = __DATA_CLASSES__ . '/Organisation.class.php';
		QApplicationBase::$ClassFile['qqnodeorganisation'] = __DATA_CLASSES__ . '/Organisation.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeorganisation'] = __DATA_CLASSES__ . '/Organisation.class.php';
		QApplicationBase::$ClassFile['organisationmetacontrol'] = __DATA_META_CONTROLS__ . '/OrganisationMetaControl.class.php';
		QApplicationBase::$ClassFile['organisationdatagrid'] = __DATA_META_CONTROLS__ . '/OrganisationDataGrid.class.php';

	// ClassPaths for the Person class
		QApplicationBase::$ClassFile['person'] = __DATA_CLASSES__ . '/Person.class.php';
		QApplicationBase::$ClassFile['qqnodeperson'] = __DATA_CLASSES__ . '/Person.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeperson'] = __DATA_CLASSES__ . '/Person.class.php';
		QApplicationBase::$ClassFile['personmetacontrol'] = __DATA_META_CONTROLS__ . '/PersonMetaControl.class.php';
		QApplicationBase::$ClassFile['persondatagrid'] = __DATA_META_CONTROLS__ . '/PersonDataGrid.class.php';

	// ClassPaths for the Project class
		QApplicationBase::$ClassFile['project'] = __DATA_CLASSES__ . '/Project.class.php';
		QApplicationBase::$ClassFile['qqnodeproject'] = __DATA_CLASSES__ . '/Project.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeproject'] = __DATA_CLASSES__ . '/Project.class.php';
		QApplicationBase::$ClassFile['projectmetacontrol'] = __DATA_META_CONTROLS__ . '/ProjectMetaControl.class.php';
		QApplicationBase::$ClassFile['projectdatagrid'] = __DATA_META_CONTROLS__ . '/ProjectDataGrid.class.php';

	// ClassPaths for the ProjectType class
		QApplicationBase::$ClassFile['projecttype'] = __DATA_CLASSES__ . '/ProjectType.class.php';
		QApplicationBase::$ClassFile['qqnodeprojecttype'] = __DATA_CLASSES__ . '/ProjectType.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeprojecttype'] = __DATA_CLASSES__ . '/ProjectType.class.php';
		QApplicationBase::$ClassFile['projecttypemetacontrol'] = __DATA_META_CONTROLS__ . '/ProjectTypeMetaControl.class.php';
		QApplicationBase::$ClassFile['projecttypedatagrid'] = __DATA_META_CONTROLS__ . '/ProjectTypeDataGrid.class.php';

	// ClassPaths for the Recording class
		QApplicationBase::$ClassFile['recording'] = __DATA_CLASSES__ . '/Recording.class.php';
		QApplicationBase::$ClassFile['qqnoderecording'] = __DATA_CLASSES__ . '/Recording.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenoderecording'] = __DATA_CLASSES__ . '/Recording.class.php';
		QApplicationBase::$ClassFile['recordingmetacontrol'] = __DATA_META_CONTROLS__ . '/RecordingMetaControl.class.php';
		QApplicationBase::$ClassFile['recordingdatagrid'] = __DATA_META_CONTROLS__ . '/RecordingDataGrid.class.php';

	// ClassPaths for the RecordingType class
		QApplicationBase::$ClassFile['recordingtype'] = __DATA_CLASSES__ . '/RecordingType.class.php';
		QApplicationBase::$ClassFile['qqnoderecordingtype'] = __DATA_CLASSES__ . '/RecordingType.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenoderecordingtype'] = __DATA_CLASSES__ . '/RecordingType.class.php';
		QApplicationBase::$ClassFile['recordingtypemetacontrol'] = __DATA_META_CONTROLS__ . '/RecordingTypeMetaControl.class.php';
		QApplicationBase::$ClassFile['recordingtypedatagrid'] = __DATA_META_CONTROLS__ . '/RecordingTypeDataGrid.class.php';

	// ClassPaths for the Session class
		QApplicationBase::$ClassFile['session'] = __DATA_CLASSES__ . '/Session.class.php';
		QApplicationBase::$ClassFile['qqnodesession'] = __DATA_CLASSES__ . '/Session.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodesession'] = __DATA_CLASSES__ . '/Session.class.php';
		QApplicationBase::$ClassFile['sessionmetacontrol'] = __DATA_META_CONTROLS__ . '/SessionMetaControl.class.php';
		QApplicationBase::$ClassFile['sessiondatagrid'] = __DATA_META_CONTROLS__ . '/SessionDataGrid.class.php';

	// ClassPaths for the SessionKey class
		QApplicationBase::$ClassFile['sessionkey'] = __DATA_CLASSES__ . '/SessionKey.class.php';
		QApplicationBase::$ClassFile['qqnodesessionkey'] = __DATA_CLASSES__ . '/SessionKey.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodesessionkey'] = __DATA_CLASSES__ . '/SessionKey.class.php';
		QApplicationBase::$ClassFile['sessionkeymetacontrol'] = __DATA_META_CONTROLS__ . '/SessionKeyMetaControl.class.php';
		QApplicationBase::$ClassFile['sessionkeydatagrid'] = __DATA_META_CONTROLS__ . '/SessionKeyDataGrid.class.php';

	// ClassPaths for the Tape class
		QApplicationBase::$ClassFile['tape'] = __DATA_CLASSES__ . '/Tape.class.php';
		QApplicationBase::$ClassFile['qqnodetape'] = __DATA_CLASSES__ . '/Tape.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodetape'] = __DATA_CLASSES__ . '/Tape.class.php';
		QApplicationBase::$ClassFile['tapemetacontrol'] = __DATA_META_CONTROLS__ . '/TapeMetaControl.class.php';
		QApplicationBase::$ClassFile['tapedatagrid'] = __DATA_META_CONTROLS__ . '/TapeDataGrid.class.php';

	// ClassPaths for the TapeLocation class
		QApplicationBase::$ClassFile['tapelocation'] = __DATA_CLASSES__ . '/TapeLocation.class.php';
		QApplicationBase::$ClassFile['qqnodetapelocation'] = __DATA_CLASSES__ . '/TapeLocation.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodetapelocation'] = __DATA_CLASSES__ . '/TapeLocation.class.php';
		QApplicationBase::$ClassFile['tapelocationmetacontrol'] = __DATA_META_CONTROLS__ . '/TapeLocationMetaControl.class.php';
		QApplicationBase::$ClassFile['tapelocationdatagrid'] = __DATA_META_CONTROLS__ . '/TapeLocationDataGrid.class.php';

	// ClassPaths for the Template class
		QApplicationBase::$ClassFile['template'] = __DATA_CLASSES__ . '/Template.class.php';
		QApplicationBase::$ClassFile['qqnodetemplate'] = __DATA_CLASSES__ . '/Template.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodetemplate'] = __DATA_CLASSES__ . '/Template.class.php';
		QApplicationBase::$ClassFile['templatemetacontrol'] = __DATA_META_CONTROLS__ . '/TemplateMetaControl.class.php';
		QApplicationBase::$ClassFile['templatedatagrid'] = __DATA_META_CONTROLS__ . '/TemplateDataGrid.class.php';

	// ClassPaths for the TemplateConfig class
		QApplicationBase::$ClassFile['templateconfig'] = __DATA_CLASSES__ . '/TemplateConfig.class.php';
		QApplicationBase::$ClassFile['qqnodetemplateconfig'] = __DATA_CLASSES__ . '/TemplateConfig.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodetemplateconfig'] = __DATA_CLASSES__ . '/TemplateConfig.class.php';
		QApplicationBase::$ClassFile['templateconfigmetacontrol'] = __DATA_META_CONTROLS__ . '/TemplateConfigMetaControl.class.php';
		QApplicationBase::$ClassFile['templateconfigdatagrid'] = __DATA_META_CONTROLS__ . '/TemplateConfigDataGrid.class.php';

	// ClassPaths for the User class
		QApplicationBase::$ClassFile['user'] = __DATA_CLASSES__ . '/User.class.php';
		QApplicationBase::$ClassFile['qqnodeuser'] = __DATA_CLASSES__ . '/User.class.php';
		QApplicationBase::$ClassFile['qqreversereferencenodeuser'] = __DATA_CLASSES__ . '/User.class.php';
		QApplicationBase::$ClassFile['usermetacontrol'] = __DATA_META_CONTROLS__ . '/UserMetaControl.class.php';
		QApplicationBase::$ClassFile['userdatagrid'] = __DATA_META_CONTROLS__ . '/UserDataGrid.class.php';

?>
<?php

// Define autoload function.
function __autoload($strClassName) {
	if (file_exists($strFileName = sprintf('%s/%s.class.php', dirname(__FILE__), $strClassName))) {
		require_once($strFileName);
	} else {
		throw new Exception(sprintf('The requested class (%s) cannot be loaded', $strClassName));
	}
}

?>
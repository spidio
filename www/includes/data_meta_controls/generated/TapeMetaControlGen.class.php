<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Tape class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Tape object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a TapeMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Tape $Tape the actual Tape data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QLabel $NumberControl
	 * property-read QLabel $NumberLabel
	 * property QListBox $LibraryIdControl
	 * property-read QLabel $LibraryIdLabel
	 * property QListBox $OrganisationIdControl
	 * property-read QLabel $OrganisationIdLabel
	 * property QListBox $TapeModeEnumIdControl
	 * property-read QLabel $TapeModeEnumIdLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class TapeMetaControlGen extends QBaseClass {
		// General Variables
		protected $objTape;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Tape's individual data fields
		protected $txtId;
		protected $lblNumber;
		protected $lstLibrary;
		protected $lstOrganisation;
		protected $lstTapeModeEnum;
		protected $calAdded;
		protected $txtComment;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Tape's individual data fields
		protected $lblId;
		protected $lblLibraryId;
		protected $lblOrganisationId;
		protected $lblTapeModeEnumId;
		protected $lblAdded;
		protected $lblComment;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * TapeMetaControl to edit a single Tape object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Tape object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeMetaControl
		 * @param Tape $objTape new or existing Tape object
		 */
		 public function __construct($objParentObject, Tape $objTape) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this TapeMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Tape object
			$this->objTape = $objTape;

			// Figure out if we're Editing or Creating New
			if ($this->objTape->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Tape object creation - defaults to CreateOrEdit
 		 * @return TapeMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objTape = Tape::Load($strId);

				// Tape was found -- return it!
				if ($objTape)
					return new TapeMetaControl($objParentObject, $objTape);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Tape object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new TapeMetaControl($objParentObject, new Tape());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Tape object creation - defaults to CreateOrEdit
		 * @return TapeMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return TapeMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this TapeMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Tape object creation - defaults to CreateOrEdit
		 * @return TapeMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return TapeMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objTape->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Tape::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objTape->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QLabel lblNumber
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblNumber_Create($strControlId = null) {
			$this->lblNumber = new QLabel($this->objParentObject, $strControlId);
			$this->lblNumber->Name = QApplication::Translate('Number');
			if ($this->blnEditMode)
				$this->lblNumber->Text = $this->objTape->Number;
			else
				$this->lblNumber->Text = 'N/A';
			return $this->lblNumber;
		}

		/**
		 * Create and setup QListBox lstLibrary
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstLibrary_Create($strControlId = null) {
			$this->lstLibrary = new QListBox($this->objParentObject, $strControlId);
			$this->lstLibrary->Name = QApplication::Translate('Library');
			$this->lstLibrary->Required = true;
			if (!$this->blnEditMode)
				$this->lstLibrary->AddItem(QApplication::Translate('- Select One -'), null);
			$objLibraryArray = Library::LoadAll();
			if ($objLibraryArray) foreach ($objLibraryArray as $objLibrary) {
				$objListItem = new QListItem($objLibrary->__toString(), $objLibrary->Id);
				if (($this->objTape->Library) && ($this->objTape->Library->Id == $objLibrary->Id))
					$objListItem->Selected = true;
				$this->lstLibrary->AddItem($objListItem);
			}
			return $this->lstLibrary;
		}

		/**
		 * Create and setup QLabel lblLibraryId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblLibraryId_Create($strControlId = null) {
			$this->lblLibraryId = new QLabel($this->objParentObject, $strControlId);
			$this->lblLibraryId->Name = QApplication::Translate('Library');
			$this->lblLibraryId->Text = ($this->objTape->Library) ? $this->objTape->Library->__toString() : null;
			$this->lblLibraryId->Required = true;
			return $this->lblLibraryId;
		}

		/**
		 * Create and setup QListBox lstOrganisation
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstOrganisation_Create($strControlId = null) {
			$this->lstOrganisation = new QListBox($this->objParentObject, $strControlId);
			$this->lstOrganisation->Name = QApplication::Translate('Organisation');
			$this->lstOrganisation->Required = true;
			if (!$this->blnEditMode)
				$this->lstOrganisation->AddItem(QApplication::Translate('- Select One -'), null);
			$objOrganisationArray = Organisation::LoadAll();
			if ($objOrganisationArray) foreach ($objOrganisationArray as $objOrganisation) {
				$objListItem = new QListItem($objOrganisation->__toString(), $objOrganisation->Id);
				if (($this->objTape->Organisation) && ($this->objTape->Organisation->Id == $objOrganisation->Id))
					$objListItem->Selected = true;
				$this->lstOrganisation->AddItem($objListItem);
			}
			return $this->lstOrganisation;
		}

		/**
		 * Create and setup QLabel lblOrganisationId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOrganisationId_Create($strControlId = null) {
			$this->lblOrganisationId = new QLabel($this->objParentObject, $strControlId);
			$this->lblOrganisationId->Name = QApplication::Translate('Organisation');
			$this->lblOrganisationId->Text = ($this->objTape->Organisation) ? $this->objTape->Organisation->__toString() : null;
			$this->lblOrganisationId->Required = true;
			return $this->lblOrganisationId;
		}

		/**
		 * Create and setup QListBox lstTapeModeEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstTapeModeEnum_Create($strControlId = null) {
			$this->lstTapeModeEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstTapeModeEnum->Name = QApplication::Translate('Tape Mode Enum');
			$this->lstTapeModeEnum->AddItem(QApplication::Translate('- Select One -'), null);
			foreach (TapeModeEnum::$NameArray as $intId => $strValue)
				$this->lstTapeModeEnum->AddItem(new QListItem($strValue, $intId, $this->objTape->TapeModeEnumId == $intId));
			return $this->lstTapeModeEnum;
		}

		/**
		 * Create and setup QLabel lblTapeModeEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblTapeModeEnumId_Create($strControlId = null) {
			$this->lblTapeModeEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblTapeModeEnumId->Name = QApplication::Translate('Tape Mode Enum');
			$this->lblTapeModeEnumId->Text = ($this->objTape->TapeModeEnumId) ? TapeModeEnum::$NameArray[$this->objTape->TapeModeEnumId] : null;
			return $this->lblTapeModeEnumId;
		}

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objTape->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objTape->Added) ? $this->objTape->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objTape->Comment;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objTape->Comment;
			return $this->lblComment;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objTape->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objTape->Owner;
			$this->txtOwner->MaxLength = Tape::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objTape->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objTape->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objTape->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objTape->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objTape->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objTape->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objTape->Status) ? AuthStatusEnum::$NameArray[$this->objTape->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Tape object.
		 * @param boolean $blnReload reload Tape from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objTape->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objTape->Id;
			if ($this->lblId) $this->lblId->Text = $this->objTape->Id;

			if ($this->lblNumber) if ($this->blnEditMode) $this->lblNumber->Text = $this->objTape->Number;

			if ($this->lstLibrary) {
					$this->lstLibrary->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstLibrary->AddItem(QApplication::Translate('- Select One -'), null);
				$objLibraryArray = Library::LoadAll();
				if ($objLibraryArray) foreach ($objLibraryArray as $objLibrary) {
					$objListItem = new QListItem($objLibrary->__toString(), $objLibrary->Id);
					if (($this->objTape->Library) && ($this->objTape->Library->Id == $objLibrary->Id))
						$objListItem->Selected = true;
					$this->lstLibrary->AddItem($objListItem);
				}
			}
			if ($this->lblLibraryId) $this->lblLibraryId->Text = ($this->objTape->Library) ? $this->objTape->Library->__toString() : null;

			if ($this->lstOrganisation) {
					$this->lstOrganisation->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstOrganisation->AddItem(QApplication::Translate('- Select One -'), null);
				$objOrganisationArray = Organisation::LoadAll();
				if ($objOrganisationArray) foreach ($objOrganisationArray as $objOrganisation) {
					$objListItem = new QListItem($objOrganisation->__toString(), $objOrganisation->Id);
					if (($this->objTape->Organisation) && ($this->objTape->Organisation->Id == $objOrganisation->Id))
						$objListItem->Selected = true;
					$this->lstOrganisation->AddItem($objListItem);
				}
			}
			if ($this->lblOrganisationId) $this->lblOrganisationId->Text = ($this->objTape->Organisation) ? $this->objTape->Organisation->__toString() : null;

			if ($this->lstTapeModeEnum) $this->lstTapeModeEnum->SelectedValue = $this->objTape->TapeModeEnumId;
			if ($this->lblTapeModeEnumId) $this->lblTapeModeEnumId->Text = ($this->objTape->TapeModeEnumId) ? TapeModeEnum::$NameArray[$this->objTape->TapeModeEnumId] : null;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objTape->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objTape->Added) ? $this->objTape->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->txtComment) $this->txtComment->Text = $this->objTape->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objTape->Comment;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objTape->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objTape->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objTape->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objTape->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objTape->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objTape->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objTape->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objTape->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objTape->Status) ? AuthStatusEnum::$NameArray[$this->objTape->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC TAPE OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Tape instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveTape() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objTape->Id = $this->txtId->Text;
				if ($this->lstLibrary) $this->objTape->LibraryId = $this->lstLibrary->SelectedValue;
				if ($this->lstOrganisation) $this->objTape->OrganisationId = $this->lstOrganisation->SelectedValue;
				if ($this->lstTapeModeEnum) $this->objTape->TapeModeEnumId = $this->lstTapeModeEnum->SelectedValue;
				if ($this->calAdded) $this->objTape->Added = $this->calAdded->DateTime;
				if ($this->txtComment) $this->objTape->Comment = $this->txtComment->Text;
				if ($this->txtOwner) $this->objTape->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objTape->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objTape->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objTape->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Tape object
				$this->objTape->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Tape instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteTape() {
			$this->objTape->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Tape': return $this->objTape;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Tape fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'NumberControl':
					if (!$this->lblNumber) return $this->lblNumber_Create();
					return $this->lblNumber;
				case 'NumberLabel':
					if (!$this->lblNumber) return $this->lblNumber_Create();
					return $this->lblNumber;
				case 'LibraryIdControl':
					if (!$this->lstLibrary) return $this->lstLibrary_Create();
					return $this->lstLibrary;
				case 'LibraryIdLabel':
					if (!$this->lblLibraryId) return $this->lblLibraryId_Create();
					return $this->lblLibraryId;
				case 'OrganisationIdControl':
					if (!$this->lstOrganisation) return $this->lstOrganisation_Create();
					return $this->lstOrganisation;
				case 'OrganisationIdLabel':
					if (!$this->lblOrganisationId) return $this->lblOrganisationId_Create();
					return $this->lblOrganisationId;
				case 'TapeModeEnumIdControl':
					if (!$this->lstTapeModeEnum) return $this->lstTapeModeEnum_Create();
					return $this->lstTapeModeEnum;
				case 'TapeModeEnumIdLabel':
					if (!$this->lblTapeModeEnumId) return $this->lblTapeModeEnumId_Create();
					return $this->lblTapeModeEnumId;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Tape fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'NumberControl':
						return ($this->lblNumber = QType::Cast($mixValue, 'QControl'));
					case 'LibraryIdControl':
						return ($this->lstLibrary = QType::Cast($mixValue, 'QControl'));
					case 'OrganisationIdControl':
						return ($this->lstOrganisation = QType::Cast($mixValue, 'QControl'));
					case 'TapeModeEnumIdControl':
						return ($this->lstTapeModeEnum = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
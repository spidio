<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Camera class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Camera object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a CameraMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Camera $Camera the actual Camera data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $OrganisationIdControl
	 * property-read QLabel $OrganisationIdLabel
	 * property QTextBox $BrandControl
	 * property-read QLabel $BrandLabel
	 * property QTextBox $ModelControl
	 * property-read QLabel $ModelLabel
	 * property QTextBox $SerialControl
	 * property-read QLabel $SerialLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class CameraMetaControlGen extends QBaseClass {
		// General Variables
		protected $objCamera;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Camera's individual data fields
		protected $txtId;
		protected $lstOrganisation;
		protected $txtBrand;
		protected $txtModel;
		protected $txtSerial;
		protected $txtComment;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Camera's individual data fields
		protected $lblId;
		protected $lblOrganisationId;
		protected $lblBrand;
		protected $lblModel;
		protected $lblSerial;
		protected $lblComment;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * CameraMetaControl to edit a single Camera object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Camera object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this CameraMetaControl
		 * @param Camera $objCamera new or existing Camera object
		 */
		 public function __construct($objParentObject, Camera $objCamera) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this CameraMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Camera object
			$this->objCamera = $objCamera;

			// Figure out if we're Editing or Creating New
			if ($this->objCamera->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this CameraMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Camera object creation - defaults to CreateOrEdit
 		 * @return CameraMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objCamera = Camera::Load($strId);

				// Camera was found -- return it!
				if ($objCamera)
					return new CameraMetaControl($objParentObject, $objCamera);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Camera object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new CameraMetaControl($objParentObject, new Camera());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this CameraMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Camera object creation - defaults to CreateOrEdit
		 * @return CameraMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return CameraMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this CameraMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Camera object creation - defaults to CreateOrEdit
		 * @return CameraMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return CameraMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objCamera->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Camera::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objCamera->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstOrganisation
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstOrganisation_Create($strControlId = null) {
			$this->lstOrganisation = new QListBox($this->objParentObject, $strControlId);
			$this->lstOrganisation->Name = QApplication::Translate('Organisation');
			$this->lstOrganisation->Required = true;
			if (!$this->blnEditMode)
				$this->lstOrganisation->AddItem(QApplication::Translate('- Select One -'), null);
			$objOrganisationArray = Organisation::LoadAll();
			if ($objOrganisationArray) foreach ($objOrganisationArray as $objOrganisation) {
				$objListItem = new QListItem($objOrganisation->__toString(), $objOrganisation->Id);
				if (($this->objCamera->Organisation) && ($this->objCamera->Organisation->Id == $objOrganisation->Id))
					$objListItem->Selected = true;
				$this->lstOrganisation->AddItem($objListItem);
			}
			return $this->lstOrganisation;
		}

		/**
		 * Create and setup QLabel lblOrganisationId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOrganisationId_Create($strControlId = null) {
			$this->lblOrganisationId = new QLabel($this->objParentObject, $strControlId);
			$this->lblOrganisationId->Name = QApplication::Translate('Organisation');
			$this->lblOrganisationId->Text = ($this->objCamera->Organisation) ? $this->objCamera->Organisation->__toString() : null;
			$this->lblOrganisationId->Required = true;
			return $this->lblOrganisationId;
		}

		/**
		 * Create and setup QTextBox txtBrand
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtBrand_Create($strControlId = null) {
			$this->txtBrand = new QTextBox($this->objParentObject, $strControlId);
			$this->txtBrand->Name = QApplication::Translate('Brand');
			$this->txtBrand->Text = $this->objCamera->Brand;
			$this->txtBrand->Required = true;
			$this->txtBrand->MaxLength = Camera::BrandMaxLength;
			return $this->txtBrand;
		}

		/**
		 * Create and setup QLabel lblBrand
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblBrand_Create($strControlId = null) {
			$this->lblBrand = new QLabel($this->objParentObject, $strControlId);
			$this->lblBrand->Name = QApplication::Translate('Brand');
			$this->lblBrand->Text = $this->objCamera->Brand;
			$this->lblBrand->Required = true;
			return $this->lblBrand;
		}

		/**
		 * Create and setup QTextBox txtModel
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtModel_Create($strControlId = null) {
			$this->txtModel = new QTextBox($this->objParentObject, $strControlId);
			$this->txtModel->Name = QApplication::Translate('Model');
			$this->txtModel->Text = $this->objCamera->Model;
			$this->txtModel->Required = true;
			$this->txtModel->MaxLength = Camera::ModelMaxLength;
			return $this->txtModel;
		}

		/**
		 * Create and setup QLabel lblModel
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblModel_Create($strControlId = null) {
			$this->lblModel = new QLabel($this->objParentObject, $strControlId);
			$this->lblModel->Name = QApplication::Translate('Model');
			$this->lblModel->Text = $this->objCamera->Model;
			$this->lblModel->Required = true;
			return $this->lblModel;
		}

		/**
		 * Create and setup QTextBox txtSerial
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtSerial_Create($strControlId = null) {
			$this->txtSerial = new QTextBox($this->objParentObject, $strControlId);
			$this->txtSerial->Name = QApplication::Translate('Serial');
			$this->txtSerial->Text = $this->objCamera->Serial;
			$this->txtSerial->MaxLength = Camera::SerialMaxLength;
			return $this->txtSerial;
		}

		/**
		 * Create and setup QLabel lblSerial
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblSerial_Create($strControlId = null) {
			$this->lblSerial = new QLabel($this->objParentObject, $strControlId);
			$this->lblSerial->Name = QApplication::Translate('Serial');
			$this->lblSerial->Text = $this->objCamera->Serial;
			return $this->lblSerial;
		}

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objCamera->Comment;
			$this->txtComment->Required = true;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objCamera->Comment;
			$this->lblComment->Required = true;
			return $this->lblComment;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objCamera->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objCamera->Owner;
			$this->txtOwner->MaxLength = Camera::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objCamera->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objCamera->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objCamera->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objCamera->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objCamera->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objCamera->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objCamera->Status) ? AuthStatusEnum::$NameArray[$this->objCamera->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Camera object.
		 * @param boolean $blnReload reload Camera from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objCamera->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objCamera->Id;
			if ($this->lblId) $this->lblId->Text = $this->objCamera->Id;

			if ($this->lstOrganisation) {
					$this->lstOrganisation->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstOrganisation->AddItem(QApplication::Translate('- Select One -'), null);
				$objOrganisationArray = Organisation::LoadAll();
				if ($objOrganisationArray) foreach ($objOrganisationArray as $objOrganisation) {
					$objListItem = new QListItem($objOrganisation->__toString(), $objOrganisation->Id);
					if (($this->objCamera->Organisation) && ($this->objCamera->Organisation->Id == $objOrganisation->Id))
						$objListItem->Selected = true;
					$this->lstOrganisation->AddItem($objListItem);
				}
			}
			if ($this->lblOrganisationId) $this->lblOrganisationId->Text = ($this->objCamera->Organisation) ? $this->objCamera->Organisation->__toString() : null;

			if ($this->txtBrand) $this->txtBrand->Text = $this->objCamera->Brand;
			if ($this->lblBrand) $this->lblBrand->Text = $this->objCamera->Brand;

			if ($this->txtModel) $this->txtModel->Text = $this->objCamera->Model;
			if ($this->lblModel) $this->lblModel->Text = $this->objCamera->Model;

			if ($this->txtSerial) $this->txtSerial->Text = $this->objCamera->Serial;
			if ($this->lblSerial) $this->lblSerial->Text = $this->objCamera->Serial;

			if ($this->txtComment) $this->txtComment->Text = $this->objCamera->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objCamera->Comment;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objCamera->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objCamera->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objCamera->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objCamera->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objCamera->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objCamera->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objCamera->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objCamera->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objCamera->Status) ? AuthStatusEnum::$NameArray[$this->objCamera->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC CAMERA OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Camera instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveCamera() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objCamera->Id = $this->txtId->Text;
				if ($this->lstOrganisation) $this->objCamera->OrganisationId = $this->lstOrganisation->SelectedValue;
				if ($this->txtBrand) $this->objCamera->Brand = $this->txtBrand->Text;
				if ($this->txtModel) $this->objCamera->Model = $this->txtModel->Text;
				if ($this->txtSerial) $this->objCamera->Serial = $this->txtSerial->Text;
				if ($this->txtComment) $this->objCamera->Comment = $this->txtComment->Text;
				if ($this->txtOwner) $this->objCamera->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objCamera->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objCamera->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objCamera->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Camera object
				$this->objCamera->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Camera instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteCamera() {
			$this->objCamera->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Camera': return $this->objCamera;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Camera fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'OrganisationIdControl':
					if (!$this->lstOrganisation) return $this->lstOrganisation_Create();
					return $this->lstOrganisation;
				case 'OrganisationIdLabel':
					if (!$this->lblOrganisationId) return $this->lblOrganisationId_Create();
					return $this->lblOrganisationId;
				case 'BrandControl':
					if (!$this->txtBrand) return $this->txtBrand_Create();
					return $this->txtBrand;
				case 'BrandLabel':
					if (!$this->lblBrand) return $this->lblBrand_Create();
					return $this->lblBrand;
				case 'ModelControl':
					if (!$this->txtModel) return $this->txtModel_Create();
					return $this->txtModel;
				case 'ModelLabel':
					if (!$this->lblModel) return $this->lblModel_Create();
					return $this->lblModel;
				case 'SerialControl':
					if (!$this->txtSerial) return $this->txtSerial_Create();
					return $this->txtSerial;
				case 'SerialLabel':
					if (!$this->lblSerial) return $this->lblSerial_Create();
					return $this->lblSerial;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Camera fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'OrganisationIdControl':
						return ($this->lstOrganisation = QType::Cast($mixValue, 'QControl'));
					case 'BrandControl':
						return ($this->txtBrand = QType::Cast($mixValue, 'QControl'));
					case 'ModelControl':
						return ($this->txtModel = QType::Cast($mixValue, 'QControl'));
					case 'SerialControl':
						return ($this->txtSerial = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
<?php

class SQConditionBitwiseNot extends QQConditionComparison {
	protected $strOperator = ' ~ ';
}

class SQConditionBitwiseOr extends QQConditionComparison {
	protected $strOperator = ' | ';
}

class SQConditionBitwiseXor extends QQConditionComparison {
	protected $strOperator = ' ^ ';
}

class SQConditionBitwiseAnd extends QQConditionComparison {
	protected $strOperator = ' & ';
}

class SQConditionShiftLeft extends QQConditionComparison {
	protected $strOperator = ' << ';
}

class SQConditionShiftRight extends QQConditionComparison {
	protected $strOperator = ' >> ';
}

abstract class SQ extends QQ {
	static public function BitwiseNot(QQNode $objQueryNode, $mixValue) {
		return new SQConditionBitwiseNot($objQueryNode, $mixValue);
	}
	
	static public function BitwiseOr(QQNode $objQueryNode, $mixValue) {
		return new SQConditionBitwiseOr($objQueryNode, $mixValue);
	}
	
	static public function BitwiseXor(QQNode $objQueryNode, $mixValue) {
		return new SQConditionBitwiseXor($objQueryNode, $mixValue);
	}
	
	static public function BitwiseAnd(QQNode $objQueryNode, $mixValue) {
		return new SQConditionBitwiseAnd($objQueryNode, $mixValue);
	}
	
	static public function ShiftLeft(QQNode $objQueryNode, $mixValue) {
		return new SQConditionShiftLeft($objQueryNode, $mixValue);
	}
	
	static public function ShiftRight(QQNode $objQueryNode, $mixValue) {
		return new SQConditionShiftRight($objQueryNode, $mixValue);
	}
	
	static public function _(QQNode $objQueryNode, $strSymbol, $mixValue, $mixValueTwo = null) {
			try {
				switch(strtolower(trim($strSymbol))) {
					case '~': return SQ::BitwiseNot($objQueryNode, $mixValue);
					case '|': return SQ::BitwiseOr($objQueryNode, $mixValue);
					case '^': return SQ::BitwiseXor($objQueryNode, $mixValue);
					case '&': return SQ::BitwiseAnd($objQueryNode, $mixValue);
					case '<<': return SQ::ShiftLeft($objQueryNode, $mixValue);
					case '>>': return SQ::ShiftRight($objQueryNode, $mixValue);
					default:
						return parent::_($objQueryNode, $strSymbol, $mixValue, $mixValueTwo);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
}

?>
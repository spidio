<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Person class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Person object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a PersonMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Person $Person the actual Person data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $AddressIdControl
	 * property-read QLabel $AddressIdLabel
	 * property QTextBox $NameControl
	 * property-read QLabel $NameLabel
	 * property QTextBox $EmailControl
	 * property-read QLabel $EmailLabel
	 * property QIntegerTextBox $PhoneControl
	 * property-read QLabel $PhoneLabel
	 * property QIntegerTextBox $PhoneAControl
	 * property-read QLabel $PhoneALabel
	 * property QIntegerTextBox $PhoneCControl
	 * property-read QLabel $PhoneCLabel
	 * property QIntegerTextBox $MobileControl
	 * property-read QLabel $MobileLabel
	 * property QIntegerTextBox $MobileAControl
	 * property-read QLabel $MobileALabel
	 * property QIntegerTextBox $MobileCControl
	 * property-read QLabel $MobileCLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class PersonMetaControlGen extends QBaseClass {
		// General Variables
		protected $objPerson;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Person's individual data fields
		protected $txtId;
		protected $lstAddress;
		protected $txtName;
		protected $txtEmail;
		protected $txtPhone;
		protected $txtPhoneA;
		protected $txtPhoneC;
		protected $txtMobile;
		protected $txtMobileA;
		protected $txtMobileC;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Person's individual data fields
		protected $lblId;
		protected $lblAddressId;
		protected $lblName;
		protected $lblEmail;
		protected $lblPhone;
		protected $lblPhoneA;
		protected $lblPhoneC;
		protected $lblMobile;
		protected $lblMobileA;
		protected $lblMobileC;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * PersonMetaControl to edit a single Person object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Person object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this PersonMetaControl
		 * @param Person $objPerson new or existing Person object
		 */
		 public function __construct($objParentObject, Person $objPerson) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this PersonMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Person object
			$this->objPerson = $objPerson;

			// Figure out if we're Editing or Creating New
			if ($this->objPerson->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this PersonMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Person object creation - defaults to CreateOrEdit
 		 * @return PersonMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objPerson = Person::Load($strId);

				// Person was found -- return it!
				if ($objPerson)
					return new PersonMetaControl($objParentObject, $objPerson);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Person object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new PersonMetaControl($objParentObject, new Person());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this PersonMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Person object creation - defaults to CreateOrEdit
		 * @return PersonMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return PersonMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this PersonMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Person object creation - defaults to CreateOrEdit
		 * @return PersonMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return PersonMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objPerson->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Person::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objPerson->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstAddress
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstAddress_Create($strControlId = null) {
			$this->lstAddress = new QListBox($this->objParentObject, $strControlId);
			$this->lstAddress->Name = QApplication::Translate('Address');
			$this->lstAddress->Required = true;
			if (!$this->blnEditMode)
				$this->lstAddress->AddItem(QApplication::Translate('- Select One -'), null);
			$objAddressArray = Address::LoadAll();
			if ($objAddressArray) foreach ($objAddressArray as $objAddress) {
				$objListItem = new QListItem($objAddress->__toString(), $objAddress->Id);
				if (($this->objPerson->Address) && ($this->objPerson->Address->Id == $objAddress->Id))
					$objListItem->Selected = true;
				$this->lstAddress->AddItem($objListItem);
			}
			return $this->lstAddress;
		}

		/**
		 * Create and setup QLabel lblAddressId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblAddressId_Create($strControlId = null) {
			$this->lblAddressId = new QLabel($this->objParentObject, $strControlId);
			$this->lblAddressId->Name = QApplication::Translate('Address');
			$this->lblAddressId->Text = ($this->objPerson->Address) ? $this->objPerson->Address->__toString() : null;
			$this->lblAddressId->Required = true;
			return $this->lblAddressId;
		}

		/**
		 * Create and setup QTextBox txtName
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtName_Create($strControlId = null) {
			$this->txtName = new QTextBox($this->objParentObject, $strControlId);
			$this->txtName->Name = QApplication::Translate('Name');
			$this->txtName->Text = $this->objPerson->Name;
			$this->txtName->Required = true;
			$this->txtName->MaxLength = Person::NameMaxLength;
			return $this->txtName;
		}

		/**
		 * Create and setup QLabel lblName
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblName_Create($strControlId = null) {
			$this->lblName = new QLabel($this->objParentObject, $strControlId);
			$this->lblName->Name = QApplication::Translate('Name');
			$this->lblName->Text = $this->objPerson->Name;
			$this->lblName->Required = true;
			return $this->lblName;
		}

		/**
		 * Create and setup QTextBox txtEmail
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtEmail_Create($strControlId = null) {
			$this->txtEmail = new QTextBox($this->objParentObject, $strControlId);
			$this->txtEmail->Name = QApplication::Translate('Email');
			$this->txtEmail->Text = $this->objPerson->Email;
			$this->txtEmail->Required = true;
			$this->txtEmail->MaxLength = Person::EmailMaxLength;
			return $this->txtEmail;
		}

		/**
		 * Create and setup QLabel lblEmail
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblEmail_Create($strControlId = null) {
			$this->lblEmail = new QLabel($this->objParentObject, $strControlId);
			$this->lblEmail->Name = QApplication::Translate('Email');
			$this->lblEmail->Text = $this->objPerson->Email;
			$this->lblEmail->Required = true;
			return $this->lblEmail;
		}

		/**
		 * Create and setup QIntegerTextBox txtPhone
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPhone_Create($strControlId = null) {
			$this->txtPhone = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPhone->Name = QApplication::Translate('Phone');
			$this->txtPhone->Text = $this->objPerson->Phone;
			return $this->txtPhone;
		}

		/**
		 * Create and setup QLabel lblPhone
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPhone_Create($strControlId = null, $strFormat = null) {
			$this->lblPhone = new QLabel($this->objParentObject, $strControlId);
			$this->lblPhone->Name = QApplication::Translate('Phone');
			$this->lblPhone->Text = $this->objPerson->Phone;
			$this->lblPhone->Format = $strFormat;
			return $this->lblPhone;
		}

		/**
		 * Create and setup QIntegerTextBox txtPhoneA
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPhoneA_Create($strControlId = null) {
			$this->txtPhoneA = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPhoneA->Name = QApplication::Translate('Phone A');
			$this->txtPhoneA->Text = $this->objPerson->PhoneA;
			return $this->txtPhoneA;
		}

		/**
		 * Create and setup QLabel lblPhoneA
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPhoneA_Create($strControlId = null, $strFormat = null) {
			$this->lblPhoneA = new QLabel($this->objParentObject, $strControlId);
			$this->lblPhoneA->Name = QApplication::Translate('Phone A');
			$this->lblPhoneA->Text = $this->objPerson->PhoneA;
			$this->lblPhoneA->Format = $strFormat;
			return $this->lblPhoneA;
		}

		/**
		 * Create and setup QIntegerTextBox txtPhoneC
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPhoneC_Create($strControlId = null) {
			$this->txtPhoneC = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPhoneC->Name = QApplication::Translate('Phone C');
			$this->txtPhoneC->Text = $this->objPerson->PhoneC;
			return $this->txtPhoneC;
		}

		/**
		 * Create and setup QLabel lblPhoneC
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPhoneC_Create($strControlId = null, $strFormat = null) {
			$this->lblPhoneC = new QLabel($this->objParentObject, $strControlId);
			$this->lblPhoneC->Name = QApplication::Translate('Phone C');
			$this->lblPhoneC->Text = $this->objPerson->PhoneC;
			$this->lblPhoneC->Format = $strFormat;
			return $this->lblPhoneC;
		}

		/**
		 * Create and setup QIntegerTextBox txtMobile
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtMobile_Create($strControlId = null) {
			$this->txtMobile = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtMobile->Name = QApplication::Translate('Mobile');
			$this->txtMobile->Text = $this->objPerson->Mobile;
			return $this->txtMobile;
		}

		/**
		 * Create and setup QLabel lblMobile
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblMobile_Create($strControlId = null, $strFormat = null) {
			$this->lblMobile = new QLabel($this->objParentObject, $strControlId);
			$this->lblMobile->Name = QApplication::Translate('Mobile');
			$this->lblMobile->Text = $this->objPerson->Mobile;
			$this->lblMobile->Format = $strFormat;
			return $this->lblMobile;
		}

		/**
		 * Create and setup QIntegerTextBox txtMobileA
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtMobileA_Create($strControlId = null) {
			$this->txtMobileA = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtMobileA->Name = QApplication::Translate('Mobile A');
			$this->txtMobileA->Text = $this->objPerson->MobileA;
			return $this->txtMobileA;
		}

		/**
		 * Create and setup QLabel lblMobileA
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblMobileA_Create($strControlId = null, $strFormat = null) {
			$this->lblMobileA = new QLabel($this->objParentObject, $strControlId);
			$this->lblMobileA->Name = QApplication::Translate('Mobile A');
			$this->lblMobileA->Text = $this->objPerson->MobileA;
			$this->lblMobileA->Format = $strFormat;
			return $this->lblMobileA;
		}

		/**
		 * Create and setup QIntegerTextBox txtMobileC
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtMobileC_Create($strControlId = null) {
			$this->txtMobileC = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtMobileC->Name = QApplication::Translate('Mobile C');
			$this->txtMobileC->Text = $this->objPerson->MobileC;
			return $this->txtMobileC;
		}

		/**
		 * Create and setup QLabel lblMobileC
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblMobileC_Create($strControlId = null, $strFormat = null) {
			$this->lblMobileC = new QLabel($this->objParentObject, $strControlId);
			$this->lblMobileC->Name = QApplication::Translate('Mobile C');
			$this->lblMobileC->Text = $this->objPerson->MobileC;
			$this->lblMobileC->Format = $strFormat;
			return $this->lblMobileC;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objPerson->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objPerson->Owner;
			$this->txtOwner->MaxLength = Person::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objPerson->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objPerson->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objPerson->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objPerson->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objPerson->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objPerson->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objPerson->Status) ? AuthStatusEnum::$NameArray[$this->objPerson->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Person object.
		 * @param boolean $blnReload reload Person from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objPerson->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objPerson->Id;
			if ($this->lblId) $this->lblId->Text = $this->objPerson->Id;

			if ($this->lstAddress) {
					$this->lstAddress->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstAddress->AddItem(QApplication::Translate('- Select One -'), null);
				$objAddressArray = Address::LoadAll();
				if ($objAddressArray) foreach ($objAddressArray as $objAddress) {
					$objListItem = new QListItem($objAddress->__toString(), $objAddress->Id);
					if (($this->objPerson->Address) && ($this->objPerson->Address->Id == $objAddress->Id))
						$objListItem->Selected = true;
					$this->lstAddress->AddItem($objListItem);
				}
			}
			if ($this->lblAddressId) $this->lblAddressId->Text = ($this->objPerson->Address) ? $this->objPerson->Address->__toString() : null;

			if ($this->txtName) $this->txtName->Text = $this->objPerson->Name;
			if ($this->lblName) $this->lblName->Text = $this->objPerson->Name;

			if ($this->txtEmail) $this->txtEmail->Text = $this->objPerson->Email;
			if ($this->lblEmail) $this->lblEmail->Text = $this->objPerson->Email;

			if ($this->txtPhone) $this->txtPhone->Text = $this->objPerson->Phone;
			if ($this->lblPhone) $this->lblPhone->Text = $this->objPerson->Phone;

			if ($this->txtPhoneA) $this->txtPhoneA->Text = $this->objPerson->PhoneA;
			if ($this->lblPhoneA) $this->lblPhoneA->Text = $this->objPerson->PhoneA;

			if ($this->txtPhoneC) $this->txtPhoneC->Text = $this->objPerson->PhoneC;
			if ($this->lblPhoneC) $this->lblPhoneC->Text = $this->objPerson->PhoneC;

			if ($this->txtMobile) $this->txtMobile->Text = $this->objPerson->Mobile;
			if ($this->lblMobile) $this->lblMobile->Text = $this->objPerson->Mobile;

			if ($this->txtMobileA) $this->txtMobileA->Text = $this->objPerson->MobileA;
			if ($this->lblMobileA) $this->lblMobileA->Text = $this->objPerson->MobileA;

			if ($this->txtMobileC) $this->txtMobileC->Text = $this->objPerson->MobileC;
			if ($this->lblMobileC) $this->lblMobileC->Text = $this->objPerson->MobileC;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objPerson->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objPerson->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objPerson->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objPerson->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objPerson->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objPerson->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objPerson->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objPerson->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objPerson->Status) ? AuthStatusEnum::$NameArray[$this->objPerson->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC PERSON OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Person instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SavePerson() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objPerson->Id = $this->txtId->Text;
				if ($this->lstAddress) $this->objPerson->AddressId = $this->lstAddress->SelectedValue;
				if ($this->txtName) $this->objPerson->Name = $this->txtName->Text;
				if ($this->txtEmail) $this->objPerson->Email = $this->txtEmail->Text;
				if ($this->txtPhone) $this->objPerson->Phone = $this->txtPhone->Text;
				if ($this->txtPhoneA) $this->objPerson->PhoneA = $this->txtPhoneA->Text;
				if ($this->txtPhoneC) $this->objPerson->PhoneC = $this->txtPhoneC->Text;
				if ($this->txtMobile) $this->objPerson->Mobile = $this->txtMobile->Text;
				if ($this->txtMobileA) $this->objPerson->MobileA = $this->txtMobileA->Text;
				if ($this->txtMobileC) $this->objPerson->MobileC = $this->txtMobileC->Text;
				if ($this->txtOwner) $this->objPerson->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objPerson->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objPerson->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objPerson->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Person object
				$this->objPerson->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Person instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeletePerson() {
			$this->objPerson->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Person': return $this->objPerson;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Person fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'AddressIdControl':
					if (!$this->lstAddress) return $this->lstAddress_Create();
					return $this->lstAddress;
				case 'AddressIdLabel':
					if (!$this->lblAddressId) return $this->lblAddressId_Create();
					return $this->lblAddressId;
				case 'NameControl':
					if (!$this->txtName) return $this->txtName_Create();
					return $this->txtName;
				case 'NameLabel':
					if (!$this->lblName) return $this->lblName_Create();
					return $this->lblName;
				case 'EmailControl':
					if (!$this->txtEmail) return $this->txtEmail_Create();
					return $this->txtEmail;
				case 'EmailLabel':
					if (!$this->lblEmail) return $this->lblEmail_Create();
					return $this->lblEmail;
				case 'PhoneControl':
					if (!$this->txtPhone) return $this->txtPhone_Create();
					return $this->txtPhone;
				case 'PhoneLabel':
					if (!$this->lblPhone) return $this->lblPhone_Create();
					return $this->lblPhone;
				case 'PhoneAControl':
					if (!$this->txtPhoneA) return $this->txtPhoneA_Create();
					return $this->txtPhoneA;
				case 'PhoneALabel':
					if (!$this->lblPhoneA) return $this->lblPhoneA_Create();
					return $this->lblPhoneA;
				case 'PhoneCControl':
					if (!$this->txtPhoneC) return $this->txtPhoneC_Create();
					return $this->txtPhoneC;
				case 'PhoneCLabel':
					if (!$this->lblPhoneC) return $this->lblPhoneC_Create();
					return $this->lblPhoneC;
				case 'MobileControl':
					if (!$this->txtMobile) return $this->txtMobile_Create();
					return $this->txtMobile;
				case 'MobileLabel':
					if (!$this->lblMobile) return $this->lblMobile_Create();
					return $this->lblMobile;
				case 'MobileAControl':
					if (!$this->txtMobileA) return $this->txtMobileA_Create();
					return $this->txtMobileA;
				case 'MobileALabel':
					if (!$this->lblMobileA) return $this->lblMobileA_Create();
					return $this->lblMobileA;
				case 'MobileCControl':
					if (!$this->txtMobileC) return $this->txtMobileC_Create();
					return $this->txtMobileC;
				case 'MobileCLabel':
					if (!$this->lblMobileC) return $this->lblMobileC_Create();
					return $this->lblMobileC;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Person fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'AddressIdControl':
						return ($this->lstAddress = QType::Cast($mixValue, 'QControl'));
					case 'NameControl':
						return ($this->txtName = QType::Cast($mixValue, 'QControl'));
					case 'EmailControl':
						return ($this->txtEmail = QType::Cast($mixValue, 'QControl'));
					case 'PhoneControl':
						return ($this->txtPhone = QType::Cast($mixValue, 'QControl'));
					case 'PhoneAControl':
						return ($this->txtPhoneA = QType::Cast($mixValue, 'QControl'));
					case 'PhoneCControl':
						return ($this->txtPhoneC = QType::Cast($mixValue, 'QControl'));
					case 'MobileControl':
						return ($this->txtMobile = QType::Cast($mixValue, 'QControl'));
					case 'MobileAControl':
						return ($this->txtMobileA = QType::Cast($mixValue, 'QControl'));
					case 'MobileCControl':
						return ($this->txtMobileC = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
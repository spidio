<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php _p(QApplication::$EncodingType); ?>" />
		<title><?php _p($this->strPageTitle); ?></title>
		<style type="text/css">@import url("<?php _p(__VIRTUAL_DIRECTORY__ . __CSS_ASSETS__); ?>/dashboard.css");</style>
	</head>
	<body>

<?php $this->RenderBegin(); ?>

		<div id="titleBar">
			<h1><?php $this->pnlTitle->Render(); ?></h1>
		</div>
	
		<div id="dashboard">
			<div id="left">
				<p><strong>Select a Class to View/Edit</strong></p>
				<p><?php $this->lstClassNames->Render('FontSize=10px','Width=100px'); ?></p>
				<p><?php $this->objDefaultWaitIcon->Render(); ?></p>
			</div>
			<div id="right">
				<?php $this->pnlList->Render(); ?>
				<?php $this->pnlEdit->Render(); ?>
			</div>
		</div>

<?php $this->RenderEnd(); ?>
	
	</body>
</html>
<?php
	/**
	 * This is a MetaControl class, providing a QForm or QPanel access to event handlers
	 * and QControls to perform the Create, Edit, and Delete functionality
	 * of the Recording class.  This code-generated class
	 * contains all the basic elements to help a QPanel or QForm display an HTML form that can
	 * manipulate a single Recording object.
	 *
	 * To take advantage of some (or all) of these control objects, you
	 * must create a new QForm or QPanel which instantiates a RecordingMetaControl
	 * class.
	 *
	 * Any and all changes to this file will be overwritten with any subsequent
	 * code re-generation.
	 * 
	 * @package Spidio
	 * @subpackage MetaControls
	 * property-read Recording $Recording the actual Recording data class being edited
	 * property QTextBox $IdControl
	 * property-read QLabel $IdLabel
	 * property QListBox $ProjectIdControl
	 * property-read QLabel $ProjectIdLabel
	 * property QListBox $TapeIdControl
	 * property-read QLabel $TapeIdLabel
	 * property QListBox $CameraIdControl
	 * property-read QLabel $CameraIdLabel
	 * property QListBox $CameraPurposeIdControl
	 * property-read QLabel $CameraPurposeIdLabel
	 * property QListBox $TapeModeEnumIdControl
	 * property-read QLabel $TapeModeEnumIdLabel
	 * property QListBox $RecordingTypeIdControl
	 * property-read QLabel $RecordingTypeIdLabel
	 * property QIntegerTextBox $StartControl
	 * property-read QLabel $StartLabel
	 * property QIntegerTextBox $EndControl
	 * property-read QLabel $EndLabel
	 * property QCheckBox $WidescreenControl
	 * property-read QLabel $WidescreenLabel
	 * property QDateTimePicker $DateControl
	 * property-read QLabel $DateLabel
	 * property QIntegerTextBox $SceneControl
	 * property-read QLabel $SceneLabel
	 * property QIntegerTextBox $TakeControl
	 * property-read QLabel $TakeLabel
	 * property QIntegerTextBox $AngleControl
	 * property-read QLabel $AngleLabel
	 * property QDateTimePicker $AddedControl
	 * property-read QLabel $AddedLabel
	 * property QDateTimePicker $ChangedControl
	 * property-read QLabel $ChangedLabel
	 * property QTextBox $CommentControl
	 * property-read QLabel $CommentLabel
	 * property QLabel $OptlockControl
	 * property-read QLabel $OptlockLabel
	 * property QTextBox $OwnerControl
	 * property-read QLabel $OwnerLabel
	 * property QIntegerTextBox $GroupControl
	 * property-read QLabel $GroupLabel
	 * property QIntegerTextBox $PermsControl
	 * property-read QLabel $PermsLabel
	 * property QListBox $StatusControl
	 * property-read QLabel $StatusLabel
	 * property-read string $TitleVerb a verb indicating whether or not this is being edited or created
	 * property-read boolean $EditMode a boolean indicating whether or not this is being edited or created
	 */

	class RecordingMetaControlGen extends QBaseClass {
		// General Variables
		protected $objRecording;
		protected $objParentObject;
		protected $strTitleVerb;
		protected $blnEditMode;

		// Controls that allow the editing of Recording's individual data fields
		protected $txtId;
		protected $lstProject;
		protected $lstTape;
		protected $lstCamera;
		protected $lstCameraPurpose;
		protected $lstTapeModeEnum;
		protected $lstRecordingType;
		protected $txtStart;
		protected $txtEnd;
		protected $chkWidescreen;
		protected $calDate;
		protected $txtScene;
		protected $txtTake;
		protected $txtAngle;
		protected $calAdded;
		protected $calChanged;
		protected $txtComment;
		protected $lblOptlock;
		protected $txtOwner;
		protected $txtGroup;
		protected $txtPerms;
		protected $lstStatusObject;

		// Controls that allow the viewing of Recording's individual data fields
		protected $lblId;
		protected $lblProjectId;
		protected $lblTapeId;
		protected $lblCameraId;
		protected $lblCameraPurposeId;
		protected $lblTapeModeEnumId;
		protected $lblRecordingTypeId;
		protected $lblStart;
		protected $lblEnd;
		protected $lblWidescreen;
		protected $lblDate;
		protected $lblScene;
		protected $lblTake;
		protected $lblAngle;
		protected $lblAdded;
		protected $lblChanged;
		protected $lblComment;
		protected $lblOwner;
		protected $lblGroup;
		protected $lblPerms;
		protected $lblStatus;

		// QListBox Controls (if applicable) to edit Unique ReverseReferences and ManyToMany References

		// QLabel Controls (if applicable) to view Unique ReverseReferences and ManyToMany References


		/**
		 * Main constructor.  Constructor OR static create methods are designed to be called in either
		 * a parent QPanel or the main QForm when wanting to create a
		 * RecordingMetaControl to edit a single Recording object within the
		 * QPanel or QForm.
		 *
		 * This constructor takes in a single Recording object, while any of the static
		 * create methods below can be used to construct based off of individual PK ID(s).
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this RecordingMetaControl
		 * @param Recording $objRecording new or existing Recording object
		 */
		 public function __construct($objParentObject, Recording $objRecording) {
			// Setup Parent Object (e.g. QForm or QPanel which will be using this RecordingMetaControl)
			$this->objParentObject = $objParentObject;

			// Setup linked Recording object
			$this->objRecording = $objRecording;

			// Figure out if we're Editing or Creating New
			if ($this->objRecording->__Restored) {
				$this->strTitleVerb = QApplication::Translate('Edit');
				$this->blnEditMode = true;
			} else {
				$this->strTitleVerb = QApplication::Translate('Create');
				$this->blnEditMode = false;
			}
		 }

		/**
		 * Static Helper Method to Create using PK arguments
		 * You must pass in the PK arguments on an object to load, or leave it blank to create a new one.
		 * If you want to load via QueryString or PathInfo, use the CreateFromQueryString or CreateFromPathInfo
		 * static helper methods.  Finally, specify a CreateType to define whether or not we are only allowed to 
		 * edit, or if we are also allowed to create a new one, etc.
		 * 
		 * @param mixed $objParentObject QForm or QPanel which will be using this RecordingMetaControl
		 * @param string $strId primary key value
		 * @param QMetaControlCreateType $intCreateType rules governing Recording object creation - defaults to CreateOrEdit
 		 * @return RecordingMetaControl
		 */
		public static function Create($objParentObject, $strId = null, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			// Attempt to Load from PK Arguments
			if (strlen($strId)) {
				$objRecording = Recording::Load($strId);

				// Recording was found -- return it!
				if ($objRecording)
					return new RecordingMetaControl($objParentObject, $objRecording);

				// If CreateOnRecordNotFound not specified, throw an exception
				else if ($intCreateType != QMetaControlCreateType::CreateOnRecordNotFound)
					throw new QCallerException('Could not find a Recording object with PK arguments: ' . $strId);

			// If EditOnly is specified, throw an exception
			} else if ($intCreateType == QMetaControlCreateType::EditOnly)
				throw new QCallerException('No PK arguments specified');

			// If we are here, then we need to create a new record
			return new RecordingMetaControl($objParentObject, new Recording());
		}

		/**
		 * Static Helper Method to Create using PathInfo arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this RecordingMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Recording object creation - defaults to CreateOrEdit
		 * @return RecordingMetaControl
		 */
		public static function CreateFromPathInfo($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::PathInfo(0);
			return RecordingMetaControl::Create($objParentObject, $strId, $intCreateType);
		}

		/**
		 * Static Helper Method to Create using QueryString arguments
		 *
		 * @param mixed $objParentObject QForm or QPanel which will be using this RecordingMetaControl
		 * @param QMetaControlCreateType $intCreateType rules governing Recording object creation - defaults to CreateOrEdit
		 * @return RecordingMetaControl
		 */
		public static function CreateFromQueryString($objParentObject, $intCreateType = QMetaControlCreateType::CreateOrEdit) {
			$strId = QApplication::QueryString('strId');
			return RecordingMetaControl::Create($objParentObject, $strId, $intCreateType);
		}



		///////////////////////////////////////////////
		// PUBLIC CREATE and REFRESH METHODS
		///////////////////////////////////////////////

		/**
		 * Create and setup QTextBox txtId
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtId_Create($strControlId = null) {
			$this->txtId = new QTextBox($this->objParentObject, $strControlId);
			$this->txtId->Name = QApplication::Translate('Id');
			$this->txtId->Text = $this->objRecording->Id;
			$this->txtId->Required = true;
			$this->txtId->MaxLength = Recording::IdMaxLength;
			return $this->txtId;
		}

		/**
		 * Create and setup QLabel lblId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblId_Create($strControlId = null) {
			$this->lblId = new QLabel($this->objParentObject, $strControlId);
			$this->lblId->Name = QApplication::Translate('Id');
			$this->lblId->Text = $this->objRecording->Id;
			$this->lblId->Required = true;
			return $this->lblId;
		}

		/**
		 * Create and setup QListBox lstProject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstProject_Create($strControlId = null) {
			$this->lstProject = new QListBox($this->objParentObject, $strControlId);
			$this->lstProject->Name = QApplication::Translate('Project');
			$this->lstProject->Required = true;
			if (!$this->blnEditMode)
				$this->lstProject->AddItem(QApplication::Translate('- Select One -'), null);
			$objProjectArray = Project::LoadAll();
			if ($objProjectArray) foreach ($objProjectArray as $objProject) {
				$objListItem = new QListItem($objProject->__toString(), $objProject->Id);
				if (($this->objRecording->Project) && ($this->objRecording->Project->Id == $objProject->Id))
					$objListItem->Selected = true;
				$this->lstProject->AddItem($objListItem);
			}
			return $this->lstProject;
		}

		/**
		 * Create and setup QLabel lblProjectId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblProjectId_Create($strControlId = null) {
			$this->lblProjectId = new QLabel($this->objParentObject, $strControlId);
			$this->lblProjectId->Name = QApplication::Translate('Project');
			$this->lblProjectId->Text = ($this->objRecording->Project) ? $this->objRecording->Project->__toString() : null;
			$this->lblProjectId->Required = true;
			return $this->lblProjectId;
		}

		/**
		 * Create and setup QListBox lstTape
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstTape_Create($strControlId = null) {
			$this->lstTape = new QListBox($this->objParentObject, $strControlId);
			$this->lstTape->Name = QApplication::Translate('Tape');
			$this->lstTape->Required = true;
			if (!$this->blnEditMode)
				$this->lstTape->AddItem(QApplication::Translate('- Select One -'), null);
			$objTapeArray = Tape::LoadAll();
			if ($objTapeArray) foreach ($objTapeArray as $objTape) {
				$objListItem = new QListItem($objTape->__toString(), $objTape->Id);
				if (($this->objRecording->Tape) && ($this->objRecording->Tape->Id == $objTape->Id))
					$objListItem->Selected = true;
				$this->lstTape->AddItem($objListItem);
			}
			return $this->lstTape;
		}

		/**
		 * Create and setup QLabel lblTapeId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblTapeId_Create($strControlId = null) {
			$this->lblTapeId = new QLabel($this->objParentObject, $strControlId);
			$this->lblTapeId->Name = QApplication::Translate('Tape');
			$this->lblTapeId->Text = ($this->objRecording->Tape) ? $this->objRecording->Tape->__toString() : null;
			$this->lblTapeId->Required = true;
			return $this->lblTapeId;
		}

		/**
		 * Create and setup QListBox lstCamera
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstCamera_Create($strControlId = null) {
			$this->lstCamera = new QListBox($this->objParentObject, $strControlId);
			$this->lstCamera->Name = QApplication::Translate('Camera');
			$this->lstCamera->Required = true;
			if (!$this->blnEditMode)
				$this->lstCamera->AddItem(QApplication::Translate('- Select One -'), null);
			$objCameraArray = Camera::LoadAll();
			if ($objCameraArray) foreach ($objCameraArray as $objCamera) {
				$objListItem = new QListItem($objCamera->__toString(), $objCamera->Id);
				if (($this->objRecording->Camera) && ($this->objRecording->Camera->Id == $objCamera->Id))
					$objListItem->Selected = true;
				$this->lstCamera->AddItem($objListItem);
			}
			return $this->lstCamera;
		}

		/**
		 * Create and setup QLabel lblCameraId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCameraId_Create($strControlId = null) {
			$this->lblCameraId = new QLabel($this->objParentObject, $strControlId);
			$this->lblCameraId->Name = QApplication::Translate('Camera');
			$this->lblCameraId->Text = ($this->objRecording->Camera) ? $this->objRecording->Camera->__toString() : null;
			$this->lblCameraId->Required = true;
			return $this->lblCameraId;
		}

		/**
		 * Create and setup QListBox lstCameraPurpose
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstCameraPurpose_Create($strControlId = null) {
			$this->lstCameraPurpose = new QListBox($this->objParentObject, $strControlId);
			$this->lstCameraPurpose->Name = QApplication::Translate('Camera Purpose');
			$this->lstCameraPurpose->Required = true;
			if (!$this->blnEditMode)
				$this->lstCameraPurpose->AddItem(QApplication::Translate('- Select One -'), null);
			$objCameraPurposeArray = CameraPurpose::LoadAll();
			if ($objCameraPurposeArray) foreach ($objCameraPurposeArray as $objCameraPurpose) {
				$objListItem = new QListItem($objCameraPurpose->__toString(), $objCameraPurpose->Id);
				if (($this->objRecording->CameraPurpose) && ($this->objRecording->CameraPurpose->Id == $objCameraPurpose->Id))
					$objListItem->Selected = true;
				$this->lstCameraPurpose->AddItem($objListItem);
			}
			return $this->lstCameraPurpose;
		}

		/**
		 * Create and setup QLabel lblCameraPurposeId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblCameraPurposeId_Create($strControlId = null) {
			$this->lblCameraPurposeId = new QLabel($this->objParentObject, $strControlId);
			$this->lblCameraPurposeId->Name = QApplication::Translate('Camera Purpose');
			$this->lblCameraPurposeId->Text = ($this->objRecording->CameraPurpose) ? $this->objRecording->CameraPurpose->__toString() : null;
			$this->lblCameraPurposeId->Required = true;
			return $this->lblCameraPurposeId;
		}

		/**
		 * Create and setup QListBox lstTapeModeEnum
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstTapeModeEnum_Create($strControlId = null) {
			$this->lstTapeModeEnum = new QListBox($this->objParentObject, $strControlId);
			$this->lstTapeModeEnum->Name = QApplication::Translate('Tape Mode Enum');
			$this->lstTapeModeEnum->Required = true;
			foreach (TapeModeEnum::$NameArray as $intId => $strValue)
				$this->lstTapeModeEnum->AddItem(new QListItem($strValue, $intId, $this->objRecording->TapeModeEnumId == $intId));
			return $this->lstTapeModeEnum;
		}

		/**
		 * Create and setup QLabel lblTapeModeEnumId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblTapeModeEnumId_Create($strControlId = null) {
			$this->lblTapeModeEnumId = new QLabel($this->objParentObject, $strControlId);
			$this->lblTapeModeEnumId->Name = QApplication::Translate('Tape Mode Enum');
			$this->lblTapeModeEnumId->Text = ($this->objRecording->TapeModeEnumId) ? TapeModeEnum::$NameArray[$this->objRecording->TapeModeEnumId] : null;
			$this->lblTapeModeEnumId->Required = true;
			return $this->lblTapeModeEnumId;
		}

		/**
		 * Create and setup QListBox lstRecordingType
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstRecordingType_Create($strControlId = null) {
			$this->lstRecordingType = new QListBox($this->objParentObject, $strControlId);
			$this->lstRecordingType->Name = QApplication::Translate('Recording Type');
			$this->lstRecordingType->Required = true;
			if (!$this->blnEditMode)
				$this->lstRecordingType->AddItem(QApplication::Translate('- Select One -'), null);
			$objRecordingTypeArray = RecordingType::LoadAll();
			if ($objRecordingTypeArray) foreach ($objRecordingTypeArray as $objRecordingType) {
				$objListItem = new QListItem($objRecordingType->__toString(), $objRecordingType->Id);
				if (($this->objRecording->RecordingType) && ($this->objRecording->RecordingType->Id == $objRecordingType->Id))
					$objListItem->Selected = true;
				$this->lstRecordingType->AddItem($objListItem);
			}
			return $this->lstRecordingType;
		}

		/**
		 * Create and setup QLabel lblRecordingTypeId
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblRecordingTypeId_Create($strControlId = null) {
			$this->lblRecordingTypeId = new QLabel($this->objParentObject, $strControlId);
			$this->lblRecordingTypeId->Name = QApplication::Translate('Recording Type');
			$this->lblRecordingTypeId->Text = ($this->objRecording->RecordingType) ? $this->objRecording->RecordingType->__toString() : null;
			$this->lblRecordingTypeId->Required = true;
			return $this->lblRecordingTypeId;
		}

		/**
		 * Create and setup QIntegerTextBox txtStart
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtStart_Create($strControlId = null) {
			$this->txtStart = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtStart->Name = QApplication::Translate('Start');
			$this->txtStart->Text = $this->objRecording->Start;
			$this->txtStart->Required = true;
			return $this->txtStart;
		}

		/**
		 * Create and setup QLabel lblStart
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblStart_Create($strControlId = null, $strFormat = null) {
			$this->lblStart = new QLabel($this->objParentObject, $strControlId);
			$this->lblStart->Name = QApplication::Translate('Start');
			$this->lblStart->Text = $this->objRecording->Start;
			$this->lblStart->Required = true;
			$this->lblStart->Format = $strFormat;
			return $this->lblStart;
		}

		/**
		 * Create and setup QIntegerTextBox txtEnd
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtEnd_Create($strControlId = null) {
			$this->txtEnd = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtEnd->Name = QApplication::Translate('End');
			$this->txtEnd->Text = $this->objRecording->End;
			$this->txtEnd->Required = true;
			return $this->txtEnd;
		}

		/**
		 * Create and setup QLabel lblEnd
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblEnd_Create($strControlId = null, $strFormat = null) {
			$this->lblEnd = new QLabel($this->objParentObject, $strControlId);
			$this->lblEnd->Name = QApplication::Translate('End');
			$this->lblEnd->Text = $this->objRecording->End;
			$this->lblEnd->Required = true;
			$this->lblEnd->Format = $strFormat;
			return $this->lblEnd;
		}

		/**
		 * Create and setup QCheckBox chkWidescreen
		 * @param string $strControlId optional ControlId to use
		 * @return QCheckBox
		 */
		public function chkWidescreen_Create($strControlId = null) {
			$this->chkWidescreen = new QCheckBox($this->objParentObject, $strControlId);
			$this->chkWidescreen->Name = QApplication::Translate('Widescreen');
			$this->chkWidescreen->Checked = $this->objRecording->Widescreen;
			return $this->chkWidescreen;
		}

		/**
		 * Create and setup QLabel lblWidescreen
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblWidescreen_Create($strControlId = null) {
			$this->lblWidescreen = new QLabel($this->objParentObject, $strControlId);
			$this->lblWidescreen->Name = QApplication::Translate('Widescreen');
			$this->lblWidescreen->Text = ($this->objRecording->Widescreen) ? QApplication::Translate('Yes') : QApplication::Translate('No');
			return $this->lblWidescreen;
		}

		/**
		 * Create and setup QDateTimePicker calDate
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calDate_Create($strControlId = null) {
			$this->calDate = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calDate->Name = QApplication::Translate('Date');
			$this->calDate->DateTime = $this->objRecording->Date;
			$this->calDate->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calDate->Required = true;
			return $this->calDate;
		}

		/**
		 * Create and setup QLabel lblDate
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblDate_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblDate = new QLabel($this->objParentObject, $strControlId);
			$this->lblDate->Name = QApplication::Translate('Date');
			$this->strDateDateTimeFormat = $strDateTimeFormat;
			$this->lblDate->Text = sprintf($this->objRecording->Date) ? $this->objRecording->__toString($this->strDateDateTimeFormat) : null;
			$this->lblDate->Required = true;
			return $this->lblDate;
		}

		protected $strDateDateTimeFormat;

		/**
		 * Create and setup QIntegerTextBox txtScene
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtScene_Create($strControlId = null) {
			$this->txtScene = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtScene->Name = QApplication::Translate('Scene');
			$this->txtScene->Text = $this->objRecording->Scene;
			$this->txtScene->Required = true;
			return $this->txtScene;
		}

		/**
		 * Create and setup QLabel lblScene
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblScene_Create($strControlId = null, $strFormat = null) {
			$this->lblScene = new QLabel($this->objParentObject, $strControlId);
			$this->lblScene->Name = QApplication::Translate('Scene');
			$this->lblScene->Text = $this->objRecording->Scene;
			$this->lblScene->Required = true;
			$this->lblScene->Format = $strFormat;
			return $this->lblScene;
		}

		/**
		 * Create and setup QIntegerTextBox txtTake
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtTake_Create($strControlId = null) {
			$this->txtTake = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtTake->Name = QApplication::Translate('Take');
			$this->txtTake->Text = $this->objRecording->Take;
			$this->txtTake->Required = true;
			return $this->txtTake;
		}

		/**
		 * Create and setup QLabel lblTake
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblTake_Create($strControlId = null, $strFormat = null) {
			$this->lblTake = new QLabel($this->objParentObject, $strControlId);
			$this->lblTake->Name = QApplication::Translate('Take');
			$this->lblTake->Text = $this->objRecording->Take;
			$this->lblTake->Required = true;
			$this->lblTake->Format = $strFormat;
			return $this->lblTake;
		}

		/**
		 * Create and setup QIntegerTextBox txtAngle
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtAngle_Create($strControlId = null) {
			$this->txtAngle = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtAngle->Name = QApplication::Translate('Angle');
			$this->txtAngle->Text = $this->objRecording->Angle;
			$this->txtAngle->Required = true;
			return $this->txtAngle;
		}

		/**
		 * Create and setup QLabel lblAngle
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblAngle_Create($strControlId = null, $strFormat = null) {
			$this->lblAngle = new QLabel($this->objParentObject, $strControlId);
			$this->lblAngle->Name = QApplication::Translate('Angle');
			$this->lblAngle->Text = $this->objRecording->Angle;
			$this->lblAngle->Required = true;
			$this->lblAngle->Format = $strFormat;
			return $this->lblAngle;
		}

		/**
		 * Create and setup QDateTimePicker calAdded
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calAdded_Create($strControlId = null) {
			$this->calAdded = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calAdded->Name = QApplication::Translate('Added');
			$this->calAdded->DateTime = $this->objRecording->Added;
			$this->calAdded->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calAdded->Required = true;
			return $this->calAdded;
		}

		/**
		 * Create and setup QLabel lblAdded
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblAdded_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblAdded = new QLabel($this->objParentObject, $strControlId);
			$this->lblAdded->Name = QApplication::Translate('Added');
			$this->strAddedDateTimeFormat = $strDateTimeFormat;
			$this->lblAdded->Text = sprintf($this->objRecording->Added) ? $this->objRecording->__toString($this->strAddedDateTimeFormat) : null;
			$this->lblAdded->Required = true;
			return $this->lblAdded;
		}

		protected $strAddedDateTimeFormat;

		/**
		 * Create and setup QDateTimePicker calChanged
		 * @param string $strControlId optional ControlId to use
		 * @return QDateTimePicker
		 */
		public function calChanged_Create($strControlId = null) {
			$this->calChanged = new QDateTimePicker($this->objParentObject, $strControlId);
			$this->calChanged->Name = QApplication::Translate('Changed');
			$this->calChanged->DateTime = $this->objRecording->Changed;
			$this->calChanged->DateTimePickerType = QDateTimePickerType::DateTime;
			$this->calChanged->Required = true;
			return $this->calChanged;
		}

		/**
		 * Create and setup QLabel lblChanged
		 * @param string $strControlId optional ControlId to use
		 * @param string $strDateTimeFormat optional DateTimeFormat to use
		 * @return QLabel
		 */
		public function lblChanged_Create($strControlId = null, $strDateTimeFormat = null) {
			$this->lblChanged = new QLabel($this->objParentObject, $strControlId);
			$this->lblChanged->Name = QApplication::Translate('Changed');
			$this->strChangedDateTimeFormat = $strDateTimeFormat;
			$this->lblChanged->Text = sprintf($this->objRecording->Changed) ? $this->objRecording->__toString($this->strChangedDateTimeFormat) : null;
			$this->lblChanged->Required = true;
			return $this->lblChanged;
		}

		protected $strChangedDateTimeFormat;

		/**
		 * Create and setup QTextBox txtComment
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtComment_Create($strControlId = null) {
			$this->txtComment = new QTextBox($this->objParentObject, $strControlId);
			$this->txtComment->Name = QApplication::Translate('Comment');
			$this->txtComment->Text = $this->objRecording->Comment;
			$this->txtComment->TextMode = QTextMode::MultiLine;
			return $this->txtComment;
		}

		/**
		 * Create and setup QLabel lblComment
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblComment_Create($strControlId = null) {
			$this->lblComment = new QLabel($this->objParentObject, $strControlId);
			$this->lblComment->Name = QApplication::Translate('Comment');
			$this->lblComment->Text = $this->objRecording->Comment;
			return $this->lblComment;
		}

		/**
		 * Create and setup QLabel lblOptlock
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOptlock_Create($strControlId = null) {
			$this->lblOptlock = new QLabel($this->objParentObject, $strControlId);
			$this->lblOptlock->Name = QApplication::Translate('Optlock');
			if ($this->blnEditMode)
				$this->lblOptlock->Text = $this->objRecording->Optlock;
			else
				$this->lblOptlock->Text = 'N/A';
			return $this->lblOptlock;
		}

		/**
		 * Create and setup QTextBox txtOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QTextBox
		 */
		public function txtOwner_Create($strControlId = null) {
			$this->txtOwner = new QTextBox($this->objParentObject, $strControlId);
			$this->txtOwner->Name = QApplication::Translate('Owner');
			$this->txtOwner->Text = $this->objRecording->Owner;
			$this->txtOwner->MaxLength = Recording::OwnerMaxLength;
			return $this->txtOwner;
		}

		/**
		 * Create and setup QLabel lblOwner
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblOwner_Create($strControlId = null) {
			$this->lblOwner = new QLabel($this->objParentObject, $strControlId);
			$this->lblOwner->Name = QApplication::Translate('Owner');
			$this->lblOwner->Text = $this->objRecording->Owner;
			return $this->lblOwner;
		}

		/**
		 * Create and setup QIntegerTextBox txtGroup
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtGroup_Create($strControlId = null) {
			$this->txtGroup = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtGroup->Name = QApplication::Translate('Group');
			$this->txtGroup->Text = $this->objRecording->Group;
			$this->txtGroup->Required = true;
			return $this->txtGroup;
		}

		/**
		 * Create and setup QLabel lblGroup
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblGroup_Create($strControlId = null, $strFormat = null) {
			$this->lblGroup = new QLabel($this->objParentObject, $strControlId);
			$this->lblGroup->Name = QApplication::Translate('Group');
			$this->lblGroup->Text = $this->objRecording->Group;
			$this->lblGroup->Required = true;
			$this->lblGroup->Format = $strFormat;
			return $this->lblGroup;
		}

		/**
		 * Create and setup QIntegerTextBox txtPerms
		 * @param string $strControlId optional ControlId to use
		 * @return QIntegerTextBox
		 */
		public function txtPerms_Create($strControlId = null) {
			$this->txtPerms = new QIntegerTextBox($this->objParentObject, $strControlId);
			$this->txtPerms->Name = QApplication::Translate('Perms');
			$this->txtPerms->Text = $this->objRecording->Perms;
			$this->txtPerms->Required = true;
			return $this->txtPerms;
		}

		/**
		 * Create and setup QLabel lblPerms
		 * @param string $strControlId optional ControlId to use
		 * @param string $strFormat optional sprintf format to use
		 * @return QLabel
		 */
		public function lblPerms_Create($strControlId = null, $strFormat = null) {
			$this->lblPerms = new QLabel($this->objParentObject, $strControlId);
			$this->lblPerms->Name = QApplication::Translate('Perms');
			$this->lblPerms->Text = $this->objRecording->Perms;
			$this->lblPerms->Required = true;
			$this->lblPerms->Format = $strFormat;
			return $this->lblPerms;
		}

		/**
		 * Create and setup QListBox lstStatusObject
		 * @param string $strControlId optional ControlId to use
		 * @return QListBox
		 */
		public function lstStatusObject_Create($strControlId = null) {
			$this->lstStatusObject = new QListBox($this->objParentObject, $strControlId);
			$this->lstStatusObject->Name = QApplication::Translate('Status Object');
			$this->lstStatusObject->Required = true;
			foreach (AuthStatusEnum::$NameArray as $intId => $strValue)
				$this->lstStatusObject->AddItem(new QListItem($strValue, $intId, $this->objRecording->Status == $intId));
			return $this->lstStatusObject;
		}

		/**
		 * Create and setup QLabel lblStatus
		 * @param string $strControlId optional ControlId to use
		 * @return QLabel
		 */
		public function lblStatus_Create($strControlId = null) {
			$this->lblStatus = new QLabel($this->objParentObject, $strControlId);
			$this->lblStatus->Name = QApplication::Translate('Status Object');
			$this->lblStatus->Text = ($this->objRecording->Status) ? AuthStatusEnum::$NameArray[$this->objRecording->Status] : null;
			$this->lblStatus->Required = true;
			return $this->lblStatus;
		}



		/**
		 * Refresh this MetaControl with Data from the local Recording object.
		 * @param boolean $blnReload reload Recording from the database
		 * @return void
		 */
		public function Refresh($blnReload = false) {
			if ($blnReload)
				$this->objRecording->Reload();

			if ($this->txtId) $this->txtId->Text = $this->objRecording->Id;
			if ($this->lblId) $this->lblId->Text = $this->objRecording->Id;

			if ($this->lstProject) {
					$this->lstProject->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstProject->AddItem(QApplication::Translate('- Select One -'), null);
				$objProjectArray = Project::LoadAll();
				if ($objProjectArray) foreach ($objProjectArray as $objProject) {
					$objListItem = new QListItem($objProject->__toString(), $objProject->Id);
					if (($this->objRecording->Project) && ($this->objRecording->Project->Id == $objProject->Id))
						$objListItem->Selected = true;
					$this->lstProject->AddItem($objListItem);
				}
			}
			if ($this->lblProjectId) $this->lblProjectId->Text = ($this->objRecording->Project) ? $this->objRecording->Project->__toString() : null;

			if ($this->lstTape) {
					$this->lstTape->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstTape->AddItem(QApplication::Translate('- Select One -'), null);
				$objTapeArray = Tape::LoadAll();
				if ($objTapeArray) foreach ($objTapeArray as $objTape) {
					$objListItem = new QListItem($objTape->__toString(), $objTape->Id);
					if (($this->objRecording->Tape) && ($this->objRecording->Tape->Id == $objTape->Id))
						$objListItem->Selected = true;
					$this->lstTape->AddItem($objListItem);
				}
			}
			if ($this->lblTapeId) $this->lblTapeId->Text = ($this->objRecording->Tape) ? $this->objRecording->Tape->__toString() : null;

			if ($this->lstCamera) {
					$this->lstCamera->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstCamera->AddItem(QApplication::Translate('- Select One -'), null);
				$objCameraArray = Camera::LoadAll();
				if ($objCameraArray) foreach ($objCameraArray as $objCamera) {
					$objListItem = new QListItem($objCamera->__toString(), $objCamera->Id);
					if (($this->objRecording->Camera) && ($this->objRecording->Camera->Id == $objCamera->Id))
						$objListItem->Selected = true;
					$this->lstCamera->AddItem($objListItem);
				}
			}
			if ($this->lblCameraId) $this->lblCameraId->Text = ($this->objRecording->Camera) ? $this->objRecording->Camera->__toString() : null;

			if ($this->lstCameraPurpose) {
					$this->lstCameraPurpose->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstCameraPurpose->AddItem(QApplication::Translate('- Select One -'), null);
				$objCameraPurposeArray = CameraPurpose::LoadAll();
				if ($objCameraPurposeArray) foreach ($objCameraPurposeArray as $objCameraPurpose) {
					$objListItem = new QListItem($objCameraPurpose->__toString(), $objCameraPurpose->Id);
					if (($this->objRecording->CameraPurpose) && ($this->objRecording->CameraPurpose->Id == $objCameraPurpose->Id))
						$objListItem->Selected = true;
					$this->lstCameraPurpose->AddItem($objListItem);
				}
			}
			if ($this->lblCameraPurposeId) $this->lblCameraPurposeId->Text = ($this->objRecording->CameraPurpose) ? $this->objRecording->CameraPurpose->__toString() : null;

			if ($this->lstTapeModeEnum) $this->lstTapeModeEnum->SelectedValue = $this->objRecording->TapeModeEnumId;
			if ($this->lblTapeModeEnumId) $this->lblTapeModeEnumId->Text = ($this->objRecording->TapeModeEnumId) ? TapeModeEnum::$NameArray[$this->objRecording->TapeModeEnumId] : null;

			if ($this->lstRecordingType) {
					$this->lstRecordingType->RemoveAllItems();
				if (!$this->blnEditMode)
					$this->lstRecordingType->AddItem(QApplication::Translate('- Select One -'), null);
				$objRecordingTypeArray = RecordingType::LoadAll();
				if ($objRecordingTypeArray) foreach ($objRecordingTypeArray as $objRecordingType) {
					$objListItem = new QListItem($objRecordingType->__toString(), $objRecordingType->Id);
					if (($this->objRecording->RecordingType) && ($this->objRecording->RecordingType->Id == $objRecordingType->Id))
						$objListItem->Selected = true;
					$this->lstRecordingType->AddItem($objListItem);
				}
			}
			if ($this->lblRecordingTypeId) $this->lblRecordingTypeId->Text = ($this->objRecording->RecordingType) ? $this->objRecording->RecordingType->__toString() : null;

			if ($this->txtStart) $this->txtStart->Text = $this->objRecording->Start;
			if ($this->lblStart) $this->lblStart->Text = $this->objRecording->Start;

			if ($this->txtEnd) $this->txtEnd->Text = $this->objRecording->End;
			if ($this->lblEnd) $this->lblEnd->Text = $this->objRecording->End;

			if ($this->chkWidescreen) $this->chkWidescreen->Checked = $this->objRecording->Widescreen;
			if ($this->lblWidescreen) $this->lblWidescreen->Text = ($this->objRecording->Widescreen) ? QApplication::Translate('Yes') : QApplication::Translate('No');

			if ($this->calDate) $this->calDate->DateTime = $this->objRecording->Date;
			if ($this->lblDate) $this->lblDate->Text = sprintf($this->objRecording->Date) ? $this->objRecording->__toString($this->strDateDateTimeFormat) : null;

			if ($this->txtScene) $this->txtScene->Text = $this->objRecording->Scene;
			if ($this->lblScene) $this->lblScene->Text = $this->objRecording->Scene;

			if ($this->txtTake) $this->txtTake->Text = $this->objRecording->Take;
			if ($this->lblTake) $this->lblTake->Text = $this->objRecording->Take;

			if ($this->txtAngle) $this->txtAngle->Text = $this->objRecording->Angle;
			if ($this->lblAngle) $this->lblAngle->Text = $this->objRecording->Angle;

			if ($this->calAdded) $this->calAdded->DateTime = $this->objRecording->Added;
			if ($this->lblAdded) $this->lblAdded->Text = sprintf($this->objRecording->Added) ? $this->objRecording->__toString($this->strAddedDateTimeFormat) : null;

			if ($this->calChanged) $this->calChanged->DateTime = $this->objRecording->Changed;
			if ($this->lblChanged) $this->lblChanged->Text = sprintf($this->objRecording->Changed) ? $this->objRecording->__toString($this->strChangedDateTimeFormat) : null;

			if ($this->txtComment) $this->txtComment->Text = $this->objRecording->Comment;
			if ($this->lblComment) $this->lblComment->Text = $this->objRecording->Comment;

			if ($this->lblOptlock) if ($this->blnEditMode) $this->lblOptlock->Text = $this->objRecording->Optlock;

			if ($this->txtOwner) $this->txtOwner->Text = $this->objRecording->Owner;
			if ($this->lblOwner) $this->lblOwner->Text = $this->objRecording->Owner;

			if ($this->txtGroup) $this->txtGroup->Text = $this->objRecording->Group;
			if ($this->lblGroup) $this->lblGroup->Text = $this->objRecording->Group;

			if ($this->txtPerms) $this->txtPerms->Text = $this->objRecording->Perms;
			if ($this->lblPerms) $this->lblPerms->Text = $this->objRecording->Perms;

			if ($this->lstStatusObject) $this->lstStatusObject->SelectedValue = $this->objRecording->Status;
			if ($this->lblStatus) $this->lblStatus->Text = ($this->objRecording->Status) ? AuthStatusEnum::$NameArray[$this->objRecording->Status] : null;

		}



		///////////////////////////////////////////////
		// PROTECTED UPDATE METHODS for ManyToManyReferences (if any)
		///////////////////////////////////////////////





		///////////////////////////////////////////////
		// PUBLIC RECORDING OBJECT MANIPULATORS
		///////////////////////////////////////////////

		/**
		 * This will save this object's Recording instance,
		 * updating only the fields which have had a control created for it.
		 */
		public function SaveRecording() {
			try {
				// Update any fields for controls that have been created
				if ($this->txtId) $this->objRecording->Id = $this->txtId->Text;
				if ($this->lstProject) $this->objRecording->ProjectId = $this->lstProject->SelectedValue;
				if ($this->lstTape) $this->objRecording->TapeId = $this->lstTape->SelectedValue;
				if ($this->lstCamera) $this->objRecording->CameraId = $this->lstCamera->SelectedValue;
				if ($this->lstCameraPurpose) $this->objRecording->CameraPurposeId = $this->lstCameraPurpose->SelectedValue;
				if ($this->lstTapeModeEnum) $this->objRecording->TapeModeEnumId = $this->lstTapeModeEnum->SelectedValue;
				if ($this->lstRecordingType) $this->objRecording->RecordingTypeId = $this->lstRecordingType->SelectedValue;
				if ($this->txtStart) $this->objRecording->Start = $this->txtStart->Text;
				if ($this->txtEnd) $this->objRecording->End = $this->txtEnd->Text;
				if ($this->chkWidescreen) $this->objRecording->Widescreen = $this->chkWidescreen->Checked;
				if ($this->calDate) $this->objRecording->Date = $this->calDate->DateTime;
				if ($this->txtScene) $this->objRecording->Scene = $this->txtScene->Text;
				if ($this->txtTake) $this->objRecording->Take = $this->txtTake->Text;
				if ($this->txtAngle) $this->objRecording->Angle = $this->txtAngle->Text;
				if ($this->calAdded) $this->objRecording->Added = $this->calAdded->DateTime;
				if ($this->calChanged) $this->objRecording->Changed = $this->calChanged->DateTime;
				if ($this->txtComment) $this->objRecording->Comment = $this->txtComment->Text;
				if ($this->txtOwner) $this->objRecording->Owner = $this->txtOwner->Text;
				if ($this->txtGroup) $this->objRecording->Group = $this->txtGroup->Text;
				if ($this->txtPerms) $this->objRecording->Perms = $this->txtPerms->Text;
				if ($this->lstStatusObject) $this->objRecording->Status = $this->lstStatusObject->SelectedValue;

				// Update any UniqueReverseReferences (if any) for controls that have been created for it

				// Save the Recording object
				$this->objRecording->Save();

				// Finally, update any ManyToManyReferences (if any)
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * This will DELETE this object's Recording instance from the database.
		 * It will also unassociate itself from any ManyToManyReferences.
		 */
		public function DeleteRecording() {
			$this->objRecording->Delete();
		}		



		///////////////////////////////////////////////
		// PUBLIC GETTERS and SETTERS
		///////////////////////////////////////////////

		/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				// General MetaControlVariables
				case 'Recording': return $this->objRecording;
				case 'TitleVerb': return $this->strTitleVerb;
				case 'EditMode': return $this->blnEditMode;

				// Controls that point to Recording fields -- will be created dynamically if not yet created
				case 'IdControl':
					if (!$this->txtId) return $this->txtId_Create();
					return $this->txtId;
				case 'IdLabel':
					if (!$this->lblId) return $this->lblId_Create();
					return $this->lblId;
				case 'ProjectIdControl':
					if (!$this->lstProject) return $this->lstProject_Create();
					return $this->lstProject;
				case 'ProjectIdLabel':
					if (!$this->lblProjectId) return $this->lblProjectId_Create();
					return $this->lblProjectId;
				case 'TapeIdControl':
					if (!$this->lstTape) return $this->lstTape_Create();
					return $this->lstTape;
				case 'TapeIdLabel':
					if (!$this->lblTapeId) return $this->lblTapeId_Create();
					return $this->lblTapeId;
				case 'CameraIdControl':
					if (!$this->lstCamera) return $this->lstCamera_Create();
					return $this->lstCamera;
				case 'CameraIdLabel':
					if (!$this->lblCameraId) return $this->lblCameraId_Create();
					return $this->lblCameraId;
				case 'CameraPurposeIdControl':
					if (!$this->lstCameraPurpose) return $this->lstCameraPurpose_Create();
					return $this->lstCameraPurpose;
				case 'CameraPurposeIdLabel':
					if (!$this->lblCameraPurposeId) return $this->lblCameraPurposeId_Create();
					return $this->lblCameraPurposeId;
				case 'TapeModeEnumIdControl':
					if (!$this->lstTapeModeEnum) return $this->lstTapeModeEnum_Create();
					return $this->lstTapeModeEnum;
				case 'TapeModeEnumIdLabel':
					if (!$this->lblTapeModeEnumId) return $this->lblTapeModeEnumId_Create();
					return $this->lblTapeModeEnumId;
				case 'RecordingTypeIdControl':
					if (!$this->lstRecordingType) return $this->lstRecordingType_Create();
					return $this->lstRecordingType;
				case 'RecordingTypeIdLabel':
					if (!$this->lblRecordingTypeId) return $this->lblRecordingTypeId_Create();
					return $this->lblRecordingTypeId;
				case 'StartControl':
					if (!$this->txtStart) return $this->txtStart_Create();
					return $this->txtStart;
				case 'StartLabel':
					if (!$this->lblStart) return $this->lblStart_Create();
					return $this->lblStart;
				case 'EndControl':
					if (!$this->txtEnd) return $this->txtEnd_Create();
					return $this->txtEnd;
				case 'EndLabel':
					if (!$this->lblEnd) return $this->lblEnd_Create();
					return $this->lblEnd;
				case 'WidescreenControl':
					if (!$this->chkWidescreen) return $this->chkWidescreen_Create();
					return $this->chkWidescreen;
				case 'WidescreenLabel':
					if (!$this->lblWidescreen) return $this->lblWidescreen_Create();
					return $this->lblWidescreen;
				case 'DateControl':
					if (!$this->calDate) return $this->calDate_Create();
					return $this->calDate;
				case 'DateLabel':
					if (!$this->lblDate) return $this->lblDate_Create();
					return $this->lblDate;
				case 'SceneControl':
					if (!$this->txtScene) return $this->txtScene_Create();
					return $this->txtScene;
				case 'SceneLabel':
					if (!$this->lblScene) return $this->lblScene_Create();
					return $this->lblScene;
				case 'TakeControl':
					if (!$this->txtTake) return $this->txtTake_Create();
					return $this->txtTake;
				case 'TakeLabel':
					if (!$this->lblTake) return $this->lblTake_Create();
					return $this->lblTake;
				case 'AngleControl':
					if (!$this->txtAngle) return $this->txtAngle_Create();
					return $this->txtAngle;
				case 'AngleLabel':
					if (!$this->lblAngle) return $this->lblAngle_Create();
					return $this->lblAngle;
				case 'AddedControl':
					if (!$this->calAdded) return $this->calAdded_Create();
					return $this->calAdded;
				case 'AddedLabel':
					if (!$this->lblAdded) return $this->lblAdded_Create();
					return $this->lblAdded;
				case 'ChangedControl':
					if (!$this->calChanged) return $this->calChanged_Create();
					return $this->calChanged;
				case 'ChangedLabel':
					if (!$this->lblChanged) return $this->lblChanged_Create();
					return $this->lblChanged;
				case 'CommentControl':
					if (!$this->txtComment) return $this->txtComment_Create();
					return $this->txtComment;
				case 'CommentLabel':
					if (!$this->lblComment) return $this->lblComment_Create();
					return $this->lblComment;
				case 'OptlockControl':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OptlockLabel':
					if (!$this->lblOptlock) return $this->lblOptlock_Create();
					return $this->lblOptlock;
				case 'OwnerControl':
					if (!$this->txtOwner) return $this->txtOwner_Create();
					return $this->txtOwner;
				case 'OwnerLabel':
					if (!$this->lblOwner) return $this->lblOwner_Create();
					return $this->lblOwner;
				case 'GroupControl':
					if (!$this->txtGroup) return $this->txtGroup_Create();
					return $this->txtGroup;
				case 'GroupLabel':
					if (!$this->lblGroup) return $this->lblGroup_Create();
					return $this->lblGroup;
				case 'PermsControl':
					if (!$this->txtPerms) return $this->txtPerms_Create();
					return $this->txtPerms;
				case 'PermsLabel':
					if (!$this->lblPerms) return $this->lblPerms_Create();
					return $this->lblPerms;
				case 'StatusControl':
					if (!$this->lstStatusObject) return $this->lstStatusObject_Create();
					return $this->lstStatusObject;
				case 'StatusLabel':
					if (!$this->lblStatus) return $this->lblStatus_Create();
					return $this->lblStatus;
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			try {
				switch ($strName) {
					// Controls that point to Recording fields
					case 'IdControl':
						return ($this->txtId = QType::Cast($mixValue, 'QControl'));
					case 'ProjectIdControl':
						return ($this->lstProject = QType::Cast($mixValue, 'QControl'));
					case 'TapeIdControl':
						return ($this->lstTape = QType::Cast($mixValue, 'QControl'));
					case 'CameraIdControl':
						return ($this->lstCamera = QType::Cast($mixValue, 'QControl'));
					case 'CameraPurposeIdControl':
						return ($this->lstCameraPurpose = QType::Cast($mixValue, 'QControl'));
					case 'TapeModeEnumIdControl':
						return ($this->lstTapeModeEnum = QType::Cast($mixValue, 'QControl'));
					case 'RecordingTypeIdControl':
						return ($this->lstRecordingType = QType::Cast($mixValue, 'QControl'));
					case 'StartControl':
						return ($this->txtStart = QType::Cast($mixValue, 'QControl'));
					case 'EndControl':
						return ($this->txtEnd = QType::Cast($mixValue, 'QControl'));
					case 'WidescreenControl':
						return ($this->chkWidescreen = QType::Cast($mixValue, 'QControl'));
					case 'DateControl':
						return ($this->calDate = QType::Cast($mixValue, 'QControl'));
					case 'SceneControl':
						return ($this->txtScene = QType::Cast($mixValue, 'QControl'));
					case 'TakeControl':
						return ($this->txtTake = QType::Cast($mixValue, 'QControl'));
					case 'AngleControl':
						return ($this->txtAngle = QType::Cast($mixValue, 'QControl'));
					case 'AddedControl':
						return ($this->calAdded = QType::Cast($mixValue, 'QControl'));
					case 'ChangedControl':
						return ($this->calChanged = QType::Cast($mixValue, 'QControl'));
					case 'CommentControl':
						return ($this->txtComment = QType::Cast($mixValue, 'QControl'));
					case 'OptlockControl':
						return ($this->lblOptlock = QType::Cast($mixValue, 'QControl'));
					case 'OwnerControl':
						return ($this->txtOwner = QType::Cast($mixValue, 'QControl'));
					case 'GroupControl':
						return ($this->txtGroup = QType::Cast($mixValue, 'QControl'));
					case 'PermsControl':
						return ($this->txtPerms = QType::Cast($mixValue, 'QControl'));
					case 'StatusControl':
						return ($this->lstStatusObject = QType::Cast($mixValue, 'QControl'));
					default:
						return parent::__set($strName, $mixValue);
				}
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}
	}
?>
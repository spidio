<?php
	/**
	 * The abstract AuthActionGen class defined here is
	 * code-generated and contains all the basic CRUD-type functionality as well as
	 * basic methods to handle relationships and index-based loading.
	 *
	 * To use, you should use the AuthAction subclass which
	 * extends this AuthActionGen class.
	 *
	 * Because subsequent re-code generations will overwrite any changes to this
	 * file, you should leave this file unaltered to prevent yourself from losing
	 * any information or code changes.  All customizations should be done by
	 * overriding existing or implementing new methods, properties and variables
	 * in the AuthAction class.
	 * 
	 * @package Spidio
	 * @subpackage GeneratedDataObjects
	 * @property-read integer $Id the value for intId (Read-Only PK)
	 * @property string $Name the value for strName (Unique)
	 * @property boolean $ApplyToObject the value for blnApplyToObject (Not Null)
	 * @property-read AuthImplementedAction $_AuthImplementedActionAsAuthAction the value for the private _objAuthImplementedActionAsAuthAction (Read-Only) if set due to an expansion on the spidio_auth_implemented_action.auth_action_id reverse relationship
	 * @property-read AuthImplementedAction[] $_AuthImplementedActionAsAuthActionArray the value for the private _objAuthImplementedActionAsAuthActionArray (Read-Only) if set due to an ExpandAsArray on the spidio_auth_implemented_action.auth_action_id reverse relationship
	 * @property-read AuthPrivilege $_AuthPrivilegeAsAuthAction the value for the private _objAuthPrivilegeAsAuthAction (Read-Only) if set due to an expansion on the spidio_auth_privilege.auth_action_id reverse relationship
	 * @property-read AuthPrivilege[] $_AuthPrivilegeAsAuthActionArray the value for the private _objAuthPrivilegeAsAuthActionArray (Read-Only) if set due to an ExpandAsArray on the spidio_auth_privilege.auth_action_id reverse relationship
	 * @property-read boolean $__Restored whether or not this object was restored from the database (as opposed to created new)
	 */
	class AuthActionGen extends DatabaseClass {

		///////////////////////////////////////////////////////////////////////
		// PROTECTED MEMBER VARIABLES and TEXT FIELD MAXLENGTHS (if applicable)
		///////////////////////////////////////////////////////////////////////
		
		/**
		 * Protected member variable that maps to the database PK Identity column spidio_auth_action.id
		 * @var integer intId
		 */
		protected $intId;
		const IdDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_action.name
		 * @var string strName
		 */
		protected $strName;
		const NameMaxLength = 30;
		const NameDefault = null;


		/**
		 * Protected member variable that maps to the database column spidio_auth_action.apply_to_object
		 * @var boolean blnApplyToObject
		 */
		protected $blnApplyToObject;
		const ApplyToObjectDefault = null;


		/**
		 * Private member variable that stores a reference to a single AuthImplementedActionAsAuthAction object
		 * (of type AuthImplementedAction), if this AuthAction object was restored with
		 * an expansion on the spidio_auth_implemented_action association table.
		 * @var AuthImplementedAction _objAuthImplementedActionAsAuthAction;
		 */
		private $_objAuthImplementedActionAsAuthAction;

		/**
		 * Private member variable that stores a reference to an array of AuthImplementedActionAsAuthAction objects
		 * (of type AuthImplementedAction[]), if this AuthAction object was restored with
		 * an ExpandAsArray on the spidio_auth_implemented_action association table.
		 * @var AuthImplementedAction[] _objAuthImplementedActionAsAuthActionArray;
		 */
		private $_objAuthImplementedActionAsAuthActionArray = array();

		/**
		 * Private member variable that stores a reference to a single AuthPrivilegeAsAuthAction object
		 * (of type AuthPrivilege), if this AuthAction object was restored with
		 * an expansion on the spidio_auth_privilege association table.
		 * @var AuthPrivilege _objAuthPrivilegeAsAuthAction;
		 */
		private $_objAuthPrivilegeAsAuthAction;

		/**
		 * Private member variable that stores a reference to an array of AuthPrivilegeAsAuthAction objects
		 * (of type AuthPrivilege[]), if this AuthAction object was restored with
		 * an ExpandAsArray on the spidio_auth_privilege association table.
		 * @var AuthPrivilege[] _objAuthPrivilegeAsAuthActionArray;
		 */
		private $_objAuthPrivilegeAsAuthActionArray = array();

		/**
		 * Protected array of virtual attributes for this object (e.g. extra/other calculated and/or non-object bound
		 * columns from the run-time database query result for this object).  Used by InstantiateDbRow and
		 * GetVirtualAttribute.
		 * @var string[] $__strVirtualAttributeArray
		 */
		protected $__strVirtualAttributeArray = array();

		/**
		 * Protected internal member variable that specifies whether or not this object is Restored from the database.
		 * Used by Save() to determine if Save() should perform a db UPDATE or INSERT.
		 * @var bool __blnRestored;
		 */
		protected $__blnRestored;




		///////////////////////////////
		// PROTECTED MEMBER OBJECTS
		///////////////////////////////





		///////////////////////////////
		// CLASS-WIDE LOAD AND COUNT METHODS
		///////////////////////////////

		/**
		 * Static method to retrieve the Database object that owns this class.
		 * @return QDatabaseBase reference to the Database object that can query this class
		 */
		public static function GetDatabase() {
			return QApplication::$Database[1];
		}

		/**
		 * Load a AuthAction from PK Info
		 * @param integer $intId
		 * @return AuthAction
		 */
		public static function Load($intId) {
			// Use QuerySingle to Perform the Query
			return AuthAction::QuerySingle(
				QQ::Equal(QQN::AuthAction()->Id, $intId)
			);
		}

		/**
		 * Load all AuthActions
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthAction[]
		 */
		public static function LoadAll($objOptionalClauses = null) {
			// Call AuthAction::QueryArray to perform the LoadAll query
			try {
				return AuthAction::QueryArray(QQ::All(), $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Count all AuthActions
		 * @return int
		 */
		public static function CountAll() {
			// Call AuthAction::QueryCount to perform the CountAll query
			return AuthAction::QueryCount(QQ::All());
		}




		///////////////////////////////
		// QCODO QUERY-RELATED METHODS
		///////////////////////////////

		/**
		 * Internally called method to assist with calling Qcodo Query for this class
		 * on load methods.
		 * @param QQueryBuilder &$objQueryBuilder the QueryBuilder object that will be created
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause object or array of QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with (sending in null will skip the PrepareStatement step)
		 * @param boolean $blnCountOnly only select a rowcount
		 * @return string the query statement
		 */
		protected static function BuildQueryStatement(&$objQueryBuilder, QQCondition $objConditions, $objOptionalClauses, $mixParameterArray, $blnCountOnly) {
			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Create/Build out the QueryBuilder object with AuthAction-specific SELET and FROM fields
			$objQueryBuilder = new QQueryBuilder($objDatabase, '' . DB_TABLE_PREFIX_1 . 'auth_action');
			AuthAction::GetSelectFields($objQueryBuilder);
			$objQueryBuilder->AddFromItem('' . DB_TABLE_PREFIX_1 . 'auth_action');

			// Set "CountOnly" option (if applicable)
			if ($blnCountOnly)
				$objQueryBuilder->SetCountOnlyFlag();

			// Apply Any Conditions
			if ($objConditions)
				try {
					$objConditions->UpdateQueryBuilder($objQueryBuilder);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

			// Iterate through all the Optional Clauses (if any) and perform accordingly
			if ($objOptionalClauses) {
				if ($objOptionalClauses instanceof QQClause)
					$objOptionalClauses->UpdateQueryBuilder($objQueryBuilder);
				else if (is_array($objOptionalClauses))
					foreach ($objOptionalClauses as $objClause)
						$objClause->UpdateQueryBuilder($objQueryBuilder);
				else
					throw new QCallerException('Optional Clauses must be a QQClause object or an array of QQClause objects');
			}

			// Get the SQL Statement
			$strQuery = $objQueryBuilder->GetStatement();

			// Prepare the Statement with the Query Parameters (if applicable)
			if ($mixParameterArray) {
				if (is_array($mixParameterArray)) {
					if (count($mixParameterArray))
						$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

					// Ensure that there are no other Unresolved Named Parameters
					if (strpos($strQuery, chr(QQNamedValue::DelimiterCode) . '{') !== false)
						throw new QCallerException('Unresolved named parameters in the query');
				} else
					throw new QCallerException('Parameter Array must be an array of name-value parameter pairs');
			}

			// Return the Objects
			return $strQuery;
		}

		/**
		 * Static Qcodo Query method to query for a single AuthAction object.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return AuthAction the queried object
		 */
		public static function QuerySingle(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthAction::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query, Get the First Row, and Instantiate a new AuthAction object
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return AuthAction::InstantiateDbRow($objDbResult->GetNextRow(), null, null, null, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for an array of AuthAction objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return AuthAction[] the queried objects as an array
		 */
		public static function QueryArray(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthAction::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, false);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);
			return AuthAction::InstantiateDbResult($objDbResult, $objQueryBuilder->ExpandAsArrayNodes, $objQueryBuilder->ColumnAliasArray);
		}

		/**
		 * Static Qcodo Query method to query for a count of AuthAction objects.
		 * Uses BuildQueryStatment to perform most of the work.
		 * @param QQCondition $objConditions any conditions on the query, itself
		 * @param QQClause[] $objOptionalClausees additional optional QQClause objects for this query
		 * @param mixed[] $mixParameterArray a array of name-value pairs to perform PrepareStatement with
		 * @return integer the count of queried objects as an integer
		 */
		public static function QueryCount(QQCondition $objConditions, $objOptionalClauses = null, $mixParameterArray = null) {
			// Get the Query Statement
			try {
				$strQuery = AuthAction::BuildQueryStatement($objQueryBuilder, $objConditions, $objOptionalClauses, $mixParameterArray, true);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Perform the Query and return the row_count
			$objDbResult = $objQueryBuilder->Database->Query($strQuery);

			// Figure out if the query is using GroupBy
			$blnGrouped = false;

			if ($objOptionalClauses) foreach ($objOptionalClauses as $objClause) {
				if ($objClause instanceof QQGroupBy) {
					$blnGrouped = true;
					break;
				}
			}

			if ($blnGrouped)
				// Groups in this query - return the count of Groups (which is the count of all rows)
				return $objDbResult->CountRows();
			else {
				// No Groups - return the sql-calculated count(*) value
				$strDbRow = $objDbResult->FetchRow();
				return QType::Cast($strDbRow[0], QType::Integer);
			}
		}

/*		public static function QueryArrayCached($strConditions, $mixParameterArray = null) {
			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Lookup the QCache for This Query Statement
			$objCache = new QCache('query', '' . DB_TABLE_PREFIX_1 . 'auth_action_' . serialize($strConditions));
			if (!($strQuery = $objCache->GetData())) {
				// Not Found -- Go ahead and Create/Build out a new QueryBuilder object with AuthAction-specific fields
				$objQueryBuilder = new QQueryBuilder($objDatabase);
				AuthAction::GetSelectFields($objQueryBuilder);
				AuthAction::GetFromFields($objQueryBuilder);

				// Ensure the Passed-in Conditions is a string
				try {
					$strConditions = QType::Cast($strConditions, QType::String);
				} catch (QCallerException $objExc) {
					$objExc->IncrementOffset();
					throw $objExc;
				}

				// Create the Conditions object, and apply it
				$objConditions = eval('return ' . $strConditions . ';');

				// Apply Any Conditions
				if ($objConditions)
					$objConditions->UpdateQueryBuilder($objQueryBuilder);

				// Get the SQL Statement
				$strQuery = $objQueryBuilder->GetStatement();

				// Save the SQL Statement in the Cache
				$objCache->SaveData($strQuery);
			}

			// Prepare the Statement with the Parameters
			if ($mixParameterArray)
				$strQuery = $objDatabase->PrepareStatement($strQuery, $mixParameterArray);

			// Perform the Query and Instantiate the Array Result
			$objDbResult = $objDatabase->Query($strQuery);
			return AuthAction::InstantiateDbResult($objDbResult);
		}*/

		/**
		 * Updates a QQueryBuilder with the SELECT fields for this AuthAction
		 * @param QQueryBuilder $objBuilder the Query Builder object to update
		 * @param string $strPrefix optional prefix to add to the SELECT fields
		 */
		public static function GetSelectFields(QQueryBuilder $objBuilder, $strPrefix = null) {
			if ($strPrefix) {
				$strTableName = $strPrefix;
				$strAliasPrefix = $strPrefix . '__';
			} else {
				$strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_action';
				$strAliasPrefix = '';
			}

			$objBuilder->AddSelectItem($strTableName, 'id', $strAliasPrefix . 'id');
			$objBuilder->AddSelectItem($strTableName, 'name', $strAliasPrefix . 'name');
			$objBuilder->AddSelectItem($strTableName, 'apply_to_object', $strAliasPrefix . 'apply_to_object');
		}




		///////////////////////////////
		// INSTANTIATION-RELATED METHODS
		///////////////////////////////

		/**
		 * Instantiate a AuthAction from a Database Row.
		 * Takes in an optional strAliasPrefix, used in case another Object::InstantiateDbRow
		 * is calling this AuthAction::InstantiateDbRow in order to perform
		 * early binding on referenced objects.
		 * @param DatabaseRowBase $objDbRow
		 * @param string $strAliasPrefix
		 * @param string $strExpandAsArrayNodes
		 * @param QBaseClass $objPreviousItem
		 * @param string[] $strColumnAliasArray
		 * @return AuthAction
		*/
		public static function InstantiateDbRow($objDbRow, $strAliasPrefix = null, $strExpandAsArrayNodes = null, $objPreviousItem = null, $strColumnAliasArray = array()) {
			// If blank row, return null
			if (!$objDbRow)
				return null;

			// See if we're doing an array expansion on the previous item
			$strAlias = $strAliasPrefix . 'id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (($strExpandAsArrayNodes) && ($objPreviousItem) &&
				($objPreviousItem->intId == $objDbRow->GetColumn($strAliasName, 'Integer'))) {

				// We are.  Now, prepare to check for ExpandAsArray clauses
				$blnExpandedViaArray = false;
				if (!$strAliasPrefix)
					$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'auth_action__';


				$strAlias = $strAliasPrefix . 'authimplementedactionasauthaction__auth_class_enum_id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objAuthImplementedActionAsAuthActionArray)) {
						$objPreviousChildItem = $objPreviousItem->_objAuthImplementedActionAsAuthActionArray[$intPreviousChildItemCount - 1];
						$objChildItem = AuthImplementedAction::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authimplementedactionasauthaction__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objAuthImplementedActionAsAuthActionArray[] = $objChildItem;
					} else
						$objPreviousItem->_objAuthImplementedActionAsAuthActionArray[] = AuthImplementedAction::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authimplementedactionasauthaction__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				$strAlias = $strAliasPrefix . 'authprivilegeasauthaction__id';
				$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
				if ((array_key_exists($strAlias, $strExpandAsArrayNodes)) &&
					(!is_null($objDbRow->GetColumn($strAliasName)))) {
					if ($intPreviousChildItemCount = count($objPreviousItem->_objAuthPrivilegeAsAuthActionArray)) {
						$objPreviousChildItem = $objPreviousItem->_objAuthPrivilegeAsAuthActionArray[$intPreviousChildItemCount - 1];
						$objChildItem = AuthPrivilege::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authprivilegeasauthaction__', $strExpandAsArrayNodes, $objPreviousChildItem, $strColumnAliasArray);
						if ($objChildItem)
							$objPreviousItem->_objAuthPrivilegeAsAuthActionArray[] = $objChildItem;
					} else
						$objPreviousItem->_objAuthPrivilegeAsAuthActionArray[] = AuthPrivilege::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authprivilegeasauthaction__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
					$blnExpandedViaArray = true;
				}

				// Either return false to signal array expansion, or check-to-reset the Alias prefix and move on
				if ($blnExpandedViaArray)
					return false;
				else if ($strAliasPrefix == '' . DB_TABLE_PREFIX_1 . 'auth_action__')
					$strAliasPrefix = null;
			}

			// Create a new instance of the AuthAction object
			$objToReturn = new AuthAction();
			$objToReturn->__blnRestored = true;

			$strAliasName = array_key_exists($strAliasPrefix . 'id', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'id'] : $strAliasPrefix . 'id';
			$objToReturn->intId = $objDbRow->GetColumn($strAliasName, 'Integer');
			$strAliasName = array_key_exists($strAliasPrefix . 'name', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'name'] : $strAliasPrefix . 'name';
			$objToReturn->strName = $objDbRow->GetColumn($strAliasName, 'VarChar');
			$strAliasName = array_key_exists($strAliasPrefix . 'apply_to_object', $strColumnAliasArray) ? $strColumnAliasArray[$strAliasPrefix . 'apply_to_object'] : $strAliasPrefix . 'apply_to_object';
			$objToReturn->blnApplyToObject = $objDbRow->GetColumn($strAliasName, 'Bit');

			// Instantiate Virtual Attributes
			foreach ($objDbRow->GetColumnNameArray() as $strColumnName => $mixValue) {
				$strVirtualPrefix = $strAliasPrefix . '__';
				$strVirtualPrefixLength = strlen($strVirtualPrefix);
				if (substr($strColumnName, 0, $strVirtualPrefixLength) == $strVirtualPrefix)
					$objToReturn->__strVirtualAttributeArray[substr($strColumnName, $strVirtualPrefixLength)] = $mixValue;
			}

			// Prepare to Check for Early/Virtual Binding
			if (!$strAliasPrefix)
				$strAliasPrefix = '' . DB_TABLE_PREFIX_1 . 'auth_action__';




			// Check for AuthImplementedActionAsAuthAction Virtual Binding
			$strAlias = $strAliasPrefix . 'authimplementedactionasauthaction__auth_class_enum_id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objAuthImplementedActionAsAuthActionArray[] = AuthImplementedAction::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authimplementedactionasauthaction__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objAuthImplementedActionAsAuthAction = AuthImplementedAction::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authimplementedactionasauthaction__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			// Check for AuthPrivilegeAsAuthAction Virtual Binding
			$strAlias = $strAliasPrefix . 'authprivilegeasauthaction__id';
			$strAliasName = array_key_exists($strAlias, $strColumnAliasArray) ? $strColumnAliasArray[$strAlias] : $strAlias;
			if (!is_null($objDbRow->GetColumn($strAliasName))) {
				if (($strExpandAsArrayNodes) && (array_key_exists($strAlias, $strExpandAsArrayNodes)))
					$objToReturn->_objAuthPrivilegeAsAuthActionArray[] = AuthPrivilege::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authprivilegeasauthaction__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
				else
					$objToReturn->_objAuthPrivilegeAsAuthAction = AuthPrivilege::InstantiateDbRow($objDbRow, $strAliasPrefix . 'authprivilegeasauthaction__', $strExpandAsArrayNodes, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}

		/**
		 * Instantiate an array of AuthActions from a Database Result
		 * @param DatabaseResultBase $objDbResult
		 * @param string $strExpandAsArrayNodes
		 * @param string[] $strColumnAliasArray
		 * @return AuthAction[]
		 */
		public static function InstantiateDbResult(QDatabaseResultBase $objDbResult, $strExpandAsArrayNodes = null, $strColumnAliasArray = null) {
			$objToReturn = array();
			
			if (!$strColumnAliasArray)
				$strColumnAliasArray = array();

			// If blank resultset, then return empty array
			if (!$objDbResult)
				return $objToReturn;

			// Load up the return array with each row
			if ($strExpandAsArrayNodes) {
				$objLastRowItem = null;
				while ($objDbRow = $objDbResult->GetNextRow()) {
					$objItem = AuthAction::InstantiateDbRow($objDbRow, null, $strExpandAsArrayNodes, $objLastRowItem, $strColumnAliasArray);
					if ($objItem) {
						$objToReturn[] = $objItem;
						$objLastRowItem = $objItem;
					}
				}
			} else {
				while ($objDbRow = $objDbResult->GetNextRow())
					$objToReturn[] = AuthAction::InstantiateDbRow($objDbRow, null, null, null, $strColumnAliasArray);
			}

			return $objToReturn;
		}




		///////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Single Load and Array)
		///////////////////////////////////////////////////
			
		/**
		 * Load a single AuthAction object,
		 * by Id Index(es)
		 * @param integer $intId
		 * @return AuthAction
		*/
		public static function LoadById($intId) {
			return AuthAction::QuerySingle(
				QQ::Equal(QQN::AuthAction()->Id, $intId)
			);
		}
			
		/**
		 * Load a single AuthAction object,
		 * by Name Index(es)
		 * @param string $strName
		 * @return AuthAction
		*/
		public static function LoadByName($strName) {
			return AuthAction::QuerySingle(
				QQ::Equal(QQN::AuthAction()->Name, $strName)
			);
		}



		////////////////////////////////////////////////////
		// INDEX-BASED LOAD METHODS (Array via Many to Many)
		////////////////////////////////////////////////////




		//////////////////////////
		// SAVE, DELETE AND RELOAD
		//////////////////////////

		/**
		 * Save this AuthAction
		 * @param bool $blnForceInsert
		 * @param bool $blnForceUpdate
		 * @return int
		 */
		public function Save($blnForceInsert = false, $blnForceUpdate = false) {
			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			$mixToReturn = null;

			try {
				if ((!$this->__blnRestored) || ($blnForceInsert)) {
					// Perform an INSERT query
					$objDatabase->NonQuery('
						INSERT INTO `' . DB_TABLE_PREFIX_1 . 'auth_action` (
							`name`,
							`apply_to_object`
						) VALUES (
							' . $objDatabase->SqlVariable($this->strName) . ',
							' . $objDatabase->SqlVariable($this->blnApplyToObject) . '
						)
					');

					// Update Identity column and return its value
					$mixToReturn = $this->intId = $objDatabase->InsertId('' . DB_TABLE_PREFIX_1 . 'auth_action', 'id');
				} else {
					// Perform an UPDATE query

					// First checking for Optimistic Locking constraints (if applicable)

					// Perform the UPDATE query
					$objDatabase->NonQuery('
						UPDATE
							`' . DB_TABLE_PREFIX_1 . 'auth_action`
						SET
							`name` = ' . $objDatabase->SqlVariable($this->strName) . ',
							`apply_to_object` = ' . $objDatabase->SqlVariable($this->blnApplyToObject) . '
						WHERE
							`id` = ' . $objDatabase->SqlVariable($this->intId) . '
					');
				}

			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// Update __blnRestored and any Non-Identity PK Columns (if applicable)
			$this->__blnRestored = true;


			// Return 
			return $mixToReturn;
		}

		/**
		 * Delete this AuthAction
		 * @return void
		 */
		public function Delete() {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Cannot delete this AuthAction with an unset primary key.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();


			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'auth_action`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($this->intId) . '');
		}

		/**
		 * Delete all AuthActions
		 * @return void
		 */
		public static function DeleteAll() {
			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				DELETE FROM
					`' . DB_TABLE_PREFIX_1 . 'auth_action`');
		}

		/**
		 * Truncate ' . DB_TABLE_PREFIX_1 . 'auth_action table
		 * @return void
		 */
		public static function Truncate() {
			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the Query
			$objDatabase->NonQuery('
				TRUNCATE `' . DB_TABLE_PREFIX_1 . 'auth_action`');
		}

		/**
		 * Reload this AuthAction from the database.
		 * @return void
		 */
		public function Reload() {
			// Make sure we are actually Restored from the database
			if (!$this->__blnRestored)
				throw new QCallerException('Cannot call Reload() on a new, unsaved AuthAction object.');

			// Reload the Object
			$objReloaded = AuthAction::Load($this->intId);

			// Update $this's local variables to match
			$this->strName = $objReloaded->strName;
			$this->blnApplyToObject = $objReloaded->blnApplyToObject;
		}



		////////////////////
		// PUBLIC OVERRIDERS
		////////////////////

				/**
		 * Override method to perform a property "Get"
		 * This will get the value of $strName
		 *
		 * @param string $strName Name of the property to get
		 * @return mixed
		 */
		public function __get($strName) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Id':
					/**
					 * Gets the value for intId (Read-Only PK)
					 * @return integer
					 */
					return $this->intId;

				case 'Name':
					/**
					 * Gets the value for strName (Unique)
					 * @return string
					 */
					return $this->strName;

				case 'ApplyToObject':
					/**
					 * Gets the value for blnApplyToObject (Not Null)
					 * @return boolean
					 */
					return $this->blnApplyToObject;


				///////////////////
				// Member Objects
				///////////////////

				////////////////////////////
				// Virtual Object References (Many to Many and Reverse References)
				// (If restored via a "Many-to" expansion)
				////////////////////////////

				case '_AuthImplementedActionAsAuthAction':
					/**
					 * Gets the value for the private _objAuthImplementedActionAsAuthAction (Read-Only)
					 * if set due to an expansion on the spidio_auth_implemented_action.auth_action_id reverse relationship
					 * @return AuthImplementedAction
					 */
					return $this->_objAuthImplementedActionAsAuthAction;

				case '_AuthImplementedActionAsAuthActionArray':
					/**
					 * Gets the value for the private _objAuthImplementedActionAsAuthActionArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_auth_implemented_action.auth_action_id reverse relationship
					 * @return AuthImplementedAction[]
					 */
					return (array) $this->_objAuthImplementedActionAsAuthActionArray;

				case '_AuthPrivilegeAsAuthAction':
					/**
					 * Gets the value for the private _objAuthPrivilegeAsAuthAction (Read-Only)
					 * if set due to an expansion on the spidio_auth_privilege.auth_action_id reverse relationship
					 * @return AuthPrivilege
					 */
					return $this->_objAuthPrivilegeAsAuthAction;

				case '_AuthPrivilegeAsAuthActionArray':
					/**
					 * Gets the value for the private _objAuthPrivilegeAsAuthActionArray (Read-Only)
					 * if set due to an ExpandAsArray on the spidio_auth_privilege.auth_action_id reverse relationship
					 * @return AuthPrivilege[]
					 */
					return (array) $this->_objAuthPrivilegeAsAuthActionArray;


				case '__Restored':
					return $this->__blnRestored;

				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

				/**
		 * Override method to perform a property "Set"
		 * This will set the property $strName to be $mixValue
		 *
		 * @param string $strName Name of the property to set
		 * @param string $mixValue New value of the property
		 * @return mixed
		 */
		public function __set($strName, $mixValue) {
			switch ($strName) {
				///////////////////
				// Member Variables
				///////////////////
				case 'Name':
					/**
					 * Sets the value for strName (Unique)
					 * @param string $mixValue
					 * @return string
					 */
					try {
						return ($this->strName = QType::Cast($mixValue, QType::String));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}

				case 'ApplyToObject':
					/**
					 * Sets the value for blnApplyToObject (Not Null)
					 * @param boolean $mixValue
					 * @return boolean
					 */
					try {
						return ($this->blnApplyToObject = QType::Cast($mixValue, QType::Boolean));
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}


				///////////////////
				// Member Objects
				///////////////////
				default:
					try {
						return parent::__set($strName, $mixValue);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}

		/**
		 * Lookup a VirtualAttribute value (if applicable).  Returns NULL if none found.
		 * @param string $strName
		 * @return string
		 */
		public function GetVirtualAttribute($strName) {
			if (array_key_exists($strName, $this->__strVirtualAttributeArray))
				return $this->__strVirtualAttributeArray[$strName];
			return null;
		}



		///////////////////////////////
		// ASSOCIATED OBJECTS' METHODS
		///////////////////////////////

			
		
		// Related Objects' Methods for AuthImplementedActionAsAuthAction
		//-------------------------------------------------------------------

		/**
		 * Gets all associated AuthImplementedActionsAsAuthAction as an array of AuthImplementedAction objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthImplementedAction[]
		*/ 
		public function GetAuthImplementedActionAsAuthActionArray($objOptionalClauses = null) {
			if ((is_null($this->intId)))
				return array();

			try {
				return AuthImplementedAction::LoadArrayByAuthActionId($this->intId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated AuthImplementedActionsAsAuthAction
		 * @return int
		*/ 
		public function CountAuthImplementedActionsAsAuthAction() {
			if ((is_null($this->intId)))
				return 0;

			return AuthImplementedAction::CountByAuthActionId($this->intId);
		}

		/**
		 * Associates a AuthImplementedActionAsAuthAction
		 * @param AuthImplementedAction $objAuthImplementedAction
		 * @return void
		*/ 
		public function AssociateAuthImplementedActionAsAuthAction(AuthImplementedAction $objAuthImplementedAction) {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateAuthImplementedActionAsAuthAction on this unsaved AuthAction.');
			if ((is_null($objAuthImplementedAction->AuthClassEnumId)) || (is_null($objAuthImplementedAction->AuthActionId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateAuthImplementedActionAsAuthAction on this AuthAction with an unsaved AuthImplementedAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_auth_implemented_action`
				SET
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
				WHERE
					`auth_class_enum_id` = ' . $objDatabase->SqlVariable($objAuthImplementedAction->AuthClassEnumId) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($objAuthImplementedAction->AuthActionId) . '
			');
		}

		/**
		 * Unassociates a AuthImplementedActionAsAuthAction
		 * @param AuthImplementedAction $objAuthImplementedAction
		 * @return void
		*/ 
		public function UnassociateAuthImplementedActionAsAuthAction(AuthImplementedAction $objAuthImplementedAction) {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthImplementedActionAsAuthAction on this unsaved AuthAction.');
			if ((is_null($objAuthImplementedAction->AuthClassEnumId)) || (is_null($objAuthImplementedAction->AuthActionId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthImplementedActionAsAuthAction on this AuthAction with an unsaved AuthImplementedAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_auth_implemented_action`
				SET
					`auth_action_id` = null
				WHERE
					`auth_class_enum_id` = ' . $objDatabase->SqlVariable($objAuthImplementedAction->AuthClassEnumId) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($objAuthImplementedAction->AuthActionId) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

		/**
		 * Unassociates all AuthImplementedActionsAsAuthAction
		 * @return void
		*/ 
		public function UnassociateAllAuthImplementedActionsAsAuthAction() {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthImplementedActionAsAuthAction on this unsaved AuthAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_auth_implemented_action`
				SET
					`auth_action_id` = null
				WHERE
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

		/**
		 * Deletes an associated AuthImplementedActionAsAuthAction
		 * @param AuthImplementedAction $objAuthImplementedAction
		 * @return void
		*/ 
		public function DeleteAssociatedAuthImplementedActionAsAuthAction(AuthImplementedAction $objAuthImplementedAction) {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthImplementedActionAsAuthAction on this unsaved AuthAction.');
			if ((is_null($objAuthImplementedAction->AuthClassEnumId)) || (is_null($objAuthImplementedAction->AuthActionId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthImplementedActionAsAuthAction on this AuthAction with an unsaved AuthImplementedAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_auth_implemented_action`
				WHERE
					`auth_class_enum_id` = ' . $objDatabase->SqlVariable($objAuthImplementedAction->AuthClassEnumId) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($objAuthImplementedAction->AuthActionId) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

		/**
		 * Deletes all associated AuthImplementedActionsAsAuthAction
		 * @return void
		*/ 
		public function DeleteAllAuthImplementedActionsAsAuthAction() {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthImplementedActionAsAuthAction on this unsaved AuthAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_auth_implemented_action`
				WHERE
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

			
		
		// Related Objects' Methods for AuthPrivilegeAsAuthAction
		//-------------------------------------------------------------------

		/**
		 * Gets all associated AuthPrivilegesAsAuthAction as an array of AuthPrivilege objects
		 * @param QQClause[] $objOptionalClauses additional optional QQClause objects for this query
		 * @return AuthPrivilege[]
		*/ 
		public function GetAuthPrivilegeAsAuthActionArray($objOptionalClauses = null) {
			if ((is_null($this->intId)))
				return array();

			try {
				return AuthPrivilege::LoadArrayByAuthActionId($this->intId, $objOptionalClauses);
			} catch (QCallerException $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}
		}

		/**
		 * Counts all associated AuthPrivilegesAsAuthAction
		 * @return int
		*/ 
		public function CountAuthPrivilegesAsAuthAction() {
			if ((is_null($this->intId)))
				return 0;

			return AuthPrivilege::CountByAuthActionId($this->intId);
		}

		/**
		 * Associates a AuthPrivilegeAsAuthAction
		 * @param AuthPrivilege $objAuthPrivilege
		 * @return void
		*/ 
		public function AssociateAuthPrivilegeAsAuthAction(AuthPrivilege $objAuthPrivilege) {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateAuthPrivilegeAsAuthAction on this unsaved AuthAction.');
			if ((is_null($objAuthPrivilege->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call AssociateAuthPrivilegeAsAuthAction on this AuthAction with an unsaved AuthPrivilege.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_auth_privilege`
				SET
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objAuthPrivilege->Id) . '
			');
		}

		/**
		 * Unassociates a AuthPrivilegeAsAuthAction
		 * @param AuthPrivilege $objAuthPrivilege
		 * @return void
		*/ 
		public function UnassociateAuthPrivilegeAsAuthAction(AuthPrivilege $objAuthPrivilege) {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthPrivilegeAsAuthAction on this unsaved AuthAction.');
			if ((is_null($objAuthPrivilege->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthPrivilegeAsAuthAction on this AuthAction with an unsaved AuthPrivilege.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_auth_privilege`
				SET
					`auth_action_id` = null
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objAuthPrivilege->Id) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

		/**
		 * Unassociates all AuthPrivilegesAsAuthAction
		 * @return void
		*/ 
		public function UnassociateAllAuthPrivilegesAsAuthAction() {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthPrivilegeAsAuthAction on this unsaved AuthAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				UPDATE
					`spidio_auth_privilege`
				SET
					`auth_action_id` = null
				WHERE
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

		/**
		 * Deletes an associated AuthPrivilegeAsAuthAction
		 * @param AuthPrivilege $objAuthPrivilege
		 * @return void
		*/ 
		public function DeleteAssociatedAuthPrivilegeAsAuthAction(AuthPrivilege $objAuthPrivilege) {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthPrivilegeAsAuthAction on this unsaved AuthAction.');
			if ((is_null($objAuthPrivilege->Id)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthPrivilegeAsAuthAction on this AuthAction with an unsaved AuthPrivilege.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_auth_privilege`
				WHERE
					`id` = ' . $objDatabase->SqlVariable($objAuthPrivilege->Id) . ' AND
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}

		/**
		 * Deletes all associated AuthPrivilegesAsAuthAction
		 * @return void
		*/ 
		public function DeleteAllAuthPrivilegesAsAuthAction() {
			if ((is_null($this->intId)))
				throw new QUndefinedPrimaryKeyException('Unable to call UnassociateAuthPrivilegeAsAuthAction on this unsaved AuthAction.');

			// Get the Database Object for this Class
			$objDatabase = AuthAction::GetDatabase();

			// Perform the SQL Query
			$objDatabase->NonQuery('
				DELETE FROM
					`spidio_auth_privilege`
				WHERE
					`auth_action_id` = ' . $objDatabase->SqlVariable($this->intId) . '
			');
		}





		////////////////////////////////////////
		// METHODS for SOAP-BASED WEB SERVICES
		////////////////////////////////////////

		public static function GetSoapComplexTypeXml() {
			$strToReturn = '<complexType name="AuthAction"><sequence>';
			$strToReturn .= '<element name="Id" type="xsd:int"/>';
			$strToReturn .= '<element name="Name" type="xsd:string"/>';
			$strToReturn .= '<element name="ApplyToObject" type="xsd:boolean"/>';
			$strToReturn .= '<element name="__blnRestored" type="xsd:boolean"/>';
			$strToReturn .= '</sequence></complexType>';
			return $strToReturn;
		}

		public static function AlterSoapComplexTypeArray(&$strComplexTypeArray) {
			if (!array_key_exists('AuthAction', $strComplexTypeArray)) {
				$strComplexTypeArray['AuthAction'] = AuthAction::GetSoapComplexTypeXml();
			}
		}

		public static function GetArrayFromSoapArray($objSoapArray) {
			$objArrayToReturn = array();

			foreach ($objSoapArray as $objSoapObject)
				array_push($objArrayToReturn, AuthAction::GetObjectFromSoapObject($objSoapObject));

			return $objArrayToReturn;
		}

		public static function GetObjectFromSoapObject($objSoapObject) {
			$objToReturn = new AuthAction();
			if (property_exists($objSoapObject, 'Id'))
				$objToReturn->intId = $objSoapObject->Id;
			if (property_exists($objSoapObject, 'Name'))
				$objToReturn->strName = $objSoapObject->Name;
			if (property_exists($objSoapObject, 'ApplyToObject'))
				$objToReturn->blnApplyToObject = $objSoapObject->ApplyToObject;
			if (property_exists($objSoapObject, '__blnRestored'))
				$objToReturn->__blnRestored = $objSoapObject->__blnRestored;
			return $objToReturn;
		}

		public static function GetSoapArrayFromArray($objArray) {
			if (!$objArray)
				return null;

			$objArrayToReturn = array();

			foreach ($objArray as $objObject)
				array_push($objArrayToReturn, AuthAction::GetSoapObjectFromObject($objObject, true));

			return unserialize(serialize($objArrayToReturn));
		}

		public static function GetSoapObjectFromObject($objObject, $blnBindRelatedObjects) {
			return $objObject;
		}




	}



	/////////////////////////////////////
	// ADDITIONAL CLASSES for QCODO QUERY
	/////////////////////////////////////

	class QQNodeAuthAction extends QQNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'AuthAction';
		
		public function __construct($strName, $strPropertyName, $strType, $objParentNode = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_action';
			parent::__construct($strName, $strPropertyName, $strType, $objParentNode);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'integer', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'ApplyToObject':
					return new QQNode('apply_to_object', 'ApplyToObject', 'boolean', $this);
				case 'AuthImplementedActionAsAuthAction':
					return new QQReverseReferenceNodeAuthImplementedAction($this, 'authimplementedactionasauthaction', 'reverse_reference', 'auth_action_id');
				case 'AuthPrivilegeAsAuthAction':
					return new QQReverseReferenceNodeAuthPrivilege($this, 'authprivilegeasauthaction', 'reverse_reference', 'auth_action_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'integer', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

	class QQReverseReferenceNodeAuthAction extends QQReverseReferenceNode {
		protected $strTableName;
		protected $strPrimaryKey = 'id';
		protected $strClassName = 'AuthAction';
		
		public function __construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName = null) {
			$this->strTableName = '' . DB_TABLE_PREFIX_1 . 'auth_action';
			parent::__construct($objParentNode, $strName, $strType, $strForeignKey, $strPropertyName);
		}
		
		public function __get($strName) {
			switch ($strName) {
				case 'Id':
					return new QQNode('id', 'Id', 'integer', $this);
				case 'Name':
					return new QQNode('name', 'Name', 'string', $this);
				case 'ApplyToObject':
					return new QQNode('apply_to_object', 'ApplyToObject', 'boolean', $this);
				case 'AuthImplementedActionAsAuthAction':
					return new QQReverseReferenceNodeAuthImplementedAction($this, 'authimplementedactionasauthaction', 'reverse_reference', 'auth_action_id');
				case 'AuthPrivilegeAsAuthAction':
					return new QQReverseReferenceNodeAuthPrivilege($this, 'authprivilegeasauthaction', 'reverse_reference', 'auth_action_id');

				case '_PrimaryKeyNode':
					return new QQNode('id', 'Id', 'integer', $this);
				default:
					try {
						return parent::__get($strName);
					} catch (QCallerException $objExc) {
						$objExc->IncrementOffset();
						throw $objExc;
					}
			}
		}
	}

?>